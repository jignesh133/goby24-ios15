//
//  AppDelegate.swift
//  Goby24
//
//  Created by Jignesh Bhensadadiya on 7/9/21.
//

import UIKit
import SwiftUI
import FBSDKCoreKit
import GoogleSignIn
import GoogleMaps
import MBProgressHUD
import Localize_Swift
import IQKeyboardManagerSwift
//import Firebase

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

   @State var userProfileData:[String:Any] = [String:Any]()
    var viewShowLoad: UIView?
    var objSpinKit: RTSpinKitView?
    var loadingNotification:MBProgressHUD?
    var  objAJProgressView:AJProgressView = AJProgressView()
    var countryNames:[String] = [String]()
    var selectedTab:Int = 0
    var arrCountry:[Model_Countrys] = [Model_Countrys]()
    var arrCountryWithCode:[Model_CountrysWithDialCode] = [Model_CountrysWithDialCode]()
    var countryNamesWithCodes:[String] = [String]()


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        IQKeyboardManager.shared.enable = true

//        FirebaseApp.configure()
         // GET DATA
        self.getCountryStateData()
        self.getCountryCodes()
        
        // UPDATE LANGEUAGE
        self.updateLanguage()
        UITextView.appearance().backgroundColor = .clear

        UIScrollView.appearance().bounces = false

        GMSServices.provideAPIKey(GOOGLE_KEY)

        GoogleApi.shared.initialiseWithKey(GOOGLE_KEY)

        GIDSignIn.sharedInstance.restorePreviousSignIn { user, error in
          if error != nil || user == nil {
            // Show the app's signed-out state.            
          } else {
            // Show the app's signed-in state.
          }
        }

       // countryList = Locale.isoRegionCodes.compactMap { Locale.current.localizedString(forRegionCode: $0) }

        //WebAccess.getUserProfileData()
        
       // UserDefaults.standard.removeObject(forKey: Enum_Login.isLogin.rawValue)
       // UserDefaults.standard.synchronize()
        ApplicationDelegate.shared.application( application, didFinishLaunchingWithOptions: launchOptions)
        
       // self.resetUserDefautls()
        //userProfileData = userDefault.value(forKey: Enum_Login.userProfileData.rawValue) as? [String : Any] ?? [:]
        
        return true
    }
    // MARK: UISceneSession Lifecycle
    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }
    
    
    func application(_ app: UIApplication,open url: URL,options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {

        ApplicationDelegate.shared.application(
            app,
            open: url,
            sourceApplication: options[UIApplication.OpenURLOptionsKey.sourceApplication] as? String,
            annotation: options[UIApplication.OpenURLOptionsKey.annotation]
        )
        var handled: Bool

        handled = GIDSignIn.sharedInstance.handle(url)
        if handled {
          return true
        }
        return false

    }
    func resetUserDefautls(){
        let domain = Bundle.main.bundleIdentifier!
        UserDefaults.standard.removePersistentDomain(forName: domain)
        UserDefaults.resetDefaults()
        UserDefaults.standard.synchronize()
    }
    //MARK: -CUSTOM LOADING METHODS
    func startLoadingview(strText:String = "") {
        // Pass your image here which will come in centre of ProgressView
        objAJProgressView.imgLogo = UIImage(named:"logoloader")!

        // Pass the colour for the layer of progressView
        objAJProgressView.firstColor = UIColor.init(red: 0/255, green: 173/255, blue: 239/255, alpha: 1.0)

        // If you  want to have layer of animated colours you can also add second and third colour
        objAJProgressView.secondColor = UIColor.init(red: 0/255, green: 173/255, blue: 239/255, alpha: 1.0)
        objAJProgressView.thirdColor = UIColor.init(red: 0/255, green: 173/255, blue: 239/255, alpha: 1.0)

        // Set duration to control the speed of progressView
        objAJProgressView.duration = 3.0

        // Set width of layer of progressView
        objAJProgressView.lineWidth = 4.0

        //Set backgroundColor of progressView
        objAJProgressView.bgColor =  UIColor.black.withAlphaComponent(0.2)

        objAJProgressView.show()
        
    }
    
    
    func stopLoadingView() {
        if (objAJProgressView.isAnimating ?? false){
            objAJProgressView.hide()
        }
    }
    func updateLanguage(){
        let code = userDefault.object(forKey: Enum_userDefaults.languageCode.rawValue) as? String ?? "en"
        Localize.setCurrentLanguage(code)
    }
    // CountryCodes

    func getCountryCodes(){
      if let path = Bundle.main.path(forResource: "CountryCodes", ofType: "json") {
            do {
                  let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                  let jsonResult = try JSONSerialization.jsonObject(with: data, options: .mutableLeaves)
                
                if let jsonResult = jsonResult as? [[String:Any]] {
                            // do stuff
                    print(jsonResult.count)
                    do{
                        let result:[[String : Any]] = UtilsObjs.cleanJson(to: jsonResult ) as? [[String : Any]] ?? []
                        let data1 =  try JSONSerialization.data(withJSONObject: result , options: JSONSerialization.WritingOptions.prettyPrinted)
                        let convertedString = String(data: data1, encoding: String.Encoding.utf8)
                        let jsonData = Data((convertedString?.utf8)!)
                        self.arrCountryWithCode = try JSONDecoder().decode([Model_CountrysWithDialCode].self, from: jsonData)
  
                        for obj in self.arrCountryWithCode {
                            var val  =  obj.dial_code ?? ""
                            val = val + " "
                            val = val + obj.name!
                            self.countryNamesWithCodes.append(val)
                        }
                    }catch(let error){
                        print(error)
                    }
                    
                  }
              } catch {
                   // handle error
              }
        }
    }
    func getCountryStateData(){
      if let path = Bundle.main.path(forResource: "data", ofType: "json") {
            do {
                  let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                  let jsonResult = try JSONSerialization.jsonObject(with: data, options: .mutableLeaves)
                
                if let jsonResult = jsonResult as? [[String:Any]] {
                            // do stuff
                    print(jsonResult.count)
                    do{
                        let result:[[String : Any]] = UtilsObjs.cleanJson(to: jsonResult ) as? [[String : Any]] ?? []
                        let data1 =  try JSONSerialization.data(withJSONObject: result , options: JSONSerialization.WritingOptions.prettyPrinted)
                        let convertedString = String(data: data1, encoding: String.Encoding.utf8)
                        let jsonData = Data((convertedString?.utf8)!)
                        self.arrCountry = try JSONDecoder().decode([Model_Countrys].self, from: jsonData)
                        self.countryNames = self.arrCountry.compactMap({$0.countryName})
                        NSLog("LOG")
                    }catch(let error){
                        print(error)
                    }
                    
                  }
              } catch {
                   // handle error
              }
        }
    }
}




    
