//
//  DashboardTab.swift
//  Goby24
//
//  Created by Jignesh Bhensadadiya on 7/11/21.
//

import SwiftUI

struct DashboardTab: View {
    @State var selectedIndex: Int = 0
    @State var isProfileTabClicked: Bool = false
    @EnvironmentObject var settings: UserSettings
    
    init() {
        UITableView.appearance().tableHeaderView = UIView(frame: CGRect(x: 0, y: 0, width: 0, height: Double.leastNonzeroMagnitude))
    }
    var body: some View {
        TabView (selection: $selectedIndex){
            
            NavigationView{
               // TouristPackageTransactionHistoryView()
               // BasicRideTransactionHistoryView()
                HomeView(tabSelection: $selectedIndex)
                    .ignoresSafeArea(.keyboard, edges: .bottom)
            }
            .tabItem {
                Image.init("Hometab", tintColor: UIColor.init(red: 0, green: 0.68, blue: 0.94, alpha: 1))
                Text(Localizable.HomePage.localized()).modifier(CustomTextM(fontName: UIFont.Poppins.regular.fontName, fontSize: 13, fontColor: Color.white))
                    .onTapGesture(perform: {
                        selectedIndex = 0
                    })
            }
            .tag(0)
            //.ignoresSafeArea(.keyboard, edges: .bottom)
            
            NavigationView{
                FindRides()
                    .ignoresSafeArea(.keyboard, edges: .bottom)
            }
            .tabItem {
                Image.init("searchride", tintColor: UIColor.init(red: 0, green: 0.68, blue: 0.94, alpha: 1))
                Text("Search Ride").modifier(CustomTextM(fontName: UIFont.Poppins.regular.fontName, fontSize: 13, fontColor: Color.white))
                    .onTapGesture(perform: {
                        selectedIndex = 1
                    })
            }
            .tag(1)
            .ignoresSafeArea(.keyboard, edges: .bottom)
            .onAppear() {
              //  selectedIndex = 1
            }
            
            NavigationView{
                OfferRideView()       // .navigationBarHidden(true) // not needed, but just in case
                    .ignoresSafeArea(.keyboard, edges: .bottom)
            }
            .font(.system(size: 30, weight: .bold, design: .rounded))
            .tabItem {
                Image.init("offerride", tintColor: UIColor.init(red: 0, green: 0.68, blue: 0.94, alpha: 1))
                Text(Localizable.Offer_a_ride.localized()).modifier(CustomTextM(fontName: UIFont.Poppins.regular.fontName, fontSize: 13, fontColor: Color.white))
                    .onTapGesture(perform: {
                        selectedIndex = 2
                    })
            }
            .tag(2)
            .ignoresSafeArea(.keyboard, edges: .bottom)
            .onAppear() {
             //   selectedIndex = 2
            }
            
            NavigationView{
                TouristPackageListView()
                    .ignoresSafeArea(.keyboard, edges: .bottom)
            }
            .font(.system(size: 30, weight: .bold, design: .rounded))
            .tabItem {
                Image.init("touristp", tintColor: UIColor.init(red: 0, green: 0.68, blue: 0.94, alpha: 1))
                Text((Localizable.Tourist_Package.localized())).modifier(CustomTextM(fontName: UIFont.Poppins.regular.fontName, fontSize: 13, fontColor: Color.white))
                    .onTapGesture(perform: {
                        selectedIndex = 3
                    })
            }
            .tag(3)
           .ignoresSafeArea(.keyboard, edges: .bottom)
            .onAppear() {
               // selectedIndex = 3
            }
            
            NavigationView{
                ProfileView(isFromTabbar:$isProfileTabClicked)
                    .ignoresSafeArea(.keyboard, edges: .bottom)
            }
            .tabItem {
                Image.init("profiletab", tintColor: UIColor.init(red: 0, green: 0.68, blue: 0.94, alpha: 1))
                Text(Localizable.Profile.localized()).modifier(CustomTextM(fontName: UIFont.Poppins.regular.fontName, fontSize: 13, fontColor: Color.white))
                    .onTapGesture(perform: {
                         selectedIndex = 4
                    })
                    .onAppear(){
                        print("ViewClicked")
                        isProfileTabClicked = true
                    }
            }
            .tag(4)
            .onAppear() {
               // selectedIndex = 4
            }
            .ignoresSafeArea(.keyboard, edges: .bottom)
        }.onChange(of: selectedIndex, perform: { (index) in
            print("----> " + "\(index)")
            appDelegate?.selectedTab = index
        })
        .tabViewStyle(DefaultTabViewStyle())
        .accentColor(Color.init(hex: "00AEEF"))
        .onReceive(self.settings.$openTab) { (index) in
            selectedIndex = index
        }
        .hiddenNavigationBarStyle()
        .navigationBarHidden(true) // not needed, but just in case
        .ignoresSafeArea(.keyboard, edges: .bottom)
    }
}

struct DashboardTab_Previews: PreviewProvider {
    static var previews: some View {
        DashboardTab()
    }
}
extension Image {
    init(_ named: String, tintColor: UIColor) {
        let uiImage = UIImage(named: named) ?? UIImage()
        let tintedImage = uiImage.withTintColor(tintColor,
                                                renderingMode: .alwaysTemplate)
        self = Image(uiImage: tintedImage)
    }
}
extension UITabBarController{
    open override func viewDidLoad() {
        let standardAppearence = UITabBarAppearance()
        //  standardAppearence.selectionIndicatorTintColor = .red
        //   standardAppearence.selectionIndicatorImage = UIImage().createSelectionIndicator(color: .red, size: CGSize(width: tabBar.frame.width/CGFloat(tabBar.items!.count), height:  tabBar.frame.height), lineWidth: 2.0)
        tabBar.standardAppearance = standardAppearence
    }
}
extension UITabBar {
    override open func sizeThatFits(_ size: CGSize) -> CGSize {
        super.sizeThatFits(size)
        var sizeThatFits = super.sizeThatFits(size)
        sizeThatFits.height = 100
        return sizeThatFits
    }
}
//class SelectedTab: ObservableObject {
//    @Published var index = 0
//}
