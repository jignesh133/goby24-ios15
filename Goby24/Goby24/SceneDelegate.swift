//
//  SceneDelegate.swift
//  Goby24
//
//  Created by Jignesh Bhensadadiya on 7/9/21.
//

import UIKit
import SwiftUI
import FBSDKCoreKit


class UserSettings: ObservableObject {
    
    @Published var loggedIn : Bool = false
    @Published var languageseted : Bool = false
    @Published var isMoveToOfferViewHome : Bool = false
    @Published var isMoveToSearchViewHome: Bool = false
    @Published var isMoveToTouristViewHome: Bool = false
    @Published var languageUpdated : Bool = false
    @Published var restart : Bool = false

    @Published var openTab : Int = 0

}
class UserTabSettings: ObservableObject {
    @State var openTab : Int = 0
}

class SceneDelegate: UIResponder, UIWindowSceneDelegate {

    var window: UIWindow?

    func scene(_ scene: UIScene, openURLContexts URLContexts: Set<UIOpenURLContext>) {
        
        guard let url = URLContexts.first?.url else {
            return
        }

        ApplicationDelegate.shared.application(UIApplication.shared,open: url,sourceApplication:nil,annotation:[UIApplication.OpenURLOptionsKey.annotation])
    }

        
    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        
        let settings = UserSettings()
        settings.loggedIn = getBoolFromAny(userDefault.value(forKey: Enum_Login.isLogin.rawValue) ?? false) ?? false
        
        appDelegate?.userProfileData = userDefault.value(forKey: Enum_Login.userProfileData.rawValue) as? [String : Any] ?? [:]
        print(settings.loggedIn)

        let contentView =  StartView()
        
        // Use a UIHostingController as window root view controller.
        if let windowScene = scene as? UIWindowScene {
            let window = UIWindow(windowScene: windowScene)
            window.rootViewController = UIHostingController(rootView: contentView.environmentObject(settings),ignoreSafeArea:true)
            self.window = window
            window.makeKeyAndVisible()
        }

        
    }

    func sceneDidDisconnect(_ scene: UIScene) {
        // Called as the scene is being released by the system.
        // This occurs shortly after the scene enters the background, or when its session is discarded.
        // Release any resources associated with this scene that can be re-created the next time the scene connects.
        // The scene may re-connect later, as its session was not necessarily discarded (see `application:didDiscardSceneSessions` instead).
    }

    func sceneDidBecomeActive(_ scene: UIScene) {
        // Called when the scene has moved from an inactive state to an active state.
        // Use this method to restart any tasks that were paused (or not yet started) when the scene was inactive.
    }

    func sceneWillResignActive(_ scene: UIScene) {
        // Called when the scene will move from an active state to an inactive state.
        // This may occur due to temporary interruptions (ex. an incoming phone call).
    }

    func sceneWillEnterForeground(_ scene: UIScene) {
        // Called as the scene transitions from the background to the foreground.
        // Use this method to undo the changes made on entering the background.
    }

    func sceneDidEnterBackground(_ scene: UIScene) {
        // Called as the scene transitions from the foreground to the background.
        // Use this method to save data, release shared resources, and store enough scene-specific state information
        // to restore the scene back to its current state.
    }
}

struct StartView: View {
    @EnvironmentObject var settings: UserSettings

    var body: some View {
       print("Language OR LOGIN clicked")
        if (settings.restart == true ){
            return AnyView(DashboardTab().environmentObject(settings))
        }else if (settings.loggedIn == true){
            return AnyView(DashboardTab().environmentObject(settings))
        }else if (settings.languageseted == false ){
            return AnyView(NavigationView{CountrySelectionView()})
        }else if(settings.languageUpdated == true){
            return AnyView(NavigationView{Login().environmentObject(settings)})
        }else{
            return AnyView(NavigationView{Login().environmentObject(settings)})
        }
    }
}

struct SceneDelegate_Previews: PreviewProvider {
    static var previews: some View {
        /*@START_MENU_TOKEN@*/Text("Hello, World!")/*@END_MENU_TOKEN@*/
    }
}
struct GeometryGetter: View {
    @Binding var rect: CGRect
    
    var body: some View {
        return GeometryReader { geometry in
            self.makeView(geometry: geometry)
        }
    }
    
    func makeView(geometry: GeometryProxy) -> some View {
        DispatchQueue.main.async {
            self.rect = geometry.frame(in: .global)
        }

        return Rectangle().fill(Color.clear)
    }
}
extension UIViewController {
    func present<Content: View>(style: UIModalPresentationStyle = .automatic, transitionStyle: UIModalTransitionStyle = .coverVertical, @ViewBuilder builder: () -> Content) {
        let toPresent = UIHostingController(rootView: AnyView(EmptyView()))
        toPresent.modalPresentationStyle = style
        toPresent.modalTransitionStyle = transitionStyle
        toPresent.view.backgroundColor = UIColor.init(red: 0, green: 0, blue: 0, alpha: 0.5)
        toPresent.rootView = AnyView(
            builder()
                .environment(\.viewController, toPresent)
        )
        self.present(toPresent, animated: true, completion: nil)
    }
}
struct ViewControllerHolder {
    weak var value: UIViewController?
}

struct ViewControllerKey: EnvironmentKey {
    static var defaultValue: ViewControllerHolder {
        return ViewControllerHolder(value: UIApplication.shared.windows.first?.rootViewController)
    }
}

extension EnvironmentValues {
    var viewController: UIViewController? {
        get { return self[ViewControllerKey.self].value }
        set { self[ViewControllerKey.self].value = newValue }
    }
}
