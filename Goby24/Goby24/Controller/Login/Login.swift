//
//  Login.swift
//  GOBY24
//
//  Created by Jignesh Bhensadadiya on 7/9/21.
//

import SwiftUI
import FBSDKLoginKit
import GoogleSignIn
import AuthenticationServices

//struct AppleUser:Codable{
//    let userId:String
//    let firstName:String
//    let lastName:String
//    let email:String
//
//    init?(credentials:ASAuthorizationAppleIDCredential){
//        guard
//            let firstName = credentials.fullName?.givenName,
//        let lastName = credentials.fullName?.familyName,
//        let email = credentials.email
//        else{return nil}
//        self.userId = credentials.user
//        self.firstName = firstName
//        self.lastName = lastName
//        self.email = email
//
//    }
//}

struct Login: View {
    //MARK: - PROPERTIES
    @State var username: String = ""//"yogeshsharmaa17@gmail.com"
    @State var password: String = ""//"Yagya@19012017"
    
    @State private var isSignUpActive:Bool = false
    @State private var isForgotPasswordActive:Bool = false
    @State private var isLogin:Bool = false
    
    @EnvironmentObject var settings: UserSettings
    
    
    func loginApp(){
        var param:[String:Any] = [String:Any]()
        param["email"] = username
        param["password"] = password
        WebAccess.postDataWith(_Url: WebAccess.LOGIN_URL, _parameters: param) { (result) in
            appDelegate?.stopLoadingView()
            switch (result){
            case .Success(let _data):
                print(_data)
                let responseData = UtilsObjs.cleanJson(to: _data)  as? [String:Any] ?? [:]
                let token:String = "Bearer " + getStringFromAny((responseData["access"] ?? ""))
                userDefault.set(getStringFromAny(token), forKey: Enum_Login.token.rawValue)
                userDefault.setValue(true, forKey: Enum_Login.isLogin.rawValue)
                userDefault.synchronize()
                print(token)
                // GET PROFILE DATA
                WebCommanMethods.getUserProfileData { (result) in
                    settings.loggedIn = true
                }

                break
            case .Error(let msg):
                ShowAlert(title: APPNAME, msg: msg, view: UIApplication.getTopViewController()!)
                break
            }
        }
    }
    func loginApp_Google(token:String){
        var param:[String:Any] = [String:Any]()
        param["auth_token"] = token
        param["type"] = "ios"
        
        WebAccess.postDataWith(_Url: WebAccess.LOGIN_GOOGLE_URL, _parameters: param,baseUrl: BASEURLSOCIAL) { (result) in
            switch (result){
            case .Success(let _data):
                print(_data)
                let responseData = UtilsObjs.cleanJson(to: _data)  as? [String:Any] ?? [:]
                let token:String = "Bearer " + getStringFromAny((responseData["access"] ?? ""))
                userDefault.set(getStringFromAny(token), forKey: Enum_Login.token.rawValue)
                userDefault.setValue(true, forKey: Enum_Login.isLogin.rawValue)
                userDefault.synchronize()
                print(token)
                // GET PROFILE DATA
                WebCommanMethods.getUserProfileData { (result) in
                    settings.loggedIn = true
                }

                break
            case .Error(let msg):
                ShowAlert(title: APPNAME, msg: msg, view: UIApplication.getTopViewController()!)
                break
            }
        }
    }
    func loginApp_facebok(token:String){
        var param:[String:Any] = [String:Any]()
        param["auth_token"] = token
        param["type"] = "ios"
        
        WebAccess.postDataWith(_Url: WebAccess.LOGIN_FACEBOOK_URL, _parameters: param,baseUrl: BASEURLSOCIAL) { (result) in
            switch (result){
            case .Success(let _data):
                print(_data)
                let responseData = UtilsObjs.cleanJson(to: _data)  as? [String:Any] ?? [:]
                let token:String = "Bearer " + getStringFromAny((responseData["access"] ?? ""))
                userDefault.set(getStringFromAny(token), forKey: Enum_Login.token.rawValue)
                userDefault.setValue(true, forKey: Enum_Login.isLogin.rawValue)
                userDefault.synchronize()
                print(token)
                // GET PROFILE DATA
                WebCommanMethods.getUserProfileData { (result) in
                    settings.loggedIn = true
                }

                break
            case .Error(let msg):
                ShowAlert(title: APPNAME, msg: msg, view: UIApplication.getTopViewController()!)
                break
            }
        }
    }
//    func tokenGenerate(token:String){
//        var param:[String:Any] = [String:Any]()
//        param["auth_token"] = token
//        param["type"] = "ios"
//
//        WebAccess.postDataWith(_Url: WebAccess.LOGIN_FACEBOOK_URL, _parameters: param) { (result) in
//            switch (result){
//            case .Success(let _data):
//                print(_data)
//                let responseData = _data//["data"] as? [String:Any] ?? [:]
//                let token:String = "Bearer " + getStringFromAny((responseData["access"] ?? ""))
//                userDefault.set(getStringFromAny(token), forKey: Enum_Login.token.rawValue)
//                userDefault.setValue(true, forKey: Enum_Login.isLogin.rawValue)
//                userDefault.synchronize()
//                print(token)
//                // GET PROFILE DATA
//                WebCommanMethods.getUserProfileData()
//                settings.loggedIn = true
//                break
//            case .Error(let msg):
//                ShowAlert(title: APPNAME, msg: msg, view: UIApplication.getTopViewController()!)
//                break
//            }
//        }
//    }
    func appleLogin(token:String,first_name:String,last_name:String,email:String,userId:String){
        if (token.count == 0){
            ShowAlert(title: APPNAME, msg: "TOKEN NOT FOUND", view: UIApplication.getTopViewController()!)
            return
        }
        var param:[String:Any] = [String:Any]()
        param["access_token"] = token
        param["first_name"] = first_name
        param["last_name"] = last_name
        param["email"] = email
        param["user_id"] = userId
        
        WebAccess.postDataWith(_Url: WebAccess.LOGIN_APPLE_URL, _parameters: param,baseUrl: BASEURLSOCIAL) { (result) in
            switch (result){
            case .Success(let _data):
                print(_data)
                let responseData = UtilsObjs.cleanJson(to: _data)  as? [String:Any] ?? [:]
                let token:String = "Bearer " + getStringFromAny((responseData["access"] ?? ""))
                userDefault.set(getStringFromAny(token), forKey: Enum_Login.token.rawValue)
                userDefault.setValue(true, forKey: Enum_Login.isLogin.rawValue)
                userDefault.synchronize()
                print(token)
                // GET PROFILE DATA
                WebCommanMethods.getUserProfileData { (result) in
                    settings.loggedIn = true
                }

                break
            case .Error(let msg):
                ShowAlert(title: APPNAME, msg: msg, view: UIApplication.getTopViewController()!)
                break
            }
        }
    }
    
    //MARK: VALIDATE STRING
    func validateLogin() -> String {
        if (username.isEmpty == true){
            return "Please enter email id"
        }else if (username.isValidEmail() == false){
            return "Please enter valid email id"
        }else if (password.isEmpty == true){
            return "Please enter password"
        }else{
            return ""
        }
    }
    
    var body: some View {
        
        ScrollView {
            
            ZStack{
                
                NavigationLink(destination: SignUp().environmentObject(settings),isActive: $isSignUpActive) {EmptyView()}
                NavigationLink(destination: ForgotPassword(),isActive: $isForgotPasswordActive) {EmptyView()}
                NavigationLink(destination: DashboardTab(),isActive: $isLogin) {EmptyView()}
                
                VStack{
                    Image("smallLogo").padding(.top, 50)
                    
                    Text("Sign in here").padding(.top, 50.0).modifier(CustomTextM(fontName: UIFont.Rubik.medium.fontName, fontSize: 16, fontColor: Color.init(hex: "2E2E2E")))
                    
                    Spacer()
                    
                    VStack(alignment: .center, spacing: 0) {
                        
                        VStack(alignment: .center, spacing: 25) {
                            
                            CustomTextField(placeHolderText: Localizable.Email.localized(),text: self.$username)
                            CustomTextField(placeHolderText: Localizable.Password.localized(),text:self.$password,isPasswordType: true)
                            
                        }.padding(.horizontal, 30)
                        
                        
                        VStack(alignment: .center, spacing: 25){
                            //Button
                            Button(action: {
                                print("CLICKED")
                                let str = self.validateLogin()
                                if(str.count > 0){
                                    print(str)
                                    ShowAlert(title: APPNAME, msg: ("SUCESS" + str), view: UIApplication.getTopViewController()!)
                                }else{
                                    
                                    self.loginApp()
                                    //fghfgh
                                }
                            }){
                                VStack{
                                    Text(Localizable.Confirm.localized()).frame(minWidth: 10, maxWidth: .infinity)
                                }
                            }
                            .modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 16, fontColor: Color.white))
                            .modifier(ButtonStyle(buttonHeight: 50, buttonColor: Color.init(hex: "00ADEF"), buttonRadius: 10))
                        }.padding(.horizontal,30).padding(.vertical,30)
                        
                        HStack{
                            
                            Button(action: {}){
                                HStack(alignment: .center, spacing: 20) {
                                    Text("Don’t have account?")
                                }
                            }
                            .modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 13, fontColor: Color.init(hex: "2E2E2E")))
                            
                            Button(action: {
                                isSignUpActive = true
                            }){
                                HStack(alignment: .center, spacing: 0) {
                                    Text(Localizable.Sign_up.localized())
                                }
                            }
                            
                            
                            .modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 13, fontColor: Color.init(hex: "00AEEF")))
                            
                            
                        }
                        VStack(){
                            Button(action: {
                                isForgotPasswordActive = true
                            }){
                                HStack(alignment: .center, spacing: 20) {
                                    Text("Forgot Password ")
                                }
                            }
                            .modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 13, fontColor: Color.init(hex: "00AEEF")))
                        }.padding(.vertical,8)
                        
                        LabelDividerCode(label: "or")
                        
                        
                        VStack(alignment: .center, spacing: 25){
                            //Button
                            Button(action: {
                                print("Continue with Google")
                                GIDSignIn.sharedInstance.signOut()
                                let signInConfig = GIDConfiguration.init(clientID: "673594831440-1l979l0t30gqtpuisipun69jc4mse361.apps.googleusercontent.com")
                                GIDSignIn.sharedInstance.signIn(with: signInConfig, presenting: UIApplication.getTopViewController()!) { user, error in
                                    guard error == nil else { return }
                                    guard let user = user else { return }
                                    user.authentication.do { authentication, error in
                                        guard error == nil else { return }
                                        guard let authentication = authentication else { return }
                                        
                                        let idToken = authentication.idToken
                                        
                                        // MAKE API CALL FOR GOOGLE
                                        self.loginApp_Google(token: idToken ?? "")
                                        
                                    }
                                }
                                
                            }){
                                HStack{
                                    Image("google").padding(.trailing, 0.0)
                                    Text("Continue with Google")
                                }
                            }
                            
                            .modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 16, fontColor: Color.black))
                            .modifier(ButtonStyle(buttonHeight: 50, buttonColor: Color.white, buttonRadius: 10))
                            
                            .modifier(ButtonStyle(buttonHeight: 50, buttonColor: Color.white, buttonRadius: 10))
                            .overlay(RoundedRectangle(cornerRadius: 10).stroke(Color.gray, lineWidth: 1))
                            
                            //Button
                            Button(action: {
                                print("Continue with Facebook")
                                
                                let loginManager = LoginManager()
                                if AccessToken.current?.isExpired == true{
                                    loginManager.logOut()
                                }
                               // if (AccessToken.current?.isExpired == false)
                               // {
                                    
                                    loginManager.logIn(permissions: ["public_profile","email"], from: UIApplication.getTopViewController()!) { (result, error) in
                                        if let userToken = result?.authenticationToken{
                                            // GET USER TOKEN HERE
                                            let _:AuthenticationToken = userToken
                                            print(AccessToken.current?.tokenString ?? "")
                                            
                                            //print token id and user id
                                            print("TOKEN IS \(AccessToken.current?.tokenString ?? "")")
                                            print("USER ID IS \(AccessToken.current?.userID ?? "")")
                                            
                                            
                                            //                                        guard let accessToken = FBSDKLoginKit.AccessToken.current else { return }
                                            //                                        let graphRequest = FBSDKLoginKit.GraphRequest(graphPath: "me",
                                            //                                                                                      parameters: ["fields": "email, name"],
                                            //                                                                                      tokenString: accessToken.tokenString,
                                            //                                                                                      version: nil,
                                            //                                                                                      httpMethod: .get)
                                            //                                        graphRequest.start { (connection, result, error) -> Void in
                                            //                                            if error == nil {
                                            //                                                print("result \(result)")
                                            //                                            }
                                            //                                            else {
                                            //                                                print("error \(error)")
                                            //                                            }
                                            //                                        }
                                            //
                                            //
                                            
                                            
                                            self.loginApp_facebok(token: AccessToken.current?.tokenString ?? "")
                                        }
                                    }
                               // }
                            }){
                                HStack{
                                    Image("facebook").padding(.trailing, 0.0)
                                    Text("Continue with Facebook")
                                }
                            }
                            .modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 16, fontColor: Color.white))
                            .modifier(ButtonStyle(buttonHeight: 50, buttonColor: Color.init(hex: "4267B2"), buttonRadius: 10))
                            
                            VStack {
                                SignInWithAppleButton(.continue,              //1
                                                      onRequest: { (request) in             //2
                                                        request.requestedScopes = [.fullName, .email]
                                                      },
                                                      onCompletion: { (result) in           //3
                                                        switch result {
                                                        case .success(let authorization):
                                                            if let appleIDCredential = authorization.credential as? ASAuthorizationAppleIDCredential {
                                                                
                                                                let authCode = appleIDCredential.authorizationCode
                                                                // if let appleUser = AppleUser(credentials: appleIDCredential){
                                                                //                                                                    UserDefaults.standard.setValue(appleUserData, forKey: appleUser.userId)
                                                                //                                                                }
                                                                
                                                                let authCodeString:String = String(data: authCode!, encoding: .utf8) ?? ""
                                                                let email = appleIDCredential.email ?? ""
                                                                let firstName = appleIDCredential.fullName?.givenName ?? ""
                                                                let lastName = appleIDCredential.fullName?.familyName ?? ""
                                                                let userId = appleIDCredential.user
                                                                
                                                                var details = (authCodeString + " " + email + " ")
                                                                details = details + " " + firstName
                                                                details = details + " " + lastName
                                                                details = details + " " + userId
                                                                
                                                                appleLogin(token: authCodeString, first_name: firstName, last_name: lastName, email: email, userId: userId)
                                                                
                                                            }
                                                            break
                                                        case .failure(let error):
                                                            ShowAlert(title: APPNAME, msg: error.localizedDescription, view: UIApplication.getTopViewController()!)
                                                            break
                                                        }
                                                      }).modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 16, fontColor: Color.white))
                                
                            }.signInWithAppleButtonStyle(.black).frame(height: 50).cornerRadius(10)//.padding(.horizontal)
                            
                        }.padding(.horizontal,30).padding(.vertical,30)
                        
                        
                        Spacer()
                        
                        
                    }.padding(.vertical,50)
                }
                
            }
            
            .navigationBarBackButtonHidden(true) // not needed, but just in case
            .hiddenNavigationBarStyle()
        }
    }
}
struct Login_Previews: PreviewProvider {
    static var previews: some View {
        Login()
    }
}
