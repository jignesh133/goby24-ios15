//
//  LoginFirst.swift
//  Goby24
//
//  Created by Jignesh Bhensadadiya on 7/10/21.
//

import SwiftUI

struct LoginFirst: View {

    var body: some View {
        ZStack{
            VStack{
                Image("smallLogo").padding(.top, 50)
                
                Text("You need to login first").padding(.top, 30).modifier(CustomTextM(fontName: UIFont.Rubik.medium.fontName, fontSize: 26, fontColor: Color.init(hex: "2E2E2E")))
                
                Image("loginFirst").padding(.top, 50)

                VStack{
                    Button(action: {
                        print("CONFIRM BUTTON ACTION CLICKED")
                        
                    }){
                        Text(Localizable.Continue.localized())
                    }
                    .modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 16, fontColor: Color.white))
                    .modifier(ButtonStyle(buttonHeight: 50, buttonColor: Color.init(hex: "00ADEF"), buttonRadius: 10))
                }.padding(.horizontal,30).padding(.vertical,30)
                Spacer()
                

            }
            
        }.showNavigationBarWithBackStyle()
    }

}

struct LoginFirst_Previews: PreviewProvider {
    static var previews: some View {
        LoginFirst()
    }
}
