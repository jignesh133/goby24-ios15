    //
    //  ContentView.swift
    //  GOBY24
    //
    //  Created by Jignesh Bhensadadiya on 7/8/21.
    //
    
    import SwiftUI

struct CountrySelectionView: View {
    //MARK: - PROPERTIES
    @State var strLanguage: String?
    @State var strLanguageCode: String?

    @State var strCountry: String?
    @State var isFromProfile:Bool = false
    
    @State var languageIndex: Int = 0
    @State var countryIndex: Int = 0

    var languages = ["English","French", "German","Italian"];
    var languagesCodes = ["en","it", "de","fr"];

    @EnvironmentObject var settings: UserSettings
    @Environment(\.presentationMode) var presentationMode

    var body: some View {
        
        
        ZStack{
            
            
            VStack{
                //  logo
                Image("main_logo").padding(.top, 30)
                Spacer()
            }
            VStack(alignment: .center, spacing: 20){
                //Button
                Button(action: {
                    print("Button tapped!")
                    RPicker.selectOption(title: "Choose your language", cancelText: "Cancel", doneText: "Done", dataArray: languages, selectedIndex: languageIndex) { (val, index) in
                        strLanguage = val
                        strLanguageCode = languagesCodes[index]
                    }
                }){
                    ZStack() {
                        Text(strLanguage ?? "Choose your language")
                        HStack() {
                            Spacer()
                            Image("down").padding(.trailing,20)
                        }
                    }
                }
                .modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 16, fontColor: Color.init(hex: "2E2E2E")))
                .modifier(ButtonStyle(buttonHeight: 50, buttonColor: Color.init(hex: "F2F2F2"), buttonRadius: 10))
                
                //Button
                Button(action: {
                    RPicker.selectOption(title: "Choose your country", cancelText: "Cancel", doneText: "Done", dataArray: appDelegate?.countryNames, selectedIndex: countryIndex) { (val, index) in
                        strCountry = val

                    }
                }){
                    ZStack() {
                        Text(strCountry ?? "Choose your country")
                        HStack() {
                            Spacer()
                            Image("down").padding(.trailing,20)
                        }
                    }
                }
                .modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 16, fontColor: Color.init(hex: "2E2E2E")))
                .modifier(ButtonStyle(buttonHeight: 50, buttonColor: Color.init(hex: "F2F2F2"), buttonRadius: 10))
                
                //Button
                Button(action: {
                    print("CONFIRM BUTTON ACTION CLICKED")
                    if ((strCountry ?? "").removeWhiteSpace().count == 0 ){
                        ShowAlert(title: APPNAME, msg: "Please select country", view: UIApplication.getTopViewController()!)
                    }else if ((strLanguage ?? "").removeWhiteSpace().count == 0 ){
                        ShowAlert(title: APPNAME, msg: "Please select language", view: UIApplication.getTopViewController()!)
                    }else{
                        
                        if(isFromProfile == true){
                            let alert = UIAlertController(title: APPNAME, message: "Are you sure you want to change language", preferredStyle: .alert)
                            let cancel = UIAlertAction(title:(Localizable.Cancel.localized()), style: .cancel) { (res) in}
                            alert.addAction(cancel)
                            let logout = UIAlertAction(title:(Localizable.Log_Out.localized()), style: .default) { (res) in
                                print("CONFIRM BUTTON ACTION CLICKED")
                                
                                userDefault.setValue(strLanguage, forKey: Enum_userDefaults.language.rawValue)
                                userDefault.setValue(strLanguageCode, forKey: Enum_userDefaults.languageCode.rawValue)
                                userDefault.setValue(strCountry, forKey: Enum_userDefaults.country.rawValue)
                                
                                userDefault.synchronize()

                                appDelegate?.updateLanguage()
                                appDelegate?.resetUserDefautls()
                                
                                self.settings.loggedIn = false
                                self.presentationMode.wrappedValue.dismiss()
                            }
                            alert.addAction(logout)
                            UIApplication.getTopViewController()!.present(alert, animated: true, completion: nil)
                        }else{
                            userDefault.setValue(strLanguage, forKey: Enum_userDefaults.language.rawValue)
                            userDefault.setValue(strLanguageCode, forKey: Enum_userDefaults.languageCode.rawValue)
                            userDefault.setValue(strCountry, forKey: Enum_userDefaults.country.rawValue)
                            userDefault.synchronize()

                            
                            appDelegate?.updateLanguage()
                            settings.languageseted = true
                        }
                    }
                }){
                    HStack(alignment: .center, spacing: 20) {
                        Text("Confirm")
                    }
                }
                .modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 16, fontColor: Color.white))
                .modifier(ButtonStyle(buttonHeight: 50, buttonColor: Color.init(hex: "00ADEF"), buttonRadius: 60))
                .padding(.top)
                
                //Button
                if(isFromProfile == true){
                    Button(action: {
                        print("Cancel")
                        self.presentationMode.wrappedValue.dismiss()
                    }){
                        HStack(alignment: .center, spacing: 20) {
                            Text("Cancel")
                        }
                    }
                    .modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 16, fontColor: Color.white))
                    .modifier(ButtonStyle(buttonHeight: 50, buttonColor: Color.init(hex: "00ADEF"), buttonRadius: 60))
                    
                }
            }
            .padding(.horizontal,40)
            .padding(.vertical, 0)
        }
        .hiddenNavigationBarStyle()
        .onAppear(){
            strLanguageCode = userDefault.value(forKey: Enum_userDefaults.languageCode.rawValue) as? String ?? nil
            strLanguage = userDefault.value(forKey: Enum_userDefaults.language.rawValue) as? String ?? nil
            strCountry = userDefault.value(forKey: Enum_userDefaults.country.rawValue) as? String ?? nil

            if ((strLanguageCode?.count ?? 0) > 0){
                languageIndex =  self.languagesCodes.firstIndex(of: strLanguageCode ?? "") ?? 0
            }
            if ((strCountry?.count ?? 0) > 0){
                countryIndex =  appDelegate?.countryNames.firstIndex(of: strCountry ?? "") ?? 0
            }

        }
        
    }
}
    
    
    
