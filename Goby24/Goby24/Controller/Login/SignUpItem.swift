//
//  SignUpItem.swift
//  Goby24
//
//  Created by Jignesh Bhensadadiya on 7/11/21.
//

import SwiftUI

struct SignUpItem: View {
    @State var title: String = "dashboard"
    @State var image: String = "dashboard"
    
    var body: some View {
        ZStack{
            VStack{
                HStack{
                    Image(image).padding()
                    Text(title).modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 16, fontColor: Color.black))
                    Spacer()
                    Button(action: {
                        print("CONFIRM BUTTON ACTION CLICKED")
                        
                    }){
                        Image("roundnext").padding()
                    }
                    
                   
                }.frame(width: .infinity, height: 50)
                Divider().padding(.horizontal,15)
            }
        }.background(Color.clear)
    }
}

struct SignUpItem_Previews: PreviewProvider {
    static var previews: some View {
        SignUpItem()
    }
}
