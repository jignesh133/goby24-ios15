//
//  ForgotPassword.swift
//  Goby24
//
//  Created by Jignesh Bhensadadiya on 7/10/21.
//

import SwiftUI

struct ForgotPassword: View {
    //MARK: - PROPERTIES
    @State var username: String = ""
    @State private var isResetPassword:Bool = false
    @State var isLoaderShown:Bool = false
    @State var isAlertPresent:Bool = false
    @State var alertMessage:String = ""
  
    
    @Environment(\.presentationMode) var presentationMode
 
    
    func getPassword() {

        if (username.removeWhiteSpace().count == 0){
     
            ShowAlert(title: APPNAME, msg: "Please enter email Id", view: UIApplication.getTopViewController()!)

        }else if(username.removeWhiteSpace().isValidEmail() == false){
            ShowAlert(title: APPNAME, msg: "Please enter valid email Id", view: UIApplication.getTopViewController()!)
        }else{
            
            var param:[String:Any] = [String:Any]()
            param["email"] = username
            
            isLoaderShown = false
            WebAccess.postDataWith(_Url: WebAccess.PASSWORD_RESET, _parameters: param) { (result) in
                switch (result){
                case .Success(let _data):
                    print(_data)
                    isLoaderShown = false
                    ShowAlertWithCompletation(title: APPNAME, msg: "please Check your Email - reset password link has been sent", view: UIApplication.getTopViewController()!) { (result) in
                        if (result == true){
                            NavigationUtil.popToRootView()
                        }
                    }
                    break
                case .Error(let error):
                    ShowAlertWithCompletation(title: APPNAME, msg: error, view: UIApplication.getTopViewController()!) { (result) in

                    }
                    
                    isLoaderShown=false
                    break
                }
            }
        }
    }
    
    
    var body: some View {
        LoadingView(isShowing: $isLoaderShown) {


        ZStack{
            
            NavigationLink(destination: ResetPassword(),isActive: $isResetPassword) {EmptyView()}

            VStack{
             //   Image("smallLogo").padding(.top, 50)
                
                Text("Forgot Password").padding(.top, 50.0).modifier(CustomTextM(fontName: UIFont.Rubik.medium.fontName, fontSize: 16, fontColor: Color.init(hex: "2E2E2E")))
                
                Text("Enter your email for the verification process").padding([.top, .leading, .trailing], 10).modifier(CustomTextM(fontName: UIFont.Roboto.regular.fontName, fontSize: 13, fontColor: Color.init(hex: "2E2E2E")))

                Spacer()
                
                VStack(alignment: .center, spacing: 0) {
                    
                    VStack(alignment: .center, spacing: 25) {
                        
                        CustomTextField(placeHolderText: "Enter your email ",
                                      text: self.$username)
                        
                    }.padding(.horizontal, 30)
                    
                    
                    VStack(alignment: .center, spacing: 20){
                        //Button
                        Button(action: {
                            print("")
                            //isResetPassword = true
                            self.getPassword()
                        }){
                            Text(Localizable.Continue.localized())
                        }
                        .modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 16, fontColor: Color.white))
                        .modifier(ButtonStyle(buttonHeight: 50, buttonColor: Color.init(hex: "00ADEF"), buttonRadius: 10))
                    }.padding(.horizontal,30).padding(.vertical,30)
                    Spacer()
                }.padding(.vertical,50)
            }
            
        }
        .showNavigationBarWithBackStyle()
        
        }
    }

}

struct ForgotPassword_Previews: PreviewProvider {
    static var previews: some View {
        ForgotPassword()
    }
}
