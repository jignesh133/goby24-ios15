//
//  ResetPassword.swift
//  Goby24
//
//  Created by Jignesh Bhensadadiya on 7/10/21.
//

import SwiftUI

struct ResetPassword: View {
    
    //MARK: - PROPERTIES
    @State var password1: String = ""
    @State var password2: String = ""
    @State var isLoaderShown:Bool = false
    @State var isAlertPresent:Bool = false
    @State var alertMessage:String = ""

    
    @Environment(\.presentationMode) var presentationMode
    
    func validatePassword() -> String{
        if (password1.removeWhiteSpace().count == 0){
            return "Please enter old Password"
        }else if (password2.removeWhiteSpace().count == 0){
            return "Please enter new password"
        }else{
            return ""
        }
    }
    func resetPassword() {
        
        var param:[String:Any] = [String:Any]()
        param["old_password"] = password1
        param["new_password"] = password2

        isLoaderShown = false
        WebAccess.putDataWith(_Url: WebAccess.CHANGE_PASSWORD, _parameters: param) { (result) in
            switch (result){
            case .Success(let _data):
                print(_data)
                isLoaderShown = false
                ShowAlertWithCompletation(title: APPNAME, msg: "Password change sucessfully", view: UIApplication.getTopViewController()!) { (result) in
                    if (result == true){
                        self.presentationMode.wrappedValue.dismiss()
                    }
                }

                NavigationUtil.popToRootView()
                break
            case .Error(let error):
                isLoaderShown = false

                ShowAlertWithCompletation(title: APPNAME, msg: error, view: UIApplication.getTopViewController()!) { (result) in
                    if (result == true){
//                        self.presentationMode.wrappedValue.dismiss()
                    }

                }

                isLoaderShown=false
                break
            }
        }
    }
    
    var body: some View {
        LoadingView(isShowing: $isLoaderShown) {
            
            ZStack{
                VStack{
                    // Image("smallLogo").padding(.top, 50)
                    
                    Text(Localizable.Change_Password.localized()).padding(.top, 50.0).modifier(CustomTextM(fontName: UIFont.Rubik.medium.fontName, fontSize: 16, fontColor: Color.init(hex: "2E2E2E")))
                    
                    Text("Set the new password for your account so you can login and access all the features.").padding([.top, .leading, .trailing], 15).modifier(CustomTextM(fontName: UIFont.Roboto.regular.fontName, fontSize: 13, fontColor: Color.init(hex: "2E2E2E"))).multilineTextAlignment(.center)
                    
                    Spacer()
                    
                    VStack(alignment: .center, spacing: 0) {
                        
                        VStack(alignment: .center, spacing: 25) {
                            
                            CustomTextField(placeHolderText: (Localizable.Old_Password.localized()),
                                            text: self.$password1,isPasswordType: true)
                            CustomTextField(placeHolderText: (Localizable.New_Password.localized()),
                                            text: self.$password2,isPasswordType: true)
                            
                        }.padding(.horizontal, 30)
                        
                        
                        VStack(alignment: .center, spacing: 20){
                            //Button
                            Button(action: {
                                print("CONFIRM BUTTON ACTION CLICKED")
                                let str = self.validatePassword()
                                if (str.count == 0){
                                    resetPassword()
                                }else{
                                    ShowAlert(title: APPNAME, msg: str, view: UIApplication.getTopViewController()!)
                                }
                                //NavigationUtil.popToRootView()
                            }){
                                Text(Localizable.Reset_Your_Password.localized())
                            }
                            .modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 16, fontColor: Color.white))
                            .modifier(ButtonStyle(buttonHeight: 50, buttonColor: Color.init(hex: "00ADEF"), buttonRadius: 10))
                        }.padding(.horizontal,30).padding(.vertical,30)
                        Spacer()
                    }.padding(.vertical,50)
                }
            }.showNavigationBarWithBackStyle()
        }
        
    }
}


struct ResetPassword_Previews: PreviewProvider {
    static var previews: some View {
        ResetPassword()
    }
}
