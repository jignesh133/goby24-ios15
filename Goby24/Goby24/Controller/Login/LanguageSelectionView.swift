    //
    //  ContentView.swift
    //  GOBY24
    //
    //  Created by Jignesh Bhensadadiya on 7/8/21.
    //
    
    import SwiftUI

    struct LanguageSelectionView: View {
        //MARK: - PROPERTIES
        @State var strLanguage: String = "English"
        @State var strCountry: String = "Choose your  country *"
        @State var languageExpand:Bool = false
        
        var languages = ["English", "French","German","Italian"];
        
        var body: some View {
            VStack(alignment: .center, spacing: 20){
                
                Group{
                    // 3
                    HStack(){
                        Text("Select Language").padding(.top, 10).modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 16, fontColor: Color.init(hex: "2E2E2E"))).padding(.horizontal)
                        Spacer()
                    }.padding(.horizontal)
                    
                    DisclosureGroup(isExpanded: $languageExpand) {
                        Group{
                            HStack{
                                Text("English").modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 13, fontColor: Color.init(hex: "2E2E2E")))
                                Spacer()
                            }.padding(.leading,15).frame(height: 40).onTapGesture {
                                withAnimation{
                                    strLanguage = "English"
                                    languageExpand.toggle()
                                    userDefault.setValue(strLanguage, forKey: Enum_userDefaults.language.rawValue)
                                    userDefault.setValue(languageCode.English.rawValue, forKey: Enum_userDefaults.languageCode.rawValue)
                                    userDefault.synchronize()
                                    appDelegate?.updateLanguage()
                                }
                            }
                        }
                        Group{
                            HStack{
                                Text("French").modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 13, fontColor: Color.init(hex: "2E2E2E")))
                                Spacer()
                            }.padding(.leading,15).frame(height: 40).onTapGesture {
                                withAnimation{
                                    strLanguage = "French"
                                    languageExpand.toggle()
                                    userDefault.setValue(strLanguage, forKey: Enum_userDefaults.language.rawValue)
                                    userDefault.setValue(languageCode.French.rawValue, forKey: Enum_userDefaults.languageCode.rawValue)

                                    userDefault.synchronize()
                                    appDelegate?.updateLanguage()
                                }
                            }
                        }
                        
                        Group{
                            HStack{
                                Text("German").modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 13, fontColor: Color.init(hex: "2E2E2E")))
                                Spacer()
                            }.padding(.leading,15).frame(height: 40).onTapGesture {
                                withAnimation{
                                    strLanguage = "German"
                                    languageExpand.toggle()
                                    userDefault.setValue(strLanguage, forKey: Enum_userDefaults.language.rawValue)
                                    userDefault.setValue(languageCode.German.rawValue, forKey: Enum_userDefaults.languageCode.rawValue)

                                    userDefault.synchronize()
                                    appDelegate?.updateLanguage()
                                }
                            }
                        }
                        Group{
                            HStack{
                                Text("Italian").modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 13, fontColor: Color.init(hex: "2E2E2E")))
                                Spacer()
                            }.padding(.leading,15).frame(height: 40).padding(.bottom,10).onTapGesture {
                                withAnimation{
                                    strLanguage = "Italian"
                                    languageExpand.toggle()
                                    userDefault.setValue(strLanguage, forKey: Enum_userDefaults.language.rawValue)
                                    userDefault.setValue(languageCode.Italian.rawValue, forKey: Enum_userDefaults.languageCode.rawValue)

                                    userDefault.synchronize()
                                    appDelegate?.updateLanguage()

                                }
                            }
                        }
                        
                    } label: {
                        HStack{
                            Text(strLanguage).padding(.horizontal).modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 15, fontColor: Color.init(hex: "2E2E2E")))
                            Spacer()
                            Image(languageExpand ? "up":"down").padding(.trailing,-10)
                            
                        }
                        .padding(.trailing)
                        .frame(minWidth: 0, maxWidth: .infinity)
                        .frame(height: 40)
                        .onTapGesture {
                            withAnimation{
                                languageExpand.toggle()
                            }
                        }
                    }
                    .accentColor(Color.clear)
                    .background(Color.init(hex: "F2F2F2"))
                    .cornerRadius(24)
                    .modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 15, fontColor: Color.init(hex: "2E2E2E"))).multilineTextAlignment(.center)
                    .padding()
                }
            }.showNavigationBarWithBackStyle().onAppear(){
                strLanguage = userDefault.object(forKey: Enum_userDefaults.language.rawValue) as? String ?? "English"
            }
        }
    }
    
    
    
