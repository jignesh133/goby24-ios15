

import SwiftUI
struct NavLogo: View {

    var body: some View {
            VStack {
                Image("appLogo")
            }
    }
}

struct SignUp2: View {
    @EnvironmentObject var settings: UserSettings
    //    @Environment(\.currentTab) var tab
    @Environment(\.presentationMode) var presentationMode
    
    
    var body: some View {
        //   NavigationView {
        ScrollView{
            VStack{
                
                VStack{
                    Group{
                        ZStack{
                            VStack{
                                
                                Button(action: {
                                    // tab.wrappedValue = .home
                                    settings.openTab = 0
                                }){
                                    HStack{
                                        Image("dashboard").padding()
                                        Text("Dashboard").modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 16, fontColor: Color.black))
                                        Spacer()
                                        
                                        Image("roundnext").padding()
                                    }
                                }
                            }.frame(width: .infinity, height: 50)
                            Divider().padding(.horizontal,15)
                            
                        }.background(Color.clear)
                    }
                    
                    Group{
                        ZStack{
                            VStack{
                                
                                Button(action: {
                                    self.presentationMode.wrappedValue.dismiss()
                                }){
                                    HStack{
                                        Image("signupround").padding()
                                        Text("Profile").modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 16, fontColor: Color.black))
                                        Spacer()
                                        
                                        Image("roundnext").padding()
                                    }
                                }
                            }.frame(width: .infinity, height: 50)
                            Divider().padding(.horizontal,15)
                            
                        }.background(Color.clear)
                    }
                    
                    //                        NavigationLink(destination: LoginFirst()) {
                    //                            SignUpItem(title: "Dashboard", image: "dashboard")
                    //                        }
                    //                        NavigationLink(destination: LoginFirst()) {
                    //                            SignUpItem(title: (Localizable.Profile.localized()), image: "profile")
                    //                        }
                    NavigationLink(destination: Inbox()) {
                        SignUpItem(title: "Inbox", image: "inbox")
                    }
                    NavigationLink(destination: MyRides()) {
                        SignUpItem(title: (Localizable.My_Rides.localized()), image: "myrides")
                    }
                    NavigationLink(destination: LoginFirst()) {
                        SignUpItem(title: (Localizable.Payments__Refunds.localized()), image: "wallet")
                    }
                }.padding()
                
                VStack{
                    Text("Other")
                        .modifier(CustomTextM(fontName: UIFont.Montserrat.semiBold.fontName, fontSize: 16, fontColor: Color.black))
                        .padding(.top,30)
                    
                    
                }
                VStack{
                    //CountrySelectionView
                    NavigationLink(destination: CountrySelectionView(isFromProfile:true)) {
                        SignUpItem(title: "Change Language", image: "changelanguage")
                    }.isDetailLink(false)
                    NavigationLink(destination: webview(urlString: "https://www.apple.com/legal/privacy/en-ww/")) {
                        SignUpItem(title: (Localizable.Privacy_Policy.localized()), image: "privacy")
                    }
                    NavigationLink(destination: webview(urlString: "https://www.apple.com/legal/internet-services/itunes/us/terms.html")) {
                        SignUpItem(title: "Terms and conditions", image: "terms-and-conditions ")
                    }
                    NavigationLink(destination: ContactUsView()) {
                        SignUpItem(title: "Contact Support", image: "headphone")
                    }
                    //Button
                    Button(action: {
                        let alert = UIAlertController(title: APPNAME, message: "Are you sure you want to logout", preferredStyle: .alert)
                        let cancel = UIAlertAction(title:(Localizable.Cancel.localized()), style: .cancel) { (res) in
                        }
                        alert.addAction(cancel)
                        let logout = UIAlertAction(title:(Localizable.Log_Out.localized()), style: .default) { (res) in
                            print("CONFIRM BUTTON ACTION CLICKED")
                            appDelegate?.resetUserDefautls()
                            self.settings.loggedIn = false
                        }
                        alert.addAction(logout)
                        UIApplication.getTopViewController()!.present(alert, animated: true, completion: nil)
                    }){
                        SignUpItem(title: (Localizable.Log_Out.localized()), image: "logout")
                    }
                }.padding()
                Spacer()
            }
        }.showNavigationBarWithBackStyle()
    }
}
struct SignUp2_Previews: PreviewProvider {
    static var previews: some View {
        SignUp2()
    }
}
