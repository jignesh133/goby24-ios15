//
//  MostPopularRidesListView.swift
//  Goby24
//
//  Created by Jignesh Bhensadadiya on 9/2/21.
//

import SwiftUI

struct MostPopularRidesListView: View {
    @State var arrTrending:[Model_trending_routes] = []

    var body: some View {
        
        ScrollView(.vertical){
            VStack{
                ForEach(0..<(self.arrTrending.count), id: \.self) { row in
                    MostPopularRideView(rides: self.arrTrending[row])
                }
            }
        }.showNavigationBarWithBackStyle().padding(.bottom,30)
    }
}

struct MostPopularRidesListView_Previews: PreviewProvider {
    static var previews: some View {
        MostPopularRidesListView()
    }
}
