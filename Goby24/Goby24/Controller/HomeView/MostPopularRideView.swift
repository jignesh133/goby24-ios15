//
//  MostPopularRideView.swift
//  Goby24
//
//  Created by Jignesh Bhensadadiya on 7/21/21.
//

import SwiftUI

struct MostPopularRideView: View {
    var rides:Model_trending_routes?
    
    func getFullName(obj:Model_trending_routes) -> String {
        return ((obj.journeyFrom ?? "") + " - " + (obj.journeyTo ?? ""))
    }
    var body: some View {
        
        HStack(alignment:.center){
            Text(self.getFullName(obj: rides!)).modifier(CustomTextM(fontName: UIFont.Poppins.regular.fontName, fontSize: 16, fontColor: Color.init(hex: "2E2E2E"))).padding()
            Text(rides?.price ?? "").modifier(CustomTextM(fontName: UIFont.Montserrat.semiBold.fontName, fontSize: 16, fontColor: Color.init(hex: "2E2E2E"))).padding()
            Image("next").padding(.trailing,20).padding()


        }.background(RoundedRectangle(cornerRadius: 25).fill(Color.white).shadow(color: Color.black.opacity(0.12), radius: 3)).padding()
    }
}

struct MostPopularRideView_Previews: PreviewProvider {
    static var previews: some View {
        MostPopularRideView()
            .previewLayout(.sizeThatFits)
    }
}
