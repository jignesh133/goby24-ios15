
//
//  HomeHeaderView.swift
//  Goby24
//
//  Created by Jignesh Bhensadadiya on 7/13/21.
// "Your ride. your choice"
import SwiftUI
import Localize_Swift
import SDWebImageSwiftUI
import SwiftUIPager

struct HomeView: View {
    @State var source: String = ""
    @State var destination: String = ""
    @State var arrTrending:[Model_trending_routes] = []
    @State var arrSliderImages:[Model_SliderImages] = []

    @State var pickupLocation:Model_searchLocation?
    @State var pickOffLocation:Model_searchLocation?
    @State var isOpenPickUpLocation:Bool = false
    @State var isOpenPickOffLocation:Bool = false
    @State var leavingFrom: String = ""
    @State var goingTo: String = ""
    @State var selection: Int? = nil
    @ObservedObject private var findRide = SearchRouteData.shared
    @Binding var tabSelection: Int
    
    @StateObject var page: Page = .first()
    var items = Array(0..<10)

    
    func getAllTrending(){
        WebAccess.getDataWith(_Url: WebAccess.TRENDING_ROUTES , _parameters: [:],isHideLoader: true) { (result) in
            switch (result){
            case .Success(let _data):
                do{
                    let result:[String:Any] = UtilsObjs.cleanJson(to: _data ) as? [String : Any] ?? [String:Any]()
                    print(result)
                    let data1 =  try JSONSerialization.data(withJSONObject: result["result"] ?? [] , options: JSONSerialization.WritingOptions.prettyPrinted)
                    let convertedString = String(data: data1, encoding: String.Encoding.utf8)
                    let jsonData = Data((convertedString?.utf8)!)
                    self.$arrTrending.wrappedValue.removeAll()
                    self.arrTrending = try JSONDecoder().decode([Model_trending_routes].self, from: jsonData)
                    NSLog("LOG")
                }catch(let error){
                    print(error)
                }
                break
            case .Error(let msg):
                ShowAlert(title: APPNAME, msg: msg, view: UIApplication.getTopViewController()!)
                break
            }
        }
    }
    func getSliderImages(){
        WebAccess.getDataWith(_Url: WebAccess.SLIDERIMAGES , _parameters: [:],isHideLoader: true) { (result) in
            switch (result){
            case .Success(let _data):
                do{
                    let result:[String:Any] = UtilsObjs.cleanJson(to: _data ) as? [String : Any] ?? [String:Any]()
                    print(result)
                    let data1 =  try JSONSerialization.data(withJSONObject: result["result"] ?? [] , options: JSONSerialization.WritingOptions.prettyPrinted)
                    let convertedString = String(data: data1, encoding: String.Encoding.utf8)
                    let jsonData = Data((convertedString?.utf8)!)
                    self.$arrSliderImages.wrappedValue.removeAll()
                    self.arrSliderImages = try JSONDecoder().decode([Model_SliderImages].self, from: jsonData)
                    NSLog("LOG")
                }catch(let error){
                    print(error)
                }
                break
            case .Error(let msg):
                ShowAlert(title: APPNAME, msg: msg, view: UIApplication.getTopViewController()!)
                break
            }
        }
    }
    var body: some View {

        
        ScrollView{
            
            VStack{
                
                Pager(page: page,
                      data: Array(0..<self.arrSliderImages.count),
                        id: \.self,
                        content: { index in
                            VStack{
                                let urlString = IMAGEURL + getStringFromAny(self.arrSliderImages[index].image ?? "")//(driverProfile.profilePic ?? "")
                                WebImage(url: URL.init(string: urlString ))
                                    .placeholder(Image(systemName: "homebgheader")) // Placeholder Image
                                    .renderingMode(.original)
                                    .resizable()
                                    .aspectRatio(contentMode: .fill)
                                    .padding(10)
                                    .background(RoundedRectangle(cornerRadius: 25).fill(Color.white).shadow(color: Color.black.opacity(0.12), radius: 3))
                                    .frame(width: 280, height: 130)
                            }
                   })
                    .preferredItemSize(CGSize.init(width: 300, height: 150))
                    .frame(width: 300, height: 150).padding(.bottom,30)
                
//                ScrollView(.horizontal){
//                    HStack{
//                        ForEach(0..<(self.arrSliderImages.count), id: \.self) { row in
//                            let urlString = IMAGEURL + getStringFromAny(self.arrSliderImages[row].image ?? "")//(driverProfile.profilePic ?? "")
//                            WebImage(url: URL.init(string: urlString ))
//                                .placeholder(Image(systemName: "homebgheader")) // Placeholder Image
//                                .renderingMode(.original)
//                                .resizable()
//                                .aspectRatio(contentMode: .fill)
//                                .background(RoundedRectangle(cornerRadius: 25).fill(Color.white).shadow(color: Color.black.opacity(0.12), radius: 3)).padding()
//                                .frame(width: 300, height: 150)
//                        }
//                    }
//                }.frame(width: 300, height: 150).padding(.bottom,30)
                
//                Group{
//                    
//                    NavigationLink(destination:SearchLocation(strText: self.pickupLocation?.structuredFormatting?.mainText ?? "", onLocationSelect: { (location) in
//                        pickupLocation = location
//                        leavingFrom = location.descriptionField ?? "NONE"
//                    }),isActive: $isOpenPickUpLocation) {EmptyView()}
//                    NavigationLink(destination:SearchLocation(strText: self.pickOffLocation?.structuredFormatting?.mainText ?? "", onLocationSelect: { (location) in
//                        pickOffLocation = location
//                        goingTo = location.descriptionField ?? "NONE"
//                    }),isActive: $isOpenPickOffLocation) {EmptyView()}
//
//                    
//                    Text(Localizable.YOUR_RIDE_YOUR_CHOICE.localized()).modifier(CustomTextM(fontName: UIFont.Rubik.medium.fontName, fontSize: 26, fontColor: Color.init(hex: "2E2E2E")))
//  
//                    TextField((Localizable.Leaving_from.localized()), text: $leavingFrom, onEditingChanged: { (change) in
//                        if change == true{
//                            UIApplication.shared.endEditing()
//                            isOpenPickUpLocation = true
//                        }
//                    })
//                    .padding(EdgeInsets(top: 0, leading: 15, bottom: 0, trailing: 15))
//                    .frame(width: .infinity, height: 45, alignment: .center)
//                    .background(Color(red: 0.95, green: 0.95, blue: 0.95))
//                    .cornerRadius(22.5)
//                    .modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 16, fontColor: Color.black))
//                    .padding(.horizontal,25).padding(.top,5)
//                    
//                    TextField((Localizable.Going_to.localized()), text: $goingTo, onEditingChanged: { (change) in
//                        if change == true{
//                            UIApplication.shared.endEditing()
//                            
//                            isOpenPickOffLocation = true
//                        }
//                    })
//                    .padding(EdgeInsets(top: 0, leading: 15, bottom: 0, trailing: 15))
//                    .frame(width: .infinity, height: 45, alignment: .center)
//                    .background(Color(red: 0.95, green: 0.95, blue: 0.95))
//                    .cornerRadius(22.5)
//                    .modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 16, fontColor: Color.black))
//                    .padding(.horizontal,25).padding(.top,5)
//                    
//                    
//                    
////                    CustomTextField(placeHolderText: Localizable.Leaving_from.localized(),
////                                    text: self.$source).padding(.horizontal)
////
////                    CustomTextField(placeHolderText: Localizable.Going_to.localized(),
////                                    text: self.$destination).padding(.horizontal)
//                    
//                    NavigationLink(destination: SearchRidesResultsView(), tag: 1, selection: $selection) {
//                        
//                        Button(action: {
//                            
//                            if (self.pickupLocation == nil){
//                                ShowAlert(title: APPNAME, msg: "Please enter source address", view: UIApplication.getTopViewController()!)
//                            }else if (self.pickOffLocation == nil){
//                                ShowAlert(title: APPNAME, msg: "Please enter destination address", view: UIApplication.getTopViewController()!)
//                            }else{
//                                self.findRide.journeyDate = Date()
//                                self.findRide.sourceLocation = self.pickupLocation
//                                self.findRide.destinationLocation = self.pickOffLocation
//                                selection = 1
//                            }
//                            
//                        }){
//                            Text(Localizable.Search.localized()).frame(minWidth: 0,  maxWidth: .infinity)
//                        }
//                        .modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 16, fontColor: Color.white))
//                        .modifier(ButtonStyle(buttonHeight: 50, buttonColor: Color.init(hex: "00ADEF"), buttonRadius: 50))
//                        .padding()
//                        .buttonStyle(PlainButtonStyle())
//                        
//                    }.isDetailLink(false)
//
//                }.padding(.horizontal)
                Group{
                    Text("Get started today!").modifier(CustomTextM(fontName: UIFont.Rubik.medium.fontName, fontSize: 26, fontColor: Color.init(hex: "2E2E2E")))
                    
                    Group{
                        Button(action: {
                            print("")
                            self.tabSelection = 1
                        }){
                            HStack() {
                                Text(Localizable.Find_a_ride.localized()).padding(.leading,30)
                                Spacer()
                                Image("next").padding(.trailing,20)
                            }
                        }
                        
                        .modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 16, fontColor: Color.black))
                        .modifier(ButtonStyle(buttonHeight: 50, buttonColor: Color.white, buttonRadius: 10))
                        
                        .overlay(RoundedRectangle(cornerRadius: 50).stroke(Color.init(hex:"D0D0D0"), lineWidth: 1))
                        .padding(.horizontal)
                        .contentShape(Rectangle())
                        .onTapGesture {
                            self.tabSelection = 1
                        }
                        
                        Button(action: {
                            print("")
                            self.tabSelection = 2
                        }){
                            HStack() {
                                Text(Localizable.Offer_a_ride.localized()).padding(.leading,30)
                                Spacer()
                                Image("next").padding(.trailing,20)
                            }
                        }
                        
                        .modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 16, fontColor: Color.black))
                        
                        .modifier(ButtonStyle(buttonHeight: 50, buttonColor: Color.white, buttonRadius: 10))
                        .overlay(RoundedRectangle(cornerRadius: 50).stroke(Color.init(hex:"D0D0D0"), lineWidth: 1))
                        .padding(.horizontal)
                        .contentShape(Rectangle())
                        .onTapGesture {
                            self.tabSelection = 2
                        }
                        
                        Button(action: {
                            print("")
                            self.tabSelection = 3
                        }){
                            HStack() {
                                Text(Localizable.Tourist_Package.localized()).padding(.leading,30)
                                Spacer()
                                Image("next").padding(.trailing,20)
                            }
                        }
                        
                        .modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 16, fontColor: Color.black))
                        .modifier(ButtonStyle(buttonHeight: 50, buttonColor: Color.white, buttonRadius: 10))
                        
                        .overlay(RoundedRectangle(cornerRadius: 50).stroke(Color.init(hex:"D0D0D0"), lineWidth: 1))
                        .padding(.horizontal)
                        .contentShape(Rectangle())
                        .onTapGesture {
                            self.tabSelection = 3
                        }
                        
                    }
                }.padding(.horizontal)
                
                VStack(alignment:.center,spacing:0){
                    Group{
                        Text(Localizable.Where_do_you_want_to_go.localized()).modifier(CustomTextM(fontName: UIFont.Rubik.medium.fontName, fontSize: 26, fontColor: Color.init(hex: "2E2E2E"))).padding(.top,30)
                        
                        HStack{
                            Spacer()
                            //                        See our most popular rides
                            
                            NavigationLink(destination: MostPopularRidesListView(arrTrending:self.arrTrending), tag: 4, selection: $selection) {
                                
                                Button(action: {
                                    selection = 4
                                }){
                                    HStack() {
                                        Spacer()
                                        Text("See our most popular rides").padding(.trailing,25)
                                    }
                                }
                                .modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 16, fontColor: Color.init(hex:"00AEEF")))
                                .modifier(ButtonStyle(buttonHeight: 40, buttonColor: Color.clear, buttonRadius: 10))
                            }
                        }
                        ScrollView(.horizontal){
                            HStack{
                                ForEach(0..<(self.arrTrending.count), id: \.self) { row in
                                    MostPopularRideView(rides: self.arrTrending[row])
                                }
                            }
                        }.padding(.bottom,30)
                        
                        Text("Go literally anywhere from anywhere").padding(.trailing,30).modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 16, fontColor: Color.init(hex:"00AEEF")))
                        Text("3 things you’ll love about Goby24").padding([.trailing,.bottom],30).modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 16, fontColor: Color.init(hex:"00AEEF")))
                        
                        
                    }// #F5F5F5
                    
                }.background(Color.init(hex:"F5F5F5"))
                
            }
        }.showNavigationBarStyle().onAppear(){
            UIApplication.shared.endEditing()

            if (appDelegate?.selectedTab == 0){
                DispatchQueue.main.async {
                    self.getAllTrending()
                    self.getSliderImages()
                }
            }
        }
    }
}

//struct HomeView_Previews: PreviewProvider {
//    static var previews: some View {
//        HomeView(tabSelection: .constant(0) )
//            .previewLayout(.sizeThatFits)
//    }
//}
