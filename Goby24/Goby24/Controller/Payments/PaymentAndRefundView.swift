//
//  PaymentAndRefundView.swift
//  Goby24
//
//  Created by Jignesh Bhensadadiya on 7/15/21.
//

import SwiftUI

struct PaymentAndRefundView: View {
    @State var refundClicked: Bool = false
    
    var body: some View {
        VStack{
            HStack{
                //Button
                Button(action: {
                    refundClicked = false
                }){
                    if(refundClicked == false){
                        VStack{
                            Text("Payments").modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 13, fontColor: Color.init(hex: "00AEEF")))
                            Divider().background(Color.init(hex: "00AEEF")).frame(height: 1.5)
                        }
                    }else{
                        VStack{
                            Text("Refunds").modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 13, fontColor: Color.init(hex: "2E2E2E")))
                            Divider().background(Color.init(hex: "2E2E2E")).frame(height: 1.5)
                            
                        }
                    }
                }.frame(minWidth: 0, maxWidth: .infinity)
                
                
                //ACCOUNT Button
                Button(action: {
                    refundClicked = true
                }){
                    if(refundClicked == true){
                        VStack{
                            Text("Payments").modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 13, fontColor: Color.init(hex: "00AEEF")))
                            Divider().background(Color.init(hex: "00AEEF")).frame( height: 1.5)
                        }
                    }else{
                        VStack{
                            Text("Refunds").modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 13, fontColor: Color.init(hex: "2E2E2E")))
                            Divider().background(Color.init(hex: "2E2E2E")).frame( height: 1.5)
                            
                        }
                    }
                }.frame(minWidth: 0, maxWidth: .infinity)
            }.padding()
            
            
            if(refundClicked == false){
                PaymentView()
            }else{
                RefundView()
            }
            Spacer()
        }
        
    }
}

struct PaymentAndRefundView_Previews: PreviewProvider {
    static var previews: some View {
        PaymentAndRefundView()
    }
}
