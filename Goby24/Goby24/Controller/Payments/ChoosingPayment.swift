//
//  ChoosingPayment.swift
//  Goby24
//
//  Created by Jignesh Bhensadadiya on 7/15/21.
//

import SwiftUI

struct ChoosingPayment: View {
    var body: some View {
        ZStack{
            Color.init(hex: "00A761").ignoresSafeArea()
            VStack{
                Spacer()
                Image("confirm")
                Text("Congratulation your refund is completed").modifier(CustomTextM(fontName: UIFont.Rubik.medium.fontName, fontSize: 26, fontColor: Color.white)).multilineTextAlignment(.center).padding()
                Spacer()
                //Button
                Button(action: {
                    print("Continue with Google")
                    
                }){
                    HStack{
                        Text("Got It")
                    }
                }
                
                .modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 16, fontColor: Color.black))
                .modifier(ButtonStyle(buttonHeight: 50, buttonColor: Color.white, buttonRadius: 50))
                .overlay(RoundedRectangle(cornerRadius: 10).stroke(Color.gray, lineWidth: 1))
                .padding(.horizontal,30).padding(.bottom,60)
            }
        }
    }
}

struct ChoosingPayment_Previews: PreviewProvider {
    static var previews: some View {
        ChoosingPayment()
    }
}
