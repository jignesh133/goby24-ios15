//
//  PaymentRefundMethodsView.swift
//  Goby24
//
//  Created by Jignesh Bhensadadiya on 7/15/21.
//

import SwiftUI

struct PaymentRefundMethodsView: View {
    
    @State var isExpanded: Bool = true
    var body: some View {
        
        VStack{
            Text("Refund payment method").padding(.top, 20).modifier(CustomTextM(fontName: UIFont.Rubik.medium.fontName, fontSize: 26, fontColor: Color.init(hex: "2E2E2E"))).multilineTextAlignment(.center).padding(.horizontal,10)

            DisclosureGroup(isExpanded: $isExpanded) {
                Group{
                    Divider().background(Color.init(hex: "707070")).padding(.top,10)
                    HStack{
                        Image("bKash")
                        Text("bKash").modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 15, fontColor: Color.init(hex: "2E2E2E")))
                        Spacer()
                    }.padding(.leading,25)
                }
                Group{
                    Divider().background(Color.init(hex: "707070"))
                    HStack{
                        Image("mpesa")
                        Text("Mpesa").modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 15, fontColor: Color.init(hex: "2E2E2E")))
                        Spacer()
                    }.padding(.leading,25)
                }
                
                Group{
                    Divider().background(Color.init(hex: "707070"))
                    HStack{
                        Image("creditcad")
                        Text("Credit card").modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 15, fontColor: Color.init(hex: "2E2E2E")))
                        Spacer()
                    }.padding(.leading,25)
                }
                Group{
                    Divider().background(Color.init(hex: "707070"))
                    HStack{
                        Image("paypal")
                        Text("Paypal").modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 15, fontColor: Color.init(hex: "2E2E2E")))
                        Spacer()
                    }.padding(.leading,25)
                }
                
            } label: {
                HStack{
                    Spacer()
                    Text("Add Payment")
                    Spacer()
                }
                .onTapGesture {
                    withAnimation{
                        isExpanded.toggle()
                    }
                }
            }
            .accentColor(Color.init(hex: "2E2E2E"))
            .padding().frame(width: 300).background(Color.init(hex: "F5F5F5")).cornerRadius(24).accentColor(.black).modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 15, fontColor: Color.init(hex: "2E2E2E"))).multilineTextAlignment(.center)
            .padding(.top,75)
            Spacer()
           
        }.padding(.top,30)
    }
}

struct PaymentRefundMethodsView_Previews: PreviewProvider {
    static var previews: some View {
        PaymentRefundMethodsView()
    }
}
