//
//  CompletedTransactionView.swift
//  Goby24
//
//  Created by Jignesh Bhensadadiya on 9/26/21.
//

import SwiftUI

struct CompletedTransactionView: View {
    @State var isViewPresented: Bool = false
    @State var selection  = 0

    
    var body: some View {
        VStack{
            HStack{
                Spacer()
                Text(Localizable.Completed_transaction.localized()).modifier(CustomTextM(fontName: UIFont.Rubik.medium.fontName, fontSize: 26, fontColor: Color.init(hex: "2E2E2E")))

                Spacer()

                
            }.padding(.horizontal,30).padding(.vertical)
            
            HStack{
                //Button
                Button(action: {
                    selection = 0
                }){
                    if(selection == 0){
                        VStack{
                            Spacer()
                            Text("Basic Ride").modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 13, fontColor: Color.init(hex: "00AEEF")))
                            Divider().background(Color.init(hex: "00AEEF")).frame(height: 1.5)
                        }.frame(height:70)
                    }else{
                        VStack{
                            Spacer()
                            Text("Basic Ride").modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 13, fontColor: Color.init(hex: "2E2E2E")))
                            Divider().background(Color.init(hex: "2E2E2E")).frame(height: 1.5)
                            
                        }.frame(height:70)
                    }
                }.frame(minWidth: 0, maxWidth: .infinity)

               
                //ACCOUNT Button
                Button(action: {
                    selection = 1
                }){
                    if(selection == 1){
                        VStack{
                            Spacer()

                            Text("Ride Request").modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 13, fontColor: Color.init(hex: "00AEEF")))
                            Divider().background(Color.init(hex: "00AEEF")).frame( height: 1.5)
                        }.frame(height:70)
                    }else{
                        VStack{
                            Spacer()

                            Text("Ride Request").modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 13, fontColor: Color.init(hex: "2E2E2E")))
                            Divider().background(Color.init(hex: "2E2E2E")).frame( height: 1.5)
                            
                        }.frame(height:70)
                    }
                }.frame(minWidth: 0, maxWidth: .infinity)
                
                //Button
                Button(action: {
                    selection = 2
                }){
                    if(selection == 2){
                        VStack{
                            Spacer()

                            Text("Tourist Package").modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 13, fontColor: Color.init(hex: "00AEEF")))
                            Divider().background(Color.init(hex: "00AEEF")).frame(height: 1.5)
                        }.frame(height:70)
                    }else{
                        VStack{
                            Spacer()

                            Text("Tourist Package").modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 13, fontColor: Color.init(hex: "2E2E2E")))
                            Divider().background(Color.init(hex: "2E2E2E")).frame(height: 1.5)
                            
                        }.frame(height:70)
                    }
                }.frame(minWidth: 0, maxWidth: .infinity)

                //Button
                Button(action: {
                    selection = 3
                }){
                    if(selection == 3){
                        VStack{
                            Spacer()

                            Text("Tourist Package Request").modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 13, fontColor: Color.init(hex: "00AEEF")))
                            Divider().background(Color.init(hex: "00AEEF")).frame(height: 1.5)
                        }.frame(height:70)
                    }else{
                        VStack{
                            Spacer()

                            Text("Tourist Package Request").modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 13, fontColor: Color.init(hex: "2E2E2E")))
                            Divider().background(Color.init(hex: "2E2E2E")).frame(height: 1.5)
                            
                        }.frame(height:70)
                    }
                }.frame(minWidth: 0, maxWidth: .infinity)

            }.padding()

            if (selection == 0){
                BasicRideTransactionHistoryView()
            }else if (selection == 1){
                RideRequestTransactionHistoryView()
            }else if (selection == 2){
                TouristPackageTransactionHistoryView()
            }else if (selection == 3){
                TouristPackageRequestTransactionHistoryView()
            }
            Spacer()
        }.showNavigationBarWithBackStyle()
    }
}

struct CompletedTransactionView_Previews: PreviewProvider {
    static var previews: some View {
        CompletedTransactionView()
    }
}
