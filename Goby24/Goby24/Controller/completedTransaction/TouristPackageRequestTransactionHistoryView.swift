//
//  BasicRideTransactionHistoryView.swift
//  Goby24
//
//  Created by Jignesh Bhensadadiya on 9/24/21.
//

import SwiftUI

struct TouristPackageRequestTransactionHistoryView: View {
    @State var arrHistory:[Model_Tourist_Package_Request_CompletedTransaction] =  [Model_Tourist_Package_Request_CompletedTransaction]()
    @Environment(\.viewController) private var viewControllerHolder: UIViewController?
    @State var showingSheet:Bool = false

    func getHistory(isHideLoader:Bool = false){
        
        let param:[String:Any] = [String:Any]()
        print(param)
        WebAccess.getDataWith(_Url: WebAccess.TOURIST_PACKAGE_REQUEST_TRANSACTION_HISTORY, _parameters: param,isHideLoader: isHideLoader) { (result) in

            switch (result){
            case .Success(let _data):
                print(_data)
                do{
                    let result:[String:Any] = UtilsObjs.cleanJson(to: _data ) as? [String : Any] ?? [String:Any]()
                    print(result)
                    let data1 =  try JSONSerialization.data(withJSONObject: result["result"] ?? [] , options: JSONSerialization.WritingOptions.prettyPrinted)
                    let convertedString = String(data: data1, encoding: String.Encoding.utf8)
                    let jsonData = Data((convertedString?.utf8)!)
                    self.arrHistory = try JSONDecoder().decode([Model_Tourist_Package_Request_CompletedTransaction].self, from: jsonData)
                    NSLog("LOG")
                    
                }catch(let error){
                    print(error)
                }

                break

            case .Error(let msg):
                ShowAlert(title: APPNAME, msg: msg, view: UIApplication.getTopViewController()!)
                break
            }
            
        }
    }
    func acceptTouristRequestList(tid:String){
        
        let param:[String:Any] = [String:Any]()
        print(param)
        WebAccess.putDataWith(_Url: (WebAccess.TOURIST_SPOT_REQUEST_ACCEPT + tid), _parameters: param) { (result) in

            switch (result){
            case .Success(let _data):
                print(_data)
                
                break

            case .Error(let msg):
                ShowAlert(title: APPNAME, msg: msg, view: UIApplication.getTopViewController()!)
                break
            }
            
        }
    }
    func deleteTouristRequestList(tid:String){
        
        let param:[String:Any] = [String:Any]()
        print(param)
        WebAccess.deleteDataWith(_Url: (WebAccess.TOURIST_SPOT_REQUEST + tid + "/"), _parameters: param) { (result) in

            switch (result){
            case .Success(let _data):
                print(_data)
                ShowAlert(title: APPNAME, msg: "Request deleted sucessfully ", view: UIApplication.getTopViewController()!)
//                self.arrTouristRequestList =  self.arrTouristRequestList.filter({"\($0.id ?? 0)" != tid})
//                self.getTouristRequestList(isHideLoader: true)
                break
                
            case .Error(let msg):
                ShowAlert(title: APPNAME, msg: msg, view: UIApplication.getTopViewController()!)
                break
            }
            
        }
    }
    var body: some View {
        VStack{
            NavigationLink(destination: CreateTouristRequestUpdateView(),isActive: $showingSheet) {EmptyView()}.isDetailLink(false)

            ScrollView{
                ForEach(0..<$arrHistory.wrappedValue.count,id: \.self) { row in
                    TouristPackageRequestTransactionHistoryItemView(history: arrHistory[row]) { (tid) in
                        print("DELETE")
                        //self.deleteTouristRequestList(tid: tid)
                    } onAcceptRequest: { (tid) in
                        print("ACCEPT")
                      //  self.acceptTouristRequestList(tid: tid)
                    } onEditRequest: { (obj) in
                        print("EDIT")
//                        self.touristRequestList = obj
//                        showingSheet = true
                    } onContactRequest: { (tid) in
                        print("CONTACT")
                    
                    }

                }
            }
        }.onAppear(){
            self.getHistory()
        }
    }
    
}

struct TouristPackageRequestTransactionHistoryView_Previews: PreviewProvider {
    static var previews: some View {
        TouristPackageTransactionHistoryView()
    }
}
struct TouristPackageRequestTransactionHistoryItemView: View {
    @State var history:Model_Tourist_Package_Request_CompletedTransaction?
    @State private var isOpen:Bool = false
    
    let onDeleteRequest: (String) -> Void
    let onAcceptRequest: (String) -> Void
    let onEditRequest: (Model_TouristRequestList) -> Void
    let onContactRequest: (String) -> Void

    var body: some View {
        VStack(alignment:.leading){
            
            HStack{  // FOR TOP HEADER
                
                VStack(alignment:.leading){  // FOR BUTTON
                    HStack{
                        Button(action: {
                            withAnimation {
                                isOpen.toggle()
                            }
                        }) {
                            Image("roundDown")
                        }
                    }.frame(width: 40, height: 40 ,alignment: .top)
                    Spacer()
                }.frame(maxWidth: 50,maxHeight: 40).fixedSize(horizontal: false, vertical: false)
                
                
                VStack{
                    VStack(alignment:.leading){
                        Text(Localizable.Date.localized()).modifier(CustomTextM(fontName: UIFont.Montserrat.semiBold.fontName, fontSize: 12, fontColor: Color.init(hex: "00AEEF")))
                        Text(history?.tourist_package_request?.journeyStartDate ?? "").modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 12, fontColor: Color.init(hex: "000000")))
                        Divider()
                        
                    }.frame(maxWidth: .infinity)
                }
                VStack{
                    VStack(alignment:.leading){
                        Text("Transaction ID").modifier(CustomTextM(fontName: UIFont.Montserrat.semiBold.fontName, fontSize: 12, fontColor: Color.init(hex: "00AEEF")))
                        Text(history?.transactionId ?? "").modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 12, fontColor: Color.init(hex: "000000")))

                        Divider()
                    }.frame(maxWidth: .infinity)
                }
            }
            if (isOpen == true){  // FOR OPEN SECTION
                HStack{
                    VStack(alignment:.leading){  // FOR BUTTON
                        Spacer()
                    }.frame(maxWidth: 50,maxHeight: 40).fixedSize(horizontal: false, vertical: false)
                    
                    VStack{

                        Group{
                            
                            Group{
                                HStack{
                                    HStack{
                                        Text("Payment Method").modifier(CustomTextM(fontName: UIFont.Montserrat.semiBold.fontName, fontSize: 12, fontColor: Color.init(hex: "00AEEF")))
                                        Spacer()
                                    }
                                    HStack{
                                        Text(history?.paymentMethod ?? "").modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 12, fontColor: Color.init(hex: "000000")))
                                        
                                        Spacer()
                                    }
                                }
                                Spacer(minLength: 5)

                            }
                            Spacer(minLength: 5)

                            Group{
                                HStack{
                                    HStack{
                                        Text("Location").modifier(CustomTextM(fontName: UIFont.Montserrat.semiBold.fontName, fontSize: 12, fontColor: Color.init(hex: "00AEEF")))
                                        Spacer()
                                    }
                                    HStack{
                                        Image("orangechain").padding([.leading],0)
//                                        Text(history?.package_name ?? "").modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 12, fontColor: Color.init(hex: "000000")))

                                        VStack{
                                            Text(history?.tourist_package_request?.journeyFrom?.removeWhiteSpace() ?? "").modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 12, fontColor: Color.init(hex: "000000")))
                                            Text(history?.tourist_package_request?.journeyTo?.removeWhiteSpace() ?? "").modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 12, fontColor: Color.init(hex: "000000")))

                                        }
                                        Spacer()
                                    }
                                }
                                Spacer(minLength: 5)

                            }
                            Spacer(minLength: 5)

                            Group{
                                HStack{
                                    HStack{
                                        Text("Ratings").modifier(CustomTextM(fontName: UIFont.Montserrat.semiBold.fontName, fontSize: 12, fontColor: Color.init(hex: "00AEEF")))
                                        Spacer()
                                    }
                                    HStack{
                                        Text(history?.rider?.riderRating ?? "").modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 12, fontColor: Color.init(hex: "000000")))
                                        
                                        Spacer()
                                    }
                                }
                                Spacer(minLength: 5)

                            }
                            Spacer(minLength: 5)

                            Group{
                                HStack{
                                    HStack{
                                        Text("Amount").modifier(CustomTextM(fontName: UIFont.Montserrat.semiBold.fontName, fontSize: 12, fontColor: Color.init(hex: "00AEEF")))
                                        Spacer()
                                    }

                                    HStack{
                                        Text(history?.amountPaid ?? "").modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 12, fontColor: Color.init(hex: "000000")))
                                        Text(history?.currency ?? "").modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 12, fontColor: Color.init(hex: "000000")))

                                        Spacer()
                                    }
                                }
                                Spacer(minLength: 5)
                            }
                            
                            Spacer(minLength: 5)
                            Group{
                                HStack{
                                    HStack{
                                        Text("Rider Name").modifier(CustomTextM(fontName: UIFont.Montserrat.semiBold.fontName, fontSize: 12, fontColor: Color.init(hex: "00AEEF")))
                                        Spacer()
                                    }
                                    HStack{
                                        Text((history?.rider?.firstName ?? "") ).modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 12, fontColor: Color.init(hex: "000000")))
                                        
                                        Spacer()
                                    }
                                }
                                Spacer(minLength: 5)

                            }
//                            Spacer(minLength: 5)
                            
                            Group{
                                HStack{
                                    HStack{
                                        Text("Paymet Status").modifier(CustomTextM(fontName: UIFont.Montserrat.semiBold.fontName, fontSize: 12, fontColor: Color.init(hex: "00AEEF")))
                                        Spacer()
                                    }
                                    HStack{
                                        Text((history?.paymentStatus ?? "") ).modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 12, fontColor: Color.init(hex: "000000")))
                                        
                                        Spacer()
                                    }
                                }
                                Spacer(minLength: 5)

                            }
                            
//                            Group{
//                                HStack{
//                                    HStack{
//                                        Text("Paymet Status").modifier(CustomTextM(fontName: UIFont.Montserrat.semiBold.fontName, fontSize: 12, fontColor: Color.init(hex: "00AEEF")))
//                                        Spacer()
//                                    }
//                                    HStack{
//                                        Text((history?.paymentStatus ?? "") ).modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 12, fontColor: Color.init(hex: "000000")))
//
//                                        Spacer()
//                                    }
//                                }
//                                Spacer(minLength: 5)
//
//                            }
                        }
                        
                    }
                }
            }
        }
    }
}

