
import Foundation
import SwiftUI

struct NotificationView: View {
    @State var arrNotification:[Model_Notification] = [Model_Notification]()

    func getNotification(){
        
        WebAccess.getDataWith(_Url: WebAccess.NOTIFICATION, _parameters: [:]) { (result) in
            switch (result){
            case .Success(let _data):
                print(_data)
                do{
                    let result:[String:Any] = UtilsObjs.cleanJson(to: _data ) as? [String : Any] ?? [String:Any]()
                    print(result)
                    let data1 =  try JSONSerialization.data(withJSONObject: result["result"] ?? [] , options: JSONSerialization.WritingOptions.prettyPrinted)
                    let convertedString = String(data: data1, encoding: String.Encoding.utf8)
                    let jsonData = Data((convertedString?.utf8)!)
                    self.arrNotification = try JSONDecoder().decode([Model_Notification].self, from: jsonData)
                    NSLog("LOG")
                }catch(let error){
                    print(error)
                }
                break
            case .Error(let msg):
                ShowAlert(title: APPNAME, msg: msg, view: UIApplication.getTopViewController()!)
                break
            }
        }
    }
    var body: some View {
        
        ScrollView(.vertical){
            VStack{
                ForEach(0..<(self.arrNotification.count), id: \.self) { row in
                    NotificatonItemView(notification: self.arrNotification[row])
                }
            }
        }.showNavigationBarWithBackStyle().padding(.bottom,30)
        .onAppear{
            self.getNotification()
        }
    }
}

struct NotificationView_Previews: PreviewProvider {
    static var previews: some View {
        NotificationView()
    }
}

struct Model_Notification : Codable {

        let body : String?
        let id : Int?
        let timestamp : String?
        let title : String?
        let user : Int?

        enum CodingKeys: String, CodingKey {
                case body = "body"
                case id = "id"
                case timestamp = "timestamp"
                case title = "title"
                case user = "user"
        }
    
        init(from decoder: Decoder) throws {
                let values = try decoder.container(keyedBy: CodingKeys.self)
                body = try values.decodeIfPresent(String.self, forKey: .body)
                id = try values.decodeIfPresent(Int.self, forKey: .id)
                timestamp = try values.decodeIfPresent(String.self, forKey: .timestamp)
                title = try values.decodeIfPresent(String.self, forKey: .title)
                user = try values.decodeIfPresent(Int.self, forKey: .user)
        }

}
struct NotificatonItemView: View {
    
    var notification:Model_Notification?
    
    var body: some View {
        
        VStack(alignment:.leading){
            
            HStack{
                Image("notifications").padding([.leading,.trailing])
                Text(notification?.title?.removeWhiteSpace() ?? "").modifier(CustomTextM(fontName:UIFont.Montserrat.semiBold.fontName, fontSize: 16, fontColor: Color.init(hex: "2E2E2E"))).padding([.top])
                Spacer()
                let strTime = notification?.timestamp?.stringToDate(strCurrentFormat: "yyyy-MM-dd'T'HH:mm:ss SSSZ").getFormattedDate(formatter: "h:m a")
                
                let strDate = notification?.timestamp?.stringToDate(strCurrentFormat: "yyyy-MM-dd'T'HH:mm:ss SSSZ").getFormattedDate(formatter: "dd MMM yyyy")
                VStack{
                    Text(strTime ?? "").modifier(CustomTextM(fontName: UIFont.Poppins.regular.fontName, fontSize: 16, fontColor: Color.init(hex: "2E2E2E"))).padding([.trailing,.top])
                    Text(strDate ?? "").modifier(CustomTextM(fontName: UIFont.Poppins.regular.fontName, fontSize: 16, fontColor: Color.init(hex: "2E2E2E"))).padding([.trailing])
                }
            }
            Text(notification?.body?.removeWhiteSpace() ?? "").modifier(CustomTextM(fontName:  UIFont.Poppins.regular.fontName, fontSize: 16, fontColor: Color.init(hex: "2E2E2E"))).padding()
        }.background(RoundedRectangle(cornerRadius: 25).fill(Color.white).shadow(color: Color.black.opacity(0.12), radius: 3)).padding()
    }
}

