//
//  AddNewSpotView.swift
//  Goby24
//
//  Created by Jignesh Bhensadadiya on 8/15/21.
//

import SwiftUI

struct BankRegistrationGuideView: View {
    //    @State var spotName: String = ""
    //    let onAddClicked: (String) -> Void
    @Environment(\.viewController) private var viewControllerHolder: UIViewController?
    
    var body: some View {
        
        VStack{
            ScrollView{
                HStack{
                    Spacer()
                    Button(action: {
                        self.viewControllerHolder?.dismiss(animated: true, completion: nil)
                    }) {
                        Image("cross")
                    }
                }
                HStack{
                    Text("Bank Account Registration Guide") .modifier(CustomTextM(fontName: UIFont.Montserrat.bold.fontName, fontSize: 22, fontColor: Color.init(hex: "2E2E2E")))
                    Spacer()
                }
                Spacer(minLength: 10)
                HStack{
                    Text("Bank Name") .modifier(CustomTextM(fontName: UIFont.Montserrat.bold.fontName, fontSize: 22, fontColor: Color.init(hex: "2E2E2E")))
                    Spacer()
                }
                Spacer(minLength: 10)
                Text("This field should include the full name of your bank in English letters. \nAccount Holder Name \nThis field should include the full official name under which your bank account is registered, exactly as it appears on your bank statement. The account name should contain English letters and numbers only, no symbols. In case the account is not under your name or is a company account, please note that you might be asked to provide further information or documents in the future. \nAccount Number \nThis field should include your full account number as it appears on your bank statement or check. \nSWIFT / BIC \nThis field should include your bank SWIFT/BIC code, using capital letters only. Must be 8 or 11 characters. We advise that you verify the exact SWIFT code in the following link: http://www.swift.com/bsl/facelets/bicsearch.faces \nBank Code \nThis field should include your bank code and branch code together. It should contain 5 digits.").modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 13, fontColor: Color.init(hex: "2E2E2E")))
            }
        }.padding().background(Color.white).cornerRadius(10).frame(width: max(UIScreen.main.bounds.size.width-40, 300), height: min(UIScreen.main.bounds.size.height-40, 650), alignment: .center)
    }
}

struct BankRegistrationGuideView_Previews: PreviewProvider {
    static var previews: some View {
        Group {
//            AddNewSpotView { (item) in
//
//            }
        }
    }
}
