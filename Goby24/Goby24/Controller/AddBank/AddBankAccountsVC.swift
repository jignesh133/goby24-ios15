//
//  AddBankAccountsVC.swift
//  Goby24
//
//  Created by Jignesh Bhensadadiya on 9/18/21.
//

import SwiftUI

struct AddBankAccountsVC: View {
    @State var personalSelected: Bool = true
    
    @State var agreeTerms: Bool = false
    @State var confirmBankDetails: Bool = false
    
    
    @State var accountNumber:String = ""
    
    @State var country:String?
    @State var currency:String?
    @State var bankName:String?
    @State var bankBranch:String?
    
    @State var bankCode:String?
    @State var accHolName:String?
    @State var accNo:String?
    @Environment(\.viewController) private var viewControllerHolder: UIViewController?
    
    
    var body: some View {
        ScrollView{
            
            Text("Add Bank Account") .modifier(CustomTextM(fontName: UIFont.Rubik.medium.fontName, fontSize: 26, fontColor: Color.init(hex: "2E2E2E")))
            
            VStack{
                VStack{
                    Group{
                        HStack{
                            Text("Enter Bank Details") .modifier(CustomTextM(fontName: UIFont.Montserrat.semiBold.fontName, fontSize: 16, fontColor: Color.init(hex: "2E2E2E")))
                            Spacer()
                            
                            
                            Button(action: {
                                self.viewControllerHolder?.present(style: .overCurrentContext, transitionStyle: .crossDissolve) {
                                    BankRegistrationGuideView()
                                }
                            }){
                                HStack{
                                    Text("Need help to fill the details?").padding(.trailing,10)
                                }
                            }.frame(height:45)
                            .modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 13, fontColor: Color.init(hex: "00AEEF")))
                            .onTapGesture {
                                self.viewControllerHolder?.present(style: .overCurrentContext, transitionStyle: .crossDissolve) {
                                    BankRegistrationGuideView()
                                }
                            }
                            
                        }
                        Spacer(minLength: 8)
                        
                        HStack{
                            VStack(alignment: .leading, spacing: 0){
                                Text("Country").modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 13, fontColor: Color.init(hex: "00AEEF"))).padding(.leading,8)
                                
                                ZStack{
                                    
                                    Button(action: {
                                        RPicker.selectOption(title: "Please select Country", cancelText: "Cancel", doneText: "Done", dataArray: ["A","B"], selectedIndex: 0) { (val, index) in
                                        }
                                    }){
                                        HStack() {
                                            Text(country ?? "Choose").padding(.horizontal)
                                            Spacer()
                                            Image("down").padding(.trailing,10)
                                        }
                                    }.frame(height:45)
                                    .modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 13, fontColor: Color.init(hex: "C4C4C4")))
                                    .modifier(ButtonStyle(buttonHeight: 50, buttonColor: Color.init(hex: "F2F2F2"), buttonRadius: 50))
                                }
                                
                                
                            }
                            VStack(alignment: .leading, spacing: 0){
                                Text("Currency").modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 13, fontColor: Color.init(hex: "00AEEF"))).padding(.leading,8)
                                
                                ZStack{
                                    
                                    Button(action: {
                                        RPicker.selectOption(title: "Please select Currency", cancelText: "Cancel", doneText: "Done", dataArray: ["A","B"], selectedIndex: 0) { (val, index) in
                                        }
                                        
                                    }){
                                        HStack() {
                                            Text(currency ?? "Choose").padding(.horizontal)
                                            Spacer()
                                            Image("down").padding(.trailing,10)
                                        }
                                    }.frame(height:45)
                                    .modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 13, fontColor: Color.init(hex: "C4C4C4")))
                                    .modifier(ButtonStyle(buttonHeight: 50, buttonColor: Color.init(hex: "F2F2F2"), buttonRadius: 50))
                                }
                                
                                
                            }
                        }
                    }
                    
                    HStack{
                        Spacer()
                        
                        //Button
                        Button(action: {
                            personalSelected = true
                        }){
                            if (personalSelected == true){
                                Image(systemName: "largecircle.fill.circle").frame(width: 15, height: 15).scaledToFit()
                                Text("Business").modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 13, fontColor: Color.init(hex: "00AEEF")))
                            }else{
                                Image(systemName: "circle").frame(width: 15, height: 15).scaledToFit()
                                Text("Personal").modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 13, fontColor: Color.init(hex: "00AEEF")))
                            }
                        }
                        Spacer()
                        //Button
                        Button(action: {
                            personalSelected = false
                        }){
                            if (personalSelected == true){
                                Image(systemName: "circle").frame(width: 15, height: 15).scaledToFit()
                                Text("Personal").modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 13, fontColor: Color.init(hex: "00AEEF")))
                            }else{
                                Image(systemName: "largecircle.fill.circle").frame(width: 15, height: 15).scaledToFit()
                                Text("Business").modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 13, fontColor: Color.init(hex: "00AEEF")))
                            }
                        }
                        Spacer()
                        
                    }.padding()
                    
                    HStack{
                        Text("Account Details") .modifier(CustomTextM(fontName: UIFont.Montserrat.semiBold.fontName, fontSize: 16, fontColor: Color.init(hex: "2E2E2E")))
                        Spacer()
                    }
                    Spacer(minLength: 8)
                    
                    Group{
                        HStack{
                            VStack(alignment: .leading, spacing: 0){
                                Text("Bank Name").modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 13, fontColor: Color.init(hex: "00AEEF"))).padding(.leading,8)
                                
                                ZStack{
                                    
                                    Button(action: {
                                        
                                    }){
                                        HStack() {
                                            Text(bankName ?? "Choose").padding(.horizontal)
                                            Spacer()
                                            Image("down").padding(.trailing,10)
                                        }
                                    }.frame(height:45)
                                    .modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 13, fontColor: Color.init(hex: "C4C4C4")))
                                    .modifier(ButtonStyle(buttonHeight: 50, buttonColor: Color.init(hex: "F2F2F2"), buttonRadius: 50))
                                }
                                
                                
                            }
                            VStack(alignment: .leading, spacing: 0){
                                Text("Bank Branch").modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 13, fontColor: Color.init(hex: "00AEEF"))).padding(.leading,8)
                                
                                ZStack{
                                    
                                    Button(action: {
                                        
                                    }){
                                        HStack() {
                                            Text(bankBranch ?? "Choose").padding(.horizontal)
                                            Spacer()
                                            Image("down").padding(.trailing,10)
                                        }
                                    }.frame(height:45)
                                    .modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 13, fontColor: Color.init(hex: "C4C4C4")))
                                    .modifier(ButtonStyle(buttonHeight: 50, buttonColor: Color.init(hex: "F2F2F2"), buttonRadius: 50))
                                }
                                
                                
                            }
                        }
                    }
                    Group{
                        HStack{
                            VStack(alignment: .leading, spacing: 0){
                                Text("Bank Code").modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 13, fontColor: Color.init(hex: "00AEEF"))).padding(.leading,8)
                                
                                CustomTextField(placeHolderText: (Localizable.Account_holder_name.localized()), text: self.$accountNumber, isPasswordType: false, keyboardType: .emailAddress,fontSize: 14)
                                
                            }
                            
                            //                            VStack(alignment: .leading, spacing: 0){
                            //                                Text("Bank Code").modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 13, fontColor: Color.init(hex: "00AEEF"))).padding(.leading,8)
                            //
                            //                                ZStack{
                            //
                            //                                    Button(action: {
                            //
                            //                                    }){
                            //                                        Text(bankCode ?? "Choose").padding(.horizontal)
                            //
                            //                                        HStack() {
                            //                                            Spacer()
                            //                                            Image("down").padding(.trailing,10)
                            //                                        }
                            //                                    }.frame(height:45)
                            //                                    .modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 13, fontColor: Color.init(hex: "C4C4C4")))
                            //                                    .modifier(ButtonStyle(buttonHeight: 50, buttonColor: Color.init(hex: "F2F2F2"), buttonRadius: 50))
                            //                                }
                            //
                            //
                            //                            }
                            VStack(alignment: .leading, spacing: 0){
                                Text("Acc. Holder name").modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 13, fontColor: Color.init(hex: "00AEEF"))).padding(.leading,8)
                                
                                CustomTextField(placeHolderText: (Localizable.Account_holder_name.localized()), text: self.$accountNumber, isPasswordType: false, keyboardType: .emailAddress,fontSize: 14)
                                
                            }
                        }
                    }
                    Group{
                        HStack{
                            
                            VStack(alignment: .leading, spacing: 0){
                                Text("Account Number").modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 13, fontColor: Color.init(hex: "00AEEF"))).padding(.leading,8)
                                
                                CustomTextField(placeHolderText: (Localizable.Account_Number.localized()), text: self.$accountNumber, isPasswordType: false, keyboardType: .emailAddress,fontSize: 14)
                                
                                
                                
                            }
                        }
                    }
                    
                    HStack{
                        Button(action: {
                            agreeTerms.toggle() // = true
                        }){
                            if (agreeTerms == true){
                                Image(systemName: "largecircle.fill.circle").frame(width: 15, height: 15).scaledToFit()
                                Text("I agree to the terms and conditions").modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 13, fontColor: Color.init(hex: "00AEEF")))
                            }else{
                                Image(systemName: "circle").frame(width: 15, height: 15).scaledToFit()
                                Text("I agree to the terms and conditions").modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 13, fontColor: Color.init(hex: "00AEEF")))
                            }
                        }
                        Spacer()
                    }.padding()
                    HStack{
                        Button(action: {
                            confirmBankDetails.toggle() // = true
                        }){
                            if (confirmBankDetails == true){
                                Image(systemName: "largecircle.fill.circle").frame(width: 15, height: 15).scaledToFit()
                                Text("I  confirm the bank details above").modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 13, fontColor: Color.init(hex: "00AEEF")))
                            }else{
                                Image(systemName: "circle").frame(width: 15, height: 15).scaledToFit()
                                Text("I  confirm the bank details above").modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 13, fontColor: Color.init(hex: "00AEEF")))
                            }
                        }
                        Spacer()
                        
                    }.padding()
                    
                    
                    VStack(alignment: .center, spacing: 25){
                        //Button
                        Button(action: {
                            
                        }){
                            Text("Confirm Goby24 account details")
                        }
                        .modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 16, fontColor: Color.white))
                        .modifier(ButtonStyle(buttonHeight: 50, buttonColor: Color.init(hex: "00ADEF"), buttonRadius: 10))
                    }.padding(.horizontal,30).padding(.vertical,30)
                    
                }.padding().background(Color.white).clipShape(RoundedRectangle(cornerRadius: 21, style: .continuous)).shadow(radius: 10).padding(.vertical)
            }.padding()
        }.showNavigationBarWithBackStyle()
    }//.resignKeyboardOnDragGesture()
    
}

struct AddBankAccountsVC_Previews: PreviewProvider {
    static var previews: some View {
        AddBankAccountsVC()
    }
}
