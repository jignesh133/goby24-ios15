//
//  AddBankVC.swift
//  Goby24
//
//  Created by Jignesh Bhensadadiya on 8/22/21.
//

import SwiftUI

struct BankAccountsVC: View {
    var body: some View {

        ScrollView{
            VStack{
                Text("Your Bank Account") .modifier(CustomTextM(fontName: UIFont.Rubik.medium.fontName, fontSize: 26, fontColor: Color.init(hex: "2E2E2E")))

                VStack{
                    Text("Balance") .modifier(CustomTextM(fontName: UIFont.Rubik.medium.fontName, fontSize: 26, fontColor: Color.init(hex: "2E2E2E"))).padding(.top)
                    Text("300 USD") .modifier(CustomTextM(fontName: UIFont.Rubik.medium.fontName, fontSize: 26, fontColor: Color.init(hex: "2E2E2E"))).padding(.top)
                    VStack(alignment: .center, spacing: 25){
                        //Button
                        Button(action: {
                            print(" BUTTON ACTION CLICKED")
                        }){
                            Text("Withdraw")
                        }
                        .modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 16, fontColor: Color.white))
                        .modifier(ButtonStyle(buttonHeight: 50, buttonColor: Color.init(hex: "00ADEF"), buttonRadius: 50))
                    }.padding(.horizontal,30).padding(.vertical,30)

                }.background(RoundedRectangle(cornerRadius: 25).fill(Color.white).shadow(color: Color.black.opacity(0.12), radius: 10)).padding()
                
                ScrollView{
                    Group{
                        VStack(alignment:.leading){
                            Text("The Co-Operative Bank of Kenya") .modifier(CustomTextM(fontName: UIFont.Poppins.bold.fontName, fontSize: 16, fontColor: Color.init(hex: "5C5C5C"))).padding(.horizontal).padding(.top)
                            HStack{
                                VStack(alignment:.leading){
                                    Text("Account Number") .modifier(CustomTextM(fontName: UIFont.Poppins.medium.fontName, fontSize: 13, fontColor: Color.init(hex: "5C5C5C"))).padding(.horizontal).padding(.top,5)
                                    
                                    Text("X - 233444444444444") .modifier(CustomTextM(fontName: UIFont.Poppins.medium.fontName, fontSize: 13, fontColor: Color.init(hex: "5C5C5C"))).padding(.horizontal).padding(.top,5)
                                    
                                    Text("Approved") .modifier(CustomTextM(fontName: UIFont.Poppins.medium.fontName, fontSize: 13, fontColor: Color.init(hex: "00AEEF"))).padding(.horizontal).padding(.top,5).padding(.bottom)
                                }
                                VStack{
                                    HStack{
                                        Spacer()
                                        Image("banklogo")
                                        Spacer()
                                    }
                                }.frame(width: 100)
                            }
                            VStack{
                                Color.init(hex: "00ADEF").clipped()
                                HStack{
                                    Spacer()
                                    Button(action: {
                                        RPicker.selectOption(title: "Please select Currency", cancelText: "Cancel", doneText: "Done", dataArray: ["Pay","Delete Account"], selectedIndex: nil) { (val, index) in
                                        }
                                    }){
                                        Image("paydots")
                                    }.frame(width: 40).padding(.bottom,10)
                                    
                                }
                            }.frame(width: nil, height: 40).background(Color.init(hex: "00ADEF"))
                        }.background(RoundedRectangle(cornerRadius: 25).fill(Color.white).shadow(color: Color.black.opacity(0.12), radius: 10)).padding()

                    }
                }
                Group{
                    VStack(alignment:.leading){
                        Text("Add Bank Account") .modifier(CustomTextM(fontName: UIFont.Poppins.bold.fontName, fontSize: 16, fontColor: Color.init(hex: "5C5C5C"))).padding(.horizontal).padding(.top)
                       //AddBankAccountsVC
                        
                        NavigationLink(destination: AddBankAccountsVC()) {
                            HStack{
                                Spacer()
                                VStack{
                                    HStack{
                                        Spacer()
                                        Image("addBank")
                                        Spacer()
                                    }
                                }.frame(width: 100)
                            }
                        }
                        
                        
                        

                        
                        VStack{
                            Color.init(hex: "C4C4C4").clipped()
                            Text("you can add upto 2 accounts") .modifier(CustomTextM(fontName: UIFont.Poppins.medium.fontName, fontSize: 12, fontColor: Color.white)).padding()

                        }.frame(width: nil, height: 40).background(Color.init(hex: "C4C4C4"))
                    }.background(RoundedRectangle(cornerRadius: 25).fill(Color.white).shadow(color: Color.black.opacity(0.12), radius: 10)).padding()

                }
                Group{
                    VStack(alignment:.leading){
                        HStack{
                            VStack(alignment:.leading){
                                Text("Tranctions") .modifier(CustomTextM(fontName: UIFont.Poppins.bold.fontName, fontSize: 16, fontColor: Color.init(hex: "5C5C5C"))).padding(.horizontal).padding(.top)
                                Text("Updated every several minutes") .modifier(CustomTextM(fontName: UIFont.Poppins.medium.fontName, fontSize: 16, fontColor: Color.init(hex: "5C5C5C"))).padding(.horizontal)

                            }
                        }
                        
                        ScrollView{
                            ForEach(0..<5, id: \.self) { index in
                                Group{
                                    HStack{
                                        VStack{
                                            Text("25 May 2021") .modifier(CustomTextM(fontName: UIFont.Poppins.medium.fontName, fontSize: 12, fontColor: Color.init(hex: "5C5C5C")))

                                        }.frame(maxWidth:.infinity)
                                        VStack{
                                            Text("7:00 AM") .modifier(CustomTextM(fontName: UIFont.Poppins.medium.fontName, fontSize: 12, fontColor: Color.init(hex: "5C5C5C")))

                                        }.frame(maxWidth:.infinity)
                                        VStack{
                                            Text("100 USD") .modifier(CustomTextM(fontName: UIFont.Poppins.medium.fontName, fontSize: 12, fontColor: Color.init(hex: "5C5C5C")))

                                        }.frame(maxWidth:.infinity)
                                    }
                                }
                                Divider().padding(.horizontal)
                            }
                            

                        }
                        
                    }.background(RoundedRectangle(cornerRadius: 25).fill(Color.white).shadow(color: Color.black.opacity(0.12), radius: 10)).padding()

                }
            }
        }.showNavigationBarWithBackStyle()
    }
}

struct BankAccountsVC_Previews: PreviewProvider {
    static var previews: some View {
        BankAccountsVC()
    }
}
