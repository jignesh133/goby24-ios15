//
//  ratingsListView.swift
//  Goby24
//
//  Created by Jignesh Bhensadadiya on 7/21/21.
//

import SwiftUI
import SDWebImageSwiftUI
          
struct ratingsListView: View {
    @State var bookingid: String = ""
    @State var profilePic: String = ""
    @State var name: String = ""

    @State var remark: String = "Hello I’m going to visit...."
    @State var bookRide:Double = 1
    @State var professional:Double = 1
    @State var comfort:Double = 1
    @State var cleaness:Double = 1
    @State var music:Double = 1
    @State var carsmell:Double = 1
    @State var conversation:Double = 1
    @State var pickup:Double = 1
    @EnvironmentObject var settings: UserSettings
    @State var ride_type: String = ""

    func submitRatings(){
        var param:[String:Any] = [String:Any]()
        param["booking_id"] = bookingid

        param["professionalism"] = getStringFromAny(professional)
        param["Driving"] = getStringFromAny(professional)
        param["comfort"] = getStringFromAny(comfort)
        param["cleanliness"] =  getStringFromAny(cleaness)
        param["music"] =  getStringFromAny(music)
        param["car_smell"] = getStringFromAny(carsmell)
        param["conversation"] = getStringFromAny(conversation)
        param["pickup"] = getStringFromAny(pickup)
        param["remark"] = remark
        param["ride_type"] = ride_type
        
        WebAccess.postDataWith(_Url: WebAccess.FEEDBACK_RIDER, _parameters: param) { (result) in
            switch (result){
            case .Success(let _data):
                print(_data)
                ShowAlertWithCompletation(title: APPNAME, msg: "Rating send sucessfully", view: UIApplication.getTopViewController()!) { (result) in
                    self.settings.isMoveToSearchViewHome = true
                }
                break
            case .Error(let msg):
                ShowAlert(title: APPNAME, msg: msg, view: UIApplication.getTopViewController()!)
                break
            }
        }
    }
    var body: some View {
        ScrollView{
            Group{
                let urlString = IMAGEURL + profilePic//+ getStringFromAny(offeredRoute.user?.profilePic ?? "")//(driverProfile.profilePic ?? "")
                
                WebImage(url: URL.init(string: urlString ))
                    .placeholder(Image(systemName: "photo")) // Placeholder Image
                    .renderingMode(.original)
                    .resizable()
                    .aspectRatio(contentMode: .fill)
                    .frame(width: 60, height: 60)
                    .clipShape(Circle())
                    .overlay(Circle().stroke(Color.init(hex: "00AEEF"), lineWidth: 1)).padding(.vertical)

                
//                Image("userimage").clipShape(Circle()).overlay(Circle().stroke(Color.init(hex: "00AEEF"), lineWidth: 1)).padding(.top,15)
                
                Text("Name here").padding(.top, 15).modifier(CustomTextM(fontName: UIFont.Rubik.bold.fontName, fontSize: 16, fontColor: Color.init(hex: "2E2E2E")))
                Text(name).modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 14, fontColor: Color.init(hex: "2E2E2E")))
                
            }
            
            Group{
                Group{
                    HStack{
                        HStack{
                            Spacer()
                            Text("Bookride").modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 14, fontColor: Color.init(hex: "707070")))
                        }.frame(maxWidth:.infinity)
                        HStack{
                            StarsViewWithDrag(rating: bookRide, color: Color.init(hex: "F57F20")) { (value) in
                                self.bookRide = value
                            }.frame(width: 100, height: 30)
                            .overlay(RoundedRectangle(cornerRadius: 30).stroke(Color.init(hex: "E5E5E5"), lineWidth: 1))
                            Spacer()
                        }.frame(maxWidth:.infinity)
                    }.frame(width: 300, height: 40)
                }
                Group{
                    HStack{
                        HStack{
                            Spacer()
                            Text("Professionalism").modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 14, fontColor: Color.init(hex: "707070")))
                        }.frame(maxWidth:.infinity)
                        HStack{
                            StarsViewWithDrag(rating: professional, color: Color.init(hex: "F57F20")) { (value) in
                                self.professional = value
                            }.frame(width: 100, height: 30)
                            .overlay(RoundedRectangle(cornerRadius: 30).stroke(Color.init(hex: "E5E5E5"), lineWidth: 1))
                            Spacer()
                        }.frame(maxWidth:.infinity)
                    }.frame(width: 300, height: 40)
                }

                Group{
                    HStack{
                        HStack{
                            Spacer()
                            Text("Comfort").modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 14, fontColor: Color.init(hex: "707070")))
                        }.frame(maxWidth:.infinity)
                        HStack{
                            StarsViewWithDrag(rating: comfort, color: Color.init(hex: "F57F20")) { (value) in
                                self.comfort = value
                            }.frame(width: 100, height: 30)
                            .overlay(RoundedRectangle(cornerRadius: 30).stroke(Color.init(hex: "E5E5E5"), lineWidth: 1))
                            Spacer()
                        }.frame(maxWidth:.infinity)
                    }.frame(width: 300, height: 40)
                }
                
                Group{
                    HStack{
                        HStack{
                            Spacer()
                            Text("Cleanliness").modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 14, fontColor: Color.init(hex: "707070")))
                        }.frame(maxWidth:.infinity)
                        HStack{
                            StarsViewWithDrag(rating: cleaness, color: Color.init(hex: "F57F20")) { (value) in
                                self.cleaness = value
                            }.frame(width: 100, height: 30)
                            .overlay(RoundedRectangle(cornerRadius: 30).stroke(Color.init(hex: "E5E5E5"), lineWidth: 1))
                            Spacer()
                        }.frame(maxWidth:.infinity)
                    }.frame(width: 300, height: 40)
                }

                Group{
                    HStack{
                        HStack{
                            Spacer()
                            Text((Localizable.Music.localized())).modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 14, fontColor: Color.init(hex: "707070")))
                        }.frame(maxWidth:.infinity)
                        HStack{
                            StarsViewWithDrag(rating: music, color: Color.init(hex: "F57F20")) { (value) in
                                self.music = value
                            }.frame(width: 100, height: 30)
                            .overlay(RoundedRectangle(cornerRadius: 30).stroke(Color.init(hex: "E5E5E5"), lineWidth: 1))
                            Spacer()
                        }.frame(maxWidth:.infinity)
                    }.frame(width: 300, height: 40)
                }
                
                Group{
                    HStack{
                        HStack{
                            Spacer()
                            Text("Car smell").modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 14, fontColor: Color.init(hex: "707070")))
                        }.frame(maxWidth:.infinity)
                        HStack{
                            StarsViewWithDrag(rating: carsmell, color: Color.init(hex: "F57F20")) { (value) in
                                self.carsmell = value
                            }.frame(width: 100, height: 30)
                            .overlay(RoundedRectangle(cornerRadius: 30).stroke(Color.init(hex: "E5E5E5"), lineWidth: 1))
                            Spacer()
                        }.frame(maxWidth:.infinity)
                    }.frame(width: 300, height: 40)
                }
                Group{
                    HStack{
                        HStack{
                            Spacer()
                            Text("Conversation").modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 14, fontColor: Color.init(hex: "707070")))
                        }.frame(maxWidth:.infinity)
                        HStack{
                            StarsViewWithDrag(rating: carsmell, color: Color.init(hex: "F57F20")) { (value) in
                                self.carsmell = value
                            }.frame(width: 100, height: 30)
                            .overlay(RoundedRectangle(cornerRadius: 30).stroke(Color.init(hex: "E5E5E5"), lineWidth: 1))
                            Spacer()
                        }.frame(maxWidth:.infinity)
                    }.frame(width: 300, height: 40)
                }
                Group{
                    HStack{
                        HStack{
                            Spacer()
                            Text((Localizable.Pick_up.localized())).modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 14, fontColor: Color.init(hex: "707070")))
                        }.frame(maxWidth:.infinity)
                        HStack{
                            StarsViewWithDrag(rating: pickup, color: Color.init(hex: "F57F20")) { (value) in
                                self.pickup = value
                            }.frame(width: 100, height: 30)
                            .overlay(RoundedRectangle(cornerRadius: 30).stroke(Color.init(hex: "E5E5E5"), lineWidth: 1))
                            Spacer()
                        }.frame(maxWidth:.infinity)
                    }.frame(width: 300, height: 40)
                }

            }
            Group{
                VStack(alignment: .center, spacing: 0){
                    HStack{
                        Text("Remark").modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 14, fontColor: Color.init(hex: "707070")))
                        Spacer()
                    }.padding(.horizontal,30).padding(.bottom)
                    TextEditor( text: $remark).frame(width: 300, height: 80).padding().modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 16, fontColor: Color.black)).overlay(RoundedRectangle(cornerRadius: 10).stroke(Color.gray, lineWidth: 1))
                       
                }.padding()
            }
            
            Group{
                //Button
                Button(action: {
//                    NavigationUtil.popToRootView()
                    self.submitRatings()
                }){
                    Text((Localizable.Confirm.localized()))
                }
                .modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 16, fontColor: Color.white))
                .modifier(ButtonStyle(buttonHeight: 50, buttonColor: Color.init(hex: "00ADEF"), buttonRadius: 50))
                .padding()
            }
        }.showNavigationBarWithBackStyle()
    }
}
struct reviewToTraveller: View {
    @State var bookingid: String = ""

    @State var remark: String = "Hello I’m going to visit...."
    @State var behaviour:Double = 1
    @State var conversation:Double = 1
    @State var pickup:Double = 1
    @State var ride_type: String = "Basic Ride"

    @EnvironmentObject var settings: UserSettings

    func submitRatings(){
        var param:[String:Any] = [String:Any]()
        param["booking_id"] = bookingid

        param["behaviour"] = getStringFromAny(behaviour)
        param["conversation"] = getStringFromAny(conversation)
        param["pickup"] = getStringFromAny(pickup)
        param["remark"] = remark
        param["ride_type"] = ride_type

        WebAccess.postDataWith(_Url: WebAccess.FEEDBACK_RIDER, _parameters: param) { (result) in
            switch (result){
            case .Success(let _data):
                print(_data)
                ShowAlertWithCompletation(title: APPNAME, msg: "Rating send sucessfully", view: UIApplication.getTopViewController()!) { (result) in
                    self.settings.isMoveToSearchViewHome = true
                }
                break
            case .Error(let msg):
                ShowAlert(title: APPNAME, msg: msg, view: UIApplication.getTopViewController()!)
                break
            }
        }
    }
    var body: some View {
        ScrollView{
            Group{
                let urlString = IMAGEURL //+ getStringFromAny(offeredRoute.user?.profilePic ?? "")//(driverProfile.profilePic ?? "")
                
                WebImage(url: URL.init(string: urlString ))
                    .placeholder(Image(systemName: "photo")) // Placeholder Image
                    .renderingMode(.original)
                    .resizable()
                    .aspectRatio(contentMode: .fill)
                    .frame(width: 60, height: 60)
                    .clipShape(Circle())
                    .overlay(Circle().stroke(Color.init(hex: "00AEEF"), lineWidth: 1)).padding(.vertical)

                
//                Image("userimage").clipShape(Circle()).overlay(Circle().stroke(Color.init(hex: "00AEEF"), lineWidth: 1)).padding(.top,15)
                
                Text("Name here").padding(.top, 15).modifier(CustomTextM(fontName: UIFont.Rubik.bold.fontName, fontSize: 16, fontColor: Color.init(hex: "2E2E2E")))
               // Text(offeredRoute.user?.firstName ?? "").modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 14, fontColor: Color.init(hex: "2E2E2E")))
                
            }
            
            Group{
                Group{
                    HStack{
                        HStack{
                            Spacer()
                            Text("behaviour").modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 14, fontColor: Color.init(hex: "707070")))
                        }.frame(maxWidth:.infinity)
                        HStack{
                            StarsViewWithDrag(rating: behaviour, color: Color.init(hex: "F57F20")) { (value) in
                                self.behaviour = value
                            }.frame(width: 100, height: 30)
                            .overlay(RoundedRectangle(cornerRadius: 30).stroke(Color.init(hex: "E5E5E5"), lineWidth: 1))
                            Spacer()
                        }.frame(maxWidth:.infinity)
                    }.frame(width: 300, height: 40)
                }
                
                Group{
                    HStack{
                        HStack{
                            Spacer()
                            Text("Conversation").modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 14, fontColor: Color.init(hex: "707070")))
                        }.frame(maxWidth:.infinity)
                        HStack{
                            StarsViewWithDrag(rating: conversation, color: Color.init(hex: "F57F20")) { (value) in
                                self.conversation = value
                            }.frame(width: 100, height: 30)
                            .overlay(RoundedRectangle(cornerRadius: 30).stroke(Color.init(hex: "E5E5E5"), lineWidth: 1))
                            Spacer()
                        }.frame(maxWidth:.infinity)
                    }.frame(width: 300, height: 40)
                }
                Group{
                    HStack{
                        HStack{
                            Spacer()
                            Text((Localizable.Pick_up.localized())).modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 14, fontColor: Color.init(hex: "707070")))
                        }.frame(maxWidth:.infinity)
                        HStack{
                            StarsViewWithDrag(rating: pickup, color: Color.init(hex: "F57F20")) { (value) in
                                self.pickup = value
                            }.frame(width: 100, height: 30)
                            .overlay(RoundedRectangle(cornerRadius: 30).stroke(Color.init(hex: "E5E5E5"), lineWidth: 1))
                            Spacer()
                        }.frame(maxWidth:.infinity)
                    }.frame(width: 300, height: 40)
                }
                

            }
            Group{
                VStack(alignment: .center, spacing: 0){
                    HStack{
                        Text("Remark").modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 14, fontColor: Color.init(hex: "707070")))
                        Spacer()
                    }.padding(.horizontal,30).padding(.bottom)
                    TextEditor( text: $remark).frame(width: 300, height: 80).padding().modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 16, fontColor: Color.black)).overlay(RoundedRectangle(cornerRadius: 10).stroke(Color.gray, lineWidth: 1))
                       
                }.padding()
            }
            
            Group{
                //Button
                Button(action: {
//                    NavigationUtil.popToRootView()
                    self.submitRatings()
                }){
                    Text((Localizable.Confirm.localized()))
                }
                .modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 16, fontColor: Color.white))
                .modifier(ButtonStyle(buttonHeight: 50, buttonColor: Color.init(hex: "00ADEF"), buttonRadius: 50))
                .padding()
            }
        }.showNavigationBarWithBackStyle()
    }
}
