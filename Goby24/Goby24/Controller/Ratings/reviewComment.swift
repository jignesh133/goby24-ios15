//
//  reviewComment.swift
//  Goby24
//
//  Created by Jignesh Bhensadadiya on 7/21/21.
//

import SwiftUI
import SDWebImageSwiftUI

struct reviewComment: View {
    let ratingDetail : Model_RatingDetail?

    var body: some View {
        VStack(alignment:.leading){
            HStack(){
                let urlString = (IMAGEURL + (ratingDetail?.user?.profilePic ?? ""))
                WebImage(url: URL.init(string: urlString ))
                    .placeholder(Image(systemName: "userimage")) // Placeholder Image
                    .renderingMode(.original)
                    .resizable()
                    .aspectRatio(contentMode: .fill)
                    .frame(width: 60, height: 60)
                    .clipShape(Circle()).overlay(Circle().stroke(Color.init(hex: "00AEEF"), lineWidth: 1)).padding(.horizontal).padding(.top)
                
               // Image("userimage")
                HStack(spacing:2){
                    Text(ratingDetail?.user?.firstName ?? "").modifier(CustomTextM(fontName: UIFont.Montserrat.semiBold.fontName, fontSize: 14, fontColor: Color.init(hex: "2E2E2E")))
                    Text(ratingDetail?.user?.lastName ?? "").modifier(CustomTextM(fontName: UIFont.Montserrat.semiBold.fontName, fontSize: 14, fontColor: Color.init(hex: "2E2E2E")))

                    Spacer()
                }

            }
            HStack(spacing:2){
            Text("Excellent").modifier(CustomTextM(fontName: UIFont.Montserrat.semiBold.fontName, fontSize: 14, fontColor: Color.init(hex: "2E2E2E"))).padding(.horizontal)
                Text(getStringFromAny(ratingDetail?.rating ?? "")).modifier(CustomTextM(fontName: UIFont.Montserrat.semiBold.fontName, fontSize: 14, fontColor: Color.init(hex: "2E2E2E"))).padding(.horizontal)
                Spacer()

            }
            Text(ratingDetail?.remark ?? "").modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 14, fontColor: Color.init(hex: "2E2E2E"))).padding(.horizontal).padding(.bottom)

        }.frame(width: UIScreen.main.bounds.size.width - 50).background(RoundedRectangle(cornerRadius: 25).fill(Color.white).shadow(color: Color.black.opacity(0.12), radius: 3)).padding()
    }
}

//struct reviewComment_Previews: PreviewProvider {
//    static var previews: some View {
//        reviewComment()
//    }
//}
