//
//  userProfileRatingView.swift
//  Goby24
//
//  Created by Jignesh Bhensadadiya on 7/21/21.
//

import SwiftUI

struct userProfileRatingView: View {
    @State var selection: Int? = nil
    @State var isRatingAndReviewClicked:Bool = false
    
    @Environment(\.presentationMode) var presentationMode

    func getRattingText() -> String{
       return  getRating() //   + " / 5 - " + "x" + " ratings")

    }
    
    var body: some View {
        NavigationView{
            VStack{
                ZStack{
                    VStack{
                        gridientView()
                        Spacer()
                    }
                    VStack{
                        HStack{
                            Button(action: {
                                self.presentationMode.wrappedValue.dismiss()
                            }){
                                Image("backnav")
                            }.frame(width: 40, height: 40)
                            Spacer()
                        }.padding()
                        Spacer()
                        VStack{
                            Text((Localizable.Profile.localized())).padding(.top, -65).modifier(CustomTextM(fontName: UIFont.Rubik.bold.fontName, fontSize: 16, fontColor: Color.init(hex: "2E2E2E")))
                            
                            userImageView().padding(.leading,5).padding(.top,-50)
                            
                            Text("Name").padding(.top, 15).modifier(CustomTextM(fontName: UIFont.Rubik.bold.fontName, fontSize: 16, fontColor: Color.init(hex: "2E2E2E")))
                            Text(getName(isFullname: true)).modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 14, fontColor: Color.init(hex: "2E2E2E")))
                            
                            Group{
                                HStack{
                                    Text(Localizable.Experience_Level.localized()).modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 14, fontColor: Color.init(hex: "2E2E2E"))).padding(.top)
                                    Text(" Ambassador").modifier(CustomTextM(fontName: UIFont.Montserrat.semiBold.fontName, fontSize: 14, fontColor: Color.init(hex: "2E2E2E"))).padding(.top)
                                }
                            }
                            
                            Text(getAboutMe()).modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 14, fontColor: Color.init(hex: "2E2E2E"))).padding()
                            
                            Group{
//                                NavigationLink(destination: RatingAndReviewView(), tag: 0, selection: $selection) {
                                    Button(action: {
                                        selection = 0
                                    }){
                                        HStack() {
                                            StarsView(rating:getDoubleFromAny(getRating()) ).padding(.top,4)
                                            
                                            Text(getRattingText())
                                            Spacer()
                                            Image("next").padding(.trailing,20)
                                        }
                                        
                                    }.modifier(CustomTextM(fontName: UIFont.Montserrat.bold.fontName, fontSize: 15, fontColor: Color.init(hex: "2E2E2E")))
                                    .modifier(ButtonStyle(buttonHeight: 50, buttonColor: Color.init(hex: "F2F2F2"), buttonRadius: 10))
                                    .padding(.horizontal,30)
                                    .sheet(isPresented: $isRatingAndReviewClicked) {
                                       // RatingAndReviewView()
                                    }
                                    
                               // }
                            }
                            Group{
                                VStack(){
                                    HStack{
                                        Image("right")
                                        Text(Localizable.Verify_your_phone_number.localized()).modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 15, fontColor: Color.black))
                                        Spacer()
                                    }.padding(.horizontal)
                                    HStack{
                                        Image("right")
                                        Text(Localizable.Verify_your_Email.localized()).modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 15, fontColor: Color.black)).padding(.bottom)
                                        Spacer()
                                    }.padding(.horizontal)
                                }.padding(25)
                            }
                            
                            
                        }.background(RoundedRectangle(cornerRadius: 25).fill(Color.white).shadow(color: Color.black.opacity(0.12), radius: 3)).padding()
                        Spacer()
                    }
                    
                }
            }.hiddenNavigationBarStyle()
        }
    }
}

struct userProfileRatingView_Previews: PreviewProvider {
    static var previews: some View {
        userProfileRatingView()
    }
}

struct gridientView:View {
    
    var body:some View{
        ZStack{
            VStack{
                LinearGradient(gradient: Gradient(colors: [Color.init(hex:"00AEEF").opacity(1), Color.init(hex:"00AEEF").opacity(0)]), startPoint: .top, endPoint: .bottom)
            }
        }.frame(width:UIScreen.main.bounds.size.width, height:270)
        .ignoresSafeArea()
    }
}
