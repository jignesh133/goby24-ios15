//
//  RatingAndReviewView.swift
//  Goby24
//
//  Created by Jignesh Bhensadadiya on 7/21/21.
//

import SwiftUI
import Localize_Swift

struct RatingAndReviewView: View {
    @State var isUserRider:Bool = false
    @State var strUrl: String = ""
    @State var selection: Int? = nil
    @State var objReviews:[String:Any] = [String:Any]()
    @State var traveller_feedback_details:Model_feedback_rider?
    
    func getAllReviews(){
        if (isUserRider == true){
            strUrl = WebAccess.FEEDBACK_RIDER
        }else{
            strUrl = WebAccess.FEEDBACK_TRAVELLER
        }
        WebAccess.getDataWith(_Url: strUrl , _parameters: [:],isHideLoader: true) { (result) in
            switch (result){
            case .Success(let _data):
                do{
                    let result:[String:Any] = UtilsObjs.cleanJson(to: _data ) as? [String : Any] ?? [String:Any]()
                    print(result)
                    let data1 =  try JSONSerialization.data(withJSONObject: result["result"] ?? [] , options: JSONSerialization.WritingOptions.prettyPrinted)
                    let convertedString = String(data: data1, encoding: String.Encoding.utf8)
                    let jsonData = Data((convertedString?.utf8)!)
                    self.traveller_feedback_details = try JSONDecoder().decode(Model_feedback_rider.self, from: jsonData)

                    NSLog("LOG")
                }catch(let error){
                    print(error)
                }
                break
            case .Error(let msg):
               // ShowAlert(title: APPNAME, msg: msg, view: UIApplication.getTopViewController()!)
                break
            }
        }
    }
    
    func getAllRatings(){
        if (isUserRider == true){
            strUrl = WebAccess.FEEDBACK_RIDER_SUMMERY
        }else{
            strUrl = WebAccess.FEEDBACK_TRAVELLER_SUMMERY
        }
        WebAccess.getDataWith(_Url: strUrl , _parameters: [:],isHideLoader: true) { (result) in
            switch (result){
            case .Success(let _data):
                do{
                    let result:[String:Any] = UtilsObjs.cleanJson(to: _data ) as? [String : Any] ?? [String:Any]()
                    objReviews = result
                    
//                    print(result)
//                    let data1 =  try JSONSerialization.data(withJSONObject: result["result"] ?? [] , options: JSONSerialization.WritingOptions.prettyPrinted)
//                    let convertedString = String(data: data1, encoding: String.Encoding.utf8)
//                    let jsonData = Data((convertedString?.utf8)!)
//                    self.traveller_feedback_details = try JSONDecoder().decode(Model_feedback_rider.self, from: jsonData)

                    self.getAllReviews()
                    NSLog("LOG")
                }catch(let error){
                    print(error)
                }
                break
            case .Error(let msg):
              //  ShowAlert(title: APPNAME, msg: msg, view: UIApplication.getTopViewController()!)
                self.getAllReviews()
                break
            }
        }
    }
    func getExcellent()->Double{
        let obj = objReviews["Excellent"] as? [String:Any] ?? [:]
        return (getDoubleFromAny(obj["percentage"] ?? 0) )
    }
    func getVeryGood()->Double{
        let obj = objReviews["Very good"] as? [String:Any] ?? [:]
        return (getDoubleFromAny(obj["percentage"] ?? 0) )

    }
    func getGood()->Double{
        let obj = objReviews["Good"] as? [String:Any] ?? [:]
        return (getDoubleFromAny(obj["percentage"] ?? 0) )

    }
    func getDisapointing()->Double{
        let obj = objReviews["Disappointing"] as? [String:Any] ?? [:]
        return (getDoubleFromAny(obj["percentage"] ?? 0) )

    }
    func getVeryDisapointing()->Double{
        let obj = objReviews["Very disappointing"] as? [String:Any] ?? [:]
        return (getDoubleFromAny(obj["percentage"] ?? 0) )

    }
    var body: some View {

        VStack{
            Text(Localizable.Ratings.localized()).modifier(CustomTextM(fontName: UIFont.Rubik.medium.fontName, fontSize: 20, fontColor: Color.init(hex: "2E2E2E")))
            Group{
                VStack{
                    
                    Text(getStringFromAny(self.traveller_feedback_details?.noOfRatings ?? "0") + " Customers ratings").modifier(CustomTextM(fontName: UIFont.Montserrat.semiBold.fontName, fontSize: 14, fontColor: Color.init(hex: "2E2E2E"))).padding()
                    
                    HStack{
                        Text("5 Star").modifier(CustomTextM(fontName: UIFont.Montserrat.bold.fontName, fontSize: 15, fontColor: Color.init(hex: "2E2E2E")))
                        Text(Localizable.Excellent.localized()).modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 15, fontColor: Color.init(hex: "707070")))
                        Spacer()
                        ProgressView(value: getExcellent(), total: 100)
                            .accentColor(Color.init("57F20"))
                            .scaleEffect(x: 1, y: 2, anchor: .center)
                            .foregroundColor(Color.init(hex:"CBF1FF"))
                            .frame(width:90)
                        
                        Text((getStringFromAny(getExcellent()) + "%")).modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 15, fontColor: Color.init(hex: "2E2E2E")))
                    }.padding(.horizontal)
                    
                    HStack{
                        Text("4 Star").modifier(CustomTextM(fontName: UIFont.Montserrat.bold.fontName, fontSize: 15, fontColor: Color.init(hex: "2E2E2E")))
                        Text(Localizable.Very_Good.localized()).modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 15, fontColor: Color.init(hex: "707070")))
                        Spacer()
                        ProgressView(value: getVeryGood(), total: 100)
                            .accentColor(Color.init("57F20"))
                            .scaleEffect(x: 1, y: 2, anchor: .center)
                            .foregroundColor(Color.init(hex:"CBF1FF"))
                            .frame(width:90)
                        
                        Text((getStringFromAny(getVeryGood()) + "%")).modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 15, fontColor: Color.init(hex: "2E2E2E")))
                    }.padding(.horizontal)
                    HStack{
                        Text("3 Star").modifier(CustomTextM(fontName: UIFont.Montserrat.bold.fontName, fontSize: 15, fontColor: Color.init(hex: "2E2E2E")))
                        Text(Localizable.Good.localized()).modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 15, fontColor: Color.init(hex: "707070")))
                        Spacer()
                        ProgressView(value: getGood(), total: 100)
                            .accentColor(Color.init("57F20"))
                            .scaleEffect(x: 1, y: 2, anchor: .center)
                            .foregroundColor(Color.init(hex:"CBF1FF"))
                            .frame(width:90)
                        
                        Text((getStringFromAny(getGood()) + "%")).modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 15, fontColor: Color.init(hex: "2E2E2E")))
                    }.padding(.horizontal)
                    HStack{
                        Text("2 Star").modifier(CustomTextM(fontName: UIFont.Montserrat.bold.fontName, fontSize: 15, fontColor: Color.init(hex: "2E2E2E")))
                        Text((Localizable.Disapointing.localized())).modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 15, fontColor: Color.init(hex: "707070")))
                        Spacer()
                        ProgressView(value: getDisapointing(), total: 100)
                            .accentColor(Color.init("57F20"))
                            .scaleEffect(x: 1, y: 2, anchor: .center)
                            .foregroundColor(Color.init(hex:"CBF1FF"))
                            .frame(width:90)
                        
                        Text((getStringFromAny(getDisapointing()) + "%")).modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 15, fontColor: Color.init(hex: "2E2E2E")))
                    }.padding(.horizontal)
                    HStack{
                        Text("1 Star").modifier(CustomTextM(fontName: UIFont.Montserrat.bold.fontName, fontSize: 15, fontColor: Color.init(hex: "2E2E2E")))
                        Text(Localizable.Very_disapointing.localized()).modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 15, fontColor: Color.init(hex: "707070")))
                        Spacer()
                        ProgressView(value: getVeryDisapointing(), total: 100)
                            .accentColor(Color.init("57F20"))
                            .scaleEffect(x: 1, y: 2, anchor: .center)
                            .foregroundColor(Color.init(hex:"CBF1FF"))
                            .frame(width:90)
                        
                        Text((getStringFromAny(getVeryDisapointing()) + "%")).modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 15, fontColor: Color.init(hex: "2E2E2E")))
                    }.padding(.horizontal).padding(.bottom,30)
                    
                    
                }.background(RoundedRectangle(cornerRadius: 25).fill(Color.white).shadow(color: Color.black.opacity(0.12), radius: 3)).padding()
            }
            Text(Localizable.Reviews.localized()).modifier(CustomTextM(fontName: UIFont.Rubik.medium.fontName, fontSize: 20, fontColor: Color.init(hex: "2E2E2E")))
            ScrollView(.horizontal){
                HStack{
                    ForEach(0..<(self.traveller_feedback_details?.ratingDetail?.count ?? 0),id: \.self) { row in
                        reviewComment(ratingDetail: self.traveller_feedback_details?.ratingDetail?[row])
                    }
                }

            }
            
        }.showNavigationBarWithBackStyle()
        .onAppear(){
            getAllReviews()
        }
    }
}

struct RatingAndReviewView_Previews: PreviewProvider {
    static var previews: some View {
        RatingAndReviewView()
    }
}
