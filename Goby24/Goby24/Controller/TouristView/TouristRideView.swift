//
//  TouristRideView.swift
//  Goby24
//
//  Created by Jignesh Bhensadadiya on 8/14/21.
//

import SwiftUI

struct TouristRideView: View {
    @State var touristRequestList:Model_TouristRequestList?
    @State private var isOpen:Bool = false
    @State var isOpenAddVehicle:Bool = false

    
    let onDeleteRequest: (String) -> Void
    let onAcceptRequest: (String) -> Void
    let onEditRequest: (Model_TouristRequestList) -> Void
    let onContactRequest: (Model_TouristRequestList) -> Void
    let onPayRequest: (Model_TouristRequestList) -> Void

    var body: some View {
        VStack(alignment:.leading){
            NavigationLink(destination: LicensePlateNumberView(), isActive: $isOpenAddVehicle){EmptyView()}.isDetailLink(false)
            
            HStack{  // FOR TOP HEADER
                
                VStack(alignment:.leading){  // FOR BUTTON
                    HStack{
                        Button(action: {
                            withAnimation {
                                isOpen.toggle()
                            }
                        }) {
                            Image("roundDown")
                        }
                    }.frame(width: 40, height: 40 ,alignment: .top)
                    Spacer()
                }.frame(maxWidth: 50,maxHeight: 40).fixedSize(horizontal: false, vertical: false)
                
                
                VStack{
                    VStack(alignment:.leading){
                        Text(Localizable.Date.localized()).modifier(CustomTextM(fontName: UIFont.Montserrat.semiBold.fontName, fontSize: 12, fontColor: Color.init(hex: "00AEEF")))
                        Text(touristRequestList?.journeyStartDate ?? "").modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 12, fontColor: Color.init(hex: "000000")))
                        Divider()
                        
                    }.frame(maxWidth: .infinity)
                }
                VStack{
                    VStack(alignment:.leading){
                        Text(Localizable.Tourist_Spot.localized()).modifier(CustomTextM(fontName: UIFont.Montserrat.semiBold.fontName, fontSize: 12, fontColor: Color.init(hex: "00AEEF")))
                        if (touristRequestList?.newTouristSpot?.removeWhiteSpace().count != 0){
                            Text(touristRequestList?.newTouristSpot ?? "").modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 12, fontColor: Color.init(hex: "000000")))
                        }else{
                            Text(touristRequestList?.touristspot?.spotName ?? "").modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 12, fontColor: Color.init(hex: "000000")))
                            
                        }
                        Divider()
                    }.frame(maxWidth: .infinity)
                }
            }
            if (isOpen == true){  // FOR OPEN SECTION
                HStack{
                    VStack(alignment:.leading){  // FOR BUTTON
                        Spacer()
                    }.frame(maxWidth: 50,maxHeight: 40).fixedSize(horizontal: false, vertical: false)
                    
                    VStack{
                        HStack{
                            HStack{
                                Text(Localizable.Descriptions.localized()).modifier(CustomTextM(fontName: UIFont.Montserrat.semiBold.fontName, fontSize: 12, fontColor: Color.init(hex: "00AEEF")))
                                Spacer()
                            }
                            HStack{
                                Text(touristRequestList?.descriptionField ?? "").modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 12, fontColor: Color.init(hex: "000000")))
                                Spacer()
                            }
                        }
                        Spacer(minLength: 5)
                        HStack{
                            HStack{
                                Text("Location").modifier(CustomTextM(fontName: UIFont.Montserrat.semiBold.fontName, fontSize: 12, fontColor: Color.init(hex: "00AEEF")))
                                Spacer()
                            }
                            HStack{
                                Text(touristRequestList?.journeyFrom ?? "").modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 12, fontColor: Color.init(hex: "000000")))
                                Spacer()
                            }
                        }
                        Spacer(minLength: 5)
                        HStack{
                            HStack{
                                Text(Localizable.Price.localized()).modifier(CustomTextM(fontName: UIFont.Montserrat.semiBold.fontName, fontSize: 12, fontColor: Color.init(hex: "00AEEF")))
                                Spacer()
                            }
                            HStack{
                                Text(touristRequestList?.packageBudget ?? "").modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 12, fontColor: Color.init(hex: "000000")))
                                Spacer()
                            }
                        }
                        Spacer(minLength: 5)
                        HStack{
                            HStack{
                                Text("Action").modifier(CustomTextM(fontName: UIFont.Montserrat.semiBold.fontName, fontSize: 12, fontColor: Color.init(hex: "00AEEF")))
                                Spacer()
                            }
                            
                            if (getId() != getStringFromAny(touristRequestList?.user?.id ?? 0)){
                                HStack{
                                    Button(action: {
                                        print("Contact")
                                        
                                        onContactRequest(touristRequestList!)
                                    }) {
                                        Text(Localizable.Contact.localized())
                                    }
                                    .modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 12, fontColor: Color.white))
                                    .modifier(ButtonStyle(buttonHeight: 25, buttonColor: Color.init(hex: "00ADEF"), buttonRadius: 25))
                                    
                                    
                                    if (touristRequestList?.requestStatus == "expired"){
                                        Text("Ride Expired")
                                        
                                    }else if (touristRequestList?.requestStatus == "cancel"){
                                        Text("Ride Cancelled")
                                        
                                    }else{
                                        
                                        Button(action: {
                                            
                                            let data = userDefault.value(forKey: Enum_Login.userProfileData.rawValue) as? [String:Any] ?? [:]
                                            print(data)
                                            if getUserIsRider() == false{
                                                ShowAlert(title: APPNAME, msg: "Your position is not driver", view: UIApplication.getTopViewController()!)
                                            }
                                            else if (((getBoolFromAny(data["is_vehicle_verified"] ?? false)) == false)){
                                                let alert = UIAlertController(title: "Do you want be rider", message: "You need to add your vehicle information to be a rider. Would you like to be add vehicle information now?", preferredStyle: .alert)
                                                let cancel = UIAlertAction(title:"Cancel", style: .cancel) { (res) in
                                                    
                                                }
                                                let sure = UIAlertAction(title:"Yes ", style: .default) { (res) in
                                                    // GIE TRAVELLER RATING
                                                    isOpenAddVehicle = true
                                                }
                                                alert.addAction(cancel)
                                                alert.addAction(sure)
                                                
                                                UIApplication.getTopViewController()!.present(alert, animated: true, completion: nil)
                                                
                                                
                                                //ShowAlert(title: APPNAME, msg: "Your Vehicle not verified", view: UIApplication.getTopViewController()!)
                                            }else{
                                                onAcceptRequest(getStringFromAny(touristRequestList?.id ?? ""))
                                            }
                                            
                                            print("Accept")
                                        }) {
                                            Text(Localizable.Accept.localized())
                                        }
                                        .modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 12, fontColor: Color.white))
                                        .modifier(ButtonStyle(buttonHeight: 25, buttonColor: Color.init(hex: "00ADEF"), buttonRadius: 25))
                                        
                                    }
                                    Spacer()
                                }
                            }else{
                                HStack{
                                    if (touristRequestList?.requestStatus == "Accepted"){
                                        
                                        if (touristRequestList?.ispaid == false){
                                            Button(action: {
                                                print("Pay")
                                                
                                                onPayRequest(touristRequestList!)
                                            }) {
                                                Text(Localizable.Pay.localized())
                                            }
                                            .modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 12, fontColor: Color.white))
                                            .modifier(ButtonStyle(buttonHeight: 25, buttonColor: Color.init(hex: "00ADEF"), buttonRadius: 25))
                                        }else{
                                            Text("Paid").modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 12, fontColor: Color.black))
                                        }
                                    }else{
                                        Button(action: {
                                            print("Edit")
                                            onEditRequest(touristRequestList!)
                                        }) {
                                            Text(Localizable.Edit.localized())
                                        }
                                        .modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 12, fontColor: Color.white))
                                        .modifier(ButtonStyle(buttonHeight: 25, buttonColor: Color.init(hex: "00ADEF"), buttonRadius: 25))
                                        
                                        Button(action: {
                                            print("Delete")
                                            
                                            onDeleteRequest(getStringFromAny(touristRequestList?.id ?? ""))
                                            
                                        }) {
                                            Text(Localizable.Delete.localized())
                                        }
                                        .modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 12, fontColor: Color.white))
                                        .modifier(ButtonStyle(buttonHeight: 25, buttonColor: Color.init(hex: "00ADEF"), buttonRadius: 25))
                                        
                                    }
                                    
                                    
                                    Spacer()
                                }
                            }
                            
                        }
                    }
                }
            }
        }
    }
}

//struct TouristRideView_Previews: PreviewProvider {
//    static var previews: some View {
//        TouristRideView()
//    }
//}
