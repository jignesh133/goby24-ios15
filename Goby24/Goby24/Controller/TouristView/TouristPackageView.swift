//
//  TouristPackageView.swift
//  Goby24
//
//  Created by Jignesh Bhensadadiya on 7/17/21.
//

import SwiftUI

struct TouristPackageView: View {
    @State var selection: Int? = nil

    var body: some View {
        VStack{
            
            
            Text("Sundarban Package").padding(.top, 25).modifier(CustomTextM(fontName: UIFont.Rubik.medium.fontName, fontSize: 26, fontColor: Color.init(hex: "2E2E2E")))
            

            
            
            VStack{
                NavigationLink(destination: PickDate(), tag: 0, selection: $selection) {
                    
                    Button(action: {
                        selection = 0
                        
                    }){
                        Text(Localizable.Journey_Date.localized()).frame(minWidth: 0,  maxWidth: .infinity)
                        
                    }
                    
                    .modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 16, fontColor: Color.black))
                    .modifier(ButtonStyle(buttonHeight: 50, buttonColor: Color.white, buttonRadius: 50))
                    .overlay(RoundedRectangle(cornerRadius: 50).stroke(Color.init(red: 0, green: 0.682, blue: 0.937), lineWidth: 1))
                    .buttonStyle(PlainButtonStyle())

                    
                }
                NavigationLink(destination: NumberOfSeatsToBook(), tag: 1, selection: $selection) {
                    Button(action: {
                        selection = 1
                        
                    }){
                        
                        Text(Localizable.Number_of_Tourists.localized())
                            .frame(minWidth: 0,  maxWidth: .infinity)
                    }
                    
                    
                    .modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 16, fontColor: Color.black))
                    .modifier(ButtonStyle(buttonHeight: 50, buttonColor: Color.white, buttonRadius: 50))
                    .overlay(RoundedRectangle(cornerRadius: 50).stroke(Color.init(red: 0, green: 0.682, blue: 0.937), lineWidth: 1))
                    .padding(.top,20)
                    .buttonStyle(PlainButtonStyle())

                }
                
                
            }.padding(.horizontal,25).padding(.top,20)
            Spacer()
            
            NavigationLink(destination: CongratsForBookingView(), tag: 2, selection: $selection) {
                
                Button(action: {
                    selection = 2
                }){
                    Text(Localizable.Continue.localized()).frame(minWidth: 0,  maxWidth: .infinity)
                }
                .modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 16, fontColor: Color.white))
                .modifier(ButtonStyle(buttonHeight: 50, buttonColor: Color.init(hex: "00ADEF"), buttonRadius: 50))
                .padding()
                .buttonStyle(PlainButtonStyle())

            }
        }
        .showNavigationBarWithBackStyle()
    }
}

struct TouristPackageView_Previews: PreviewProvider {
    static var previews: some View {
        TouristPackageView()
    }
}
