//
//  PackageFilterView.swift
//  Goby24
//
//  Created by Jignesh Bhensadadiya on 7/17/21.
//

import SwiftUI
//import Sliders
struct PackageFilterView: View{
    @State var pricesort: String = ""
    @State var ratingsort: String = ""
    @State var timesort: String = ""
    @State var sliderValue = 0.0
    var minimumValue = 0.0
    var maximumvalue = 100.0
    @Environment(\.presentationMode) var presentationMode
    
    var body: some View {
        
        VStack{
            HStack{
                Button(action: {
                    self.presentationMode.wrappedValue.dismiss()
                    
                }){
                    Image("close")
                }.padding(.horizontal)
                
                Spacer()
                Button(action: {
                    
                }){
                    Text("Clear All").modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 16, fontColor: Color.init(hex: "2E2E2E")))
                }.padding(.horizontal)
            }.padding(.top)
            
            Text(Localizable.Filter.localized()).modifier(CustomTextM(fontName: UIFont.Rubik.medium.fontName, fontSize: 26, fontColor: Color.init(hex: "2E2E2E"))).padding(.top,20)
            ScrollView{
                VStack{
                    Group{
                        VStack{
                            HStack{
                                Text(Localizable.Days.localized()).modifier(CustomTextM(fontName: UIFont.Montserrat.bold.fontName, fontSize: 16, fontColor: Color.init(hex: "2E2E2E")))
                                Spacer()
                            }
                            HStack{
                                Text("Min Value").modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 10, fontColor: Color.init(hex: "00AEEF")))
                                Spacer()
                                Text("Max Value").modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 10, fontColor: Color.init(hex: "00AEEF")))
                            }.padding(.vertical)
                            
                            
                            HStack {
                                Slider(value: $sliderValue, in: minimumValue...maximumvalue)
                            }
                            HStack {
                                Text("\(Int(minimumValue))").modifier(CustomTextM(fontName: UIFont.Rubik.medium.fontName, fontSize: 10, fontColor: Color.init(hex: "00AEEF")))
                                Spacer()
                                Text("\(Int(maximumvalue))").modifier(CustomTextM(fontName: UIFont.Rubik.medium.fontName, fontSize: 10, fontColor: Color.init(hex: "00AEEF")))
                            }
                            // 3.
                            Text("\(Int(sliderValue))").modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 10, fontColor: Color.init(hex: "00AEEF")))
                            
                        }
                    }
                    
                    Group{
                        VStack{
                            HStack{
                                Text((Localizable.Price.localized())).modifier(CustomTextM(fontName: UIFont.Montserrat.bold.fontName, fontSize: 16, fontColor: Color.init(hex: "2E2E2E")))
                                Spacer()
                            }
                            HStack{
                                Text("Min Value").modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 10, fontColor: Color.init(hex: "00AEEF")))
                                Spacer()
                                Text("Max Value").modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 10, fontColor: Color.init(hex: "00AEEF")))
                            }.padding(.vertical)
                            
                            
                            HStack {
                                Slider(value: $sliderValue, in: minimumValue...maximumvalue)
                            }
                            HStack {
                                Text("\(Int(minimumValue))").modifier(CustomTextM(fontName: UIFont.Rubik.medium.fontName, fontSize: 10, fontColor: Color.init(hex: "00AEEF")))
                                Spacer()
                                Text("\(Int(maximumvalue))").modifier(CustomTextM(fontName: UIFont.Rubik.medium.fontName, fontSize: 10, fontColor: Color.init(hex: "00AEEF")))
                            }
                            // 3.
                            Text("\(Int(sliderValue))").modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 10, fontColor: Color.init(hex: "00AEEF")))
                            
                        }
                    }
                    Group{
                        VStack{
                            HStack{
                                Text(Localizable.No_fo_tourists.localized()).modifier(CustomTextM(fontName: UIFont.Montserrat.bold.fontName, fontSize: 16, fontColor: Color.init(hex: "2E2E2E")))
                                Spacer()
                            }
                            HStack{
                                Text("Min Value").modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 10, fontColor: Color.init(hex: "00AEEF")))
                                Spacer()
                                Text("Max Value").modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 10, fontColor: Color.init(hex: "00AEEF")))
                            }.padding(.vertical)
                            
                            
                            HStack {
                                Slider(value: $sliderValue, in: minimumValue...maximumvalue)
                            }
                            HStack {
                                Text("\(Int(minimumValue))").modifier(CustomTextM(fontName: UIFont.Rubik.medium.fontName, fontSize: 10, fontColor: Color.init(hex: "00AEEF")))
                                Spacer()
                                Text("\(Int(maximumvalue))").modifier(CustomTextM(fontName: UIFont.Rubik.medium.fontName, fontSize: 10, fontColor: Color.init(hex: "00AEEF")))
                            }
                            // 3.
                            Text("\(Int(sliderValue))").modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 10, fontColor: Color.init(hex: "00AEEF")))
                            
                        }
                    }
                    Group{
                        // 2
                        HStack(){
                            Text(Localizable.Choose_Country.localized()).padding(.top, 10).modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 16, fontColor: Color.init(hex: "2E2E2E")))
                            Spacer()
                        }.padding(.horizontal)
                        
                        Button(action: {}){
                            ZStack() {
                                HStack() {
                                    Image("ind").padding([.leading],20)
                                    Text("India").padding(.horizontal)
                                    Spacer()
                                    Image("down").padding(.trailing,20)
                                }
                            }
                        }
                        .modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 13, fontColor: Color.init(hex: "2E2E2E")))
                        .modifier(ButtonStyle(buttonHeight: 50, buttonColor: Color.init(hex: "F2F2F2"), buttonRadius: 50))
                        .padding(.horizontal).padding(.top,5)
                    }
                    Group{
                        // 2
                        HStack(){
                            Text(Localizable.Choose_your_city.localized()).padding(.top, 10).modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 16, fontColor: Color.init(hex: "2E2E2E")))
                            Spacer()
                        }.padding(.horizontal)
                        
                        Button(action: {}){
                            ZStack() {
                                HStack() {
                                    Text("Mumbai").padding(.horizontal)
                                    Spacer()
                                    Image("down").padding(.trailing,20)
                                }
                            }
                        }
                        .modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 13, fontColor: Color.init(hex: "2E2E2E")))
                        .modifier(ButtonStyle(buttonHeight: 50, buttonColor: Color.init(hex: "F2F2F2"), buttonRadius: 50))
                        .padding(.horizontal).padding(.top,5)
                    }.padding(.top)
                    
                    Spacer()
                    Button(action: {
                        
                    }){
                        Text(Localizable.Filter.localized())
                    }
                    .modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 16, fontColor: Color.white))
                    .modifier(ButtonStyle(buttonHeight: 50, buttonColor: Color.init(hex: "00ADEF"), buttonRadius: 50))
                    .frame(width:200)
                    .padding(.top,30)
                    
                }.padding().padding(.horizontal)
            }
        }
    }
    
}
//{
//    @State var value = 0.5
//    @State var range = 0.2...0.8
//
//    var body: some View {
//
//
//        VStack{
//            HStack{
//
//            }
//        }
//        RangeSlider(range: $range)
//            .rangeSliderStyle(
//                HorizontalRangeSliderStyle(
//                    track:
//                        HorizontalRangeTrack(
//                            view: Capsule().foregroundColor(Color.init(hex: "00AEEF"))
//                        )
//                        .background(Capsule().foregroundColor(Color.init(hex: "D0D0D0")))
//                        .frame(height: 8),
//                    lowerThumb: Circle().foregroundColor(Color.init(hex: "00AEEF"))        .overlay(RoundedRectangle(cornerRadius: 16).stroke(Color.init(hex: "00AEEF"), lineWidth: 1))
//                    ,
//                    upperThumb: Circle().foregroundColor(Color.white)        .overlay(RoundedRectangle(cornerRadius: 16).stroke(Color.init(hex: "D0D0D0"), lineWidth: 1)),
//                    lowerThumbSize: CGSize(width: 32, height: 32),
//                    upperThumbSize: CGSize(width: 32, height: 32),
//                    options: .forceAdjacentValue
//                )
//            )
//    }
//}

struct PackageFilterView_Previews: PreviewProvider {
    static var previews: some View {
        PackageFilterView()
    }
}
