//
//  RequestFromTouristPayementView.swift
//  Goby24
//
//  Created by Jignesh Bhensadadiya on 12/8/21.
//

import SwiftUI
struct RequestFromTouristPayementView: View {
    @State var paymentClicked: Bool = true
//    var touristPackage:Model_TouristPackage
    var touristRequest:Model_TouristRequestList?

    
    var body: some View {
        VStack{
            HStack{
                //Button
                Button(action: {
                    paymentClicked = true
                }){
                    if(paymentClicked == true){
                        VStack{
                            Text(Localizable.Payments.localized()).modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 13, fontColor: Color.init(hex: "00AEEF")))
                            Divider().background(Color.init(hex: "00AEEF")).frame(height: 1.5)
                        }
                    }else{
                        VStack{
                            Text(Localizable.Payments.localized()).modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 13, fontColor: Color.init(hex: "2E2E2E")))
                            Divider().background(Color.init(hex: "2E2E2E")).frame(height: 1.5)
                        }
                    }
                }.frame(minWidth: 0, maxWidth: .infinity)
                
                
                //ACCOUNT Button 2E2E2E
                Button(action: {
                    paymentClicked = false
                }){
                    if(paymentClicked == false){
                        VStack{
                            Text(Localizable.Profile.localized()).modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 13, fontColor: Color.init(hex: "00AEEF")))
                            Divider().background(Color.init(hex: "00AEEF")).frame( height: 1.5)
                        }
                    }else{
                        VStack{
                            Text(Localizable.Profile.localized()).modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 13, fontColor: Color.init(hex: "2E2E2E")))
                            Divider().background(Color.init(hex: "2E2E2E")).frame( height: 1.5)
                        }
                    }
                }.frame(minWidth: 0, maxWidth: .infinity)
            }.padding()
            
            
            if(paymentClicked == false){
                RequestFromTouristDetailsView(touristRequest: self.touristRequest)
            }else{
                RequestFromTouristEpaymentView(touristRequest: self.touristRequest)
            }
            Spacer()
        }.showNavigationBarWithBackStyle()
        
    }
}
//struct RequestFromTouristPayementView: View {
//    var body: some View {
//        Text(/*@START_MENU_TOKEN@*/"Hello, World!"/*@END_MENU_TOKEN@*/)
//    }
//}
//
//struct RequestFromTouristPayementView_Previews: PreviewProvider {
//    static var previews: some View {
//        RequestFromTouristPayementView()
//    }
//}
