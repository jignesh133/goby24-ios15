//
//  IamonlydriverView.swift
//  Goby24
//
//  Created by Jignesh Bhensadadiya on 7/20/21.
//

import SwiftUI

struct IamonlydriverView: View {
    
    @State var location: String = ""
    @State var package: String = ""
    @State var arrPackages:[Model_TouristPackage] = [Model_TouristPackage]()

    @State var isDropDownOpen:Bool = false
    @State var isTouristPackageOpen:Bool = false

    @ObservedObject private var touristPackageOffer = TouristPackageOffer.shared

    @State var selectedPackage:Model_TouristPackage?
    @State var selectedCountry:String?
  
    @State var startDate:Date = Date()
    
    func getTouristPackageList(){
        
        var param:[String:Any] = [String:Any]()
        param["country"] = touristPackageOffer.strCountry?.lowercased()
        
        WebAccess.getDataWith(_Url: WebAccess.TOURIST_PACKAGES, _parameters: param) { (result) in
            switch (result){
            case .Success(let _data):
                print(_data)
                do{
                    let result:[String:Any] = UtilsObjs.cleanJson(to: _data ) as? [String : Any] ?? [String:Any]()
                    print(result)
                    let data1 =  try JSONSerialization.data(withJSONObject: result["result"] ?? [] , options: JSONSerialization.WritingOptions.prettyPrinted)
                    let convertedString = String(data: data1, encoding: String.Encoding.utf8)
                    let jsonData = Data((convertedString?.utf8)!)
                    self.arrPackages = try JSONDecoder().decode([Model_TouristPackage].self, from: jsonData)
                    NSLog("LOG")
                    if (arrPackages.count == 0){
                        selectedPackage = nil
                        selectedCountry = nil
                    }
                }catch(let error){
                    print(error)
                }

                break
            case .Error(let msg):
                ShowAlert(title: APPNAME, msg: msg, view: UIApplication.getTopViewController()!)
                break
            }
        }
    }
    
    var body: some View {
        
        VStack{

            Group{
                //Button
                Button(action: {
                    // ACTION CLICKED
                    withAnimation{
                        isDropDownOpen.toggle()
                    }
                    
                }){
                    ZStack() {
                        Text(touristPackageOffer.strCountry ?? "Choose Location")
                        HStack() {
                            Spacer()
                            Image("down").padding(.trailing,20)
                        }
                    }
                }
                .modifier(CustomTextM(fontName: UIFont.Montserrat.semiBold.fontName, fontSize: 16, fontColor: Color.init(hex: "2E2E2E")))
                .modifier(ButtonStyle(buttonHeight: 50, buttonColor: Color.init(hex: "F2F2F2"), buttonRadius: 50))
             
                if (isDropDownOpen == true){
                    ScrollView{
                        VStack{
                            ForEach(0..<(appDelegate?.arrCountry.count ?? 0), id: \.self) { row in
                                Button(action: {
                                    touristPackageOffer.strCountry = (appDelegate?.arrCountry[row].countryName)
                                    isDropDownOpen = false
                                    self.getTouristPackageList()
                                }){
                                    Text(appDelegate?.countryNames[row] ?? "")
                                }
                                .modifier(CustomTextM(fontName: UIFont.Montserrat.semiBold.fontName, fontSize: 16, fontColor: Color.init(hex: "2E2E2E")))
                                .modifier(ButtonStyle(buttonHeight: 50, buttonColor: Color.init(hex: "F2F2F2"), buttonRadius: 0))
                            }
                        }
                    }.frame(maxHeight:150).cornerRadius(10).padding(.top,-40)

                }
                
                //Button
                Button(action: {
                    // ACTION CLICKED
                    withAnimation{
                        isTouristPackageOpen.toggle()
                    }
                }){
                    ZStack() {
                        Text(touristPackageOffer.touristPackage?.packageName ?? "Choose Tourist Package")
                        HStack() {
                            Spacer()
                            Image("down").padding(.trailing,20)
                        }
                    }
                }
                .modifier(CustomTextM(fontName: UIFont.Montserrat.semiBold.fontName, fontSize: 16, fontColor: Color.init(hex: "2E2E2E")))
                .modifier(ButtonStyle(buttonHeight: 50, buttonColor: Color.init(hex: "F2F2F2"), buttonRadius: 50))
                
                if (isTouristPackageOpen == true && self.arrPackages.count > 0){
                    ScrollView{
                        VStack{
                            ForEach(0..<(self.$arrPackages.wrappedValue.count ), id: \.self) { row in
                                Button(action: {
                                    touristPackageOffer.touristPackage = self.arrPackages[row]
                                    
                                    isTouristPackageOpen = false

                                }){
                                    Text(self.arrPackages[row].packageName ?? "")
                                }
                                .modifier(CustomTextM(fontName: UIFont.Montserrat.semiBold.fontName, fontSize: 16, fontColor: Color.init(hex: "2E2E2E")))
                                .modifier(ButtonStyle(buttonHeight: 50, buttonColor: Color.init(hex: "F2F2F2"), buttonRadius: 0))
                            }
                        }
                    }.frame(maxHeight:150).cornerRadius(10).padding(.top,-40)

                }
                
            }.padding()
            
            Group{
                VStack(spacing: 0){
                    Text((Localizable.Choose_no_of_passengers.localized())).modifier(CustomTextM(fontName: UIFont.Rubik.medium.fontName, fontSize: 19, fontColor: Color.init(hex: "2E2E2E")))
                    
                    CustomSteper(count: touristPackageOffer.passanger ?? "1") { (count) in
                        touristPackageOffer.passanger = getStringFromAny(count)

                    }.padding(.top)
                    
                    Text(Localizable.Choose_number_of_days_you_want_to_spent.localized()).modifier(CustomTextM(fontName: UIFont.Rubik.medium.fontName, fontSize: 19, fontColor: Color.init(hex: "2E2E2E"))).padding(.top)
                    
                    CustomSteper(count: touristPackageOffer.days ?? "1") { (count) in
                        touristPackageOffer.days = getStringFromAny(count)
                    }.padding(.top)
                }
            }
            
            Group{
                Text((Localizable.Choose_your_available_date_range.localized())).modifier(CustomTextM(fontName: UIFont.Rubik.medium.fontName, fontSize: 19, fontColor: Color.init(hex: "2E2E2E"))).padding(.top)
                HStack{
                    VStack{
                        Text((Localizable.Start.localized())).modifier(CustomTextM(fontName: UIFont.Rubik.medium.fontName, fontSize: 16, fontColor: Color.init(hex: "F57F20")))
                        
                        
                        //Button
                        Button(action: {
                            RPicker.selectDate(title: "Select available start date",minDate: Date(), didSelectDate: { (selectedDate) in
                                touristPackageOffer.availableStartDate = selectedDate.getFormattedDate(formatter: "d.M.yy")
                                startDate = selectedDate
                            })
                        }){
                            HStack() {
                                Spacer()

                                Text(touristPackageOffer.availableStartDate)
                                Spacer()
                                Image("down").padding(.trailing,20)
                            }
                        }
                        .modifier(CustomTextM(fontName: UIFont.Montserrat.semiBold.fontName, fontSize: 16, fontColor: Color.init(hex: "2E2E2E")))
                        .modifier(ButtonStyle(buttonHeight: 50, buttonColor: Color.init(hex: "F2F2F2"), buttonRadius: 50)).padding(.horizontal)
                    }
                    VStack(spacing:20){
                        Text("End").modifier(CustomTextM(fontName: UIFont.Rubik.medium.fontName, fontSize: 16, fontColor: Color.init(hex: "F57F20")))
                        
                        
                        //Button
                        Button(action: {
                            RPicker.selectDate(title: "Select available End date",minDate: startDate, didSelectDate: { (selectedDate) in
                                touristPackageOffer.availableEndDate = selectedDate.getFormattedDate(formatter: "d.M.yy")
                            })
                            
                        }){
                            HStack() {
                                Spacer()

                                Text(touristPackageOffer.availableEndDate)
                                Spacer()
                                Image("down").padding(.trailing,20)
                            }
                        }
                        .modifier(CustomTextM(fontName: UIFont.Montserrat.semiBold.fontName, fontSize: 16, fontColor: Color.init(hex: "2E2E2E")))
                        .modifier(ButtonStyle(buttonHeight: 50, buttonColor: Color.init(hex: "F2F2F2"), buttonRadius: 50)).padding(.horizontal)
                    }
                }
            }
            
            Group{
                Text(Localizable.Choose_your_available_time_range.localized()).modifier(CustomTextM(fontName: UIFont.Rubik.medium.fontName, fontSize: 19, fontColor: Color.init(hex: "2E2E2E"))).padding(.top)
                HStack{
                    VStack{
                        Text(Localizable.Start.localized()).modifier(CustomTextM(fontName: UIFont.Rubik.medium.fontName, fontSize: 16, fontColor: Color.init(hex: "F57F20")))
                        
                        
                        //Button
                        Button(action: {
                            RPicker.selectDate(title: "Select available Start Time",datePickerMode: .time,  didSelectDate: { (selectedDate) in
                                touristPackageOffer.availableStartTime = selectedDate.getFormattedDate(formatter: "hh:mm a")
                            })
                        }){
                            HStack() {
                                Spacer()

                                Text(touristPackageOffer.availableStartTime)
                                Spacer()
                                Image("down").padding(.trailing,20)
                            }
                        }
                        .modifier(CustomTextM(fontName: UIFont.Montserrat.semiBold.fontName, fontSize: 16, fontColor: Color.init(hex: "2E2E2E")))
                        .modifier(ButtonStyle(buttonHeight: 50, buttonColor: Color.init(hex: "F2F2F2"), buttonRadius: 50)).padding(.horizontal)
                    }
                    VStack(spacing:20){
                        Text("End").modifier(CustomTextM(fontName: UIFont.Rubik.medium.fontName, fontSize: 16, fontColor: Color.init(hex: "F57F20")))
                        
                        
                        //Button
                        Button(action: {
                            RPicker.selectDate(title: "Select available End Time",datePickerMode: .time,  didSelectDate: { (selectedDate) in
                                touristPackageOffer.availableEndTime = selectedDate.getFormattedDate(formatter: "hh:mm a")
                            })

                        }){
                                HStack() {
                                    Spacer()

                                    Text(touristPackageOffer.availableEndTime)

                                    Spacer()
                                    Image("down").padding(.trailing,20)
                                }
                            
                        }
                        .modifier(CustomTextM(fontName: UIFont.Montserrat.semiBold.fontName, fontSize: 16, fontColor: Color.init(hex: "2E2E2E")))
                        .modifier(ButtonStyle(buttonHeight: 50, buttonColor: Color.init(hex: "F2F2F2"), buttonRadius: 50)).padding(.horizontal)
                    }
                }
            }
            
            if ((touristPackageOffer.touristPackage?.packagePrice?.count ?? 0) > 0){
                Divider().padding(.vertical)
                Text("Price").modifier(CustomTextM(fontName: UIFont.Rubik.medium.fontName, fontSize: 22, fontColor: Color.init(hex: "2E2E2E")))
                Text(touristPackageOffer.touristPackage?.packagePrice ?? "").modifier(CustomTextM(fontName: UIFont.Rubik.bold.fontName, fontSize: 30, fontColor: Color.init(hex: "F57F20")))
            }
        }
    }
}

//struct IamonlydriverView_Previews: PreviewProvider {
//    static var previews: some View {
//        IamonlydriverView()
//    }
//}
