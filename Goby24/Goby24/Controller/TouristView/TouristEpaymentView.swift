//
//  TouristEpaymentView.swift
//  Goby24
//
//  Created by Jignesh Bhensadadiya on 8/9/21.
//

import SwiftUI

struct TouristEpaymentView: View {
    var tourPackage:Model_TouristPackage
    @State var isPayOnline:Bool = false
    @State var cardUrl:String = ""
    @State var agreeTermsSelectedText:String = ""
    @State var isWebview:Bool = false
    @State var paymentUrl:String = ""

    
    @EnvironmentObject var settings: UserSettings
    @Environment(\.presentationMode) var presentationMode

    
    func getTotal() -> Double{
        return getDoubleFromAny(self.tourPackage.packagePrice ?? "")
    }
    func getArrivalDate() -> Date{
        return Calendar.current.date(byAdding: .day, value: getIntegerFromAny(tourPackage.noOfDays ?? 0), to: Date())!
    }
    func getRideFareDetailsWithBaseFair(){
        
    }
    func bookRide(){
        var param:[String:Any] = [String:Any]()
        param["tourist_package"] = getStringFromAny(tourPackage.id ?? "")
        param["user"] = getStringFromAny(tourPackage.provider?.id ?? "")
        param["price"] = getStringFromAny(tourPackage.packagePrice ?? "")
        param["price_after_tax"] = getStringFromAny(tourPackage.packagePrice ?? "")
        param["booking_date"] = Date().getFormattedDate(formatter: "yyyy-MM-dd")//getStringFromAny(tourPackage.dateFrom ?? "")
        param["arrival_date"] = getArrivalDate().getFormattedDate(formatter: "yyyy-MM-dd")
        WebAccess.postDataWith(_Url: WebAccess.TOURIST_PACKAGES_BOOKING, _parameters: param) { (result) in
            switch (result){
            case .Success(let _data):
                print(_data)
                let res = _data["result"] as? [String:Any]
                self.makePayment(strId: getStringFromAny(res?["id"] ?? ""))
//                    ShowAlertWithCompletation(title: APPNAME, msg: "Tour Booked sucessfully", view: UIApplication.getTopViewController()!) { (result) in
//                        if (result == true){
//                           // self.presentationMode.wrappedValue.dismiss()
//
////                            let result = _data["result"] as? [String:Any] ?? [:]
////
////                            self.paymentUrl = result["payment_page_url"] as? String ?? ""
////
////                            self.isWebview  = true
//
//                        }
//                    }
                break
            case .Error(let msg):
                ShowAlert(title: APPNAME, msg: msg, view: UIApplication.getTopViewController()!)
                break
            }
        }
    }
    func makePayment(strId:String){
        var param:[String:Any] = [String:Any]()
        param["booking"] = getStringFromAny(strId)
        
        WebAccess.postDataWith(_Url: WebAccess.TOURIST_SPOT_PAYMENT, _parameters: param) { (result) in
            switch (result){
            case .Success(let _data):
                print(_data)
                
                                            let result = _data["result"] as? [String:Any] ?? [:]
                
                                            self.paymentUrl = result["payment_page_url"] as? String ?? ""
                
                                            self.isWebview  = true

                
//                if (offeredRoute.paymentMethod?.lowercased() == "cash"){
//                    self.bookingId = getStringFromAny(result["id"] ?? "")
//                    self.bookRideCashpayment(booking: getStringFromAny(result["id"] ?? ""))
//                }else{
//                    self.bookingId = getStringFromAny(result["id"] ?? "")
//
//                    self.bookRideEpayment(booking:getStringFromAny(result["id"] ?? ""))
//                }
                break
            case .Error(let msg):
                ShowAlert(title: APPNAME, msg: msg, view: UIApplication.getTopViewController()!)
                break
            }
        }
    }
    var body: some View {
        
        NavigationLink(destination: OfferPackagePaymentWebview(urlString:paymentUrl, isFinished: { (value) in
            if (value == true){
                self.settings.isMoveToTouristViewHome = true

            }
        }),isActive: $isWebview) {EmptyView()}.isDetailLink(false)

        
        if (isPayOnline == false){
            VStack(alignment: .leading,spacing:0){
                
                Text(Localizable.Payments.localized()).modifier(CustomTextM(fontName: UIFont.Montserrat.semiBold.fontName, fontSize: 16, fontColor: Color.white)).frame(width: UIScreen.main.bounds.size.width - 40, height: 40, alignment: .center).background(Color.init(hex: "00AEEF")).cornerRadius(radius: 6,corners: [.topLeft, .topRight])
                HStack{
                    
                    Text((Localizable.Total_price_for.localized()) + " " +  "\(tourPackage.noOfTourist ?? "-")" + " passangers").modifier(CustomTextM(fontName: UIFont.Montserrat.semiBold.fontName, fontSize: 16, fontColor: Color.init(hex: "707070")))
                    Spacer()
                    
                    Text(getStringFromAny(getTotal())  + " " + (self.tourPackage.currency ?? "")).modifier(CustomTextM(fontName: UIFont.Montserrat.semiBold.fontName, fontSize: 16, fontColor: Color.init(hex: "#F57F20")))
                }.padding()
                HStack{
                    Text("GOBY24 Services").modifier(CustomTextM(fontName: UIFont.Montserrat.semiBold.fontName, fontSize: 16, fontColor: Color.init(hex: "707070")))
                    Spacer()
                    Text("0.0 " + (self.tourPackage.currency ?? "")).modifier(CustomTextM(fontName: UIFont.Montserrat.semiBold.fontName, fontSize: 16, fontColor: Color.init(hex: "#F57F20")))
                }.padding(.horizontal,10)
                Divider().padding(.horizontal).padding(.top)
                HStack{
                    Text(Localizable.Total_charge.localized()).modifier(CustomTextM(fontName: UIFont.Montserrat.semiBold.fontName, fontSize: 16, fontColor: Color.init(hex: "707070")))
                    Spacer()
                    Text(getStringFromAny(getTotal()) + " " + (self.tourPackage.currency ?? "")).modifier(CustomTextM(fontName: UIFont.Montserrat.semiBold.fontName, fontSize: 16, fontColor: Color.init(hex: "#F57F20")))
                    
                }.padding(.horizontal).padding(.top,30)
                
                HStack{
                    RadioButtonGroup(items: [(Localizable.I_agree_to_terms_and_conditions.localized())], selectedId: agreeTermsSelectedText, isTextFirst: false) { (selectedItem) in
                        if (agreeTermsSelectedText != selectedItem){
                            agreeTermsSelectedText = selectedItem
                        }else{
                            agreeTermsSelectedText = ""
                        }
                    }
                }.padding(.vertical).padding(.horizontal)
                
                HStack{
                    Spacer()
                    Button(action: {
                        if (agreeTermsSelectedText.count > 0){
                            self.bookRide()
                        }
                    }){
                        Text(Localizable.Pay.localized())
                    }
                    .modifier(CustomTextM(fontName: UIFont.Poppins.regular.fontName, fontSize: 15, fontColor: Color.white))
                    .modifier(ButtonStyle(buttonHeight: 30, buttonColor: Color.init(hex: "00ADEF"), buttonRadius: 30))
                    .frame(width:140)
                    .padding(.horizontal)
                }
                
                Spacer()
            }.frame(width:UIScreen.main.bounds.size.width - 40 ,height: 290).background(RoundedRectangle(cornerRadius: 25).fill(Color.white).shadow(color: Color.black.opacity(0.12), radius: 3)).padding(.top,20)
        }else{
            VStack{
//                SwiftUIWebview(url:URL(string: "") ?? )
            }
        }

    }
}

