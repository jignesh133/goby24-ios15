//
//  CongratsForBookingView.swift
//  Goby24
//
//  Created by Jignesh Bhensadadiya on 7/21/21.
//

import SwiftUI

struct CongratsForBookingView: View {
    var body: some View {
        VStack{
            Spacer()
            VStack{
                Text("Congratulation ").padding(.top, 25).modifier(CustomTextM(fontName: UIFont.Rubik.medium.fontName, fontSize: 26, fontColor: Color.white)).padding(.horizontal).multilineTextAlignment(.center)
                
                
                Text("Your Booking is confirmed").padding(.top, 25).modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 16, fontColor: Color.white))
                
                
                
                Text("Rider will be present on specific location at 09:30 am to pick you up").padding(.top, 25).modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 16, fontColor: Color.white)).multilineTextAlignment(.center)
                Spacer()
            }.padding(.vertical).padding(.horizontal,50)
            .frame(width: .infinity, height: 300, alignment: .center)
            .background(Color.init(hex: "00A761"))
            .cornerRadius(25)
            Spacer()
            
            Button(action: {
                NavigationUtil.popToRootView()
            }){
                Text("Got It").frame(minWidth: 0,  maxWidth: .infinity)
            }
            .modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 16, fontColor: Color.init(hex: "2E2E2E" )))
            .modifier(ButtonStyle(buttonHeight: 30, buttonColor: Color.clear, buttonRadius: 30))
            .padding(.horizontal,40).padding(.vertical)
            .overlay(RoundedRectangle(cornerRadius: 30).stroke(Color.init(hex: "00ADEF" ), lineWidth: 1)).padding(.horizontal,40)
            .buttonStyle(PlainButtonStyle())
            
            Spacer()
        }.hiddenNavigationBarStyle()
    }
}

struct CongratsForBookingView_Previews: PreviewProvider {
    static var previews: some View {
        CongratsForBookingView()
    }
}
