//
//  iamalsoguideView.swift
//  Goby24
//
//  Created by Jignesh Bhensadadiya on 7/20/21.
//

import SwiftUI

struct CreateOfferPackageView: View {
    
    @State var nameOfPackage: String = ""
    @State var desc: String = ""
    
    @State var arrPackageList:[Model_TouristPackage] = [Model_TouristPackage]()
    @State var selectedPackage:Model_TouristPackage?
    @State var selectedCountry:Model_Countrys?
    @State var selectedRegion:Model_Region?
    
    @ObservedObject private var touristPackageOffer = TouristPackageOffer.shared
    
    @State var isDropDownOpen:Bool = false
    @State var isTouristPackageOpen:Bool = false
    @State var isStateOpen:Bool = false
    
    @State var isImagePickerCameraForCover:Bool = false
    @State var isImagePickerGalleryForCover:Bool = false
    
    @State var isImagePickerCameraForBanner:Bool = false
    @State var isImagePickerGalleryForBanner:Bool = false
    
    @State var isShowingActionSheetForBanner:Bool = false
    @State var isShowingActionSheetForCover:Bool = false
    
    
    @State var bannerImage:Image?
    @State var coverImage:Image?
    
    
    @State var startDate:Date = Date()
    @State var endDate:Date = Date()

    @State var addNewSpot:String?
    
    @State var currency: String = ""
    @State var price: String = ""
    
    
    @Environment(\.viewController) private var viewControllerHolder: UIViewController?
    
    @State var onlyDriverSelected: Bool = false
    init() {
        UIScrollView.appearance().keyboardDismissMode = .onDrag

    }
    //MARK: VALIDATE STRING
    func validateData() -> String {
        if (touristPackageOffer.strCountry?.removeWhiteSpace().count == 0){
            return "Please select country"
        }else if (selectedRegion?.name?.removeWhiteSpace().count == 0){
            return "Please select Region"
        }else if (touristPackageOffer.touristPackage?.packageName?.removeWhiteSpace().count == 0){
            return "Please enter package name"
        }else if (touristPackageOffer.availableStartDate.removeWhiteSpace().count == 0){
            return "please enter available start date"
        }else if (touristPackageOffer.availableEndDate.removeWhiteSpace().count == 0){
            return "please enter available end date"
        }else if (touristPackageOffer.availableStartTime.removeWhiteSpace().count == 0){
            return "please enter available Start Time"
        }else if (touristPackageOffer.availableEndTime.removeWhiteSpace().count == 0){
            return "please enter available End Time"
        }else if (touristPackageOffer.passanger?.removeWhiteSpace().count == 0){
            return "please insert number of passenger"
        }else if (coverImage == nil){
            return "please select cover image"
        }else if (bannerImage == nil){
            return "please select banner image"
        }else if (touristPackageOffer.passanger?.removeWhiteSpace().count == 0){
            return "Please select No of Tourist"
        }else if (touristPackageOffer.days?.removeWhiteSpace().count == 0){
            return "please select No Days you want to spent"
        }else if (price.removeWhiteSpace().count == 0){
            return "please insert price"
        }else if (currency.removeWhiteSpace().count == 0){
            return "please select currency"
        }
        else{
            return ""
        }
    }
    //MARK: MAKE API CALL
    func getTouristPackageList(){
        var param:[String:Any] = [String:Any]()
        param["country"] = touristPackageOffer.strCountry?.lowercased()
        
        WebAccess.getDataWith(_Url: WebAccess.TOURIST_PACKAGES, _parameters: param) { (result) in
            switch (result){
            case .Success(let _data):
                print(_data)
                do{
                    let result:[String:Any] = UtilsObjs.cleanJson(to: _data ) as? [String : Any] ?? [String:Any]()
                    print(result)
                    let data1 =  try JSONSerialization.data(withJSONObject: result["result"] ?? [] , options: JSONSerialization.WritingOptions.prettyPrinted)
                    let convertedString = String(data: data1, encoding: String.Encoding.utf8)
                    let jsonData = Data((convertedString?.utf8)!)
                    self.arrPackageList = try JSONDecoder().decode([Model_TouristPackage].self, from: jsonData)
                    if (self.arrPackageList.count == 0){
                        selectedPackage = nil
                        // selectedCountry = nil
                        touristPackageOffer.touristPackage = nil
                    }
                    NSLog("LOG")
                }catch(let error){
                    print(error)
                }
                
                break
            case .Error(let msg):
                ShowAlert(title: APPNAME, msg: msg, view: UIApplication.getTopViewController()!)
                break
            }
        }
    }
    
    func offerPackage(){
        
        let str = self.validateData()
        
        if (str.count > 0 ){
            ShowAlert(title: APPNAME, msg: str, view: UIApplication.getTopViewController()!)
            return
        }
        
        var param:[String:Any] = [String:Any]()
        param["package_name"] = nameOfPackage
        param["package_brief"] = touristPackageOffer.touristPackage?.packageBrief ?? ""
        
        param["package_cover"] = coverImage  
        param["package_banner"] = bannerImage
        
        param["package_price"] = price //selectedPackage?.packagePrice
        param["no_of_days"] = touristPackageOffer.days
        param["date_from"] = touristPackageOffer.availableStartDate
        param["date_to"] = touristPackageOffer.availableEndDate
        param["time_from"] = touristPackageOffer.availableStartTime
        param["time_to"] =  touristPackageOffer.availableEndTime
        param["no_of_tourist"] = touristPackageOffer.passanger
        param["country"] = touristPackageOffer.strCountry
        param["city"] =  selectedRegion?.name ?? ""
        param["currency"] =  currency
        if (addNewSpot?.removeWhiteSpace().count != 0 && (addNewSpot != nil)){
            param["new_tourist_spot"] = addNewSpot
            param["spot_id"] = ""
        }else{
            param["new_tourist_spot"] = ""
            param["spot_id"] =  getStringFromAny(touristPackageOffer.touristPackage?.spot?.id ?? "")
        }
        
        if (onlyDriverSelected == true){
            param["is_guide"] = false
        }
        if (onlyDriverSelected == false){
            param["is_guide"] = true
        }
        
        print(param)
        
        DispatchQueue.main.async {
            appDelegate?.startLoadingview()
        }
        
        WebAccess.postDataWithImage(_Url: WebAccess.TOURIST_PACKAGES, _parameters: param,isHideLoader: true) { (result) in
            switch (result){
            case .Success(let _data):
                print(_data)
                DispatchQueue.main.async {
                    appDelegate?.stopLoadingView()
                }
                ShowAlert(title: APPNAME, msg: "Package Offered Sucessfully", view: UIApplication.getTopViewController()!)
                break
            case .Error(let msg):
                ShowAlert(title: APPNAME, msg: msg, view: UIApplication.getTopViewController()!)
                DispatchQueue.main.async {
                    appDelegate?.stopLoadingView()
                }
                break
            }
        }
    }
    var body: some View {
        
        ScrollView{
            
            // LOCATION
            Group{
                
                HStack{
                    //Button
                    Button(action: {
                        onlyDriverSelected = true
                    }){
                        if (onlyDriverSelected == true){
                            HStack{
                                Image(systemName: "largecircle.fill.circle").frame(width: 15, height: 15).scaledToFit()
                                Text(" ")
                                Text(Localizable.I_am_only_a_driver.localized()).modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 13, fontColor: Color.init(hex: "00AEEF")))
                            }
                        }else{
                            HStack{
                                Image(systemName: "circle").frame(width: 15, height: 15).scaledToFit()
                                Text(" ")
                                Text(Localizable.I_am_only_a_driver.localized()).modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 13, fontColor: Color.init(hex: "00AEEF")))
                            }
                        }
                    }
                    Spacer()
                    //Button
                    Button(action: {
                        onlyDriverSelected = false
                    }){
                        if (onlyDriverSelected == true){
                            HStack{
                                Image(systemName: "circle").frame(width: 15, height: 15).scaledToFit()
                                Text(" ")
                                Text(Localizable.I_am_also_a_guide.localized()).modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 13, fontColor: Color.init(hex: "00AEEF")))
                            }
                        }else{
                            HStack{
                                Image(systemName: "largecircle.fill.circle").frame(width: 15, height: 15).scaledToFit()
                                Text(" ")
                                Text((Localizable.I_am_also_a_guide.localized())).modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 13, fontColor: Color.init(hex: "00AEEF")))
                            }
                        }
                    }
                    
                }.padding()
                
                
                //Button
                Button(action: {
                    // ACTION CLICKED
                    withAnimation{
                        isDropDownOpen.toggle()
                    }
                    
                }){
                    ZStack() {
                        Text(touristPackageOffer.strCountry ?? "Choose Location")
                        HStack() {
                            Spacer()
                            Image("down").padding(.trailing,20)
                        }
                    }
                }
                .modifier(CustomTextM(fontName: UIFont.Montserrat.semiBold.fontName, fontSize: 16, fontColor: Color.init(hex: "2E2E2E")))
                .modifier(ButtonStyle(buttonHeight: 50, buttonColor: Color.init(hex: "F2F2F2"), buttonRadius: 50))
                
                if (isDropDownOpen == true){
                    ScrollView{
                        VStack{
                            ForEach(0..<(appDelegate?.arrCountry.count ?? 0), id: \.self) { row in
                                Button(action: {
                                    touristPackageOffer.strCountry = (appDelegate?.arrCountry[row].countryName ?? "")
                                    selectedCountry = appDelegate?.arrCountry[row]
                                    isDropDownOpen = false
                                    
                                    self.getTouristPackageList()
                                }){
                                    Text(appDelegate?.arrCountry[row].countryName ?? "")
                                }
                                .modifier(CustomTextM(fontName: UIFont.Montserrat.semiBold.fontName, fontSize: 16, fontColor: Color.init(hex: "2E2E2E")))
                                .modifier(ButtonStyle(buttonHeight: 50, buttonColor: Color.init(hex: "F2F2F2"), buttonRadius: 0))
                            }
                        }
                    }.frame(maxHeight:150).cornerRadius(10).padding(.top,-40)
                }
                
                
                
                //Button
                Button(action: {
                    // ACTION CLICKED
                    withAnimation{
                        isStateOpen.toggle()
                    }
                }){
                    ZStack() {
                        Text(selectedRegion?.name ?? "Choose State")
                        HStack() {
                            Spacer()
                            Image("down").padding(.trailing,20)
                        }
                    }
                }
                .modifier(CustomTextM(fontName: UIFont.Montserrat.semiBold.fontName, fontSize: 16, fontColor: Color.init(hex: "2E2E2E")))
                .modifier(ButtonStyle(buttonHeight: 50, buttonColor: Color.init(hex: "F2F2F2"), buttonRadius: 50))
                
                
                if (isStateOpen == true){
                    ScrollView{
                        VStack{
                            ForEach(0..<(selectedCountry?.regions?.count ?? 0), id: \.self) { row in
                                Button(action: {
                                    withAnimation{
                                        isStateOpen.toggle()
                                    }
                                    selectedRegion = selectedCountry?.regions?[row]
                                    //                                            touristPackageOffer.strCountry = (appDelegate?.countryList[row] ?? "")
                                    //                                            isDropDownOpen = false
                                    //                                            self.getTouristPackageList()
                                }){
                                    Text(selectedCountry?.regions?[row].name ?? "")
                                }
                                .modifier(CustomTextM(fontName: UIFont.Montserrat.semiBold.fontName, fontSize: 16, fontColor: Color.init(hex: "2E2E2E")))
                                .modifier(ButtonStyle(buttonHeight: 50, buttonColor: Color.init(hex: "F2F2F2"), buttonRadius: 0))
                            }
                        }
                    }.frame(maxHeight:150).cornerRadius(10).padding(.top,-40)
                }
                
                
                
                Group{
                    //Button
                    Button(action: {
                        // ACTION CLICKED
                        withAnimation{
                            isTouristPackageOpen.toggle()
                        }
                    }){
                        ZStack() {
                            Text(touristPackageOffer.touristPackage?.packageName ?? "Choose Tourist Package")
                            HStack() {
                                Spacer()
                                Image("down").padding(.trailing,20)
                            }
                        }
                    }
                    .modifier(CustomTextM(fontName: UIFont.Montserrat.semiBold.fontName, fontSize: 16, fontColor: Color.init(hex: "2E2E2E")))
                    .modifier(ButtonStyle(buttonHeight: 50, buttonColor: Color.init(hex: "F2F2F2"), buttonRadius: 50))
                    
                    if (isTouristPackageOpen == true && self.arrPackageList.count > 0){
                        ScrollView{
                            VStack{
                                ForEach(0..<(self.$arrPackageList.wrappedValue.count ), id: \.self) { row in
                                    Button(action: {
                                        
                                        touristPackageOffer.touristPackage = self.arrPackageList[row]
                                        
                                        self.nameOfPackage = touristPackageOffer.touristPackage?.packageName ?? ""
                                        self.desc = touristPackageOffer.touristPackage?.packageBrief ?? ""
                                        self.price = touristPackageOffer.touristPackage?.packagePrice ?? ""
                                        isTouristPackageOpen = false
                                        
                                    }){
                                        Text(self.arrPackageList[row].packageName ?? "")
                                    }
                                    .modifier(CustomTextM(fontName: UIFont.Montserrat.semiBold.fontName, fontSize: 16, fontColor: Color.init(hex: "2E2E2E")))
                                    .modifier(ButtonStyle(buttonHeight: 50, buttonColor: Color.init(hex: "F2F2F2"), buttonRadius: 0))
                                }
                            }
                        }.frame(maxHeight:150).cornerRadius(10).padding(.top,-40)
                    }
                }
                Group{
                    VStack(alignment: .leading, spacing: 0){
                        ZStack{
                            Button(action: {
                                self.viewControllerHolder?.present(style: .overCurrentContext, transitionStyle: .crossDissolve) {
                                    AddNewSpotView { (item) in
                                        addNewSpot = item
                                    }
                                }
                            }){
                                HStack{
                                    if ((addNewSpot?.removeWhiteSpace().count ?? 0) > 0){
                                        Text(addNewSpot ?? Localizable.Add_New_Spot.localized()).padding(.leading,10)
                                            .modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 13, fontColor: .black))
                                        
                                    }else{
                                        Text(addNewSpot ?? Localizable.Add_New_Spot.localized()).padding(.leading,10)
                                            .modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 13, fontColor: Color.init(hex: "C4C4C4")))
                                        
                                    }
                                    Spacer()
                                }
                            }.frame(height:45)
                            .modifier(ButtonStyle(buttonHeight: 50, buttonColor: Color.init(hex: "F2F2F2"), buttonRadius: 50))
                            .onTapGesture {
                                self.viewControllerHolder?.present(style: .overCurrentContext, transitionStyle: .crossDissolve) {
                                    AddNewSpotView { (item) in
                                        addNewSpot = item
                                    }
                                }
                            }
                            
                        }.padding(.horizontal,5)
                    }
                }
                
            }.padding(.horizontal).padding(.vertical,10)
            
            
            Group{
                Text(Localizable.Name_of_the_package.localized()).modifier(CustomTextM(fontName: UIFont.Rubik.medium.fontName, fontSize: 19, fontColor: Color.init(hex: "2E2E2E")))
                CustomTextField(placeHolderText: "Package name",text: self.$nameOfPackage).padding()
                
                Text(Localizable.Descriptions.localized()).modifier(CustomTextM(fontName: UIFont.Rubik.medium.fontName, fontSize: 19, fontColor: Color.init(hex: "2E2E2E")))
                
                TextEditor(text: $desc)
                    .keyboardType(.default)
                    .multilineTextAlignment(.leading)
                    .lineLimit(nil)
                    .padding(EdgeInsets(top: 0, leading: 15, bottom: 0, trailing: 15))
                    .frame(width: .infinity, height: 100, alignment: .center)
                    .background(Color(red: 0.95, green: 0.95, blue: 0.95))
                    .cornerRadius(10)
                    .modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 16, fontColor: Color.black))
                    .textContentType(.none).padding()
                
                
                //                TextEditor( text: $desc)
                //                    .padding(EdgeInsets(top: 0, leading: 15, bottom: 0, trailing: 15))
                //                    .frame(width: .infinity, height: 100, alignment: .center)
                //                    .cornerRadius(10)
                //                    .modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 16, fontColor: Color.black))
                //                    .textContentType(.none).padding()
                
                
                HStack{
                    VStack{
                        Text(Localizable.Upload_Cover_Image.localized()).modifier(CustomTextM(fontName: UIFont.Montserrat.semiBold.fontName, fontSize: 14, fontColor: Color.init(hex: "2E2E2E")))
                        Button(action: {
                            isShowingActionSheetForCover = true
                        }){
                            VStack{
                                if (coverImage == nil){
                                    Image("Vector").resizable().frame(width: 70, height: 70, alignment: .center)
                                }else{
                                    coverImage?.resizable().frame(width: 70, height: 70, alignment: .center)
                                }
                            }
                        }
                        .actionSheet(isPresented: $isShowingActionSheetForCover) {
                            ActionSheet(title: Text(""), message: Text("Choose Option"), buttons: [
                                .default(Text("Take Photo")) { isImagePickerCameraForCover = true },
                                .default(Text("Choose Photo")) { isImagePickerGalleryForCover = true },
                                .cancel(){ isShowingActionSheetForCover = false}
                            ])
                        }
                        .sheet(isPresented: $isImagePickerCameraForCover) {
                            
                            OpenGallary(isShown: $isImagePickerCameraForCover, image: $coverImage, sourceType: .camera, onImagePicked: { (image) in
                                isImagePickerCameraForCover = false
                            })
                            
                        }
                        .sheet(isPresented: $isImagePickerGalleryForCover) {
                            
                            OpenGallary(isShown: $isImagePickerGalleryForCover, image: $coverImage, sourceType: .photoLibrary, onImagePicked: { (image) in
                                isImagePickerGalleryForCover = false
                            })
                            
                        }
                        
                    }.padding(.horizontal)
                    Spacer()
                    VStack{
                        Text(Localizable.Upload_Banner_Image.localized()).modifier(CustomTextM(fontName: UIFont.Montserrat.semiBold.fontName, fontSize: 14, fontColor: Color.init(hex: "2E2E2E")))
                        Button(action: {
                            isShowingActionSheetForBanner = true
                        }){
                            VStack{
                                if (bannerImage == nil){
                                    Image("Vector").resizable().frame(width: 70, height: 70, alignment: .center)
                                }else{
                                    bannerImage?.resizable().frame(width: 70, height: 70, alignment: .center)
                                }
                            }
                        }
                        
                        
                        .actionSheet(isPresented: $isShowingActionSheetForBanner) {
                            ActionSheet(title: Text(""), message: Text("Choose Option"), buttons: [
                                .default(Text("Take Photo")) { isImagePickerCameraForBanner = true },
                                .default(Text("Choose Photo")) { isImagePickerGalleryForBanner = true },
                                .cancel(){isShowingActionSheetForBanner = false}
                            ])
                        }
                        .sheet(isPresented: $isImagePickerCameraForBanner) {
                            
                            OpenGallary(isShown: $isImagePickerCameraForBanner, image: $bannerImage, sourceType: .camera, onImagePicked: { (image) in
                                isImagePickerCameraForBanner = false
                            })
                            
                        }
                        .sheet(isPresented: $isImagePickerGalleryForBanner) {
                            
                            OpenGallary(isShown: $isImagePickerGalleryForBanner, image: $bannerImage, sourceType: .photoLibrary, onImagePicked: { (image) in
                                isImagePickerGalleryForBanner = false
                            })
                            
                        }
                        
                    }.padding(.horizontal)
                }
                
            }
            
            Group{
                VStack(spacing: 0){
                    Text((Localizable.Number_of_Tourists.localized())).modifier(CustomTextM(fontName: UIFont.Rubik.medium.fontName, fontSize: 19, fontColor: Color.init(hex: "2E2E2E")))
                    
                    CustomSteper(count: touristPackageOffer.passanger ?? "0",maxCount:"7") { (count) in
                        if (getIntegerFromAny(count) <= 7 && getIntegerFromAny(count) > 0){
                            touristPackageOffer.passanger = getStringFromAny(count)
                        }
                    }.padding(.top)
                    
                    Text((Localizable.Choose_number_of_days_you_want_to_spent.localized())).modifier(CustomTextM(fontName: UIFont.Rubik.medium.fontName, fontSize: 19, fontColor: Color.init(hex: "2E2E2E"))).padding(.top)
                    
                    CustomSteper(count: touristPackageOffer.days ?? "0") { (count) in
                        touristPackageOffer.days = getStringFromAny(count)
                    }.padding(.top)
                }
            }
            
            Group{
                Text((Localizable.Choose_your_available_date_range.localized())).modifier(CustomTextM(fontName: UIFont.Rubik.medium.fontName, fontSize: 19, fontColor: Color.init(hex: "2E2E2E"))).padding(.top)
                HStack{
                    VStack{
                        Text((Localizable.Start.localized())).modifier(CustomTextM(fontName: UIFont.Rubik.medium.fontName, fontSize: 16, fontColor: Color.init(hex: "F57F20")))
                        
                        
                        //Button
                        Button(action: {
                            RPicker.selectDate(title: "Select available start date",minDate: Date(), didSelectDate: { (selectedDate) in
                                touristPackageOffer.availableStartDate = selectedDate.getFormattedDate(formatter: "YYYY-MM-dd")
                                startDate = selectedDate
                                if (startDate >= endDate){
                                    endDate = startDate
                                    touristPackageOffer.availableEndDate = selectedDate.getFormattedDate(formatter: "YYYY-MM-dd")

                                }
                            })
                        }){
                            HStack() {
                                Spacer()
                                
                                Text(touristPackageOffer.availableStartDate)
                                Spacer()
                                Image("down").padding(.trailing,20)
                            }
                        }
                        .modifier(CustomTextM(fontName: UIFont.Montserrat.semiBold.fontName, fontSize: 16, fontColor: Color.init(hex: "2E2E2E")))
                        .modifier(ButtonStyle(buttonHeight: 50, buttonColor: Color.init(hex: "F2F2F2"), buttonRadius: 50)).padding(.horizontal)
                    }
                    VStack(spacing:20){
                        Text("End").modifier(CustomTextM(fontName: UIFont.Rubik.medium.fontName, fontSize: 16, fontColor: Color.init(hex: "F57F20")))
                        
                        
                        //Button
                        Button(action: {
                            RPicker.selectDate(title: "Select available End date",minDate: startDate, didSelectDate: { (selectedDate) in
                                touristPackageOffer.availableEndDate = selectedDate.getFormattedDate(formatter: "YYYY-MM-dd")
                                endDate = selectedDate

                            })
                            
                        }){
                            HStack() {
                                Spacer()
                                
                                Text(touristPackageOffer.availableEndDate)
                                Spacer()
                                Image("down").padding(.trailing,20)
                            }
                        }
                        .modifier(CustomTextM(fontName: UIFont.Montserrat.semiBold.fontName, fontSize: 16, fontColor: Color.init(hex: "2E2E2E")))
                        .modifier(ButtonStyle(buttonHeight: 50, buttonColor: Color.init(hex: "F2F2F2"), buttonRadius: 50)).padding(.horizontal)
                    }
                }
            }
            
            Group{
                Text((Localizable.Choose_your_available_time_range.localized())).modifier(CustomTextM(fontName: UIFont.Rubik.medium.fontName, fontSize: 19, fontColor: Color.init(hex: "2E2E2E"))).padding(.top)
                HStack{
                    VStack{
                        Text(Localizable.Start.localized()).modifier(CustomTextM(fontName: UIFont.Rubik.medium.fontName, fontSize: 16, fontColor: Color.init(hex: "F57F20")))
                        
                        
                        //Button
                        Button(action: {
                            RPicker.selectDate(title: "Select available Start Time",datePickerMode: .time,  didSelectDate: { (selectedDate) in
                                touristPackageOffer.availableStartTime = selectedDate.getFormattedDate(formatter: "HH:mm")
                            })
                        }){
                            HStack() {
                                Spacer()
                                
                                Text(touristPackageOffer.availableStartTime)
                                Spacer()
                                Image("down").padding(.trailing,20)
                            }
                        }
                        .modifier(CustomTextM(fontName: UIFont.Montserrat.semiBold.fontName, fontSize: 16, fontColor: Color.init(hex: "2E2E2E")))
                        .modifier(ButtonStyle(buttonHeight: 50, buttonColor: Color.init(hex: "F2F2F2"), buttonRadius: 50)).padding(.horizontal)
                    }
                    VStack(spacing:20){
                        Text(Localizable.End.localized()).modifier(CustomTextM(fontName: UIFont.Rubik.medium.fontName, fontSize: 16, fontColor: Color.init(hex: "F57F20")))
                        
                        
                        //Button
                        Button(action: {
                            RPicker.selectDate(title: "Select available End Time",datePickerMode: .time,  didSelectDate: { (selectedDate) in
                                touristPackageOffer.availableEndTime = selectedDate.getFormattedDate(formatter: "HH:mm")
                            })
                            
                        }){
                            HStack() {
                                Spacer()
                                
                                Text(touristPackageOffer.availableEndTime)
                                
                                Spacer()
                                Image("down").padding(.trailing,20)
                            }
                            
                        }
                        .modifier(CustomTextM(fontName: UIFont.Montserrat.semiBold.fontName, fontSize: 16, fontColor: Color.init(hex: "2E2E2E")))
                        .modifier(ButtonStyle(buttonHeight: 50, buttonColor: Color.init(hex: "F2F2F2"), buttonRadius: 50)).padding(.horizontal)
                    }
                }
            }
            
            
            if ((selectedPackage?.packagePrice?.count ?? 0) > 0){
                Divider().padding(.vertical)
                Text(Localizable.Price.localized()).modifier(CustomTextM(fontName: UIFont.Rubik.medium.fontName, fontSize: 22, fontColor: Color.init(hex: "2E2E2E")))
                
                Text(selectedPackage?.packagePrice ?? "--").modifier(CustomTextM(fontName: UIFont.Rubik.bold.fontName, fontSize: 30, fontColor: Color.init(hex: "F57F20")))
            }
            Text((Localizable.Price.localized())).modifier(CustomTextM(fontName: UIFont.Rubik.medium.fontName, fontSize: 19, fontColor: Color.init(hex: "2E2E2E"))).padding(.top)
            
            priceTextField(text: $price, currency: $currency, placeHolderText: "price", fontSize: 16).padding()
            
            Button(action: {
                // MAKE API CALL
                self.offerPackage()
            }){
                Text(Localizable.Submit.localized())
            }
            .modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 16, fontColor: Color.white))
            .modifier(ButtonStyle(buttonHeight: 50, buttonColor: Color.init(hex: "00ADEF"), buttonRadius: 50))
            .padding()
            
        }.padding()//.resignKeyboardOnDragGesture()
    }
}
