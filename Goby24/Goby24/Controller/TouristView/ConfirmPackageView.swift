//
//  ConfirmPackageView.swift
//  Goby24
//
//  Created by Jignesh Bhensadadiya on 7/17/21.
//

import SwiftUI

struct ConfirmPackageView: View {
    @State var selection: Int? = nil

    var body: some View {
        VStack{
            Text("Confirmation Page").padding(.top, 25).modifier(CustomTextM(fontName: UIFont.Poppins.semiBold.fontName, fontSize: 26, fontColor: Color.init(hex: "2E2E2E")))
            VStack{
                Text("Ranthambore National Park").padding(.top, 25).modifier(CustomTextM(fontName: UIFont.Poppins.semiBold.fontName, fontSize: 23, fontColor: Color.init(hex: "2E2E2E"))).padding(.horizontal).multilineTextAlignment(.center)
                
                HStack{
                    Text("Number of days").padding(.top, 25).modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 14, fontColor: Color.init(hex: "2E2E2E")))
                    
                    Text("..............................1").padding(.top, 25).modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 14, fontColor: Color.init(hex: "2E2E2E")))
                    
                }
                HStack{
                    Text("Number of passanger ").padding(.top, 25).modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 14, fontColor: Color.init(hex: "2E2E2E")))
                    
                    Text(" ............ 2").padding(.top, 25).modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 14, fontColor: Color.init(hex: "2E2E2E")))
                    
                }
                
                Text("Your package price is ").padding(.top, 25).modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 14, fontColor: Color.init(hex: "2E2E2E")))
                
                Text("4300TK ").padding(.top, 25).modifier(CustomTextM(fontName: UIFont.Rubik.medium.fontName, fontSize: 26, fontColor: Color.init(hex: "F57F20")))
                
            }.padding(.vertical).padding(.horizontal,40)
            .background(Color.init(hex: "F2F2F2"))
            .cornerRadius(10)
            .overlay(RoundedRectangle(cornerRadius: 10).stroke(Color.gray, lineWidth: 1))
            
            Spacer()
            NavigationLink(destination: ConfirmPackageLastView(), tag: 0, selection: $selection) {
                Button(action: {
                    selection = 0
                }){
                    Text("Confirm this package").frame(minWidth: 0,  maxWidth: .infinity)
                }
                .modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 16, fontColor: Color.white))
                .modifier(ButtonStyle(buttonHeight: 50, buttonColor: Color.init(hex: "00ADEF"), buttonRadius: 50))
                .padding(.horizontal,40).padding(.vertical)
                .buttonStyle(PlainButtonStyle())
                
            }
            
            Spacer()
        }
    }
}

struct ConfirmPackageView_Previews: PreviewProvider {
    static var previews: some View {
        ConfirmPackageView()
    }
}
