//
//  TouristPackageOfferItem.swift
//  Goby24
//
//  Created by Jignesh Bhensadadiya on 7/17/21.
//

import SwiftUI
import SDWebImageSwiftUI
struct TouristPackageOfferItem: View {
    @State var rideData:Model_TouristPackage?
    @State var isEditTour:Bool = false
    
    var body: some View {
        
        VStack {
            NavigationLink(destination:TouristOfferEditView(tourData:self.rideData),isActive: $isEditTour) {EmptyView()}.isDetailLink(false)

            ZStack(alignment:.bottomLeading){
                
                ZStack{
                    VStack{
                        let urlString = IMAGEURL + getStringFromAny(rideData?.packageBanner ?? "")
                        WebImage(url: URL.init(string: urlString ))
                            .placeholder(Image(systemName: "photo")) // Placeholder Image
                            .resizable()
                            .aspectRatio(contentMode: .fill).padding(.bottom,30)
                            .frame(width: (UIScreen.main.bounds.size.width / 2) - 30)
                            .frame(height: ((UIScreen.main.bounds.size.width / 2) - 30) * 1.15)

                            .aspectRatio(0.85, contentMode: .fit)

                    }.clipShape(RoundedRectangle(cornerRadius: 21, style: .continuous)).shadow(radius: 10)
                    
                    if(getId() == getStringFromAny(rideData?.provider?.id ?? "")){
                        VStack{
                            HStack{
                                Spacer()

                                //Button
                                Button(action: {
                                    // EDIT ICON TouristOfferEditView
                                    isEditTour = true
                                }){
                                    Image("ic_editIcon")
                                }.frame(width: 40, height: 32)
                            }
                            Spacer()
                            HStack{
                                Image("calendartourist")
                                Text( ((rideData?.noOfDays ?? "-") + " Days")).modifier(CustomTextM(fontName: UIFont.Poppins.regular.fontName, fontSize: 14, fontColor: Color.white))
                            }
                            Spacer()
                        }
                    }else{
                        VStack{
                            HStack{
                                Image("calendartourist")
                                Text( ((rideData?.noOfDays ?? "-") + " Days")).modifier(CustomTextM(fontName: UIFont.Poppins.regular.fontName, fontSize: 14, fontColor: Color.white))
                            }
                        }
                    }

                    

                }
                VStack{
                    Text(rideData?.packageName ?? "-").modifier(CustomTextM(fontName: UIFont.Poppins.semiBold.fontName, fontSize: 16, fontColor: Color.black))
                    HStack{
                        Text( rideData?.packagePrice ?? "-") .modifier(CustomTextM(fontName: UIFont.Rubik.bold.fontName, fontSize: 16, fontColor: Color.init(hex: "F57F20")))

                        Text( rideData?.currency ?? "").modifier(CustomTextM(fontName: UIFont.Rubik.bold.fontName, fontSize: 16, fontColor: Color.init(hex: "F57F20")))

                    }
                        
                }.frame(minWidth: 0, maxWidth: .infinity).frame(height: 60).background(RoundedRectangle(cornerRadius: 60).fill(Color.white).shadow(color: Color.black.opacity(0.12), radius: 3))
            }
        }
    }
}
