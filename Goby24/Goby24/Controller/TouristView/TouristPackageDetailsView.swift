//
//  TouristPackageDetailsView.swift
//  Goby24
//
//  Created by Jignesh Bhensadadiya on 7/21/21.
//

import SwiftUI
import SDWebImageSwiftUI

struct TouristPackageDetailsView: View {
    
    @State var selectedItemIndex: Int? = nil
    @State var tourData:Model_TouristPackage?  
    @State var journyDate:Date = Date()
    @State var journyDateString:String = ""
    @State var startJourney:String = ""
    @State var endJourney:String = ""
      
    @State var isOpenStartLocation:Bool = false
    @State var isOpenEndLocation:Bool = false
    
    @State var startLocation:Model_searchLocation?
    @State var endLocation:Model_searchLocation?
    @State var noofpassenger:String = ""
    
    @ObservedObject private var touristOfferBook = TouristOfferBook.shared

    var body: some View {
        ScrollView{
            
            NavigationLink(destination:SearchLocation(strText: self.startLocation?.structuredFormatting?.mainText ?? "", isNeedToPopView:true, onLocationSelect: { (location,isClear) in
                if (isClear == false){
                    startLocation = location
                    startJourney = location?.structuredFormatting?.mainText ?? ""
                }else{
                    startLocation = nil
                    startJourney =  ""
                }
            }),isActive: $isOpenStartLocation) {EmptyView()}.isDetailLink(false)
            
            
            NavigationLink(destination:SearchLocation(strText: self.endLocation?.structuredFormatting?.mainText ?? "", isNeedToPopView:true, onLocationSelect: { (location,isClear) in
                if (isClear == false){
                    
                    endLocation = location
                    endJourney = location?.structuredFormatting?.mainText ?? ""
                }else{
                    endLocation = nil
                    endJourney =  ""
                    
                }
            }),isActive: $isOpenEndLocation) {EmptyView()}.isDetailLink(false)
            
            
            VStack{
                Text(tourData?.packageName ?? "").modifier(CustomTextM(fontName: UIFont.Rubik.medium.fontName, fontSize: 26, fontColor: Color.init(hex: "2E2E2E"))).padding(.horizontal,10)
                VStack{
                    
                    let urlString = IMAGEURL + getStringFromAny(tourData?.packageCover ?? "")
                    WebImage(url: URL.init(string: urlString ))
                        .placeholder(Image(systemName: "photo")) // Placeholder Image
                        .resizable()
                        .aspectRatio(contentMode: .fill)
                        .frame(width: (UIScreen.main.bounds.size.width ) - 50)
                    
                    
                }.clipShape(RoundedRectangle(cornerRadius: 21, style: .continuous)).shadow(radius: 10).padding()
                
                VStack{
                    Text(tourData?.packageBrief ?? "").padding().modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 15, fontColor: Color.init(hex: "2E2E2E")))
                    
                    
                    VStack(spacing:0){
                        
                        HStack(spacing:5){
                            Image("calendar")
                            Text((tourData?.noOfDays ?? "0") + " Days ").modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 15, fontColor: Color.init(hex: "#00AEEF")))
                            Image("group")
                            Text( (tourData?.noOfTourist ?? "-") + " Tourist (Max)").modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 15, fontColor: Color.init(hex: "#00AEEF")))
                            Spacer()
                        }.padding(.horizontal)
                        
                        HStack(spacing:05){
                            Image("clock")
                            HStack{
                                Text(tourData?.timeFrom ?? "").modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 15, fontColor: Color.init(hex: "00AEEF")))
                                Text(" - ").modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 15, fontColor: Color.init(hex: "00AEEF")))
                                Text(tourData?.timeTo ?? "").modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 15, fontColor: Color.init(hex: "00AEEF")))
                            }
                            // Text(" 09:00am - 05:00pm").modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 15, fontColor: Color.init(hex: "#00AEEF")))
                            
                            Spacer()
                            //Button
                            Button(action: {
                                selectedItemIndex = 0
                                
                            }){
                                HStack{
                                    Text(tourData?.packagePrice ?? "")
                                    Text(tourData?.currency ?? "")
                                    
                                }
                            }
                            
                            .modifier(CustomTextM(fontName: UIFont.Rubik.bold.fontName, fontSize: 16, fontColor: Color.init(hex: "F57F20")))
                            .modifier(ButtonStyle(buttonHeight: 40, buttonColor: Color.white, buttonRadius: 10))
                            .overlay(RoundedRectangle(cornerRadius: 40).stroke(Color.gray, lineWidth: 1))
                            .frame(width:150)
                        }.padding(.vertical).padding(.leading)
                    }.padding(.horizontal)
                }.overlay(RoundedRectangle(cornerRadius: 10).stroke(Color.gray, lineWidth: 1)).padding()
                Spacer()
                
                if (getId() != getStringFromAny(tourData?.provider?.id ?? "")){
                    
                    Divider()
                    
                    VStack(alignment:.leading){
                        
                        Text(" Journey Date").modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 13, fontColor: Color.init(hex: "00AEEF"))).padding(.leading,8)
                        
                        ZStack{
                            
                            Button(action: {
                                RPicker.selectDate(title: "Select Journey Date",datePickerMode: .date,minDate: tourData?.dateFrom?.date(),maxDate:tourData?.dateTo?.date(), didSelectDate: { (selectedDate) in
                                    journyDate = selectedDate
                                    journyDateString = journyDate.getFormattedDate(formatter: "yyyy-MM-dd")
                                })
                                
                            }){
                                HStack() {
                                    if journyDateString.removeWhiteSpace().count > 0{
                                        Text(journyDateString ).padding(.leading,10)
                                            .modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 13, fontColor: .black))
                                        
                                    }else{
                                        Text("Select Journey Date").padding(.leading,10)
                                            .modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 13, fontColor: Color.init(hex: "C4C4C4")))
                                    }
                                    Spacer()
                                    Image("calender").padding(.trailing,10)
                                }
                            }.frame(height:45)
                            .modifier(ButtonStyle(buttonHeight: 50, buttonColor: Color.init(hex: "F2F2F2"), buttonRadius: 50))
                        }
                        
                        Text(Localizable.Start_journey_from.localized())
                            .modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 13, fontColor: Color.init(hex: "00AEEF"))).padding(.leading,8)
                        
                        TextField("Ex: Chittagong bus.. ", text: $startJourney, onEditingChanged: { (change) in
                            if change == true{
                                UIApplication.shared.endEditing()
                                isOpenStartLocation = true
                            }
                        })
                        .textFieldStyle( MyTextFieldStyle(fontSize: 13))
                        .keyboardType(.default)
                        .padding(.horizontal,5)
                        
                        
                        Text(Localizable.End_Journey_to.localized()).modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 13, fontColor: Color.init(hex: "00AEEF"))).padding(.leading,8)
                        
                        TextField("Ex: Chittagong bus.. ", text: $endJourney, onEditingChanged: { (change) in
                            if change == true{
                                UIApplication.shared.endEditing()
                                isOpenEndLocation = true
                            }
                        })
                        .textFieldStyle(MyTextFieldStyle(fontSize: 13))
                        .keyboardType(.default)
                        .padding(.horizontal,5)
                        
                        Text(Localizable.No_of_passengers.localized()).modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 13, fontColor: Color.init(hex: "00AEEF"))).padding(.leading,8)
                        
                        TextField("Ex: 1 ", text: $noofpassenger)
                            .textFieldStyle(MyTextFieldStyle(fontSize: 13))
                            .keyboardType(.numberPad)
                            .padding(.horizontal,5)
                            .frame(width: 125)
                        
                    }.padding()
                    
                    
                    
                    NavigationLink(destination: TouristPaymentView( touristPackage: tourData!), tag: 0, selection: $selectedItemIndex) {
                        Button(action: {
                            if (journyDateString.removeWhiteSpace().count == 0){
                                ShowAlert(title: APPNAME, msg: "Please select journy date", view: UIApplication.getTopViewController()!)
                            }else if (noofpassenger.removeWhiteSpace().count != 0 && getIntegerFromAny(noofpassenger.removeWhiteSpace()) > 7 ){
                                ShowAlert(title: APPNAME, msg: "passenger can't be more than 7", view: UIApplication.getTopViewController()!)
                            }else{
                                touristOfferBook.jouneyDate = journyDateString
                                touristOfferBook.startJournyLocation = startJourney
                                touristOfferBook.endJournyLocation = endJourney
                                touristOfferBook.passanger = noofpassenger
                                selectedItemIndex = 0
                            }
                        }){
                            Text(Localizable.Confirm.localized())
                        }
                        .modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 16, fontColor: Color.white))
                        .modifier(ButtonStyle(buttonHeight: 50, buttonColor: Color.init(hex: "00ADEF"), buttonRadius: 10))
                        .padding()
                    }
                }
            }
        }.showNavigationBarWithBackStyle()
    }
}

struct TouristPackageDetailsView_Previews: PreviewProvider {
    static var previews: some View {
        TouristPackageDetailsView()
    }
}
