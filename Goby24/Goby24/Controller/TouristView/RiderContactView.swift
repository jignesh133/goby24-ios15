//
//  RiderContactView.swift
//  Goby24
//
//  Created by Jignesh Bhensadadiya on 12/16/21.
//
import SwiftUI
import NavigationStack

struct RiderContactView: View {
    var receiverId: String = ""
    @State var contactMsg: String = ""
    
    @Environment(\.presentationMode) var presentationMode
    
    func sendMessage(completion: @escaping(Bool) -> Void) {
        var param:[String:Any] = [String:Any]()
        param["message"] = contactMsg
        param["receiver"] = receiverId
        
        WebAccess.postDataWith(_Url: WebAccess.CHAT_DETAILS_HOME, _parameters: param) { (result) in
            switch (result){
            case .Success(let _data):
                print(_data)
                completion(true)
                break
            case .Error(let msg):
                ShowAlert(title: APPNAME, msg: msg, view: UIApplication.getTopViewController()!)
                break
            }
        }
    }
    var body: some View {
        VStack{
            
            Text("Start Chatting").modifier(CustomTextM(fontName: UIFont.Rubik.medium.fontName, fontSize: 26, fontColor: Color.init(hex: "2E2E2E"))).multilineTextAlignment(.center).padding()
            
            Group{
                VStack{
                    TextEditor(text: $contactMsg).modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 16, fontColor: Color.black)).textContentType(.none)
                    Spacer()
                }.padding()
            }.frame(width: 275, height: 126, alignment: .center).background(Color.init(hex: "F5F5F5")).clipShape(RoundedRectangle(cornerRadius: 14)).padding()
            Spacer()
            Button(action: {
                self.sendMessage { (result) in
                    if (result){
                        ShowAlertWithCompletation(title: APPNAME, msg: "Sucessfully send", view: UIApplication.getTopViewController()!) { (result) in
                            self.presentationMode.wrappedValue.dismiss()
                        }
                    }
                }
                
            }){
                Text(Localizable.Send.localized())
            }
            .modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 16, fontColor: Color.white))
            .modifier(ButtonStyle(buttonHeight: 40, buttonColor: Color.init(hex: "00ADEF"), buttonRadius: 40))
            .padding(.horizontal,40)
            Spacer()
            
        }.showNavigationBarWithBackStyle()
    }
}

struct RiderContactView_Previews: PreviewProvider {
    static var previews: some View {
        RiderContactView()
    }
}


 
