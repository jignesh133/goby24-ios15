//
//  RequestTouristView.swift
//  Goby24
//
//  Created by Jignesh Bhensadadiya on 7/17/21.
//

import SwiftUI

struct RequestTouristView: View {
    
    @State private var isExpanded: Bool = false
    
    var body: some View {
        VStack{
            VStack{
                Text(Localizable.Tourist_Package.localized()).padding(.top, 0).modifier(CustomTextM(fontName: UIFont.Rubik.medium.fontName, fontSize: 26, fontColor: Color.init(hex: "2E2E2E"))).padding()
                HStack{
                    Button(action: {
                        
                    }){
                        Image("filter")
                        
                    }
                    .modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 16, fontColor: Color.white))
                    .modifier(ButtonStyle(buttonHeight: 50, buttonColor: Color.clear, buttonRadius: 10))
                    .frame(width:50)
                    
                    Spacer()
                    Button(action: {
                        
                    }){
                        Image("search")
                    }
                    .modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 16, fontColor: Color.white))
                    .modifier(ButtonStyle(buttonHeight: 50, buttonColor: Color.clear, buttonRadius: 10))
                    .frame(width:50)
                    
                }
                
                ScrollView{
                    
                    Image("park").padding()
                    
                    VStack(alignment:.leading){
                        
                        Text("Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.").modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 14, fontColor: Color.init(hex: "2E2E2E"))).padding(.horizontal,40)
                            
                            .lineLimit(isExpanded ? nil : 3)
                        Button(action: {
                            isExpanded.toggle()
                        }) {
                            Text(isExpanded ? "less" : "read more")
                                .modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 14, fontColor: Color.init(hex: "00AEEF")))
                                .padding(.horizontal, 25)
                                .background(Color.white)
                        }.padding(.horizontal)
                    }
                    
                    Text(Localizable.Choose_number_of_days_you_want_to_spent.localized()).padding(.top, 50.0).modifier(CustomTextM(fontName: UIFont.Rubik.medium.fontName, fontSize: 26, fontColor: Color.init(hex: "2E2E2E"))).padding(.vertical).multilineTextAlignment(.center).padding(.horizontal,40)
                    
                    CustomSteper(count: "1") { (count) in
                        
                    }
                    Text(Localizable.Choose_no_of_passengers.localized()).padding(.top, 50.0).modifier(CustomTextM(fontName: UIFont.Rubik.medium.fontName, fontSize: 26, fontColor: Color.init(hex: "2E2E2E"))).padding(.vertical).multilineTextAlignment(.center).padding(.horizontal,40)
                    
                    CustomSteper(count: "1") { (count) in
                        
                    }

                    
                    Spacer()
                }
                //Button
                Button(action: {
                    print("CONFIRM BUTTON ACTION CLICKED")
                   
                }){
                    Text(Localizable.Confirm.localized())
                }
                .modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 16, fontColor: Color.white))
                .modifier(ButtonStyle(buttonHeight: 50, buttonColor: Color.init(hex: "00ADEF"), buttonRadius: 50))
                .padding(.vertical).padding(.horizontal,30)
            }
        }.showNavigationBarWithBackStyle()
    }
}

struct RequestTouristView_Previews: PreviewProvider {
    static var previews: some View {
        RequestTouristView()
    }
}
