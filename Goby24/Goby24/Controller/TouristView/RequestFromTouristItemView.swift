//
//  RequestFromTouristItemView.swift
//  Goby24
//
//  Created by Jignesh Bhensadadiya on 7/17/21.
//

import SwiftUI

struct RequestFromTouristItemView: View {
    var body: some View {
            
            VStack{
                ZStack{
                    Image("bgimage")
                    HStack{
                        Image("calendartourist")
                        Text("25th Days")                            .modifier(CustomTextM(fontName: UIFont.Poppins.regular.fontName, fontSize: 14, fontColor: Color.white))
                    }
                }.clipShape(RoundedRectangle(cornerRadius: 21, style: .continuous)).shadow(radius: 10)
            }
        
            
//            VStack{
//                Text("Sundarban").modifier(CustomTextM(fontName: UIFont.Poppins.semiBold.fontName, fontSize: 16, fontColor: Color.init(hex: "#2E2E2E")))
//                Text("4300TK").modifier(CustomTextM(fontName: UIFont.Rubik.bold.fontName, fontSize: 16, fontColor: Color.init(hex: "F57F20")))
//                
//            }.background(Color.white).clipShape(RoundedRectangle(cornerRadius: 21, style: .continuous)).shadow(radius: 10).padding(.top)

        }
    
}

//VStack{
//    Text("Sundarban").modifier(CustomTextM(fontName: UIFont.Poppins.semiBold.fontName, fontSize: 16, fontColor: Color.init(hex: "#2E2E2E")))
//    Text("4300TK").modifier(CustomTextM(fontName: UIFont.Rubik.bold.fontName, fontSize: 16, fontColor: Color.init(hex: "F57F20")))
//
//}.background(Color.white).clipShape(RoundedRectangle(cornerRadius: 21, style: .continuous)).shadow(radius: 10).padding(.top)

//VStack{
//    Text("Sundarban").modifier(CustomTextM(fontName: UIFont.Poppins.semiBold.fontName, fontSize: 14, fontColor: Color.white))
//    Text("4300TK").modifier(CustomTextM(fontName: UIFont.Poppins.regular.fontName, fontSize: 14, fontColor: Color.white))
//
//}
//
//    }
//
////        VStack {
////            ZStack{
////                Image("bgimage")
////                Image("transparentlayer")
/////                VStack{
////                    Spacer()
////                    HStack{
////                        Image("calendartourist")
////                        Text("25th Days")                            .modifier(CustomTextM(fontName: UIFont.Poppins.regular.fontName, fontSize: 14, fontColor: Color.white))
////                    }
//////                    Spacer()
//////                    VStack{
//////                        Text("Sundarban").modifier(CustomTextM(fontName: UIFont.Poppins.semiBold.fontName, fontSize: 16, fontColor: Color.black))
//////                        Text("25th Days")                            .modifier(CustomTextM(fontName: UIFont.Rubik.bold.fontName, fontSize: 16, fontColor: Color.init(hex: "F57F20")))
//////                    }.background(RoundedRectangle(cornerRadius: 25).fill(Color.white).shadow(color: Color.black.opacity(0.12), radius: 3))
//////                    Spacer()
//////                }
////                Spacer()
////            }.clipShape(RoundedRectangle(cornerRadius: 21, style: .continuous)).shadow(radius: 1).padding(.top)
////            Spacer()
////        }
////
////
////    }
//}

struct RequestFromTouristItemView_Previews: PreviewProvider {
    static var previews: some View {
        RequestFromTouristItemView()
    }
}
