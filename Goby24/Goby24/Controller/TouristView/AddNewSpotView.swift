//
//  AddNewSpotView.swift
//  Goby24
//
//  Created by Jignesh Bhensadadiya on 8/15/21.
//

import SwiftUI

struct AddNewSpotView: View {
    @State var spotName: String = ""
    let onAddClicked: (String) -> Void
    @Environment(\.viewController) private var viewControllerHolder: UIViewController?

    var body: some View {
        
        VStack{
            VStack{
                HStack{
                    Text((Localizable.Add_New_Spot.localized())).modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 12, fontColor: Color.init(hex: "00AEEF")))
                    Spacer()
                }
                CustomTextField(placeHolderText: (Localizable.Write_a_new_spot_name.localized()),text: self.$spotName,fontSize: 12)
                HStack{
                    Button(action: {
                        print("Cancel")
                        self.viewControllerHolder?.dismiss(animated: true, completion: nil)
                    }) {
                        Text(Localizable.Cancel.localized())
                    }
                    .modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 12, fontColor: Color.init(hex: "2E2E2E")))
                    .modifier(ButtonStyle(buttonHeight: 40, buttonColor: Color.init(hex: "D0D0D0"), buttonRadius: 40))
                    .padding(10)
                    
                    Button(action: {
                        print("Add")
                        onAddClicked(spotName)
                        self.viewControllerHolder?.dismiss(animated: true, completion: nil)

                    }) {
                        Text(Localizable.Add.localized())
                    }
                    .modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 12, fontColor: Color.white))
                    .modifier(ButtonStyle(buttonHeight: 40, buttonColor: Color.init(hex: "00AEEF"), buttonRadius: 40))
                    .padding(10)
                }

            }.padding()
            
        }.background(Color.white).cornerRadius(10).frame(width: 250, height: 140, alignment: .center)
        
    }
}

struct AddNewSpotView_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            AddNewSpotView { (item) in
                
            }
        }
    }
}
