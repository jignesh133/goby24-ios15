//
//  TourView.swift
//  Goby24
//
//  Created by Jignesh Bhensadadiya on 7/20/21.
//

import SwiftUI
import SDWebImageSwiftUI

struct TourView: View {
    @ObservedObject private var touristPackageOffer = TouristPackageOffer.shared

    var body: some View {
        VStack{
            VStack(alignment:.leading){
                HStack{
                    let urlString = IMAGEURL + getStringFromAny(touristPackageOffer.touristPackage?.packageBanner ?? "")
                    WebImage(url: URL.init(string: urlString ))
                        .placeholder(Image(systemName: "photo")) // Placeholder Image
                        .resizable().scaledToFill().frame(width: 95, height: 113).clipShape(RoundedRectangle(cornerRadius: 21, style: .continuous))
                    VStack(alignment:.leading){
                        Text(touristPackageOffer.touristPackage?.packageName ?? "").modifier(CustomTextM(fontName: UIFont.Rubik.medium.fontName, fontSize: 22, fontColor: Color.init(hex: "2E2E2E")))
                        Text(touristPackageOffer.touristPackage?.packagePrice ?? "").modifier(CustomTextM(fontName: UIFont.Rubik.medium.fontName, fontSize: 22, fontColor: Color.init(hex: "F57F20")))
                        
                        HStack(){
                            Image("calendartourist")
                                .colorMultiply(Color.init(hex: "2E2E2E"))
                            Text(((touristPackageOffer.touristPackage?.noOfDays ?? "") + " Days")).modifier(CustomTextM(fontName: UIFont.Poppins.regular.fontName, fontSize: 14, fontColor: Color.init(hex: "2E2E2E")))
                        }
                    }
                }.padding([.leading,.top])
                Button(action: {
                    print("")
                }){
                    Text("Pay " + (touristPackageOffer.touristPackage?.packagePrice ?? ""))
                }
                .modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 16, fontColor: Color.white))
                .modifier(ButtonStyle(buttonHeight: 50, buttonColor: Color.init(hex: "00ADEF"), buttonRadius: 50))
                .padding()
                
            }.padding().background(Color.white).clipShape(RoundedRectangle(cornerRadius: 21, style: .continuous)).shadow(radius: 10)
        }
        
    }
}

struct TourView_Previews: PreviewProvider {
    static var previews: some View {
        TourView()
            .previewLayout(.sizeThatFits)
    }
}
