//
//  CreateTouristRequestView.swift
//  Goby24
//
//  Created by Jignesh Bhensadadiya on 8/14/21.
//

import SwiftUI

struct CreateTouristRequestUpdateView: View {
    
    @State var touristRequestList:Model_TouristRequestList? = nil
    
    @State var txtChooseTouristSpot:String?
    @State var desc:String = ""
    @State var addNewSpot:String?
    @State var startJourney:String = ""
    @State var endJourney:String = ""
    @State var startDate:String = ""
    @State var endDate:String = ""
    @State var budget:String = ""
    @State var currency:String = ""
    @State var noofpassenger:String = ""
    @State var country:String?
    @State var tourist_spot_list:Model_TOURIST_SPOT_LIST? = nil
    @State var isDropDownOpen:Bool = false
    @State var isSpotListOpen:Bool = false
    @State var SDate:Date = Date()
    
    @State var arrTouristSpotList:[Model_TOURIST_SPOT_LIST] =  [Model_TOURIST_SPOT_LIST]()
    @State var startLocation:Model_searchLocation?
    @State var endLocation:Model_searchLocation?
    @State var isOpenStartLocation:Bool = false
    @State var isOpenEndLocation:Bool = false
    @Environment(\.viewController) private var viewControllerHolder: UIViewController?
    
    //    init(requestList:Model_TouristRequestList? = nil) {
    //        self.touristRequestList = requestList
    //        UIScrollView.appearance().keyboardDismissMode = .onDrag
    //    }
    
    //MARK: VALIDATE STRING
    func validateData() -> String {
        if ((country ?? "").removeWhiteSpace().count == 0){
            return "Please select country"
        }else if (tourist_spot_list == nil) && (addNewSpot?.removeWhiteSpace().count == 0){
            return "Please select spot list"
        }else if (startJourney.removeWhiteSpace().count == 0){
            return "please enter start journey"
        }else if (endJourney.removeWhiteSpace().count == 0){
            return "please enter end journey"
        }else if (startDate.removeWhiteSpace().count == 0){
            return "please select start date"
        }else if (endDate.removeWhiteSpace().count == 0){
            return "please select end date"
        }else if (noofpassenger.removeWhiteSpace().count == 0){
            return "please insert number of passenger"
        }else if (getDoubleFromAny(noofpassenger) > 7){
            return "please contact to goby24 admin for more than 7 passengers"
        }else if (budget.removeWhiteSpace().count == 0){
            return "please insert budget"
        }else if (currency.removeWhiteSpace().count == 0){
            return "please select currency"
        }
        else if (desc.removeWhiteSpace().count == 0){
            return "please enter desc"
        }else{
            return ""
        }
    }
    func getTouristPackageSpotList(){
        
        var param:[String:Any] = [String:Any]()
        param["country"] = country?.lowercased()
        
        WebAccess.getDataWith(_Url: WebAccess.TOURIST_SPOT_LIST, _parameters: param) { (result) in
            switch (result){
            case .Success(let _data):
                print(_data)
                do{
                    let result:[String:Any] = UtilsObjs.cleanJson(to: _data ) as? [String : Any] ?? [String:Any]()
                    print(result)
                    let data1 =  try JSONSerialization.data(withJSONObject: result["result"] ?? [] , options: JSONSerialization.WritingOptions.prettyPrinted)
                    let convertedString = String(data: data1, encoding: String.Encoding.utf8)
                    let jsonData = Data((convertedString?.utf8)!)
                    self.arrTouristSpotList = try JSONDecoder().decode([Model_TOURIST_SPOT_LIST].self, from: jsonData)
                    NSLog("LOG")
                    if (arrTouristSpotList.count == 0){
                        tourist_spot_list = nil
                    }
                }catch(let error){
                    print(error)
                }
                
                break
            case .Error(let msg):
                ShowAlert(title: APPNAME, msg: msg, view: UIApplication.getTopViewController()!)
                break
            }
        }
    }
    func submitRequest(){
        
        var param:[String:Any] = [String:Any]()
//        param["touristspot"] = getStringFromAny(tourist_spot_list?.id ?? "")
//
        if (addNewSpot?.removeWhiteSpace().count != 0){
            param["new_tourist_spot"] = addNewSpot
            param["touristspot"] = ""
        }else{
            param["new_tourist_spot"] = ""
            param["touristspot"] = getStringFromAny(tourist_spot_list?.id ?? 0)
        }
        
        
        param["journey_from"] = startJourney
        param["journey_to"] = endJourney
        param["journey_start_date"] = startDate
        param["journey_end_date"] = endDate
        param["no_of_passengers"] = noofpassenger
        param["package_budget"] = budget
        param["new_tourist_spot"] = addNewSpot
        param["description"] = desc
        param["currency"] = currency
        param["new_tourist_spot"] = (addNewSpot ?? "")
        
        
        print(param)
        WebAccess.putDataWith(_Url: WebAccess.TOURIST_SPOT_REQUEST + getStringFromAny(touristRequestList?.id ?? "") + "/" , _parameters: param) { (result) in
            switch (result){
            case .Success(let _data):
                print(_data)
                do{
                    let result:[String:Any] = UtilsObjs.cleanJson(to: _data ) as? [String : Any] ?? [String:Any]()
                    ShowAlert(title: APPNAME, msg: "Updated Sucessfully", view: UIApplication.getTopViewController()!)

                    print(result)
                }catch(let error){
                    print(error)
                }
                break
                
            case .Error(let msg):
                ShowAlert(title: APPNAME, msg: msg, view: UIApplication.getTopViewController()!)
                break
            }
            
        }
    }
    var body: some View {
        
        ScrollView{
            
            NavigationLink(destination:SearchLocation(strText: startJourney,isNeedToPopView:true,  onLocationSelect: { (location,isClear) in
                if (isClear == false){
                startLocation = location
                startJourney = location?.structuredFormatting?.mainText ?? ""
                }else{
                    startLocation = nil
                    startJourney =  ""
                }


            }),isActive: $isOpenStartLocation) {EmptyView()}.isDetailLink(false)
            
            
            NavigationLink(destination:SearchLocation(strText: endJourney,isNeedToPopView : true, onLocationSelect: { (location,isClear) in
                if (isClear == false){

                endLocation = location
                endJourney = location?.structuredFormatting?.mainText ?? ""
                }else{
                    endLocation = nil
                    endJourney =  ""

                }


            }),isActive: $isOpenEndLocation) {EmptyView()}.isDetailLink(false)
            
            VStack{
                Group{
                    Text("Chose your country").modifier(CustomTextM(fontName: UIFont.Rubik.medium.fontName, fontSize: 26, fontColor: Color.init(hex: "2E2E2E")))
                    
                    //Button
                    Button(action: {
                        withAnimation {
                            isDropDownOpen.toggle()
                        }
                    }){
                        HStack() {
                            Spacer()
                            if ((country?.removeWhiteSpace().count ?? 0) > 0){
                                Text(country!)
                            }else{
                                Text("Choose your country *")
                            }
                            Spacer()
                            Image("down").padding(.trailing,20)
                        }
                    }
                    .modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 16, fontColor: Color.init(hex: "2E2E2E")))
                    .modifier(ButtonStyle(buttonHeight: 50, buttonColor: Color.init(hex: "F2F2F2"), buttonRadius: 50))
                    .padding(.horizontal,5)
                    
                    if (isDropDownOpen == true && (appDelegate?.countryNames.count ?? 0) > 0){
                        ScrollView{
                            VStack{
                                ForEach(0..<(appDelegate?.countryNames.count ?? 0), id: \.self) { row in
                                    Button(action: {
                                        country = (appDelegate?.countryNames[row] ?? "")
                                        isDropDownOpen = false
                                        self.getTouristPackageSpotList()
                                    }){
                                        Text(appDelegate?.countryNames[row] ?? "")
                                    }
                                    .modifier(CustomTextM(fontName: UIFont.Montserrat.semiBold.fontName, fontSize: 16, fontColor: Color.init(hex: "2E2E2E")))
                                    .modifier(ButtonStyle(buttonHeight: 50, buttonColor: Color.init(hex: "F2F2F2"), buttonRadius: 0))
                                }
                            }
                        }.frame(maxHeight:150).cornerRadius(10).padding(.top,2)
                        
                    }
                }
                Group{
                    VStack(alignment: .leading, spacing: 0){
                        Text(Localizable.Choose_an_existing_tourist_spot.localized()).modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 13, fontColor: Color.init(hex: "00AEEF"))).padding(.leading,8)
                        
                        Button(action: {
                            withAnimation {
                                isSpotListOpen.toggle()
                            }
                        }){
                            HStack() {
                                Text(tourist_spot_list?.spotName ??  "Ex: Sundarbarn").padding(.horizontal)
                                Spacer()
                                Image("down").padding(.trailing,20)
                            }
                        }
                        //  if (tourist_spot_list?.spotName.length)
                        .modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 13, fontColor: Color.init(hex: "C4C4C4")))
                        .modifier(ButtonStyle(buttonHeight: 50, buttonColor: Color.init(hex: "F2F2F2"), buttonRadius: 50))
                        .padding(.horizontal,5)
                        
                        if (isSpotListOpen == true){
                            ScrollView{
                                VStack{
                                    ForEach(0..<(self.arrTouristSpotList.count ), id: \.self) { row in
                                        Button(action: {
                                            tourist_spot_list = self.arrTouristSpotList[row]
                                            isSpotListOpen = false
                                        }){
                                            Text(self.arrTouristSpotList[row].spotName ?? "")
                                        }
                                        .modifier(CustomTextM(fontName: UIFont.Montserrat.semiBold.fontName, fontSize: 16, fontColor: Color.init(hex: "2E2E2E")))
                                        .modifier(ButtonStyle(buttonHeight: 50, buttonColor: Color.init(hex: "F2F2F2"), buttonRadius: 0))
                                    }
                                }
                            }.frame(maxHeight:150).cornerRadius(10).padding(.top,2)
                        }
                    }
                }
                Group{
                    VStack(alignment: .leading, spacing: 0){
                        Text(Localizable.Add_New_Spot.localized()).modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 13, fontColor: Color.init(hex: "00AEEF"))).padding(.leading,8)
                        
                        ZStack{
                            Button(action: {
                                self.viewControllerHolder?.present(style: .overCurrentContext, transitionStyle: .crossDissolve) {
                                    AddNewSpotView { (item) in
                                        addNewSpot = item
                                    }
                                }
                            }){
                                HStack() {
                                    Text(addNewSpot ?? "ex.").padding(.leading,10)
                                    Spacer()
                                    //Image("calender").padding(.trailing,10)
                                }
                            }.frame(height:45)
                            .modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 13, fontColor: .black))
                            .modifier(ButtonStyle(buttonHeight: 50, buttonColor: Color.init(hex: "F2F2F2"), buttonRadius: 50))
                        }.padding(.horizontal,5)
                    }
                }
                Group{
                    HStack(spacing:0){
                        Group{
                            VStack(alignment: .leading, spacing: 0){
                                Text(Localizable.Start_journey_from.localized())
                                    .modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 13, fontColor: Color.init(hex: "00AEEF"))).padding(.leading,8)
                                
                                TextField("Ex: Chittagong bus.. ", text: $startJourney, onEditingChanged: { (change) in
                                    if change == true{
                                        UIApplication.shared.endEditing()
                                        isOpenStartLocation = true
                                    }
                                })
                                .textFieldStyle( MyTextFieldStyle(fontSize: 13))
                                .keyboardType(.default)
                                .padding(.horizontal,5)
                            }
                        }
                        Group{
                            VStack(alignment: .leading, spacing: 0){
                                Text(Localizable.End_Journey_to.localized()).modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 13, fontColor: Color.init(hex: "00AEEF"))).padding(.leading,8)
                                
                                TextField("Ex: Chittagong bus.. ", text: $endJourney, onEditingChanged: { (change) in
                                    if change == true{
                                        UIApplication.shared.endEditing()
                                        isOpenEndLocation = true
                                    }
                                })
                                .textFieldStyle(MyTextFieldStyle(fontSize: 13))
                                .keyboardType(.default)
                                .padding(.horizontal,5)
                                
                            }
                        }
                    }
                }
                Group{
                    HStack{
                        
                        VStack(alignment: .leading, spacing: 0){
                            Text("Start Journey Date/Time").modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 13, fontColor: Color.init(hex: "00AEEF"))).padding(.leading,8)
                            
                            ZStack{
                                
                                Button(action: {
                                    RPicker.selectDate(title: "Start Journey Date/Time",datePickerMode: .date,minDate: SDate, didSelectDate: { (selectedDate) in
                                        SDate = selectedDate
                                        startDate = selectedDate.getFormattedDate(formatter: "yyyy-MM-dd")
                                    })
                                    
                                }){
                                    
                                    HStack() {
                                        Text(startDate).padding(.leading,10)
                                        Spacer()
                                        Image("calender").padding(.trailing,10)
                                    }
                                }.frame(height:45)
                                .modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 13, fontColor: .black))
                                .modifier(ButtonStyle(buttonHeight: 50, buttonColor: Color.init(hex: "F2F2F2"), buttonRadius: 50))
                            }
                            
                            
                        }
                        Group{
                            VStack{
                                Text("End Journey Date/Time").modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 13, fontColor: Color.init(hex: "00AEEF"))).padding(.leading,8).disabled(true)
                                ZStack{
                                    Button(action: {
                                        RPicker.selectDate(title: "End Journey Date/Time",datePickerMode: .date,minDate: SDate, didSelectDate: { (selectedDate) in
                                            endDate = selectedDate.getFormattedDate(formatter: "yyyy-MM-dd")
                                        })
                                        
                                    }){
                                        HStack() {
                                            Text(endDate).padding(.leading,10)
                                            Spacer()
                                            Image("calender").padding(.trailing,10)
                                        }
                                    }.frame(height:45)
                                    .modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 13, fontColor: .black))
                                    .modifier(ButtonStyle(buttonHeight: 50, buttonColor: Color.init(hex: "F2F2F2"), buttonRadius: 50))
                                }
                            }
                        }
                    }
                }
                Group{
                    HStack{
                        Group{
                            VStack(alignment: .leading, spacing: 0){
                                Text(Localizable.No_of_passengers.localized()).modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 13, fontColor: Color.init(hex: "00AEEF"))).padding(.leading,8)
                                
                                TextField("Ex: 1 ", text: $noofpassenger)
                                    .textFieldStyle(MyTextFieldStyle(fontSize: 13))
                                    .keyboardType(.numberPad)
                                    .padding(.horizontal,5)
                                    .frame(width: 125)
                            }
                        }
                        Group{
                            VStack(alignment: .leading, spacing: 0){
                                Text(Localizable.Package_Budget.localized()).modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 13, fontColor: Color.init(hex: "00AEEF"))).padding(.leading,8)
                                priceTextField(text: $budget, currency: $currency, placeHolderText: Localizable.Price.localized(),fontSize: 13).padding(.leading,8)
                                
                            }
                        }
                    }
                }
                Group{
                    VStack(alignment: .leading, spacing: 0){
                        
                        Text((Localizable.Write_description_about_the_tourist_package.localized())).modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 13, fontColor: Color.init(hex: "00AEEF"))).padding(.leading,8)
                        
                        Group{
                            VStack{
                                TextEditor(text: $desc).modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 13, fontColor: Color.black)).textContentType(.none).multilineTextAlignment(.leading).lineLimit(nil)
                                Spacer()
                            }.padding()
                        }.frame( height: 100, alignment: .leading).background(Color.init(hex: "F5F5F5")).clipShape(RoundedRectangle(cornerRadius: 14)).padding(.horizontal,10)
                    }
                }
                
                VStack(alignment: .center, spacing: 25){
                    //Button
                    Button(action: {
                        
                        let str = self.validateData()
                        
                        if(str.count > 0){
                            print(str)
                            ShowAlert(title: APPNAME, msg: str, view: UIApplication.getTopViewController()!)
                        }else{
                            self.submitRequest()
                        }
                    }){
                        VStack{
                            Text(Localizable.Update.localized()).frame(minWidth: 10, maxWidth: .infinity)
                        }
                    }
                    .modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 16, fontColor: Color.white))
                    .modifier(ButtonStyle(buttonHeight: 50, buttonColor: Color.init(hex: "00ADEF"), buttonRadius: 50))
                }.padding(.horizontal,30).padding(.vertical,30)
                
                
                
            }.padding().background(Color.white).clipShape(RoundedRectangle(cornerRadius: 21, style: .continuous)).shadow(radius: 10).padding(.vertical)
            
        }.onAppear(){
            if (startDate == "" && endDate == ""){
                startDate = touristRequestList?.journeyStartDate ?? ""
                endDate = touristRequestList?.journeyEndDate ?? ""
                
                SDate = startDate.stringToDate()
                
            }
            if  (touristRequestList != nil){
                
                desc = touristRequestList?.descriptionField ?? ""
                addNewSpot = touristRequestList?.newTouristSpot ?? ""
                if (startJourney == ""){
                    startJourney = touristRequestList?.journeyFrom ?? ""
                }
                if (endJourney == ""){

                    endJourney = touristRequestList?.journeyTo ?? ""
                }
                
                budget = touristRequestList?.packageBudget ?? ""
                currency = touristRequestList?.currency ?? ""
                noofpassenger = touristRequestList?.noOfPassengers ?? ""
                
                txtChooseTouristSpot = touristRequestList?.touristspot?.spotName ?? ""
                
                country = touristRequestList?.touristspot?.country ?? ""
                
                
                
            }
            
        }.padding()//.resignKeyboardOnDragGesture()
        .showNavigationBarWithBackStyle()
        
        
    }
}

struct CreateTouristRequestUpdateView_Previews: PreviewProvider {
    static var previews: some View {
        CreateTouristRequestUpdateView()
    }
}
