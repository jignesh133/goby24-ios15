//
//  AllTouristView.swift
//  Goby24
//
//  Created by Jignesh Bhensadadiya on 8/14/21.
//

import SwiftUI

struct AllTouristView: View {
    @State var arrTouristRequestList:[Model_TouristRequestList] =  [Model_TouristRequestList]()
    @Environment(\.viewController) private var viewControllerHolder: UIViewController?
    @State var showingSheet:Bool = false
    @State var showingPay:Bool = false
    @State var isOpenMessage:Bool = false

    
    @State var touristRequestList:Model_TouristRequestList? = nil

    func getTouristRequestList(isHideLoader:Bool = false){
        
        let param:[String:Any] = [String:Any]()
        print(param)
        WebAccess.getDataWith(_Url: WebAccess.TOURIST_SPOT_REQUEST, _parameters: param,isHideLoader: isHideLoader) { (result) in

            switch (result){
            case .Success(let _data):
                print(_data)
                do{
                    let result:[String:Any] = UtilsObjs.cleanJson(to: _data ) as? [String : Any] ?? [String:Any]()
                    print(result)
                    let data1 =  try JSONSerialization.data(withJSONObject: result["result"] ?? [] , options: JSONSerialization.WritingOptions.prettyPrinted)
                    let convertedString = String(data: data1, encoding: String.Encoding.utf8)
                    let jsonData = Data((convertedString?.utf8)!)
                    self.arrTouristRequestList = try JSONDecoder().decode([Model_TouristRequestList].self, from: jsonData)
                    NSLog("LOG")
                    
                }catch(let error){
                    print(error)
                }

                break

            case .Error(let msg):
                ShowAlert(title: APPNAME, msg: msg, view: UIApplication.getTopViewController()!)
                break
            }
            
        }
    }
    func acceptTouristRequestList(tid:String){
        
        let param:[String:Any] = [String:Any]()
        print(param)
        WebAccess.putDataWith(_Url: (WebAccess.TOURIST_SPOT_REQUEST_ACCEPT + tid + "/"), _parameters: param) { (result) in

            switch (result){
            case .Success(let _data):
                print(_data)
                
                break

            case .Error(let msg):
                ShowAlert(title: APPNAME, msg: msg, view: UIApplication.getTopViewController()!)
                break
            }
            
        }
    }
    func deleteTouristRequestList(tid:String){
        
        let param:[String:Any] = [String:Any]()
        print(param)
        WebAccess.deleteDataWith(_Url: (WebAccess.TOURIST_SPOT_REQUEST + tid + "/"), _parameters: param) { (result) in

            switch (result){
            case .Success(let _data):
                print(_data)
                ShowAlert(title: APPNAME, msg: "Request deleted sucessfully ", view: UIApplication.getTopViewController()!)
                self.arrTouristRequestList =  self.arrTouristRequestList.filter({"\($0.id ?? 0)" != tid})
                self.getTouristRequestList(isHideLoader: true)
                break
                
            case .Error(let msg):
                ShowAlert(title: APPNAME, msg: msg, view: UIApplication.getTopViewController()!)
                break
            }
            
        }
    }
    var body: some View {
        VStack{
            
            NavigationLink(destination: CreateTouristRequestUpdateView(touristRequestList: self.touristRequestList),isActive: $showingSheet) {EmptyView()}.isDetailLink(false)
            NavigationLink(destination: RequestFromTouristPayementView(touristRequest: self.touristRequestList),isActive: $showingPay) {EmptyView()}.isDetailLink(false)
            NavigationLink(destination: RiderContactView(receiverId:getStringFromAny(self.touristRequestList?.id ?? "")), isActive: $isOpenMessage){EmptyView()}.isDetailLink(false)

            ScrollView{
                ForEach(0..<$arrTouristRequestList.wrappedValue.count,id: \.self) { row in
                    TouristRideView(touristRequestList: arrTouristRequestList[row]) { (tid) in
                        print("DELETE")
                        self.deleteTouristRequestList(tid: tid)
                    } onAcceptRequest: { (tid) in
                        print("ACCEPT")
                        self.acceptTouristRequestList(tid: tid)
                    } onEditRequest: { (obj) in
                        print("EDIT")
                        self.touristRequestList = obj
                        showingSheet = true
                    } onContactRequest: { (obj) in
                        print("CONTACT")
                        self.touristRequestList = obj

                        isOpenMessage = true
                    }onPayRequest: { (obj) in
                        print("Pay")
                        self.touristRequestList = obj

                        self.showingPay = true
                        // RequestFromTouristPayementView
                    }

                }
            }
        }.onAppear(){
            
            self.getTouristRequestList()
        }
    }
    
}

