//
//  OfferPackageView.swift
//  Goby24
//
//  Created by Jignesh Bhensadadiya on 7/20/21.
//

import SwiftUI

struct OfferPackageView: View {
    @State var onlyDriverSelected: Bool = true
    
    //    @State var selectedPackage:Model_TouristPackage?
    //    @State var selectedCountry:String?
    
    @ObservedObject private var touristPackageOffer = TouristPackageOffer.shared
    
    var body: some View {
        ScrollView{
            
            VStack{
                
                                
              //  CreateOfferPackageView(onlyDriverSelected:$onlyDriverSelected)
                    //.ignoresSafeArea(.keyboard)
                
            }.padding().background(Color.white).clipShape(RoundedRectangle(cornerRadius: 21, style: .continuous)).shadow(radius: 10)
        }
    }
}

struct OfferPackageView_Previews: PreviewProvider {
    static var previews: some View {
        OfferPackageView()
            .previewLayout(.sizeThatFits)
    }
}
