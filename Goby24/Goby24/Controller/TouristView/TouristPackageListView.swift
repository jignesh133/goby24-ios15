//
//  TouristPackageListView.swift
//  Goby24
//
//  Created by Jignesh Bhensadadiya on 7/17/21.
//

import SwiftUI

struct TouristPackageListView: View {
    @State var filterClicked: Bool = false
    @State var selectedItemIndex: Int = 0
    @EnvironmentObject var settings: UserSettings
    @Environment(\.presentationMode) var presentationMode
    @ObservedObject private var touristPackageOffer = TouristPackageOffer.shared

    var body: some View {
        ScrollView{
            Text(Localizable.Tourist_Package.localized()).padding(.top, 20).modifier(CustomTextM(fontName: UIFont.Rubik.medium.fontName, fontSize: 26, fontColor: Color.init(hex: "2E2E2E"))).padding(.horizontal)
            HStack{
                Button(action: {
                    
                }){
                    Image("filter")
                    
                }
                .modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 16, fontColor: Color.white))
                .modifier(ButtonStyle(buttonHeight: 50, buttonColor: Color.clear, buttonRadius: 10))
                .frame(width:50)
                .sheet(isPresented: self.$filterClicked) {
                    PackageFilterView()
                }
                Spacer()
                Button(action: {
                    
                }){
                    Image("search")
                }
                .modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 16, fontColor: Color.white))
                .modifier(ButtonStyle(buttonHeight: 50, buttonColor: Color.clear, buttonRadius: 10))
                .frame(width:50)
                
            }
            
            HStack(spacing:0){
                
                //Button
                Button(action: {
                    selectedItemIndex = 0
                }){
                    if(selectedItemIndex == 0){
                        VStack{
                            Text(Localizable.Tourist_Package_offers.localized()).modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 13, fontColor: Color.init(hex: "00AEEF"))).multilineTextAlignment(.center)
                            Spacer()
                            Divider().background(Color.init(hex: "00AEEF")).frame(height: 1.5)
                        }
                    }else{
                        VStack{
                            Text(Localizable.Tourist_Package_offers.localized()).modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 13, fontColor: Color.init(hex: "2E2E2E"))).multilineTextAlignment(.center)
                            Spacer()
                            Divider().background(Color.init(hex: "2E2E2E")).frame(height: 1.5)
                            
                        }
                    }
                }.frame(width: .infinity, height: 50)
                
                
                //ACCOUNT Button
                Button(action: {
                    selectedItemIndex = 1
                    
                }){
                    if(selectedItemIndex == 1){
                        VStack{
                            Text(Localizable.Request_from_tourist.localized()).modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 13, fontColor: Color.init(hex: "00AEEF"))).multilineTextAlignment(.center)
                            Spacer()
                            Divider().background(Color.init(hex: "00AEEF")).frame( height: 1.5)
                        }
                    }else{
                        VStack{
                            
                            Text(Localizable.Request_from_tourist.localized()).modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 13, fontColor: Color.init(hex: "2E2E2E"))).multilineTextAlignment(.center)
                            Spacer()
                            Divider().background(Color.init(hex: "2E2E2E")).frame( height: 1.5)
                            
                        }
                    }
                }.frame(width: .infinity, height: 50)
                
                //Button
                Button(action: {
                    selectedItemIndex = 2
                }){
                    if(selectedItemIndex == 2){
                        VStack{
                            
                            Text("Offer a package").modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 13, fontColor: Color.init(hex: "00AEEF")))
                            Spacer()
                            Divider().background(Color.init(hex: "00AEEF")).frame(height: 1.5)
                        }
                    }else{
                        VStack{
                            Text("Offer a package").modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 13, fontColor: Color.init(hex: "2E2E2E")))
                            Spacer()
                            Divider().background(Color.init(hex: "2E2E2E")).frame(height: 1.5)
                            
                        }
                    }
                }.frame(width: .infinity, height: 50)
                
                
            }.frame(width: .infinity, height: .infinity)
            
            
            if (selectedItemIndex == 0){
                TouristPackageOffers()
            }else if (selectedItemIndex == 1){
                RequestFromTouristView() // 
            }else{
                CreateOfferPackageView()
            }
            Spacer()
        }.showNavigationBarStyle()

    }
}

struct TouristPackageListView_Previews: PreviewProvider {
    static var previews: some View {
        TouristPackageListView()
    }
}
