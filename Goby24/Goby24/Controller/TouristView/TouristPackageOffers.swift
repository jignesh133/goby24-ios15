//
//  TouristPackageOffers.swift
//  Goby24
//
//  Created by Jignesh Bhensadadiya on 7/17/21.
//

import SwiftUI

struct TouristPackageOffers: View {
    private var twoColumnGrid = [GridItem(.flexible()), GridItem(.flexible())]
    @State var arrRides:[Model_TouristPackage] = [Model_TouristPackage]()
    @EnvironmentObject var settings: UserSettings
    @Environment(\.presentationMode) var presentationMode
    
    func getTouristPackageList(){
        
        let param:[String:Any] = [String:Any]()
        
        WebAccess.getDataWith(_Url: WebAccess.TOURIST_PACKAGES, _parameters: param) { (result) in
            switch (result){
            case .Success(let _data):
                do{
                    let result:[String:Any] =  UtilsObjs.cleanJsonToObject(withSkip: _data, ["provider"]) as? [String : Any] ?? [String:Any]()
                    let obj = result["result"] as? [[String:Any]] ?? []
                    
                    //_data // as? [String : Any] ?? [String:Any]()
                    print(result)
//                    let arr = _data["result"]  as? [[String : Any]] ?? []
                    
                    
//                    let res = UtilsObjs.cleanJsonToObject(withSkip: obj, ["provider"]) as? [String : Any] ?? [String:Any]()
                    
                    let data1 =  try JSONSerialization.data(withJSONObject: obj , options: JSONSerialization.WritingOptions.prettyPrinted)
                    let convertedString = String(data: data1, encoding: String.Encoding.utf8)
                    let jsonData = Data((convertedString?.utf8)!)
                    
                    self.arrRides = try JSONDecoder().decode([Model_TouristPackage].self, from: jsonData)
                    NSLog("LOG")
                }catch(let error){
                    print(error)
                }
                
                break
            case .Error(let msg):
                ShowAlert(title: APPNAME, msg: msg, view: UIApplication.getTopViewController()!)
                break
            }
        }
    }
    
    var body: some View {
        ScrollView{
            LazyVGrid(columns: twoColumnGrid, spacing: 30) {
                ForEach(0..<self.arrRides.count, id: \.self) { row in
                    NavigationLink(destination: TouristPackageDetailsView(tourData: arrRides[row])) {
                        
                        TouristPackageOfferItem(rideData: self.arrRides[row])
                            .frame(width: (UIScreen.main.bounds.size.width / 2) - 30)
                            .frame(height: ((UIScreen.main.bounds.size.width / 2) - 30) * 1.15)
                            .aspectRatio(0.85, contentMode: .fit)
                            .clipped().background(Color.clear)
                        
                    }.buttonStyle(PlainButtonStyle())
                }
            }.padding()
            .onAppear(){
                if (appDelegate?.selectedTab == 3){
                  self.getTouristPackageList()
                }
            }
            .onLoad(){
               // self.getTouristPackageList()

            }
            
        }.onReceive(self.settings.$isMoveToTouristViewHome) { (isMoveToHome) in
            if isMoveToHome == true{
                self.settings.isMoveToTouristViewHome = false
                self.presentationMode.wrappedValue.dismiss()
            }
        }
    }
}

struct TouristPackageOffers_Previews: PreviewProvider {
    static var previews: some View {
        TouristPackageOffers()
    }
}
