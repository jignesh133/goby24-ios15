//
//  myRideDetails.swift
//  Goby24
//
//  Created by Jignesh Bhensadadiya on 12/10/21.
//

import SwiftUI
import SDWebImageSwiftUI
import StarRating

struct myRideDetails: View {
    @State var objRequestRide:Model_RideRequest?
    @State var customConfig = StarRatingConfiguration(spacing: 8,numberOfStars: 5,stepType: .exact,minRating: 0,borderWidth: 1,borderColor: Color.black,emptyColor: Color.white,shadowRadius: 0,fillColors: [Color.black],starVertices: 5,starWeight: 0.45)
   
    var body: some View {
        
        VStack{
            
            VStack(alignment: .leading,spacing:0){
                
                Text(Localizable.Profile_Overview.localized()).modifier(CustomTextM(fontName: UIFont.Montserrat.semiBold.fontName, fontSize: 16, fontColor: Color.white)).frame(width: UIScreen.main.bounds.size.width - 40, height: 40, alignment: .center).background(Color.init(hex: "00AEEF")).cornerRadius(radius: 6,corners: [.topLeft, .topRight])
                HStack{
                    let urlString = IMAGEURL + getStringFromAny(objRequestRide?.rider?.profilePic ?? "")//(driverProfile.profilePic ?? "")
                    WebImage(url: URL.init(string: urlString ))
                        .placeholder(Image(systemName: "photo")) // Placeholder Image
                        .renderingMode(.original)
                        .resizable()
                        .aspectRatio(contentMode: .fill)
                        .frame(width: 60, height: 60)
                        .clipShape(Circle())
                        .overlay(Circle().stroke(Color.init(hex: "00AEEF"), lineWidth: 1)).padding(.vertical)
                        VStack(alignment:.leading){
                            
                            HStack(spacing:0){
                                Text(objRequestRide?.rider?.firstName?.removeWhiteSpace() ?? "").modifier(CustomTextM(fontName: UIFont.Rubik.bold.fontName, fontSize: 16, fontColor: Color.black)).padding(.leading,10)
                                Text(objRequestRide?.rider?.lastName?.removeWhiteSpace() ?? "").modifier(CustomTextM(fontName: UIFont.Rubik.bold.fontName, fontSize: 16, fontColor: Color.black)).padding(.leading,10)
                            }
                            
                            
                            HStack{
                                StarRating(initialRating: Double(objRequestRide?.rider?.riderRating ?? "")!,configuration: $customConfig).frame(width: 75, height: 10, alignment: .center)
                                
                                Text(objRequestRide?.rider?.riderRating ?? "").modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 12, fontColor: Color.black)).frame(height: 30, alignment: .center)
                            }
                            
                    }
                }.padding()
                HStack{
                    Spacer()
                    Text(Localizable.Member_Verification.localized()).modifier(CustomTextM(fontName: UIFont.Montserrat.semiBold.fontName, fontSize: 16, fontColor: Color.black))
                    Spacer()
                }
                
                VStack(){
                    HStack{
                        if ((objRequestRide?.rider?.isMobileNoVerified ?? false)  == true){
                            Image("right")
                        }else{
                            Image("wrong")
                        }
                        
                        Text(Localizable.Verify_your_phone_number.localized()).modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 15, fontColor: Color.black))
                        Spacer()
                    }
                    HStack{
                        if ((objRequestRide?.rider?.isEmailVerified ?? false)  == true){
                            Image("right")
                        }else{
                            Image("wrong")
                            
                        }
                        Text(Localizable.Verify_your_Email.localized()).modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 15, fontColor: Color.black))
                        Spacer()
                    }.padding(.bottom)
                }.padding(.horizontal,50).padding(.top)
                Spacer()
            }.frame(width:UIScreen.main.bounds.size.width - 40 ,height: 250).background(RoundedRectangle(cornerRadius: 25).fill(Color.white).shadow(color: Color.black.opacity(0.12), radius: 3))
            
            
            VStack(alignment: .leading,spacing:0){

                Text(Localizable.Ride_Overview.localized()).modifier(CustomTextM(fontName: UIFont.Montserrat.semiBold.fontName, fontSize: 16, fontColor: Color.white)).frame(width: UIScreen.main.bounds.size.width - 40, height: 40, alignment: .center).background(Color.init(hex: "00AEEF")).cornerRadius(radius: 6,corners: [.topLeft, .topRight])

                VStack(alignment: .center){
                    HStack{
                        Image("clockblack")
                        Text(objRequestRide?.journeyDate ?? "")//_.getFormattedDate(formatter: "EEEE dd MMM") ?? "")
                            .modifier(CustomTextM(fontName: UIFont.Poppins.regular.fontName, fontSize: 16, fontColor: Color.init(hex: "2E2E2E")))
                        Spacer()
                    }
                    HStack{
                        Image("distance")
                        Text(objRequestRide?.pickupTime ?? "").modifier(CustomTextM(fontName: UIFont.Poppins.regular.fontName, fontSize: 16, fontColor: Color.init(hex: "2E2E2E")))
                        Spacer()
                    }
                    HStack{
                        Image("car")
                        Text(objRequestRide?.rider?.vehicleBrand ?? "").modifier(CustomTextM(fontName: UIFont.Poppins.regular.fontName, fontSize: 16, fontColor: Color.init(hex: "2E2E2E")))
                        Spacer()
                    }
                }.padding(.horizontal,50).padding(.top)
                HStack{
                    HStack{Image("distancelink")}
                    VStack(spacing:10){
                        HStack{
                            Text(objRequestRide?.journeyFrom ?? "").modifier(CustomTextM(fontName: UIFont.Montserrat.semiBold.fontName, fontSize: 16, fontColor: Color.init(hex: "2E2E2E")))
                        }
                        HStack{
                            Text(objRequestRide?.journeyTo ?? "").modifier(CustomTextM(fontName: UIFont.Montserrat.semiBold.fontName, fontSize: 16, fontColor: Color.init(hex: "2E2E2E")))
                        }
                    }
                }.padding(.horizontal,50).padding(.top,10)
                Spacer()
            }.frame(width:UIScreen.main.bounds.size.width - 40 ,height: 250).background(RoundedRectangle(cornerRadius: 25).fill(Color.white).shadow(color: Color.black.opacity(0.12), radius: 3)).padding(.top,20)
            Spacer()
        }.showNavigationBarWithBackStyle()
        
    }
}

struct myRideDetails_Previews: PreviewProvider {
    static var previews: some View {
        myRideDetails()
    }
}
