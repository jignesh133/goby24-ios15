//
//  FilterRidesView.swift
//  Goby24
//
//  Created by Jignesh Bhensadadiya on 7/15/21.
//

import SwiftUI

struct FilterRidesView: View {
    @State var isExpanded: Bool = true
    var body: some View {
        VStack{
            ShowCustomNavigation().padding(.top,15)
            
            HStack{
                Text("Ride Category").modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 15, fontColor: Color.init(hex: "2E2E2E")))
                Spacer()
            }.padding(.horizontal).frame(width: 300).padding(.top,40)
            
            DisclosureGroup(isExpanded: $isExpanded) {
                Group{
                    Divider().background(Color.init(hex: "707070")).padding(.top,10)
                    HStack{
                        Text(Localizable.Ride_Cancelled.localized()).modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 15, fontColor: Color.init(hex: "2E2E2E")))
                        Spacer()
                    }.padding(.leading,25)
                }
                Group{
                    Divider().background(Color.init(hex: "707070"))
                    HStack{
                        Text(Localizable.Upcoming_Ride.localized()).modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 15, fontColor: Color.init(hex: "2E2E2E")))
                        Spacer()
                    }.padding(.leading,25)
                }
                
                Group{
                    Divider().background(Color.init(hex: "707070"))
                    HStack{
                        Text(Localizable.Ride_Completed.localized()).modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 15, fontColor: Color.init(hex: "2E2E2E")))
                        Spacer()
                    }.padding(.leading,25)
                }
                
            } label: {
                HStack{
                    Spacer()
                    Text("Choose ride")
                    Spacer()
                }
                .onTapGesture {
                    withAnimation{
                        isExpanded.toggle()
                    }
                }
            }
            .accentColor(Color.init(hex: "2E2E2E"))
            .padding().frame(width: 300).background(Color.white).cornerRadius(24).accentColor(.black).modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 15, fontColor: Color.init(hex: "2E2E2E"))).multilineTextAlignment(.center)
            .shadow(radius: 3)
            Spacer()
            Button(action: {
                print("CONFIRM ")
            }){
                Text("Filter Out Rides")
            }
            .modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 16, fontColor: Color.white))
            .modifier(ButtonStyle(buttonHeight: 50, buttonColor: Color.init(hex: "00ADEF"), buttonRadius: 50))
            .padding(.horizontal,30)
        }
    }
}

struct FilterRidesView_Previews: PreviewProvider {
    static var previews: some View {
        FilterRidesView()
    }
}
