//
//  RidesItemView.swift
//  Goby24
//
//  Created by Jignesh Bhensadadiya on 7/15/21.
//

import SwiftUI

struct TakenRidesItemView: View {
    
    @State var myRide:Model_MyRidesTaken
    @State var isRatingOpen:Bool = false
    let onClickCancel: (String) -> Void

    var body: some View {
        
        VStack{
            NavigationLink(destination:ratingsListView(bookingid:getStringFromAny(self.myRide.booking ?? 0),ride_type: "Basic Ride"), isActive: $isRatingOpen)
                {EmptyView()}.isDetailLink(false)

            ZStack{
                
                Image("myridesbg").frame(height: 116)
                
                VStack(alignment: .leading, spacing: 0) {
                    
                    HStack{
                        Image("calendar").padding([.leading],15)
                        let date =  (myRide.date?.stringToDate(strCurrentFormat: "yyyy-MM-dd").getFormattedDate(formatter: "dd MMM yyyy")) ?? ""
                        let time = (myRide.time?.stringToDate(strCurrentFormat: "hh:mm:ss").getFormattedDate(formatter: "HH:mm a")) ?? ""
                        
                        Text(date  + " " +   time ).modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 11, fontColor: Color.init(hex: "00AEEF"))).padding([.trailing],15).frame(maxWidth: .infinity, alignment: .leading)
                        
                        Spacer()
                        Text(myRide.rideStatus ?? "").modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 12, fontColor: Color.init(hex: "2E2E2E"))).padding([.horizontal],15)
                    }
                    
                    Divider().padding([.horizontal,.top],5)
                    
                    HStack{
                        Image("orangechain").padding([.leading],15)
                        VStack{
                            Text(myRide.route?.first ?? "").modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 11, fontColor: Color.init(hex: "00AEEF"))).padding([.trailing],15).frame(maxWidth: .infinity, alignment: .leading).padding(.top,02)
                            
                            Text(myRide.route?.last ?? "").modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 11, fontColor: Color.init(hex: "00AEEF"))).padding([.trailing],15).frame(maxWidth: .infinity, alignment: .leading).padding(.top,1)
                        }
                    }.padding(.top,2)
                    
                    HStack{
                        Image("clock").padding([.leading],15)
                        
                        Text(myRide.journeyTime ?? "-").modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 11, fontColor: Color.init(hex: "00AEEF"))).padding([.trailing],5)
                        Divider().frame(width: 1, height: 10)//.padding(.vertical)
                        Text(myRide.distance ?? "").modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 11, fontColor: Color.init(hex: "00AEEF"))).padding([.trailing],5)
                        Text((myRide.fare ?? "") + " " + (myRide.currency ?? "")).modifier(CustomTextM(fontName: UIFont.Montserrat.bold.fontName, fontSize: 11, fontColor: Color.init(hex: "F57F20"))).padding([.trailing],5).frame(maxWidth: .infinity, alignment: .trailing)


                        if (myRide.rideStatus == "Ride Completed"  && myRide.istravellerrated == false ){
                            //ACCOUNT Button
                            Button(action: {
                                let alert = UIAlertController(title: "Would you like to share your experience with rating?", message: "You can share your experience by rating your ridemate, it will help us grow our ride sharing community", preferredStyle: .alert)
                                let skip = UIAlertAction(title:"Skip", style: .cancel) { (res) in
                                    
                                }
                                let sure = UIAlertAction(title:"Yes Sure", style: .default) { (res) in
                                    // GIE RIDER RATING
                                    self.isRatingOpen = true
                                }
                                alert.addAction(skip)
                                alert.addAction(sure)
                                
                                UIApplication.getTopViewController()!.present(alert, animated: true, completion: nil)
                                
                                
                            }){
                                Text("Ride Completed")
                            }
                            .modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 12, fontColor: Color.white))
                            .modifier(ButtonStyle(buttonHeight: 30, buttonColor: Color.init(hex: "00ADEF"), buttonRadius: 4))
                            .padding([.trailing],5)
                        }
//                        else  if (myRide.rideStatus == "Booked"){
//                            //ACCOUNT Button
//                            Button(action: {
//                                    // CANCEL RIDE
//                                onClickCancel(getStringFromAny(myRide.booking ?? ""))
//                            }){
//                                Text("Cancel Ride")
//                            }
//                            .modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 12, fontColor: Color.white))
//                            .modifier(ButtonStyle(buttonHeight: 30, buttonColor: Color.init(hex: "00ADEF"), buttonRadius: 4))
//                            .padding([.trailing],5)
//                        }

                    }.padding(.top,5)
                    VStack{
                        if(myRide.rideStatus == "Booked" ){
                            //ACCOUNT Button
                            Button(action: {
                                onClickCancel(getStringFromAny(myRide.booking ?? ""))
                                
                            }){
                                Text("Cancel Ride")
                            }
                            .modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 12, fontColor: Color.white))
                            .modifier(ButtonStyle(buttonHeight: 30, buttonColor: Color.init(hex: "F57F20"), buttonRadius: 15))
                            .padding(10)
                        }
                    }
                }
                Spacer()
            }
            Spacer()
        }
        
    }
    
}
struct OfferedRidesItemView: View {
    
    @State var myRide:Model_MyRidesOffered
    let onClickCancel: (String) -> Void

    var body: some View {
        
        VStack{
            
            ZStack{
                
                Image("myridesbg").frame(height: 116)
                
                VStack(alignment: .leading, spacing: 0) {
                    
                    HStack{
                        Image("calendar").padding([.leading],15)
                        let date =  (myRide.date?.stringToDate(strCurrentFormat: "yyyy-MM-dd").getFormattedDate(formatter: "dd MMM yyyy")) ?? ""
                        let time = (myRide.time?.stringToDate(strCurrentFormat: "hh:mm:ss").getFormattedDate(formatter: "HH:mm a")) ?? ""
                        
                        Text(date  + " " +   time ).modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 11, fontColor: Color.init(hex: "00AEEF"))).padding([.trailing],15).frame(maxWidth: .infinity, alignment: .leading)
                        
                        Spacer()
                        Text(myRide.rideStatus ?? "").modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 12, fontColor: Color.init(hex: "2E2E2E"))).padding([.horizontal],15)
                    }
                    
                    Divider().padding([.horizontal,.top],5)
                    
                    HStack{
                        Image("orangechain").padding([.leading],15)
                        VStack{
                            Text(myRide.route?.first ?? "").modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 11, fontColor: Color.init(hex: "00AEEF"))).padding([.trailing],15).frame(maxWidth: .infinity, alignment: .leading).padding(.top,02)
                            
                            Text(myRide.route?.last ?? "").modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 11, fontColor: Color.init(hex: "00AEEF"))).padding([.trailing],15).frame(maxWidth: .infinity, alignment: .leading).padding(.top,1)
                        }
                    }.padding(.top,2)
                    
                    HStack{
                        Image("clock").padding([.leading],15)
                        
                        Text(myRide.journeyTime ?? "-").modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 11, fontColor: Color.init(hex: "00AEEF"))).padding([.trailing],5)
                        Divider().frame(width: 1, height: 10)//.padding(.vertical)
                        Text(myRide.distance ?? "").modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 11, fontColor: Color.init(hex: "00AEEF"))).padding([.trailing],5)
                        Text((myRide.fare ?? "") + " " + (myRide.currency ?? "")).modifier(CustomTextM(fontName: UIFont.Montserrat.bold.fontName, fontSize: 11, fontColor: Color.init(hex: "F57F20"))).padding([.trailing],5).frame(maxWidth: .infinity, alignment: .trailing)


                        if (myRide.rideStatus == "Ride Completed"  && myRide.isriderrated == false ){
                            //ACCOUNT Button
                            Button(action: {
                                let alert = UIAlertController(title: "Would you like to share your experience with rating?", message: "You can share your experience by rating your ridemate, it will help us grow our ride sharing community", preferredStyle: .alert)
                                let skip = UIAlertAction(title:"Skip", style: .cancel) { (res) in
                                    
                                }
                                let sure = UIAlertAction(title:"Yes Sure", style: .default) { (res) in
                                    // GIE TRAVELLER RATING
                                }
                                alert.addAction(skip)
                                alert.addAction(sure)
                                
                                UIApplication.getTopViewController()!.present(alert, animated: true, completion: nil)
                                
                                
                            }){
                                Text("Ride Completed")
                            }
                            .modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 12, fontColor: Color.white))
                            .modifier(ButtonStyle(buttonHeight: 30, buttonColor: Color.init(hex: "00ADEF"), buttonRadius: 4))
                            .padding([.trailing],5)
                        }

                    }.padding(.top,5)
                    VStack{
                        if(myRide.rideStatus == "Upcoming Ride" ){
                            //ACCOUNT Button
                            Button(action: {
                                onClickCancel(getStringFromAny(myRide.booking ?? ""))
                                
                            }){
                                Text("Cancel Ride")
                            }
                            .modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 12, fontColor: Color.white))
                            .modifier(ButtonStyle(buttonHeight: 30, buttonColor: Color.init(hex: "F57F20"), buttonRadius: 15))
                            .padding(10)
                        }
                    }
                }
                Spacer()
            }
            Spacer()
        }
        
    }
    
}

//struct RidesItemView_Previews: PreviewProvider {
//    static var previews: some View {
//        RidesItemView()
//    }
//}
