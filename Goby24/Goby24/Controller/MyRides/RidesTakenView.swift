//
//  RidesTakenView.swift
//  Goby24
//
//  Created by Jignesh Bhensadadiya on 7/15/21.
//

import SwiftUI
import SDWebImage

struct RidesTakenView: View {
    @State var arrRides:[Model_MyRidesTaken] = [Model_MyRidesTaken]()
    func cancelRide(bookingId:String,reason:String){
        var param:[String:Any] = [String:Any]()
        param["booking"] = bookingId
        param["reason"] = reason

        WebAccess.postDataWith(_Url: (WebAccess.RIDE_CANCEL_BY_TRAVELLER) , _parameters: param) { (result) in
            switch (result){
            case .Success(let _data):
                print(_data)
                do{
                    self.getMyRides()
                }catch(let error){
                    print(error)
                }
                break
            case .Error(let msg):
                ShowAlert(title: APPNAME, msg: msg, view: UIApplication.getTopViewController()!)
                break
            }
        }
    }
    func getMyRides(){
        
        WebAccess.getDataWith(_Url: WebAccess.MY_RIDE_, _parameters: [:]) { (result) in
            switch (result){
            case .Success(let _data):
                print(_data)
                do{
                    let result:[String:Any] = UtilsObjs.cleanJson(to: _data ) as? [String : Any] ?? [String:Any]()
                    print(result)
                    let data1 =  try JSONSerialization.data(withJSONObject: result["result"] ?? [] , options: JSONSerialization.WritingOptions.prettyPrinted)
                    let convertedString = String(data: data1, encoding: String.Encoding.utf8)
                    let jsonData = Data((convertedString?.utf8)!)
                    self.arrRides = try JSONDecoder().decode([Model_MyRidesTaken].self, from: jsonData)
                    NSLog("LOG")
                }catch(let error){
                    print(error)
                }
                break
            case .Error(let msg):
                ShowAlert(title: APPNAME, msg: msg, view: UIApplication.getTopViewController()!)
                break
            }
        }
    }
    
    var body: some View {
        
        ScrollView{
            ForEach(0..<self.arrRides.count, id: \.self) { row in
                NavigationLink(destination: BasicRidesTakenViewDetailsView(ride:self.arrRides[row])) {
                    TakenRidesItemView(myRide: self.arrRides[row]) { (bookingId) in
                        RPicker.selectOption(title: "Why do you want to cancel the ride?", cancelText: "Cancel", doneText: "Done", dataArray: ["Change of Mind","Emergency Situation","want to book new ride"], selectedIndex: 0) { (val, index) in
                            self.cancelRide(bookingId: bookingId, reason: val)
                            
                        }
                    }
              //  }
            }
        }.onAppear(){
            self.getMyRides()
        }
    }
}
}
struct RidesTakenView_Previews: PreviewProvider {
    static var previews: some View {
        RidesTakenView()
    }
}
//struct BasicRidesTakenViewDetailsView: View {
//
//    @State var selection: Int? = nil
//
//    var ride:Model_MyRidesTaken
//
//
//    var body: some View {
//        ScrollView{
//            
//            VStack{
//                HStack{
//                    Text(ride.route?.first ?? "").modifier(CustomTextM(fontName: UIFont.Montserrat.semiBold.fontName, fontSize: 13, fontColor: Color.black)).multilineTextAlignment(.leading)
//                    Image("orangechain")
//                    Text(ride.route?.last ?? "").modifier(CustomTextM(fontName: UIFont.Montserrat.semiBold.fontName, fontSize: 13, fontColor: Color.black)).multilineTextAlignment(.leading)
//                }.padding()
//                let urlString = IMAGEURL + getStringFromAny(ride?.rider?.profilePic ?? "")//(driverProfile.profilePic ?? "")
//                WebImage(url: URL.init(string: urlString ))
//                    .placeholder(Image(systemName: "photo")) // Placeholder Image
//                    .renderingMode(.original)
//                    .resizable()
//                    .aspectRatio(contentMode: .fill)
//                    .frame(width: 60, height: 60)
//                    .clipShape(Circle())
//
//                
//                //                    if ((offeredRoute.user?.profilePic?.count ?? 0) > 0){
//                //                        .overlay(Circle().stroke(Color.init(hex: "00AEEF"), lineWidth: 1))
//                //                    }
//                
//
//            }
////            VStack{
//////                VStack(alignment:.leading,spacing:0){
//////                    HStack{
//////                       // Image("timerdiff")
//////
//////                        VStack(alignment:.leading,spacing:0){
//////
//////                            VStack(alignment:.leading){
//////                                // BOTTOM
//////                                HStack{
//////                                    VStack(alignment:.leading,spacing:0){
////////                                        Text(ride.pickupTime ?? "").modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 16, fontColor: Color.black))
//////                                        Text(ride.route?.first ?? "").modifier(CustomTextM(fontName: UIFont.Montserrat.semiBold.fontName, fontSize: 13, fontColor: Color.black)).multilineTextAlignment(.leading)
//////                                        Text(ride.route?.last ?? "").modifier(CustomTextM(fontName: UIFont.Montserrat.semiBold.fontName, fontSize: 13, fontColor: Color.black)).multilineTextAlignment(.leading)
//////
//////                                    }
//////                                    VStack{
//////    //                                    Button(action: {
//////    //                                        print("")
//////    //                                    }){
//////    //                                        Text((Localizable.View_map.localized())).modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 15, fontColor: Color.white))
//////    //                                    }
//////    //                                    .modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 12, fontColor: Color.white))
//////    //                                    .modifier(ButtonStyle(buttonHeight: 30, buttonColor: Color.init(hex: "00ADEF"), buttonRadius: 17))
//////    //                                    .frame(maxWidth:104)
//////    //                                    .padding([.top],10)
//////
//////                                    }
//////                                }
//////                                HStack{
//////                                   // Image("selectedperson")
//////                                    Text( getStringFromAny(offeredRoute.duration ?? "") + " from your arrival").modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 12, fontColor: Color.black))
//////                                }
//////                            }
//////
//////                            VStack(alignment:.leading){
//////                                // BOTTOM
//////                                HStack{
//////                                    VStack(alignment:.leading,spacing:0){
//////                                        Text(offeredRoute.dropTime ?? "").modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 16, fontColor: Color.black))
//////                                        Text(offeredRoute.endLabel ?? "").modifier(CustomTextM(fontName: UIFont.Montserrat.semiBold.fontName, fontSize: 13, fontColor: Color.black)).multilineTextAlignment(.leading)
//////                                        Text(offeredRoute.endDetail?.secondaryText ?? "").modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 12, fontColor: Color.black))
//////
//////                                    }
//////                                    VStack{
//////    //                                    Button(action: {
//////    //                                        print("")
//////    //                                    }){
//////    //                                        Text(Localizable.View_map.localized()).modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 15, fontColor: Color.white))
//////    //                                    }
//////    //                                    .modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 12, fontColor: Color.white))
//////    //                                    .modifier(ButtonStyle(buttonHeight: 30, buttonColor: Color.init(hex: "00ADEF"), buttonRadius: 17))
//////    //                                    .frame(maxWidth:104)
//////    //                                    .padding([.top],10)
//////                                    }
//////                                }
//////                                HStack{
//////    //                                Image("selectedperson")
//////                                    Text(getStringFromAny(offeredRoute.distance ?? "") + "away from distance").modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 12, fontColor: Color.black))
//////                                }
//////                            }.padding(.top,30)
//////                        }
//////                    }
//////
//////                }.padding().frame(width:UIScreen.main.bounds.size.width - 70).background(RoundedRectangle(cornerRadius: 25).fill(Color.white).shadow(color: Color.black.opacity(0.12), radius: 3))
////
//////                VStack{
//////                    Spacer()
//////                    Text((Localizable.Total_price_for.localized()) + " " + getStringFromAny(findRide.passenger) + " " + (Localizable.passangers.localized())).modifier(CustomTextM(fontName: UIFont.Montserrat.semiBold.fontName, fontSize: 13, fontColor: Color.init(hex: "F57F20")))
//////                    Spacer()
//////                    let total = Double(findRide.passenger) * getDoubleFromAny(offeredRoute.fare ?? "0")
//////                    let currency = offeredRoute.currency
//////                    HStack(){
//////                        Spacer()
//////
//////                    Text(getStringFromAny(total)).modifier(CustomTextM(fontName: UIFont.Montserrat.semiBold.fontName, fontSize: 26, fontColor: Color.init(hex: "F57F20")))
//////                        Text(getStringFromAny(currency ?? "")).modifier(CustomTextM(fontName: UIFont.Montserrat.semiBold.fontName, fontSize: 26, fontColor: Color.init(hex: "F57F20")))
//////
//////                        Spacer()
//////
//////                    }
//////                    Spacer()
//////                }.padding().frame(width:UIScreen.main.bounds.size.width - 70,height: 100).background(RoundedRectangle(cornerRadius: 25).fill(Color.white).shadow(color: Color.black.opacity(0.12), radius: 3))
////
////                VStack{
////                    HStack{
////
////                        SDWebImage(url: URL.init(string: (IMAGEURL + (ride.rider?.profilePic ??  "") )))
////                            .placeholder(Image(systemName: "photo"))
////                            .renderingMode(.original)
////                            .resizable()
////                            .aspectRatio(contentMode: .fill)
////                            .frame(width: 60, height: 60)
////                            .clipShape(Circle())
////
////    //                    if ((offeredRoute.user?.profilePic?.count ?? 0) > 0){
////    //                        .overlay(Circle().stroke(Color.init(hex: "00AEEF"), lineWidth: 1))
////    //                    }
////
////                        VStack(alignment:.leading){
////                            Text(ride.rider?.firstName ?? "").modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 13, fontColor: Color.black)).padding(.leading,7)
////
////                            HStack{
////                                VStack{
////                                    Spacer()
////                                    StarsView(rating: Double((offeredRoute.user?.riderRating ?? "0"))!).padding(.top,4)
////                                    Spacer()
////                                }.frame(width: 80, height: 30)
////
////                                Text(ride.rider?.lastName ?? "").modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 11, fontColor: Color.black))
////
////                                Spacer()
////
//////                                NavigationLink(destination: DriverProfileView(offeredRoute: offeredRoute), tag: 0, selection: $selection) {
//////                                    // BUTTONS
//////                                    Button(action: {
//////                                        selection = 0
//////                                    }){
//////                                        Text(Localizable.More.localized()).modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 15, fontColor: Color.white))
//////                                    }
//////                                    .modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 12, fontColor: Color.white))
//////                                    .modifier(ButtonStyle(buttonHeight: 30, buttonColor: Color.init(hex: "00ADEF"), buttonRadius: 17))
//////                                    .frame(maxWidth:104)
//////                                }
////                            }.padding(.horizontal)
////                        }
////                    }
////                    HStack{
////                        Image("askquection")
////                        Text(Localizable.Ask_a_question.localized()).modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 15, fontColor: Color.black))
////                    }
////                    Divider()
////                    HStack{
////                        Image("nosmoke")
////                        Text(getStringFromAny(offeredRoute.user?.travelPreferences?.smoking ?? " -- ")).modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 15, fontColor: Color.black))
////                    }
////                    HStack{
////                        Image("nopet")
////                        Text(getStringFromAny(offeredRoute.user?.travelPreferences?.pets ?? " -- ")).modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 15, fontColor: Color.black))
////                    }
////
////                }.padding().frame(width:UIScreen.main.bounds.size.width - 70,height: 225).background(RoundedRectangle(cornerRadius: 25).fill(Color.white).shadow(color: Color.black.opacity(0.12), radius: 3))
////
//////                NavigationLink(destination: EpaymentOverView(offeredRoute: offeredRoute), tag: 1, selection: $selection) {
//////
//////                    //Button
//////                    Button(action: {
//////                        print("CLICKED")
//////                        selection = 1
//////                    }){
//////                        Text(Localizable.Continue.localized())
//////                    }
//////                    .modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 16, fontColor: Color.white))
//////                    .modifier(ButtonStyle(buttonHeight: 50, buttonColor: Color.init(hex: "00ADEF"), buttonRadius: 50))
//////                    .padding(.horizontal,30)
//////                    .padding(.top,30)
//////                }.isDetailLink(false)
////                Spacer()
////            }
//
//        }.showNavigationBarWithBackStyle()
//    }
//}
