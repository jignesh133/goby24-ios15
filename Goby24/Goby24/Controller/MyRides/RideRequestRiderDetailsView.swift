//
//  BasicRidesTakenViewDetailsView.swift
//  Goby24
//
//  Created by Jignesh Bhensadadiya on 12/23/21.
//

import SwiftUI
import SDWebImageSwiftUI
import StarRating

struct RideRequestRiderDetailsView: View {
   
    var ride:Model_RiderRideRequest
    
    @State var customConfig = StarRatingConfiguration(spacing: 4,numberOfStars: 5,stepType: .exact,minRating: 0,borderWidth: 1,borderColor: Color.black,emptyColor: Color.white,shadowRadius: 0,fillColors: [Color.black],starVertices: 5,starWeight: 0.2)
    
    var body: some View {
        ScrollView{
            VStack{
                Text("My Ride").modifier(CustomTextM(fontName: UIFont.Montserrat.semiBold.fontName, fontSize: 12, fontColor: Color.init(hex: "2E2E2E")))
                VStack{
                    HStack{
                        VStack(alignment:.leading){
                            Text(ride.route?.first?.removeWhiteSpace() ?? "").modifier(CustomTextM(fontName: UIFont.Montserrat.semiBold.fontName, fontSize: 12, fontColor: Color.init(hex: "2E2E2E")))
                        }
                        VStack(alignment:.center){
                            Image("orangechain")
                        }
                        VStack(alignment:.leading){
                        Text(ride.route?.last?.removeWhiteSpace() ?? "").modifier(CustomTextM(fontName: UIFont.Montserrat.semiBold.fontName, fontSize: 12, fontColor: Color.init(hex: "2E2E2E")))
                        }
                    }
                }.frame(width: 300, height: 80, alignment: .center)
                .background(Color.init(hex: "CBF1FF"))
                .cornerRadius(40)
                VStack{
                    
                    let urlString = IMAGEURL + getStringFromAny(ride.rider?.profilePic ?? "")//(driverProfile.profilePic ?? "")
                    WebImage(url: URL.init(string: urlString ))
                        .placeholder(Image(systemName: "photo")) // Placeholder Image
                        .renderingMode(.original)
                        .resizable()
                        .aspectRatio(contentMode: .fill)
                        .frame(width: 80, height: 80)
                        .clipShape(Circle())
                        .overlay(Circle().stroke(Color.init(hex: "00AEEF"), lineWidth: 1)).padding(.top,-50)

                    
                    //userImageView().padding(.leading,5).padding(.top,-50)
                    HStack{
                    Text((ride.rider?.firstName ?? "")).modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 14, fontColor: Color.init(hex: "2E2E2E")))
                    Text((ride.rider?.lastName ?? "")).modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 14, fontColor: Color.init(hex: "2E2E2E")))
                    }                  //  Text("37 Years old").modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 14, fontColor: Color.init(hex: "2E2E2E")))
                    Group{
                        Button(action: {
                        }){
                            HStack() {
                                StarsView(rating:getDoubleFromAny(ride.rider?.riderRating ?? "") ).padding(.top,14).frame(width: 150)
                                Text((ride.rider?.riderRating ?? "") + " / 5 ratings").modifier(CustomTextM(fontName: UIFont.Montserrat.bold.fontName, fontSize: 14, fontColor: Color.init(hex: "2E2E2E")))
                                Spacer()
                            }
                            
                        }.modifier(CustomTextM(fontName: UIFont.Montserrat.bold.fontName, fontSize: 15, fontColor: Color.init(hex: "2E2E2E")))
                        .modifier(ButtonStyle(buttonHeight: 50, buttonColor: Color.init(hex: "F2F2F2"), buttonRadius: 10))
                        .padding(.horizontal,30)
                    }.padding()
                    
                    Group{
                        VStack(){
                            HStack{
                                if (ride.rider?.isMobileNoVerified == true){
                                    Image("right")
                                }else{
                                    Image("wrong")
                                }
                                Text(Localizable.Verify_your_phone_number.localized()).modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 15, fontColor: Color.black))
                                Spacer()
                            }.padding(.horizontal)
                            HStack{
                                if (ride.rider?.isEmailVerified == true){
                                    Image("right")
                                }else{
                                    Image("wrong")
                                    
                                }
                                Text(Localizable.Verify_your_Email.localized()).modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 15, fontColor: Color.black))
                                Spacer()
                            }.padding(.horizontal)
                        }.padding(25)
                    }
                }.background(RoundedRectangle(cornerRadius: 25).fill(Color.white).shadow(color: Color.black.opacity(0.12), radius: 3)).padding(.top,80)
                
                VStack(alignment: .leading,spacing:0){
                    
                    Text(ride.requestStatus ?? "").modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 14, fontColor: Color.init(hex: "2E2E2E"))).padding()
                    
                    Divider()
                    Spacer(minLength: 10)
                    
                    VStack{
                        HStack{
                            VStack(alignment:.leading,spacing:10){
                                Text("Amount").modifier(CustomTextM(fontName: UIFont.Poppins.regular.fontName, fontSize: 14, fontColor: Color.init(hex: "00AEEF")))
                                Text("Payment Method").modifier(CustomTextM(fontName: UIFont.Poppins.regular.fontName, fontSize: 14, fontColor: Color.init(hex: "00AEEF")))
                                Text("Country").modifier(CustomTextM(fontName: UIFont.Poppins.regular.fontName, fontSize: 14, fontColor: Color.init(hex: "00AEEF")))

                                Text("No of Days").modifier(CustomTextM(fontName: UIFont.Poppins.regular.fontName, fontSize: 14, fontColor: Color.init(hex: "00AEEF")))
                                Text("No of Tourist").modifier(CustomTextM(fontName: UIFont.Poppins.regular.fontName, fontSize: 14, fontColor: Color.init(hex: "00AEEF")))

                                Text("Journey Date").modifier(CustomTextM(fontName: UIFont.Poppins.regular.fontName, fontSize: 14, fontColor: Color.init(hex: "00AEEF")))

                                Text("Journey Time").modifier(CustomTextM(fontName: UIFont.Poppins.regular.fontName, fontSize: 14, fontColor: Color.init(hex: "00AEEF")))

                                Text("Vehicle info").modifier(CustomTextM(fontName: UIFont.Poppins.regular.fontName, fontSize: 14, fontColor: Color.init(hex: "00AEEF")))

                            }.frame(maxWidth: .infinity)
                            VStack(alignment:.leading,spacing:15){
                                HStack{
                                    Text(ride.currency ?? "").modifier(CustomTextM(fontName: UIFont.Poppins.regular.fontName, fontSize: 14, fontColor: Color.init(hex: "F57F20")))
                                    Text(ride.fare ?? "").modifier(CustomTextM(fontName: UIFont.Poppins.regular.fontName, fontSize: 14, fontColor: Color.init(hex: "F57F20")))

                                }
                                Text(ride.paymentMethod ?? "").modifier(CustomTextM(fontName: UIFont.Poppins.regular.fontName, fontSize: 14, fontColor: Color.init(hex: "2E2E2E")))
                                Text(ride.country ?? "").modifier(CustomTextM(fontName: UIFont.Poppins.regular.fontName, fontSize: 14, fontColor: Color.init(hex: "2E2E2E")))

                                Text(ride.seats ?? "").modifier(CustomTextM(fontName: UIFont.Poppins.regular.fontName, fontSize: 14, fontColor: Color.init(hex: "2E2E2E")))

                                Text(ride.seats ?? "").modifier(CustomTextM(fontName: UIFont.Poppins.regular.fontName, fontSize: 14, fontColor: Color.init(hex: "2E2E2E")))

                                
                                
                                Text(ride.date ?? "").modifier(CustomTextM(fontName: UIFont.Poppins.regular.fontName, fontSize: 14, fontColor: Color.init(hex: "2E2E2E")))
                               
                                Text(ride.time ?? "").modifier(CustomTextM(fontName: UIFont.Poppins.regular.fontName, fontSize: 14, fontColor: Color.init(hex: "2E2E2E")))
                               

                                HStack(spacing:0){
                                    Text(getStringFromAny(ride.rider?.brand ?? "")).modifier(CustomTextM(fontName: UIFont.Poppins.regular.fontName, fontSize: 14, fontColor: Color.init(hex: "2E2E2E")))
                                    
                                    Text(" / ").modifier(CustomTextM(fontName: UIFont.Poppins.regular.fontName, fontSize: 14, fontColor: Color.init(hex: "2E2E2E")))
                                    
                                    Text(getStringFromAny(ride.rider?.model ?? "")).modifier(CustomTextM(fontName: UIFont.Poppins.regular.fontName, fontSize: 14, fontColor: Color.init(hex: "2E2E2E")))
                                }
                                Spacer(minLength: 10)
                            }.frame(maxWidth: .infinity)
                        }
                        
                        
                      
                    }
                    
                    Spacer(minLength: 10)
                }.frame(width:UIScreen.main.bounds.size.width - 40 ).background(RoundedRectangle(cornerRadius: 25).fill(Color.white).shadow(color: Color.black.opacity(0.12), radius: 3)).padding(.top,20)
                Spacer()
            }
        }.showNavigationBarWithBackStyle()
        
    }
}

//struct BasicRidesTakenViewDetailsView_Previews: PreviewProvider {
//    static var previews: some View {
//        BasicRidesTakenViewDetailsView()
//    }
//}
