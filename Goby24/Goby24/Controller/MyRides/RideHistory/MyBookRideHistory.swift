//
//  MyBookRideHIstory.swift
//  Goby24
//
//  Created by Jignesh Bhensadadiya on 9/15/21.
//

import SwiftUI

struct MyBookRideHistory: View {
    var body: some View {
        VStack{
            
            RidesTakenView().padding()
        }.showNavigationBarWithBackStyle()
    }
}

struct MyBookRideHistory_Previews: PreviewProvider {
    static var previews: some View {
        MyBookRideHistory()
    }
}
