//
//  MyOfferRideHistory.swift
//  Goby24
//
//  Created by Jignesh Bhensadadiya on 9/15/21.
//

import SwiftUI

struct MyOfferRideHistory: View {
    var body: some View {
        VStack{
            Text(Localizable.Rides_Offered.localized()).modifier(CustomTextM(fontName: UIFont.Rubik.medium.fontName, fontSize: 26, fontColor: Color.init(hex: "2E2E2E"))).padding()

            RidesOfferedView().padding()
        }.showNavigationBarWithBackStyle()
    }
}

struct MyOfferRideHistory_Previews: PreviewProvider {
    static var previews: some View {
        MyOfferRideHistory()
    }
}
