//
//  MyRides.swift
//  Demo
//
//  Created by Jignesh Bhensadadiya on 7/13/21.
//  Copyright © 2021 mac-00018. All rights reserved.
//

import SwiftUI

struct MyRides: View {
    
    @State var ridesTaken: Bool = false
    @State var isViewPresented: Bool = false
    @State var selection  = 0
    

    
    var body: some View {
        VStack{
            HStack{
//                Button(action: {
//                    self.isViewPresented = true
//                }){
//                    Image("filter")
//                }.sheet(isPresented: self.$isViewPresented) {
//                    FilterRidesView()
//                }
                Spacer()
                Text(Localizable.My_Rides.localized()).modifier(CustomTextM(fontName: UIFont.Rubik.medium.fontName, fontSize: 26, fontColor: Color.init(hex: "2E2E2E")))

                Spacer()
//                Button(action: {
//                    // SEARCH ICON
//                }){
//                    Image("search")
//                }

                
            }.padding(.horizontal,30).padding(.vertical)
            
            HStack{
                //Button
                Button(action: {
                    selection = 0
                }){
                    if(selection == 0){
                        VStack{
                            Text("Basic Ride").modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 13, fontColor: Color.init(hex: "00AEEF")))
                            Divider().background(Color.init(hex: "00AEEF")).frame(height: 1.5)
                        }
                    }else{
                        VStack{
                            Text("Basic Ride").modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 13, fontColor: Color.init(hex: "2E2E2E")))
                            Divider().background(Color.init(hex: "2E2E2E")).frame(height: 1.5)
                            
                        }
                    }
                }.frame(minWidth: 0, maxWidth: .infinity)

               
                //ACCOUNT Button
                Button(action: {
                    selection = 1
                }){
                    if(selection == 1){
                        VStack{
                            Text("Ride Request").modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 13, fontColor: Color.init(hex: "00AEEF")))
                            Divider().background(Color.init(hex: "00AEEF")).frame( height: 1.5)
                        }
                    }else{
                        VStack{
                            Text("Ride Request").modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 13, fontColor: Color.init(hex: "2E2E2E")))
                            Divider().background(Color.init(hex: "2E2E2E")).frame( height: 1.5)
                            
                        }
                    }
                }.frame(minWidth: 0, maxWidth: .infinity)
                
                //Button
                Button(action: {
                    selection = 2
                }){
                    if(selection == 2){
                        VStack{
                            Text("Tourist Ride").modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 13, fontColor: Color.init(hex: "00AEEF")))
                            Divider().background(Color.init(hex: "00AEEF")).frame(height: 1.5)
                        }
                    }else{
                        VStack{
                            Text("Tourist Ride").modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 13, fontColor: Color.init(hex: "2E2E2E")))
                            Divider().background(Color.init(hex: "2E2E2E")).frame(height: 1.5)
                            
                        }
                    }
                }.frame(minWidth: 0, maxWidth: .infinity)

                
            }.padding()

            if (selection == 0){
                BasicRidesView()
            }else if (selection == 1){
                RideRequestListView()
            }else if (selection == 2){
                TouristRideListView()
            }
            Spacer()
        }.showNavigationBarWithBackStyle()
    }
}

struct MyRides_Previews: PreviewProvider {
    static var previews: some View {
        MyRides()
    }
}
