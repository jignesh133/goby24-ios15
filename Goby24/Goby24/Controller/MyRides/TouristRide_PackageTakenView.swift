//
//  TouristRide_PackageTakenView.swift
//  Goby24
//
//  Created by Jignesh Bhensadadiya on 9/24/21.
//

import SwiftUI

struct TouristRide_PackageTakenView: View {
    @State var arrRides:[Model_TouristPackageTaken] = [Model_TouristPackageTaken]()
    
    func cancelPackage(bookingId:String,reason:String){
        var param:[String:Any] = [String:Any]()
        param["booking"] = bookingId
        param["reason"] = reason

        WebAccess.putDataWith(_Url: (WebAccess.PACKAGE_OFFER_CANCELLED + bookingId + "/") , _parameters: [:]) { (result) in
            switch (result){
            case .Success(let _data):
                print(_data)
                do{
                    self.getMyRides()
                }catch(let error){
                    print(error)
                }
                break
            case .Error(let msg):
                ShowAlert(title: APPNAME, msg: msg, view: UIApplication.getTopViewController()!)
                break
            }
        }
    }
    func getMyRides(){
        
        WebAccess.getDataWith(_Url: WebAccess.TOURIST_RIDE_TAKEN_HISTORY, _parameters: [:]) { (result) in
            switch (result){
            case .Success(let _data):
                print(_data)
                do{
                    let result:[String:Any] = UtilsObjs.cleanJson(to: _data ) as? [String : Any] ?? [String:Any]()
                    print(result)
                    let data1 =  try JSONSerialization.data(withJSONObject: result["result"] ?? [] , options: JSONSerialization.WritingOptions.prettyPrinted)
                    let convertedString = String(data: data1, encoding: String.Encoding.utf8)
                    let jsonData = Data((convertedString?.utf8)!)
                    self.arrRides = try JSONDecoder().decode([Model_TouristPackageTaken].self, from: jsonData)
                    NSLog("LOG")
                }catch(let error){
                    print(error)
                }
                break
            case .Error(let msg):
                ShowAlert(title: APPNAME, msg: msg, view: UIApplication.getTopViewController()!)
                break
            }
        }
    }
    
    var body: some View {
        
        ScrollView{
            ForEach(0..<self.arrRides.count, id: \.self) { row in
                NavigationLink(destination: TouristRidePackageTakenDetailsView(ride:self.arrRides[row])) {
                TouristRide_PackageTakenItemView(myPackage: self.arrRides[row]){ (bookingId) in
                    RPicker.selectOption(title: "Why do you want to cancel the ride?", cancelText: "Cancel", doneText: "Done", dataArray: ["Change of Mind","Emergency Situation","want to book new ride"], selectedIndex: 0) { (val, index) in
                        self.cancelPackage(bookingId: bookingId, reason: val)

                    }
                }
                }
            }
        }.onAppear(){
            self.getMyRides()
        }
    }
}



struct TouristRide_PackageTakenItemView: View {
    
    @State var myPackage:Model_TouristPackageTaken
    let onClickCancel: (String) -> Void

    var body: some View {
        
        VStack{
            ZStack{
                
                Image("myridesbg").frame(height: 116)
                
                VStack(alignment: .leading, spacing: 0) {
                    
                    HStack{
                        Image("calendar").padding([.leading],15)

                        let date =  (myPackage.bookingDate?.stringToDate(strCurrentFormat: "yyyy-MM-dd").getFormattedDate(formatter: "dd MMM yyyy")) ?? ""
                        //let time = ""//(myPackage.bookingTime?.stringToDate(strCurrentFormat: "hh:mm:ss").getFormattedDate(formatter: "HH:mm a")) ?? ""
                        
                        Text(date).modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 11, fontColor: Color.init(hex: "00AEEF"))).padding([.trailing],15).frame(maxWidth: .infinity, alignment: .leading)
                        
                        Spacer()
                        Text(myPackage.status ?? "").modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 12, fontColor: Color.init(hex: "2E2E2E"))).padding([.horizontal],15)
                    }
                    
                    Divider().padding([.horizontal,.top],5)
                    
                    HStack{
                       // Image("orangechain").padding([.leading],15)
                        VStack{
                            Text(myPackage.touristPackage?.packageName ?? "").modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 11, fontColor: Color.init(hex: "00AEEF"))).padding([.horizontal],15).frame(maxWidth: .infinity, alignment: .leading).padding(.top,02)
                        }.padding(.vertical,10)
                    }.padding(.top,2)
                    
                    HStack{
                        Image("clock").padding([.leading],15)
                        
                        Text(myPackage.touristPackage?.dateFrom ?? "-").modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 11, fontColor: Color.init(hex: "00AEEF"))).padding([.trailing],5)
                        Divider().frame(width: 1, height: 10)
                        Text((myPackage.touristPackage?.packagePrice ?? "")).modifier(CustomTextM(fontName: UIFont.Montserrat.bold.fontName, fontSize: 11, fontColor: Color.init(hex: "F57F20"))).padding([.trailing],5)
                        Text( myPackage.touristPackage?.currency ?? "").modifier(CustomTextM(fontName: UIFont.Montserrat.bold.fontName, fontSize: 11, fontColor: Color.init(hex: "F57F20"))).padding([.trailing],5)
                        Spacer()
//                        }


                        if (myPackage.status == "Package Completed"){
                            //ACCOUNT Button
                            Button(action: {
                                let alert = UIAlertController(title: "Would you like to share your experience with rating?", message: "You can share your experience by rating your ridemate, it will help us grow our ride sharing community", preferredStyle: .alert)
                                let skip = UIAlertAction(title:"Skip", style: .cancel) { (res) in
                                    
                                }
                                let sure = UIAlertAction(title:"Yes Sure", style: .default) { (res) in
                                    // GIE TRAVELLER RATING
                                }
                                alert.addAction(skip)
                                alert.addAction(sure)
                                
                                UIApplication.getTopViewController()!.present(alert, animated: true, completion: nil)
                                
                            }){
                                Text("Package Completed")
                            }
                            .modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 12, fontColor: Color.white))
                            .modifier(ButtonStyle(buttonHeight: 30, buttonColor: Color.init(hex: "00ADEF"), buttonRadius: 4))
                            .padding([.trailing],5)
                        }
//                        else if (myPackage.status == "Accepted"){
//                            
//                            Button(action: {
//                                    
//                                onClickCancel(getStringFromAny(myPackage.id ?? ""))
//
//                            }){
//                                Text("Cancel Package")
//                            }
//                            .modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 12, fontColor: Color.white))
//                            .modifier(ButtonStyle(buttonHeight: 30, buttonColor: Color.init(hex: "00ADEF"), buttonRadius: 4))
//                            .padding([.trailing],5)
//
//                        }
                    }.padding(.top,5)
                    
                    VStack{
                        if(myPackage.status == "Accepted" ){
                            //ACCOUNT Button
                            Button(action: {
                                onClickCancel(getStringFromAny(myPackage.id ?? ""))

                            }){
                                Text("Cancel Package")
                            }
                            .modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 12, fontColor: Color.white))
                            .modifier(ButtonStyle(buttonHeight: 30, buttonColor: Color.init(hex: "F57F20"), buttonRadius: 15))
                            .padding(10)
                        }
                    }
                }
                Spacer()
            }
            Spacer()
        }
        
    }
    
}



