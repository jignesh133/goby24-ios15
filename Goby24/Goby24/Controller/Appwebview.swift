//
//  webview.swift
//  Goby24
//
//  Created by Jignesh Bhensadadiya on 7/21/21.
//

import SwiftUI
import WebKit



struct SwiftUIWebview: UIViewRepresentable {
    let url: URL
    let needToGoBack: (Bool) -> Void

    func makeCoordinator() -> Coordinator {
        Coordinator(needToGoBack: needToGoBack)
    }
    
    func makeUIView(context: Context) -> WKWebView {
        // Enable javascript in WKWebView to interact with the web app
        let preferences = WKPreferences()
        preferences.javaScriptCanOpenWindowsAutomatically = true
       
        let webPref = WKWebpagePreferences()
        webPref.allowsContentJavaScript = true
       
        
        let configuration = WKWebViewConfiguration()
        configuration.preferences = preferences
        configuration.defaultWebpagePreferences = webPref
        
        let webView = WKWebView(frame: CGRect.zero, configuration: configuration)
        webView.navigationDelegate = context.coordinator
        webView.allowsBackForwardNavigationGestures = true
        webView.scrollView.isScrollEnabled = true
        webView.addObserver(context.coordinator, forKeyPath: #keyPath(WKWebView.title), options: .new, context: nil)

       return webView
    }
    
    func updateUIView(_ webView: WKWebView, context: Context) {
        if let url = URL(string: self.url.absoluteString) {
            webView.load(URLRequest(url: url))
        }
    }
    
    class Coordinator : NSObject, WKNavigationDelegate {
       
        let needToGoBack: (Bool) -> Void
        init(needToGoBack: @escaping (Bool) -> Void) {
            self.needToGoBack = needToGoBack
        }
        
        deinit {
            appDelegate?.stopLoadingView()
        }
       
        func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
            print(webView.title ?? "")
            appDelegate?.stopLoadingView()
        }
        
        func webViewWebContentProcessDidTerminate(_ webView: WKWebView) {
            appDelegate?.stopLoadingView()
        }
        
        func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
            appDelegate?.stopLoadingView()
        }
        
        func webView(_ webView: WKWebView, didCommit navigation: WKNavigation!) {
        }
        
        func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
            appDelegate?.startLoadingview()
        }
        
        func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void){
            
//            if(navigationAction.navigationType == .other) {
//                if navigationAction.request.url != nil {
//
//                    if (navigationAction.request.url?.absoluteString ?? "").contains("https://www.goby24.ch/payment-success"){
//                        needToGoBack(true)
//                    }
//                }
//
//            }
            print("TEST")
            print(navigationAction.request.url?.absoluteString ?? "NO URL")
            print("TEST")
            if (navigationAction.request.url?.absoluteString ?? "").contains("goby24.ch/payment-success"){
                needToGoBack(true)
            }
            decisionHandler(.allow)
        }
        
        
        func webView(_ webView: WKWebView, didReceiveServerRedirectForProvisionalNavigation navigation: WKNavigation!) {
            if let url = webView.url?.absoluteString{
                print("url = \(url)")
            }
        }
        override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
            if keyPath == "title" {
              //  print(webview.tit)
            }
        }

    }
}
