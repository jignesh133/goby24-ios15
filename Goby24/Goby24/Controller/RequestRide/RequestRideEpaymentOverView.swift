//
//  RequestRideEpaymentOverView.swift
//  Goby24
//
//  Created by Jignesh Bhensadadiya on 9/17/21.
//

import SwiftUI

struct RequestRideEpaymentOverView: View {
    @State var paymentClicked: Bool = true
    @State var objRequestRide:Model_RideRequest?

    
    var body: some View {
        VStack{
            HStack{
                //Button
                Button(action: {
                    paymentClicked = true
                }){
                    if(paymentClicked == true){
                        VStack{
                            Text(Localizable.Payments.localized()).modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 13, fontColor: Color.init(hex: "00AEEF")))
                            Divider().background(Color.init(hex: "00AEEF")).frame(height: 1.5)
                        }
                    }else{
                        VStack{
                            Text(Localizable.Payments.localized()).modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 13, fontColor: Color.init(hex: "2E2E2E")))
                            Divider().background(Color.init(hex: "2E2E2E")).frame(height: 1.5)
                        }
                    }
                }.frame(minWidth: 0, maxWidth: .infinity)
                
                
                //ACCOUNT Button 2E2E2E
                Button(action: {
                    paymentClicked = false
                }){
                    if(paymentClicked == false){
                        VStack{
                            Text(Localizable.Profile.localized()).modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 13, fontColor: Color.init(hex: "00AEEF")))
                            Divider().background(Color.init(hex: "00AEEF")).frame( height: 1.5)
                        }
                    }else{
                        VStack{
                            Text(Localizable.Profile.localized()).modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 13, fontColor: Color.init(hex: "2E2E2E")))
                            Divider().background(Color.init(hex: "2E2E2E")).frame( height: 1.5)
                        }
                    }
                }.frame(minWidth: 0, maxWidth: .infinity)
            }.padding()
            
            
            if(paymentClicked == false){
                RequestRideRiderOverView(objRequestRide: self.objRequestRide)
            }else{
                RequestRideEpaymentView(objRequestRide: self.objRequestRide)
            }
            Spacer()
        }.showNavigationBarWithBackStyle()
        
    }
}
