//
//  RequestRideView.swift
//  Goby24
//
//  Created by Jignesh Bhensadadiya on 9/17/21.
//

//
//  RequestFromTouristView.swift
//  Goby24
//
//  Created by Jignesh Bhensadadiya on 7/17/21.
//

import SwiftUI

struct RequestRideView: View{
    
    @State var selectedItemIndex: Int = 0
    
    var body: some View {
        VStack{
            HStack(alignment: .center, spacing:0){
                Spacer()
                
                //Button
                Button(action: {
                    selectedItemIndex = 0
                }){
                    if(selectedItemIndex == 0){
                        VStack{
                            Text((Localizable.Create_Request.localized())).modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 13, fontColor: Color.init(hex: "00AEEF"))).multilineTextAlignment(.center)
                            Divider().background(Color.init(hex: "00AEEF")).frame(height: 1.5)
                        }
                    }else{
                        VStack{
                            Text(Localizable.Create_Request.localized()).modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 13, fontColor: Color.init(hex: "2E2E2E"))).multilineTextAlignment(.center)
                            Divider().background(Color.init(hex: "2E2E2E")).frame(height: 1.5)
                            
                        }
                    }
                }.frame(width: 130, height: 50)
                
                
                //ACCOUNT Button
                Button(action: {
                    selectedItemIndex = 1
                    
                }){
                    if(selectedItemIndex == 1){
                        VStack{
                            Text(Localizable.All_Requests.localized()).modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 13, fontColor: Color.init(hex: "00AEEF"))).multilineTextAlignment(.center)
                            Divider().background(Color.init(hex: "00AEEF")).frame( height: 1.5)
                        }
                    }else{
                        VStack{
                            Text(Localizable.All_Requests.localized()).modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 13, fontColor: Color.init(hex: "2E2E2E"))).multilineTextAlignment(.center)
                            Divider().background(Color.init(hex: "2E2E2E")).frame( height: 1.5)
                        }
                    }
                }.frame(width: 130, height: 50)
                
                Spacer()
                
            }.frame(width: 260, height: .infinity)
            
            if (selectedItemIndex == 0){
                RequestRideCreateView(index: self.$selectedItemIndex)
            }else{
                RequestRideHistoryView(objArrRequestRIdeList: ArrRequestRIdeList.init(), selectedItemIndex: self.$selectedItemIndex)
            }
        }        .showNavigationBarWithBackStyle()
    }
}

struct RequestRideView_Previews: PreviewProvider {
    static var previews: some View {
        RequestRideView()
    }
}
