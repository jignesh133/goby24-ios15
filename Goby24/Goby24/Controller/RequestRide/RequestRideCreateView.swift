//
//  RequestRideCreateView.swift
//  Goby24
//
//  Created by Jignesh Bhensadadiya on 9/17/21.
//

import SwiftUI

struct RequestRideCreateView: View {
    
    @Binding var selectedItemIndex : Int

    @State var txtChooseTouristSpot:String?
    @State var desc:String = ""
    @State var country:String?
    @State var addNewSpot:String?
    @State var paymentMethods:String = "E-Payment"

    @State var startJourney:String = ""
    @State var endJourney:String = ""
    
    @State var startDate:String = ""
    @State var startTime:String = ""

    @State var budget:String = ""
    @State var currency:String = ""
    @State var noofpassenger:String = ""
   
//    @State var isRatingView:Bool = false
//    @State var bookingId:String = ""
//
    
    @State var isDropDownOpen:Bool = false
    @State var isPaymentMethodDropDownOpen:Bool = false
    
    @State var SDate:Date = Date()
    @State var STime:Date = Date()

    @State var startLocation:Model_searchLocation?
    @State var endLocation:Model_searchLocation?
    
    @State var isOpenStartLocation:Bool = false
    @State var isOpenEndLocation:Bool = false

    @State var arrPaymentMethods:[String] = ["E-Payment","Cash"]

    

//    $showingSheet
    @Environment(\.viewController) private var viewControllerHolder: UIViewController?

    init(index:Binding<Int>) {
        UIScrollView.appearance().keyboardDismissMode = .onDrag
        _selectedItemIndex = index
    }
    //MARK: VALIDATE STRING
    func validateData() -> String {
        if ((country ?? "").removeWhiteSpace().count == 0){
            return "Please select country"
        }else if (paymentMethods.removeWhiteSpace().count == 0){
            return "please select payment methods"
        }else if (startJourney.removeWhiteSpace().count == 0){
            return "please enter start journey "
        }else if (endJourney.removeWhiteSpace().count == 0){
            return "please enter end journey "
        }else if (startDate.removeWhiteSpace().count == 0){
            return "please enter journey date"
        }else if (startTime.removeWhiteSpace().count == 0){
            return "please enter journey time"
        }else if (noofpassenger.removeWhiteSpace().count == 0){
            return "please insert number of passenger"
        }else if (noofpassenger.removeWhiteSpace().count > 7){
            return "Maximum 7 passenger allowed"
        }        else if (budget.removeWhiteSpace().count == 0){
            return "please insert budget"
        }else{
            return ""
        }
    }

    func submitRequest(){
        
        var param:[String:Any] = [String:Any]()
        param["country"] = country
        param["journey_from"] = startJourney
        param["journey_to"] =  endJourney
        param["journey_date"] = SDate.getFormattedDate(formatter: "yyyy-MM-dd")
        param["pickup_time"] = startTime
        param["seats"] = noofpassenger
        param["budget"] = budget
        param["payment_method"] = paymentMethods
        param["currency"] = currency
        param["remark"] = desc
        

        print(param)
        WebAccess.postDataWith(_Url: WebAccess.RIDE_REQUEST, _parameters: param) { (result) in
            switch (result){
            case .Success(let _data):
                print(_data)
                do{
                    let result:[String:Any] = UtilsObjs.cleanJson(to: _data ) as? [String : Any] ?? [String:Any]()
                    ShowAlert(title: APPNAME, msg: getStringFromAny(result["result"] ?? ""), view: UIApplication.getTopViewController()!)
                    
                    
                    print(result)
                }catch(let error){
                    print(error)
                }
                break

            case .Error(let msg):
                ShowAlert(title: APPNAME, msg: msg, view: UIApplication.getTopViewController()!)
                break
            }
            
        }
    }
    var body: some View {
        

        
        ScrollView{

            NavigationLink(destination:SearchLocation(strText: self.startLocation?.structuredFormatting?.mainText ?? "",isNeedToPopView:true,  onLocationSelect: { (location,isClear) in
                if (isClear == false){
                startLocation = location
                startJourney = location?.structuredFormatting?.mainText ?? ""
                }else{
                    startLocation = nil
                    startJourney =  ""
                }
            }),isActive: $isOpenStartLocation) {EmptyView()}.isDetailLink(false)
            
            
            NavigationLink(destination:SearchLocation(strText: self.endLocation?.structuredFormatting?.mainText ?? "",isNeedToPopView:true,  onLocationSelect: { (location,isClear) in
                if (isClear == false){

                endLocation = location
                endJourney = location?.structuredFormatting?.mainText ?? ""
                }else{
                    endLocation = nil
                    endJourney =  ""

                }
            }),isActive: $isOpenEndLocation) {EmptyView()}.isDetailLink(false)
            
            
            VStack{
                
                
                
                VStack{
                    Group{
                       // Text("Chose your country").modifier(CustomTextM(fontName: UIFont.Rubik.medium.fontName, fontSize: 26, fontColor: Color.init(hex: "2E2E2E")))
                        Text("Chose your country")
                            .modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 13, fontColor: Color.init(hex: "00AEEF"))).padding(.leading,8)

                        //Button
                        Button(action: {
                            withAnimation {
                                isDropDownOpen.toggle()
                            }
                        }){
                            HStack() {
                                Spacer()
                                Text(country ?? "Choose your  country *")
                                Spacer()
                                Image("down").padding(.trailing,20)
                            }
                        }
                        .modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 16, fontColor: Color.init(hex: "2E2E2E")))
                        .modifier(ButtonStyle(buttonHeight: 50, buttonColor: Color.init(hex: "F2F2F2"), buttonRadius: 50))
                        .padding(.horizontal,5)
                        
                        if (isDropDownOpen == true && (appDelegate?.countryNames.count ?? 0) > 0){
                            ScrollView{
                                VStack{
                                    ForEach(0..<(appDelegate?.countryNames.count ?? 0), id: \.self) { row in
                                        Button(action: {
                                            country = (appDelegate?.countryNames[row] ?? "")
                                            isDropDownOpen = false
                                        }){
                                            Text(appDelegate?.countryNames[row] ?? "")
                                        }
                                        .modifier(CustomTextM(fontName: UIFont.Montserrat.semiBold.fontName, fontSize: 16, fontColor: Color.init(hex: "2E2E2E")))
                                        .modifier(ButtonStyle(buttonHeight: 50, buttonColor: Color.init(hex: "F2F2F2"), buttonRadius: 0))
                                    }
                                }
                            }.frame(maxHeight:150).cornerRadius(10).padding(.top,2)
                            
                        }
                    }
                    Group{
                       // Text("Payment Method").modifier(CustomTextM(fontName: UIFont.Rubik.medium.fontName, fontSize: 26, fontColor: Color.init(hex: "2E2E2E")))
                        Text("Chose payment methods")
                            .modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 13, fontColor: Color.init(hex: "00AEEF"))).padding(.leading,8)

                        //Button
                        Button(action: {
                            withAnimation {
                                isPaymentMethodDropDownOpen.toggle()
                            }
                        }){
                            HStack() {
                                Spacer()
                                Text(paymentMethods )
                                Spacer()
                                Image("down").padding(.trailing,20)
                            }
                        }
                        .modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 16, fontColor: Color.init(hex: "2E2E2E")))
                        .modifier(ButtonStyle(buttonHeight: 50, buttonColor: Color.init(hex: "F2F2F2"), buttonRadius: 50))
                        .padding(.horizontal,5)
                        
                        if (isPaymentMethodDropDownOpen == true){
                            ScrollView{
                                VStack{
                                    ForEach(0..<(arrPaymentMethods.count), id: \.self) { row in
                                        Button(action: {
                                            paymentMethods = (arrPaymentMethods[row] )
                                            isPaymentMethodDropDownOpen = false
                                        }){
                                            Text(arrPaymentMethods[row] )
                                        }
                                        .modifier(CustomTextM(fontName: UIFont.Montserrat.semiBold.fontName, fontSize: 16, fontColor: Color.init(hex: "2E2E2E")))
                                        .modifier(ButtonStyle(buttonHeight: 50, buttonColor: Color.init(hex: "F2F2F2"), buttonRadius: 0))
                                    }
                                }
                            }.frame(maxHeight:150).cornerRadius(10).padding(.top,2)
                            
                        }
                    }
                    Group{
                        HStack(spacing:0){
                            Group{
                                VStack(alignment: .leading, spacing: 0){
                                    Text(Localizable.Start_journey_from.localized())
                                        .modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 13, fontColor: Color.init(hex: "00AEEF"))).padding(.leading,8)
                                    
                                    TextField("Ex: Chittagong bus.. ", text: $startJourney, onEditingChanged: { (change) in
                                        if change == true{
                                            UIApplication.shared.endEditing()
                                            isOpenStartLocation = true
                                        }
                                    })
                                    .textFieldStyle( MyTextFieldStyle(fontSize: 13))
                                    .keyboardType(.default)
                                    .padding(.horizontal,5)
                                }
                            }
                            Group{
                                VStack(alignment: .leading, spacing: 0){
                                    Text(Localizable.End_Journey_to.localized()).modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 13, fontColor: Color.init(hex: "00AEEF"))).padding(.leading,8)
                                    
                                    TextField("Ex: Chittagong bus.. ", text: $endJourney, onEditingChanged: { (change) in
                                        if change == true{
                                            UIApplication.shared.endEditing()
                                            isOpenEndLocation = true
                                        }
                                    })
                                    .textFieldStyle(MyTextFieldStyle(fontSize: 13))
                                    .keyboardType(.default)
                                    .padding(.horizontal,5)
                                    
                                }
                            }
                        }
                    }
                    Group{
                        HStack{
                            
                            VStack(alignment: .leading, spacing: 0){
                                Text("Journey Date").modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 13, fontColor: Color.init(hex: "00AEEF"))).padding(.leading,8)
                                
                                ZStack{
                                    
                                    Button(action: {
                                        RPicker.selectDate(title: " Journey Date",datePickerMode: .date,minDate: SDate, didSelectDate: { (selectedDate) in
                                            SDate = selectedDate
                                            startDate = selectedDate.getFormattedDate(formatter: "MMM dd")
                                        })
                                        
                                    }){
                                        
                                        HStack() {
                                            Text(startDate).padding(.leading,10)
                                            Spacer()
                                            Image("calender").padding(.trailing,10)
                                        }
                                    }.frame(height:45)
                                    .modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 13, fontColor: .black))
                                    .modifier(ButtonStyle(buttonHeight: 50, buttonColor: Color.init(hex: "F2F2F2"), buttonRadius: 50))
                                }
                                
                                
                            }
                            Group{
                                VStack{
                                    
                                    Text("Journey Time").modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 13, fontColor: Color.init(hex: "00AEEF"))).padding(.leading,8).disabled(true)
                                    
                                    ZStack{
                                        
                                        Button(action: {
                                            RPicker.selectDate(title: "Journey Time",datePickerMode: .time,minDate: SDate, didSelectDate: { (selectedDate) in
                                                STime = selectedDate
                                                startTime = selectedDate.getFormattedDate(formatter: "h:m a")
                                            })
                                        }){
                                            HStack() {
                                                Text(startTime).padding(.leading,10)
                                                Spacer()
                                                Image("calender").padding(.trailing,10)
                                            }
                                        }.frame(height:45)
                                        .modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 13, fontColor:.black))
                                        .modifier(ButtonStyle(buttonHeight: 50, buttonColor: Color.init(hex: "F2F2F2"), buttonRadius: 50))
                                    }
                                }
                            }
                        }
                    }
                    Group{
                        HStack{
                            Group{
                                VStack(alignment: .leading, spacing: 0){
                                    Text(Localizable.No_of_passengers.localized()).modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 13, fontColor: Color.init(hex: "00AEEF"))).padding(.leading,8)
                                    
                                    TextField("Ex: 1 ", text: $noofpassenger)
                                        .textFieldStyle(MyTextFieldStyle(fontSize: 13))
                                        .keyboardType(.numberPad)
                                        .padding(.horizontal,5)
                                    
                                }
                            }
                            Group{
                                VStack(alignment: .leading, spacing: 0){
                                    
                                    Text(Localizable.Package_Budget.localized()).modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 13, fontColor: Color.init(hex: "00AEEF"))).padding(.leading,8)
                                    
                                    priceTextField(text: $budget, currency: $currency, placeHolderText: "price", fontSize: 13)
                                    .textFieldStyle(MyTextFieldStyle(fontSize: 13))
                                }
                            }
                        }
                    }
                    Group{
                        VStack(alignment: .leading, spacing: 0){
                            
                            Text(Localizable.Write_description_about_the_tourist_package.localized()).modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 13, fontColor: Color.init(hex: "00AEEF"))).padding(.leading,8)
                            
                            Group{
                                VStack{
                                    TextEditor(text: $desc).modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 13, fontColor: Color.black)).textContentType(.none).multilineTextAlignment(.leading).lineLimit(nil)
                                    Spacer()
                                }.padding()
                            }.frame( height: 100, alignment: .leading).background(Color.init(hex: "F5F5F5")).clipShape(RoundedRectangle(cornerRadius: 14)).padding(.horizontal,10)
                        }
                    }
                    
                    VStack(alignment: .center, spacing: 25){
                        //Button
                        Button(action: {
                            
                            let str = self.validateData()
                            if(str.count > 0){
                                print(str)
                                ShowAlert(title: APPNAME, msg: str, view: UIApplication.getTopViewController()!)
                            }else{
                                self.submitRequest()
                            }
                            
                            // ratingsListView
                        }){
                            VStack{
                            Text("Submit Request").frame(minWidth: 10, maxWidth: .infinity)
                            }
                        }
                        .modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 16, fontColor: Color.white))
                        .modifier(ButtonStyle(buttonHeight: 50, buttonColor: Color.init(hex: "00ADEF"), buttonRadius: 50))
                    }.padding(.horizontal,30).padding(.vertical,30)
                    

                    
                }.padding().background(Color.white).clipShape(RoundedRectangle(cornerRadius: 21, style: .continuous)).shadow(radius: 10).padding(.vertical)
            }.padding()
            
        } .onAppear(){
            if (startDate == "" && startTime == ""){
                startDate = Date().getFormattedDate(formatter: "MMM dd h:m a")
                startTime = Date().getFormattedDate(formatter: "h:m a")
            }
        }//.resignKeyboardOnDragGesture()

    }
}

//struct RequestRideCreateView_Previews: PreviewProvider {
//    static var previews: some View {
//        RequestRideCreateView(index: 0)
//    }
//}
