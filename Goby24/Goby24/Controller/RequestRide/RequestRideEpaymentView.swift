//
//  RequestRideEpaymentView.swift
//  Goby24
//
//  Created by Jignesh Bhensadadiya on 9/17/21.
//

import SwiftUI

struct RequestRideEpaymentView: View {
    
    @State var objRequestRide:Model_RideRequest?
    @State var isPayOnline:Bool = false
    @State var cardUrl:String = ""
    @State var isRatingView:Bool = false
    @State var isWebview:Bool = false
    @State var paymentUrl:String = ""
    @State var isTermAccepted:Bool = false
    @State var bookingId:String = ""
    @State var serviceCharge:String?
    @State var total:String?


    func requestRideEpayment(){
        WebAccess.getDataWith(_Url: (WebAccess.REQUEST_RIDE_EPAY + getStringFromAny(self.objRequestRide?.id ?? "") + "/"), _parameters: [:]) { (result) in
            switch (result){
            case .Success(let _data):
                print(_data)
                let result = _data["result"] as? [String:Any] ?? [:]

                self.paymentUrl = result["payment_page_url"] as? String ?? ""

                self.isWebview  = true

                break
            case .Error(let msg):
                ShowAlert(title: APPNAME, msg: msg, view: UIApplication.getTopViewController()!)
                break
            }
        }
    }
    func requestRideCashpayment(){
        var param:[String:Any] = [String:Any]()
        param["amount_paid"] = total //self.objRequestRide?.budget//(getStringFromAny(getTotal()))
        param["ride_request"] = objRequestRide?.id
        WebAccess.postDataWith(_Url: WebAccess.REQUEST_RIDE_CASHPAY, _parameters: param) { (result) in
            switch (result){
            case .Success( _):

                self.isWebview  = false
                ShowAlertWithCompletation(title: APPNAME, msg: "Request Ride Booked sucessfully", view: UIApplication.getTopViewController()!) { (result) in
//                    self.isRatingView = true
                }
                
                break
            case .Error(let msg):
                ShowAlert(title: APPNAME, msg: msg, view: UIApplication.getTopViewController()!)
                break
            }
        }
    }
    func getCharges(){
        var param:[String:Any] = [String:Any]()
        param["currency"] = objRequestRide?.currency//(getStringFromAny(getTotal()))
        param["base_fare"] = objRequestRide?.budget
        //    /api/ride/get-ride-fare-details-with-base-fare?currency=CHF&base_fare=100.0
        
        WebAccess.getDataWith(_Url: (WebAccess.FARE_DETAILS_WITH_BASE_FARE), _parameters: param) { (result) in
            switch (result){
            case .Success( let data):
                print(data)
                let res = data["result"] as? [String:Any]
                
                serviceCharge = getStringFromAny(res?["service_charge"]  ?? "")
                total = getStringFromAny(res?["total"]  ?? "")
                
                print(serviceCharge)
                print(total)
                print(res)

                
//                self.isWebview  = false
//                ShowAlertWithCompletation(title: APPNAME, msg: "Request Ride Booked sucessfully", view: UIApplication.getTopViewController()!) { (result) in
////                    self.isRatingView = true
//                }
                
                break
            case .Error(let msg):
                ShowAlert(title: APPNAME, msg: msg, view: UIApplication.getTopViewController()!)
                break
            }
        }
    }
    
    var body: some View {
        NavigationLink(destination:ratingsListView(bookingid:self.bookingId), isActive: $isRatingView){ EmptyView() }.isDetailLink(false)
        NavigationLink(destination: RequestRidepaymentWebview(urlString:paymentUrl, isFinished: { (value) in
            if (value == true){
                //isRatingView = true
            }
        }),isActive: $isWebview) {EmptyView()}.isDetailLink(false)
        
        if (isPayOnline == false){
            VStack(alignment: .leading,spacing:0){
                
                Text((Localizable.Payments.localized())).modifier(CustomTextM(fontName: UIFont.Montserrat.semiBold.fontName, fontSize: 16, fontColor: Color.white)).frame(width: UIScreen.main.bounds.size.width - 40, height: 40, alignment: .center).background(Color.init(hex: "00AEEF")).cornerRadius(radius: 6,corners: [.topLeft, .topRight])
                HStack{
                    
                    Text((Localizable.Total_price_for.localized()) + "\(self.objRequestRide?.seats ?? "")" + " passangers").modifier(CustomTextM(fontName: UIFont.Montserrat.semiBold.fontName, fontSize: 16, fontColor: Color.init(hex: "707070")))
                    Spacer()
                    
                    Text(self.objRequestRide?.budget ?? "").modifier(CustomTextM(fontName: UIFont.Montserrat.semiBold.fontName, fontSize: 16, fontColor: Color.init(hex: "#F57F20")))
                    Text(self.objRequestRide?.currency ?? "").modifier(CustomTextM(fontName: UIFont.Montserrat.semiBold.fontName, fontSize: 16, fontColor: Color.init(hex: "#F57F20")))

                }.padding()
                HStack{
                    Text("GOBY24 Services").modifier(CustomTextM(fontName: UIFont.Montserrat.semiBold.fontName, fontSize: 16, fontColor: Color.init(hex: "707070")))
                    Spacer()
                    Text(serviceCharge ?? "").modifier(CustomTextM(fontName: UIFont.Montserrat.semiBold.fontName, fontSize: 16, fontColor: Color.init(hex: "#F57F20")))
                    Text(self.objRequestRide?.currency ?? "" ).modifier(CustomTextM(fontName: UIFont.Montserrat.semiBold.fontName, fontSize: 16, fontColor: Color.init(hex: "#F57F20")))

                }.padding(.horizontal,10)
                Divider().padding(.horizontal).padding(.top)
                HStack{
                    Text("TOTAL").modifier(CustomTextM(fontName: UIFont.Montserrat.semiBold.fontName, fontSize: 16, fontColor: Color.init(hex: "707070")))
                    Spacer()
                    Text(total ?? "" ).modifier(CustomTextM(fontName: UIFont.Montserrat.semiBold.fontName, fontSize: 16, fontColor: Color.init(hex: "#F57F20")))
                    Text(self.objRequestRide?.currency ?? "" ).modifier(CustomTextM(fontName: UIFont.Montserrat.semiBold.fontName, fontSize: 16, fontColor: Color.init(hex: "#F57F20")))

                }.padding(.horizontal).padding(.top,30)
                
                Button(action: {
                    isTermAccepted.toggle()
                }){
                    if (isTermAccepted == true){
                        Image(systemName: "circle").scaledToFit()
                        Text(Localizable.I_agree_to_terms_and_conditions.localized()).modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 13, fontColor: Color.init(hex: "00AEEF")))
                    }else{
                        Image(systemName: "largecircle.fill.circle").scaledToFit()
                        Text((Localizable.I_agree_to_terms_and_conditions.localized())).modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 13, fontColor: Color.init(hex: "00AEEF")))
                    }
                }.padding(.vertical).padding(.horizontal)
                
                HStack{
                    Spacer()
                    Button(action: {
                        if (self.objRequestRide?.paymentMethod?.lowercased() == "cash"){
                            self.requestRideCashpayment()

                        }else{
                            self.requestRideEpayment()

                        }
                    }){
                        if (self.objRequestRide?.paymentMethod?.lowercased() == "cash"){
                            Text("paycash")
                        }else{
                            Text("Pay")
                        }
                        
                    }
                    .modifier(CustomTextM(fontName: UIFont.Poppins.regular.fontName, fontSize: 15, fontColor: Color.white))
                    .modifier(ButtonStyle(buttonHeight: 30, buttonColor: Color.init(hex: "00ADEF"), buttonRadius: 30))
                    .frame(width:140)
                    .padding(.horizontal)
                }
                
                Spacer()
            }.frame(width:UIScreen.main.bounds.size.width - 40 ,height: 290).background(RoundedRectangle(cornerRadius: 25).fill(Color.white).shadow(color: Color.black.opacity(0.12), radius: 3)).padding(.top,20)
            .onAppear(){
                self.getCharges()

                isWebview = false
            }
        }
        
    }
}

//struct RequestRideEpaymentView_Previews: PreviewProvider {
//    static var previews: some View {
//        RequestRideEpaymentView()
//    }
//}
