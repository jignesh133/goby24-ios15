//
//  RequestRideHistoryView.swift
//  Goby24
//
//  Created by Jignesh Bhensadadiya on 9/17/21.
//

import SwiftUI
import Combine

class ArrRequestRIdeList: ObservableObject {
    @Published var arrRideRequestList:[Model_RideRequest] =  [Model_RideRequest]()
    init() {
        arrRideRequestList = []
    }
}

struct RequestRideHistoryView: View {
    @State var arrRideRequestList:[Model_RideRequest] =  [Model_RideRequest]()
    @State var objrideRequestList:Model_RideRequest? = nil
    @ObservedObject var objArrRequestRIdeList = ArrRequestRIdeList()
    
    @Binding var selectedItemIndex: Int
    
    @Environment(\.viewController) private var viewControllerHolder: UIViewController?
    @State var showingSheet:Bool = false
    @State var makePayment:Bool = false
    @State var contactButton:Bool = false
    @State var riderId:String = ""
    
    
    @State var touristRequestList:Model_TouristRequestList? = nil
    
    func getTouristRequestList(isHideLoader:Bool = false){
        
        let param:[String:Any] = [String:Any]()
        print(param)
        WebAccess.getDataWith(_Url: WebAccess.RIDE_REQUEST, _parameters: param,isHideLoader: isHideLoader) { (result) in
            
            switch (result){
            case .Success(let _data):
                print(_data)
                do{
                    let result:[String:Any] = UtilsObjs.cleanJson(to: _data ) as? [String : Any] ?? [String:Any]()
                    print(result)
                    let data1 =  try JSONSerialization.data(withJSONObject: result["result"] ?? [] , options: JSONSerialization.WritingOptions.prettyPrinted)
                    let convertedString = String(data: data1, encoding: String.Encoding.utf8)
                    let jsonData = Data((convertedString?.utf8)!)
                    objArrRequestRIdeList.arrRideRequestList.removeAll()
                    //                    self.arrRideRequestList = try JSONDecoder().decode([Model_RideRequest].self, from: jsonData)
                    objArrRequestRIdeList.arrRideRequestList = try JSONDecoder().decode([Model_RideRequest].self, from: jsonData)
                    NSLog("LOG")
                    
                }catch(let error){
                    print(error)
                }
                
                break
                
            case .Error(let msg):
                ShowAlert(title: APPNAME, msg: msg, view: UIApplication.getTopViewController()!)
                break
            }
            
        }
    }
    
    func acceptRideRequest(tid:String){
        
        let param:[String:Any] = [String:Any]()
        print(param)
        WebAccess.putDataWith(_Url: (WebAccess.REQUEST_RIDE_ACCEPT + tid + "/"), _parameters: param) { (result) in
            
            switch (result){
            case .Success(let _data):
                print(_data)
                ShowAlert(title: APPNAME, msg: "Requested Ride Accepted Sucessfully", view: UIApplication.getTopViewController()!)
                //self.getTouristRequestList(isHideLoader: false)
                self.selectedItemIndex = 0
                break
                
            case .Error(let msg):
                ShowAlert(title: APPNAME, msg: msg, view: UIApplication.getTopViewController()!)
                break
            }
            
        }
    }
    
    func deleteRideRequest(tid:String){
        
        let param:[String:Any] = [String:Any]()
        print(param)
        WebAccess.deleteDataWith(_Url: (WebAccess.RIDE_REQUEST + tid + "/"), _parameters: param) { (result) in
            
            switch (result){
            case .Success(let _data):
                print(_data)
                ShowAlert(title: APPNAME, msg: "Requested Ride deleted sucessfully ", view: UIApplication.getTopViewController()!)
                //  self.arrRideRequestList =  self.arrRideRequestList.filter({"\($0.id ?? 0)" != tid})
                //                self.getTouristRequestList(isHideLoader: false)
                self.selectedItemIndex = 0
                break
                
            case .Error(let msg):
                ShowAlert(title: APPNAME, msg: msg, view: UIApplication.getTopViewController()!)
                break
            }
            
        }
    }
    
    var body: some View {
        VStack{
            NavigationLink(destination: RiderContactView(receiverId:riderId), isActive: $contactButton) {EmptyView()}.isDetailLink(false)
            
            NavigationLink(destination: RequestRideEditView(objRequestRide: self.objrideRequestList),isActive: $showingSheet) {EmptyView()}.isDetailLink(false)
            
            NavigationLink(destination: RequestRideEpaymentOverView(objRequestRide: self.objrideRequestList),isActive: $makePayment) {EmptyView()}.isDetailLink(false)
            
            ScrollView{
                
                ForEach(0..<objArrRequestRIdeList.arrRideRequestList.count,id: \.self) { row in
                    
                    //                ForEach(0..<$arrRideRequestList.wrappedValue.count,id: \.self) { row in
                    RideRequestView(rideRequest: objArrRequestRIdeList.arrRideRequestList[row]) { (tid) in
                        print("DELETE")
                        self.deleteRideRequest(tid: tid)
                    } onAcceptRequest: { (tid) in
                        print("ACCEPT")
                        self.acceptRideRequest(tid: tid)
                    } onEditRequest: { (obj) in
                        print("EDIT")
                        self.objrideRequestList = obj
                        showingSheet = true
                    } onContactRequest: { (tid) in
                        print("CONTACT")
                        riderId = tid
                        contactButton  = true
                    } onPayRequest: { (obj) in
                        print("Pay")
                        self.objrideRequestList = obj
                        self.makePayment  = true
                    }
                    
                }
            }.padding()
        }.onAppear(){
            self.getTouristRequestList()
        }
    }
    
}

//struct RequestRideHistoryView_Previews: PreviewProvider {
//    static var previews: some View {
//        RequestRideHistoryView()
//    }
//}
struct RideRequestView: View {
    @State var rideRequest:Model_RideRequest?
    @State private var isOpen:Bool = false
    
    let onDeleteRequest: (String) -> Void
    let onAcceptRequest: (String) -> Void
    let onEditRequest: (Model_RideRequest) -> Void
    let onContactRequest: (String) -> Void
    let onPayRequest: (Model_RideRequest) -> Void
    
    var body: some View {
        VStack(alignment:.leading){
            
            HStack{  // FOR TOP HEADER
                
                VStack(alignment:.leading){  // FOR BUTTON
                    HStack{
                        Button(action: {
                            withAnimation {
                                isOpen.toggle()
                            }
                        }) {
                            Image("roundDown")
                        }
                    }.frame(width: 40, height: 40 ,alignment: .top)
                    Spacer()
                }.frame(maxWidth: 50,maxHeight: 40).fixedSize(horizontal: false, vertical: false)
                
                
                VStack{
                    VStack(alignment:.leading){
                        Text(Localizable.Date.localized()).modifier(CustomTextM(fontName: UIFont.Montserrat.semiBold.fontName, fontSize: 12, fontColor: Color.init(hex: "00AEEF")))
                        Text(rideRequest?.journeyDate ?? "").modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 12, fontColor: Color.init(hex: "000000")))
                        Spacer()
                        Divider()
                        
                    }.frame(maxWidth: .infinity)
                }.frame(height:50)
                
                VStack{
                    VStack(alignment:.leading){
                        Text(Localizable.Tourist_Spot.localized()).modifier(CustomTextM(fontName: UIFont.Montserrat.semiBold.fontName, fontSize: 12, fontColor: Color.init(hex: "00AEEF")))
                        
                        Text(rideRequest?.journeyFrom ?? "").modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 12, fontColor: Color.init(hex: "000000")))
                        
                        Text(rideRequest?.journeyTo ?? "").modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 12, fontColor: Color.init(hex: "000000")))
                        Spacer()
                        Divider()
                    }.frame(maxWidth: .infinity)
                }.frame(height:50)
            }
            if (isOpen == true){  // FOR OPEN SECTION
                HStack{
                    VStack(alignment:.leading){  // FOR BUTTON
                        Spacer()
                    }.frame(maxWidth: 50,maxHeight: 40).fixedSize(horizontal: false, vertical: false)
                    
                    VStack{
                        HStack{
                            HStack{
                                Text(Localizable.Descriptions.localized()).modifier(CustomTextM(fontName: UIFont.Montserrat.semiBold.fontName, fontSize: 12, fontColor: Color.init(hex: "00AEEF")))
                                Spacer()
                            }
                            HStack{
                                Text(rideRequest?.remark ?? "").modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 12, fontColor: Color.init(hex: "000000")))
                                Spacer()
                            }
                        }
                        Spacer(minLength: 5)
                        HStack{
                            HStack{
                                Text("Passenger").modifier(CustomTextM(fontName: UIFont.Montserrat.semiBold.fontName, fontSize: 12, fontColor: Color.init(hex: "00AEEF")))
                                Spacer()
                            }
                            HStack{
                                Text(rideRequest?.seats ?? "").modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 12, fontColor: Color.init(hex: "000000")))
                                Spacer()
                            }
                        }
                        Spacer(minLength: 5)
                        HStack{
                            HStack{
                                Text(Localizable.Budget.localized()).modifier(CustomTextM(fontName: UIFont.Montserrat.semiBold.fontName, fontSize: 12, fontColor: Color.init(hex: "00AEEF")))
                                Spacer()
                            }
                            HStack{
                                Text(rideRequest?.budget ?? "").modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 12, fontColor: Color.init(hex: "000000")))
                                Text(rideRequest?.currency ?? "").modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 12, fontColor: Color.init(hex: "000000")))
                                Text(rideRequest?.paymentMethod ?? "").modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 12, fontColor: Color.init(hex: "000000")))
                                Spacer()
                            }
                        }
                        Spacer(minLength: 5)
                        HStack{
                            HStack{
                                Text("Action").modifier(CustomTextM(fontName: UIFont.Montserrat.semiBold.fontName, fontSize: 12, fontColor: Color.init(hex: "00AEEF")))
                                Spacer()
                            }
                            
                            HStack{
                                if (rideRequest?.requestStatus == "Expired"){
                                    Text("Ride Expired")
                                        .modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 12, fontColor: Color.black))
                                    
                                }
                                if (rideRequest?.requestStatus == "Accepted"){
                                    Text("Accepted")
                                        .modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 12, fontColor: Color.black))
                                    
                                }
                                if (getId() != getStringFromAny(rideRequest?.user?.id ?? 0)){
                                
                                    Button(action: {
                                        isOpen.toggle()
                                        onContactRequest(getStringFromAny(rideRequest?.user?.id ?? ""))
                                    }) {
                                        Text(Localizable.Contact.localized())
                                    }
                                    .modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 12, fontColor: Color.white))
                                    .modifier(ButtonStyle(buttonHeight: 25, buttonColor: Color.init(hex: "00ADEF"), buttonRadius: 25))
                                    if (rideRequest?.requestStatus == "Pending"){
                                        Button(action: {
                                            isOpen.toggle()
                                            
                                            onAcceptRequest(getStringFromAny(rideRequest?.id ?? ""))
                                        }) {
                                            Text(Localizable.Accept.localized())
                                        }
                                        .modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 12, fontColor: Color.white))
                                        .modifier(ButtonStyle(buttonHeight: 25, buttonColor: Color.init(hex: "00ADEF"), buttonRadius: 25))
                                        
                                    }
                                    
                                }
                                
                                if (getId() == getStringFromAny(rideRequest?.user?.id ?? 0)){
                                    
                                    if (rideRequest?.requestStatus == "Pending"){
                                        
                                        Button(action: {
                                            isOpen.toggle()
                                            
                                            onEditRequest(rideRequest!)
                                        }) {
                                            Text(Localizable.Edit.localized())
                                        }
                                        .modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 12, fontColor: Color.white))
                                        .modifier(ButtonStyle(buttonHeight: 25, buttonColor: Color.init(hex: "00ADEF"), buttonRadius: 25))
                                        
                                        Button(action: {
                                            print("Delete")
                                            isOpen.toggle()
                                            
                                            onDeleteRequest(getStringFromAny(rideRequest?.id ?? ""))
                                            
                                        }) {
                                            Text(Localizable.Delete.localized())
                                        }
                                        .modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 12, fontColor: Color.white))
                                        .modifier(ButtonStyle(buttonHeight: 25, buttonColor: Color.init(hex: "00ADEF"), buttonRadius: 25))
                                    }
                                    if (rideRequest?.requestStatus == "Accepted"){
                                        Button(action: {
                                            isOpen.toggle()
                                            onPayRequest(rideRequest!)
                                        }) {
                                            Text(Localizable.Pay.localized())
                                        }
                                        .modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 12, fontColor: Color.white))
                                        .modifier(ButtonStyle(buttonHeight: 25, buttonColor: Color.init(hex: "00ADEF"), buttonRadius: 25))
                                        
                                    }
                                }
                                Spacer()

                            }
                            
                        }
                        
                    }
                }
            }
        }
    }
}

