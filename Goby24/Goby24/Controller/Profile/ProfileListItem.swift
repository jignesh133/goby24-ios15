//
//  ProfileListItem.swift
//  Goby24
//
//  Created by Jignesh Bhensadadiya on 7/12/21.
//

import SwiftUI

struct ProfileListItem: View {
    @State var title: String = ""

    var body: some View {
        VStack{
            HStack{
                Text( "  " + title).modifier(CustomTextM(fontName: UIFont.Montserrat.semiBold.fontName, fontSize: 16, fontColor: Color.init(hex: "2E2E2E"))).padding()
                Spacer()
                Image("next").padding(.trailing,20)
            }
            Divider().padding(.horizontal)
        }
    }
}

struct ProfileListItem_Previews: PreviewProvider {
    static var previews: some View {
        ProfileListItem()
    }
}
