//
//  VehicaleDetailsView.swift
//  Goby24
//
//  Created by Jignesh Bhensadadiya on 7/14/21.
//

import SwiftUI
import SDWebImageSwiftUI

struct VehicaleDetailsView: View {
    @State var brand: String?
    @State var model: String?
    @State var kind: String = ""
    @State var color: String = ""
    @State var ext: String = ""
    
    var reloadDropDown1:Bool = false
    var reloadDropDown2:Bool = false
    
    @State var arrBrand:[Model_CarBrands] = [Model_CarBrands]()
    @State var arrModel:[Model_BrandModel] = [Model_BrandModel]()
    
    @State var selectedBrand:Model_CarBrands?
    @State var selectedModel:Model_BrandModel?
    
    @State var imgRC: Image?
    @State var imgLic: Image?

    
    @State var isImagePickerForLicience: Bool = false
    @State var isImagePickerForRC: Bool = false
    
    
    @State var isImagePickerPhotoLibraryForLicience: Bool = false
    @State var isImagePickerCameraLibraryForLicience: Bool = false
    
    @State var isImagePickerPhotoLibraryForRC: Bool = false
    @State var isImagePickerCameraForRC: Bool = false

    
    var addVihicle = AddVihicle.shared
    @Environment(\.viewController) private var viewControllerHolder: UIViewController?
    
    func getBrands(){
        let param:[String:Any] = [String:Any]()
        
        WebAccess.getDataWith(_Url: WebAccess.CAR_BRANDS, _parameters: [:]) { (result) in
            switch (result){
            case .Success(let _data):
                print(_data)
                do{
                    let result:[String:Any] = UtilsObjs.cleanJson(to: _data ) as? [String : Any] ?? [String:Any]()
                    let data1 =  try JSONSerialization.data(withJSONObject: result["result"] ?? [] , options: JSONSerialization.WritingOptions.prettyPrinted)
                    let convertedString = String(data: data1, encoding: String.Encoding.utf8)
                    let jsonData = Data((convertedString?.utf8)!)
                    self.arrBrand = try JSONDecoder().decode([Model_CarBrands].self, from: jsonData)
                    if (self.arrBrand.count > 0){
                        self.getModels()
                    }
                }catch(let error){
                    print(error)
                }
                break
            case .Error(let msg):
                ShowAlert(title: APPNAME, msg: msg, view: UIApplication.getTopViewController()!)
                break
            }
        }
    }
    func getModels(){
//        var param:[String:Any] = [String:Any]()
//        param["Key"] = selectedBrand?.name//"brand"
//        param["value"] = getStringFromAny(selectedBrand?.id ?? "") //"1"
//        https://dev.api.goby24.ch//api/ride/car-models/?value=Volvo
        WebAccess.getDataWith(_Url: (WebAccess.CAR_BRANDMODELS + "?value=" + (selectedBrand?.name ?? "") ), _parameters: [:]) { (result) in
            switch (result){
            case .Success(let _data):
                print(_data)
                do{
                    let result:[String:Any] = UtilsObjs.cleanJson(to: _data ) as? [String : Any] ?? [String:Any]()
                    let data1 =  try JSONSerialization.data(withJSONObject: result["result"] ?? [] , options: JSONSerialization.WritingOptions.prettyPrinted)
                    let convertedString = String(data: data1, encoding: String.Encoding.utf8)
                    let jsonData = Data((convertedString?.utf8)!)
                    self.arrModel = try JSONDecoder().decode([Model_BrandModel].self, from: jsonData)
                    
                }catch(let error){
                    print(error)
                }
                break
            case .Error(let msg):
                ShowAlert(title: APPNAME, msg: msg, view: UIApplication.getTopViewController()!)
                break
            }
        }
    }
    func addVehicleDetails(){
        var param:[String:Any] = [String:Any]()
        param["vehicle_no"] = addVihicle.licenceNo
        param["brand"] = brand
        param["model"] = model
        
        param["driving_license"] = imgLic?.asUIImage()
        param["vehicle_rc"] = imgRC?.asUIImage()
        


        
        WebAccess.postDataWithImage(_Url: WebAccess.ADD_Vehicle, _parameters: param) { (result) in
            switch (result){
            case .Success(let _data):
                print(_data)
                do{
                    // GET PROFILE DATA
                    WebCommanMethods.getUserProfileData { (result) in
                        ShowAlert(title: APPNAME, msg: "Vehicle added sucessfully", view: UIApplication.getTopViewController()!)

                    }

                    
                }catch(let error){
                    print(error)
                }
                break
            case .Error(let msg):
                ShowAlert(title: APPNAME, msg: msg, view: UIApplication.getTopViewController()!)
                break
            }
        }
    }
    func updateVehicleDetails(){
        var param:[String:Any] = [String:Any]()
        param["vehicle_no"] = addVihicle.licenceNo
        param["brand"] = brand
        param["model"] = model
        
        param["driving_license"] = imgLic.asUIImage()
        param["vehicle_rc"] = imgRC.asUIImage()
        
        
        WebAccess.postDataWithImage(_Url: WebAccess.ADD_Vehicle, _parameters: param) { (result) in
            switch (result){
            case .Success(let _data):
                print(_data)
                do{
                    ShowAlert(title: APPNAME, msg: "Vehicle updated sucessfully", view: UIApplication.getTopViewController()!)
                    
                }catch(let error){
                    print(error)
                }
                break
            case .Error(let msg):
                ShowAlert(title: APPNAME, msg: msg, view: UIApplication.getTopViewController()!)
                break
            }
        }
    }
    
    var body: some View {
        
        ScrollView{
            
            VStack(alignment:.leading,spacing: 5){
                // ADD CAR BRAND
                Group{
                    // 1
                    HStack{
                        Spacer()
                        Text(Localizable.What_is_your_Car_Brand.localized()).modifier(CustomTextM(fontName: UIFont.Rubik.medium.fontName, fontSize: 26, fontColor: Color.init(hex: "2E2E2E"))).multilineTextAlignment(.center).padding(.horizontal)
                        Spacer()
                    }
                    
                    DropdownView(isExpanded: false, value: selectedBrand?.name ?? (Localizable.Car_Brand.localized()), arrList: self.$arrBrand.wrappedValue.compactMap({$0.name}),onItemSelectedIndex: { (index) in
                        selectedBrand = self.arrBrand[index]
                        brand = selectedBrand?.name ?? ""
                        self.getModels()
                    })
                    
                    
                    Button(action: {
                        self.viewControllerHolder?.present(style: .overCurrentContext, transitionStyle: .crossDissolve) {
                            AddNewCarBrandView() { (val) in
                                brand = val
                                selectedBrand?.name = val
                            }
                        }
                    }){
                        HStack{
                            Spacer()
                            Text(brand ?? "Car brand doesn't exist?")
                        }
                        
                    }
                    .modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 14, fontColor: Color.init(hex: "2E2E2E")))
                    .modifier(ButtonStyle(buttonHeight: 50, buttonColor: Color.clear, buttonRadius: 50))
                    .padding(.horizontal)
                    .padding(.top,-10)
                    .onTapGesture {
                        self.viewControllerHolder?.present(style: .overCurrentContext, transitionStyle: .crossDissolve) {
                            AddNewCarBrandView() { (val) in
                                brand = val
                            }
                        }
                    }
                }
                // ADD MODEL NAME
                Group{
                    // 2
                    HStack{
                        Spacer()
                        Text(Localizable.Car_Model.localized()).modifier(CustomTextM(fontName: UIFont.Rubik.medium.fontName, fontSize: 26, fontColor: Color.init(hex: "2E2E2E"))).multilineTextAlignment(.center).padding(.horizontal)
                        Spacer()
                    }
                    DropdownView(isExpanded: false, value: selectedModel?.name ?? (Localizable.Car_Model.localized()), arrList: $arrModel.wrappedValue.compactMap({$0.name}),onItemSelectedIndex: { (index) in
                        selectedModel = self.arrModel[index]
                        model = selectedModel?.name ?? ""
                    })
                    Button(action: {
                        self.viewControllerHolder?.present(style: .overCurrentContext, transitionStyle: .crossDissolve) {
                            AddNewCarModelView() { (val) in
                                model = val
                                selectedModel?.name = val
                            }
                        }
                    }){
                        HStack{
                            Spacer()
                            
                            Text(model ?? "Car model doesn't exist?")
                        }
                    }
                    .modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 14, fontColor: Color.init(hex: "2E2E2E")))
                    .modifier(ButtonStyle(buttonHeight: 50, buttonColor: Color.clear, buttonRadius: 50))
                    .onTapGesture {
                        self.viewControllerHolder?.present(style: .overCurrentContext, transitionStyle: .crossDissolve) {
                            AddNewCarModelView() { (val) in
                                model = val
                            }
                        }
                    }
                    .padding(.horizontal)
                    
                    .padding(.top,-10)
                    
                }
                HStack{
                    Spacer()
                    VStack{
                        Text("Vehicle Licence").modifier(CustomTextM(fontName: UIFont.Poppins.regular.fontName, fontSize: 14, fontColor: Color.init(hex: "00AEEF"))).padding()
                        
                        Button(action: {
                            isImagePickerForLicience = true
                        }){
                            VStack{
                                if (imgLic == nil){
                                    Image("Vector").resizable().frame(width: 70, height: 70, alignment: .center)
                                }else{
                                    imgLic?.resizable().frame(width: 70, height: 70, alignment: .center)
                                }
                            }
                        }
                        
//                        @State var isImagePickerPhotoLibraryForLicience: Bool = false
//                        @State var isImagePickerCameraLibraryForLicience: Bool = false
//
//                        @State var isImagePickerPhotoLibraryForRC: Bool = false
//                        @State var isImagePickerCameraForRC: Bool = false
//
//
//
//                        @State var isImagePickerForLicience: Bool = false
//                        @State var isImagePickerForRC: Bool = false
//
//
                        .actionSheet(isPresented: $isImagePickerForLicience) {
                            ActionSheet(title: Text(""), message: Text("Choose Option"), buttons: [
                                .default(Text("Take Photo")) { isImagePickerCameraLibraryForLicience = true },
                                .default(Text("Choose Photo")) { isImagePickerPhotoLibraryForLicience = true },
                                .cancel(){
                                    isImagePickerForLicience = false
                                }
                            ])
                        }
                        .sheet(isPresented: $isImagePickerPhotoLibraryForLicience) {
                            
                            OpenGallary(isShown: $isImagePickerPhotoLibraryForLicience, image: $imgLic, sourceType: .photoLibrary, onImagePicked: { (image) in
                                isImagePickerPhotoLibraryForLicience = false
                            })
                            
                        }
                        .sheet(isPresented: $isImagePickerCameraLibraryForLicience) {
                            
                            OpenGallary(isShown: $isImagePickerCameraLibraryForLicience, image: $imgLic, sourceType: .camera, onImagePicked: { (image) in
                                isImagePickerCameraLibraryForLicience = false
                            })
                            
                        }
                        
//                        .sheet(isPresented: $isImagePickerForLicience) {
//                            OpenGallary(isShown: $isImagePickerForLicience, image: $imgLic, sourceType: .photoLibrary, onImagePicked: { (image) in
//                                isImagePickerForLicience = false
//                            })
//                        }
                    }
                    Spacer()
                    VStack{
                        Text("Vehicle RC ").modifier(CustomTextM(fontName: UIFont.Poppins.regular.fontName, fontSize: 14, fontColor: Color.init(hex: "00AEEF"))).padding()
                        
                        
                        Button(action: {
                            isImagePickerForRC = true
                        }){
                            VStack{
                                if (imgRC == nil){
                                    Image("Vector").resizable().frame(width: 70, height: 70, alignment: .center)
                                }else{
                                    imgRC?.resizable().frame(width: 70, height: 70, alignment: .center)
                                }
                            }
                        }
//                        .sheet(isPresented: $isImagePickerForRC) {
//                            OpenGallary(isShown: $isImagePickerForRC, image: $imgRC, sourceType: .photoLibrary, onImagePicked: { (image) in
//                                isImagePickerForRC = false
//                            })
//                        }
                        .actionSheet(isPresented: $isImagePickerForRC) {
                            ActionSheet(title: Text(""), message: Text("Choose Option"), buttons: [
                                .default(Text("Take Photo")) { isImagePickerCameraForRC = true },
                                .default(Text("Choose Photo")) { isImagePickerPhotoLibraryForRC = true },
                                .cancel(){isImagePickerForRC = false}
                            ])
                        }
                        .sheet(isPresented: $isImagePickerPhotoLibraryForRC) {
                            
                            OpenGallary(isShown: $isImagePickerPhotoLibraryForRC, image: $imgRC, sourceType: .photoLibrary, onImagePicked: { (image) in
                                isImagePickerPhotoLibraryForRC = false
                            })
                            
                        }
                        .sheet(isPresented: $isImagePickerCameraForRC) {
                            
                            OpenGallary(isShown: $isImagePickerCameraForRC, image: $imgRC, sourceType: .camera, onImagePicked: { (image) in
                                isImagePickerCameraForRC = false
                            })
                            
                        }
                    }
                    Spacer()
                }
                Button(action: {
                    if(brand?.removeWhiteSpace().count == 0){
                        ShowAlert(title: APPNAME, msg: "Please select brand", view: UIApplication.getTopViewController()!)
                    }else if (model?.removeWhiteSpace().count == 0){
                        ShowAlert(title: APPNAME, msg: "Please select model", view: UIApplication.getTopViewController()!)
                    }else if (imgLic == nil){
                        ShowAlert(title: APPNAME, msg: "Please select image of licence", view: UIApplication.getTopViewController()!)
                    }else if (imgRC == nil){
                        ShowAlert(title: APPNAME, msg: "Please select image of RC Book", view: UIApplication.getTopViewController()!)
                    }
                    else{
                        if(getUserVehicleVerified() == true){
                            self.updateVehicleDetails()
                        }else{
                            self.addVehicleDetails()
                        }
                    }
                }){
                    Text(Localizable.Done.localized())
                }
                .modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 16, fontColor: Color.white))
                .modifier(ButtonStyle(buttonHeight: 50, buttonColor: Color.init(hex: "00ADEF"), buttonRadius: 50))
                .padding(.all,30)
            } // top vstack
        }.showNavigationBarWithBackStyle().onLoad(){getBrands()} // scrollview stack
        .onAppear(){
            let userData = userDefault.value(forKey: Enum_Login.userProfileData.rawValue) as? [String:Any] ?? [:]
            let vehicleInfo = userData["vehicle_info"] as? [String:Any] ?? [:]
            
            if ((getStringFromAny(vehicleInfo["driving_license"] ?? "")).count > 0){
                
                SDWebImageManager.shared.loadImage(with: URL(string: vehicleInfo["driving_license"] as? String ?? ""), options: .continueInBackground) { (val, value, url) in
                } completed: { (image, data, error, catch, result, url) in
                    if (result == true && image != nil){
                        imgLic = Image(uiImage: image!)
                    }
                }
                
            }
            
            if ((getStringFromAny(vehicleInfo["vehicle_rc"] ?? "")).count > 0){
                SDWebImageManager.shared.loadImage(with: URL(string: vehicleInfo["vehicle_rc"] as? String ?? ""), options: .continueInBackground) { (val, value, url) in
                } completed: { (image, data, error, catch, result, url) in
                    if (result == true && image != nil){
                        imgRC = Image(uiImage: image!)
                    }
                }
            }
            
            if ((getStringFromAny(vehicleInfo["brand"] ?? "")).count > 0){
                brand = vehicleInfo["brand"] as? String ?? ""
            }
            if ((getStringFromAny(vehicleInfo["model"] ?? "")).count > 0){
                model = vehicleInfo["model"] as? String ?? ""
            }
            
            //            plateNumber = vehicleInfo["vehicle_no"] as? String ?? ""
            print(userData)
            
        }
    }
}

struct VehicaleDetailsView_Previews: PreviewProvider {
    static var previews: some View {
        VehicaleDetailsView()
    }
}
