//
//  TravelPreference.swift
//  Goby24
//
//  Created by Jignesh Bhensadadiya on 7/14/21.
//

import SwiftUI

struct TravelPreference: View {
    
    @State var chatinessExpand:Bool = false
    @State var chatinessValue:String = "I am Chatty when I feel comfortable"

    
    @State var musicExpand:Bool = false
    @State var musicValue:String = "I’ll jam depending on the mood"

    @State var smokingExpand:Bool = false
    @State var smokingValue:String = "Cigarette breaks outside the car are ok"

    @State var petsExpand:Bool = false
    @State var petsValue:String = "I’ll travel with pets depending on the animal"

    func updatePrefrence(){
        var pref:[String:Any] = [String:Any]()
        pref["Chattiness"] = chatinessValue
        pref["Music"] = musicValue
        pref["Smoking"] = smokingValue
        pref["Pets"] = petsValue
        
        var param:[String:Any] = [String:Any]()
        param["travel_preferences"] = pref
        
        WebAccess.putDataWith(_Url: WebAccess.NEW_USER, _parameters: param) { (result) in
            switch (result){
            case .Success(let profiledata):
                print(profiledata)
                let responseData = profiledata["result"] as? [String:Any] ?? [:]
                
                userDefault.set(responseData, forKey: Enum_Login.userProfileData.rawValue)
                userDefault.setValue(responseData["profile_pic"], forKey: Enum_Login.profilePic.rawValue)
                userDefault.setValue(true, forKey: Enum_Login.isLogin.rawValue)
                appDelegate?.userProfileData = responseData
                userDefault.synchronize()
                appDelegate?.userProfileData = userDefault.value(forKey: Enum_Login.userProfileData.rawValue) as? [String : Any] ?? [:]
                break
            case .Error(let msg):
                ShowAlert(title: APPNAME, msg: msg, view: UIApplication.getTopViewController()!)
                break
            }
        }
    }
    
    var body: some View {
        
        VStack(alignment:.center,spacing:0){
            ShowCustomNavigation()
            
            ScrollView{
                
                Group{
                    Text(Localizable.Choose_Travel_Preferences.localized()).padding(.top, 25).modifier(CustomTextM(fontName: UIFont.Rubik.medium.fontName, fontSize: 26, fontColor: Color.init(hex: "2E2E2E")))
                    
                    HStack(){
                        Text(Localizable.Chattiness.localized()).padding(.top, 25).modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 16, fontColor: Color.init(hex: "2E2E2E")))
                        Spacer()
                    }.padding(.horizontal)
                    
                    //1
                    DisclosureGroup(isExpanded: $chatinessExpand) {
                        Group{
                            //Divider().background(Color.init(hex: "707070")).padding(.top,10)
                            HStack{
                                Text(Localizable.Im_a_Chatbox.localized()).modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 13, fontColor: Color.init(hex: "2E2E2E")))
                                Spacer()
                            }.padding(.leading,15).frame(height: 40).onTapGesture {
                                withAnimation{
                                    chatinessValue = (Localizable.Im_a_Chatbox.localized())
                                    chatinessExpand.toggle()
                                    self.updatePrefrence()
                                }
                            }
                        }
                        Group{
                           // Divider().background(Color.init(hex: "707070"))
                            HStack{
                                Text(Localizable.Im_chatty_when_I_feel_comfortable.localized()).modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 13, fontColor: Color.init(hex: "2E2E2E")))
                                Spacer()
                            }.padding(.leading,15).frame(height: 40).onTapGesture {
                                withAnimation{
                                    chatinessValue = (Localizable.Im_chatty_when_I_feel_comfortable.localized())
                                    chatinessExpand.toggle()
                                    self.updatePrefrence()
                                }
                            }
                        }
                        
                        Group{
                           // Divider().background(Color.init(hex: "707070"))
                            HStack{
                                Text(Localizable.Im_the_quite_type.localized()).modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 13, fontColor: Color.init(hex: "2E2E2E")))
                                Spacer()
                            }.padding(.leading,15).frame(height: 40).padding(.bottom,10).onTapGesture {
                                withAnimation{
                                    chatinessValue = (Localizable.Im_the_quite_type.localized())

                                    chatinessExpand.toggle()
                                    self.updatePrefrence()
                                }
                            }
                        }
                        
                    } label: {
                        HStack{
                            Text(chatinessValue).padding(.horizontal).modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 15, fontColor: Color.init(hex: "2E2E2E")))
                            Spacer()
                            Image(chatinessExpand ? "up":"down").padding(.trailing,-10)
                        }
                        .padding(.trailing)
                        .frame(minWidth: 0, maxWidth: .infinity)
                        .frame(height: 40)
                        .onTapGesture {
                            withAnimation{
                                chatinessExpand.toggle()
                            }
                        }
                    }
                    .accentColor(Color.clear)
                    .background(Color.init(hex: "F2F2F2"))
                    .cornerRadius(24)
                    .modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 15, fontColor: Color.init(hex: "2E2E2E"))).multilineTextAlignment(.center)
                    .padding()
                }
                Group{
                    // 2
                    HStack(){
                        Text(Localizable.Music.localized()).padding(.top, 10).modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 16, fontColor: Color.init(hex: "2E2E2E")))
                        Spacer()
                    }.padding(.horizontal)
                    // 2
                    DisclosureGroup(isExpanded: $musicExpand) {
                        Group{
                            //Divider().background(Color.init(hex: "707070")).padding(.top,10)
                            HStack{
                                Text(Localizable.Its_all_about_the_playlist.localized()).modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 13, fontColor: Color.init(hex: "2E2E2E")))
                                Spacer()
                            }.padding(.leading,15).frame(height: 40).onTapGesture {
                                withAnimation{
                                    musicValue = (Localizable.Its_all_about_the_playlist.localized())
                                    musicExpand.toggle()
                                    self.updatePrefrence()
                                }
                            }
                        }
                        Group{
                           // Divider().background(Color.init(hex: "707070"))
                            HStack{
                                Text(Localizable.Ill_jam_depending_on_the_mood.localized()).modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 13, fontColor: Color.init(hex: "2E2E2E")))
                                Spacer()
                            }.padding(.leading,15).frame(height: 40).onTapGesture {
                                withAnimation{
                                    musicValue = (Localizable.Ill_jam_depending_on_the_mood.localized())

                                    musicExpand.toggle()
                                    self.updatePrefrence()
                                }
                            }
                        }
                        
                        Group{
                           // Divider().background(Color.init(hex: "707070"))
                            HStack{
                                Text(Localizable.Silence_is_golden.localized()).modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 13, fontColor: Color.init(hex: "2E2E2E")))
                                Spacer()
                            }.padding(.leading,15).frame(height: 40).padding(.bottom,10).onTapGesture {
                                withAnimation{
                                    musicValue = (Localizable.Silence_is_golden.localized())

                                    musicExpand.toggle()
                                    self.updatePrefrence()
                                }
                            }
                        }
                        
                    } label: {
                        HStack{
                            Text(musicValue).padding(.horizontal).modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 15, fontColor: Color.init(hex: "2E2E2E")))
                            Spacer()
                            Image(musicExpand ? "up":"down").padding(.trailing,-10)

                        }
                        .padding(.trailing)
                        .frame(minWidth: 0, maxWidth: .infinity)
                        .frame(height: 40)
                        .onTapGesture {
                            withAnimation{
                                musicExpand.toggle()
                            }
                        }
                    }
                    .accentColor(Color.clear)
                    .background(Color.init(hex: "F2F2F2"))
                    .cornerRadius(24)
                    .modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 15, fontColor: Color.init(hex: "2E2E2E"))).multilineTextAlignment(.center)
                    .padding()
                    
                    
                }
                Group{
                    // 3
                    HStack(){
                        Text(Localizable.Smoking.localized()).padding(.top, 10).modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 16, fontColor: Color.init(hex: "2E2E2E")))
                        Spacer()
                    }.padding(.horizontal)
                    
                    DisclosureGroup(isExpanded: $smokingExpand) {
                        Group{
                            //Divider().background(Color.init(hex: "707070")).padding(.top,10)
                            HStack{
                                Text(Localizable.Im_fine_with_smoking.localized()).modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 13, fontColor: Color.init(hex: "2E2E2E")))
                                Spacer()
                            }.padding(.leading,15).frame(height: 40).onTapGesture {
                                withAnimation{
                                    smokingValue = (Localizable.Im_fine_with_smoking.localized())
                                    smokingExpand.toggle()
                                    self.updatePrefrence()
                                }
                            }
                        }
                        Group{
                           // Divider().background(Color.init(hex: "707070"))
                            HStack{
                                Text(Localizable.Cigarette_breaks_outside_the_car_are_ok.localized()).modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 13, fontColor: Color.init(hex: "2E2E2E")))
                                Spacer()
                            }.padding(.leading,15).frame(height: 40).onTapGesture {
                                withAnimation{
                                    smokingValue = (Localizable.Cigarette_breaks_outside_the_car_are_ok.localized())
                                    smokingExpand.toggle()
                                    self.updatePrefrence()
                                }
                            }
                        }
                        
                        Group{
                           // Divider().background(Color.init(hex: "707070"))
                            HStack{
                                Text(Localizable.Please_no_smoking_in_the_car.localized()).modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 13, fontColor: Color.init(hex: "2E2E2E")))
                                Spacer()
                            }.padding(.leading,15).frame(height: 40).padding(.bottom,10).onTapGesture {
                                withAnimation{
                                    smokingValue = (Localizable.Please_no_smoking_in_the_car.localized())
                                    smokingExpand.toggle()
                                    self.updatePrefrence()
                                }
                            }
                        }
                        
                    } label: {
                        HStack{
                            Text(smokingValue).padding(.horizontal).modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 15, fontColor: Color.init(hex: "2E2E2E")))
                            Spacer()
//                            Image("down").padding(.trailing,-10)
                            Image(smokingExpand ? "up":"down").padding(.trailing,-10)

                        }
                        .padding(.trailing)
                        .frame(minWidth: 0, maxWidth: .infinity)
                        .frame(height: 40)
                        .onTapGesture {
                            withAnimation{
                                smokingExpand.toggle()
                            }
                        }
                    }
                    .accentColor(Color.clear)
                    .background(Color.init(hex: "F2F2F2"))
                    .cornerRadius(24)
                    .modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 15, fontColor: Color.init(hex: "2E2E2E"))).multilineTextAlignment(.center)
                    .padding()
                }
                Group{
                    // 4
                    HStack(){
                        Text(Localizable.Pets.localized()).padding(.top, 10).modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 16, fontColor: Color.init(hex: "2E2E2E")))
                        Spacer()
                    }.padding(.horizontal)
                    
                    
                    DisclosureGroup(isExpanded: $petsExpand) {
                        Group{
                            //Divider().background(Color.init(hex: "707070")).padding(.top,10)
                            HStack{
                                Text(Localizable.I_love_pets_Woof.localized()).modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 13, fontColor: Color.init(hex: "2E2E2E")))
                                Spacer()
                            }.padding(.leading,15).frame(height: 40).onTapGesture {
                                withAnimation{
                                    petsValue = (Localizable.I_love_pets_Woof.localized())
                                    petsExpand.toggle()
                                    self.updatePrefrence()
                                }
                            }
                        }
                        Group{
                           // Divider().background(Color.init(hex: "707070"))
                            HStack{
                                Text(Localizable.Ill_travel_with_pets_depending_on_the_animal.localized()).modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 13, fontColor: Color.init(hex: "2E2E2E")))
                                Spacer()
                            }.padding(.leading,15).frame(height: 40).onTapGesture {
                                withAnimation{
                                    petsValue = (Localizable.Ill_travel_with_pets_depending_on_the_animal.localized())
                                    petsExpand.toggle()
                                    self.updatePrefrence()
                                }
                            }
                        }
                        
                        Group{
                           // Divider().background(Color.init(hex: "707070"))
                            HStack{
                                Text(Localizable.Sorry_not_a_pet_person.localized()).modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 13, fontColor: Color.init(hex: "2E2E2E")))
                                Spacer()
                            }.padding(.leading,15).frame(height: 40).padding(.bottom,10).onTapGesture {
                                withAnimation{
                                    petsValue = (Localizable.Sorry_not_a_pet_person.localized())

                                    petsExpand.toggle()
                                    self.updatePrefrence()
                                }
                            }
                        }
                        
                    } label: {
                        HStack{
                            Text(petsValue).padding(.horizontal).modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 15, fontColor: Color.init(hex: "2E2E2E")))
                            Spacer()
//                            Image("down").padding(.trailing,-10)
                            Image(petsExpand ? "up":"down").padding(.trailing,-10)

                        }
                        .padding(.trailing)
                        .frame(minWidth: 0, maxWidth: .infinity)
                        .frame(height: 40)
                        .onTapGesture {
                            withAnimation{
                                petsExpand.toggle()
                                
                            }
                        }
                    }
                    .accentColor(Color.clear)
                    .background(Color.init(hex: "F2F2F2"))
                    .cornerRadius(24)
                    .modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 15, fontColor: Color.init(hex: "2E2E2E"))).multilineTextAlignment(.center)
                    .padding()

                    
//                    Button(action: {}){
//                        ZStack() {
//                            HStack() {
//                                Text("I’ll travel with pets depending on the animal").padding(.horizontal)
//                                Spacer()
//                                Image("down").padding(.trailing,20)
//                            }
//                        }
//                    }
//                    .modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 13, fontColor: Color.init(hex: "2E2E2E")))
//                    .modifier(ButtonStyle(buttonHeight: 50, buttonColor: Color.init(hex: "F2F2F2"), buttonRadius: 50))
//                    .padding(.horizontal).padding(.top,5)
                }
            }.padding()
            Spacer()
            
        }.padding()//.showNavigationBarWithCloseStyle()
       
        .onAppear(){
            
            let data = userDefault.value(forKey: Enum_Login.userProfileData.rawValue) as? [String:Any] ?? [:]
            let pre =  data["travel_preferences"] as? [String:Any] ?? [:]
            
            if (getStringFromAny(pre["Chattiness"] ?? "").count > 0 ){
                chatinessValue = pre["Chattiness"] as! String
            }
            
            if (getStringFromAny(pre["Music"] ?? "").count > 0 ){
                chatinessValue = pre["Music"] as! String
            }
            
            if (getStringFromAny(pre["Smoking"] ?? "").count > 0 ){
                smokingValue = pre["Smoking"] as! String
            }
            
            if (getStringFromAny(pre["Pets"] ?? "").count > 0 ){
                petsValue = pre["Pets"] as! String
            }
            
        }
    }
}

struct TravelPreference_Previews: PreviewProvider {
    static var previews: some View {
        TravelPreference()
    }
}
