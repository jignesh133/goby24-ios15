//
//  VerifyIdView.swift
//  Goby24
//
//  Created by Jignesh Bhensadadiya on 7/14/21.
//

import SwiftUI

struct VerifyIdView: View {
    
    //MARK: - PROPERTIES
    @State var firstName: String = ""
    @State var lastName: String = ""
    @State var selection: Int? = nil

    
    var body: some View {
        VStack{
            Text("Verify").modifier(CustomTextM(fontName: UIFont.Montserrat.semiBold.fontName, fontSize: 16, fontColor: Color.init(hex: "2E2E2E")))
            HStack(spacing:0){
                Text((Localizable.First_Name.localized())).modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 11, fontColor: Color.init(hex: "2E2E2E"))).frame(maxWidth:.infinity)
                Text("Id").modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 13, fontColor: Color.init(hex: "2E2E2E"))).frame(maxWidth:.infinity)
                
            }.padding(.horizontal,30).padding(.top)
            ZStack{
                Divider()
                HStack(spacing:0){
                    
                    VStack(alignment: .center, spacing: 0){
                        Circle().fill(Color.init(hex: "FF9500")).frame(width: 5, height: 5)
                    }.frame(maxWidth:.infinity)
                    VStack(alignment: .center, spacing: 0){
                        Circle().fill(Color.init(hex: "FF9500")).frame(width: 5, height: 5)
                    }.frame(maxWidth:.infinity)
                }
            }.background(Color.clear).padding(.horizontal,30)
            
            Text(Localizable.Verify_your_id.localized()).modifier(CustomTextM(fontName: UIFont.Montserrat.semiBold.fontName, fontSize: 16, fontColor: Color.init(hex: "2E2E2E"))).padding()
            
            
            Text(Localizable.Select_the_type_of_document_you_want_to_upload.localized()).modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 16, fontColor: Color.init(hex: "2E2E2E"))).padding().multilineTextAlignment(.center).padding(.horizontal,50)
            
            NavigationLink(destination: PassportPhotoPage(), tag: 0, selection: $selection) {
                //Button
                Button(action: {
                    selection = 0
                }){
                    ZStack() {
                        
                        HStack{
                            VStack(alignment:.leading,spacing:0){
                                Text(Localizable.Passport.localized()).padding(.horizontal).modifier(CustomTextM(fontName: UIFont.Montserrat.semiBold.fontName, fontSize: 13, fontColor: Color.init(hex: "2E2E2E"))).multilineTextAlignment(.leading).padding(.leading,15)
                                Text("Face photo page").padding(.horizontal).modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 13, fontColor: Color.init(hex: "2E2E2E"))).padding(.leading,15)
                                
                            }
                            
                            HStack() {
                                Spacer()
                                Image("next").padding(.trailing,20)
                            }
                        }
                        
                    }
                }
                .modifier(ButtonStyle(buttonHeight: 50, buttonColor: Color.init(hex: "F2F2F2"), buttonRadius: 50))
                .padding(.horizontal,30).padding(.top,5)
            }
            
            NavigationLink(destination: FrontOfCard(), tag: 1, selection: $selection) {
                Button(action: {
                    selection = 1
                }){
                    ZStack() {
                        HStack() {
                            Text("NID card").padding(.horizontal).modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 13, fontColor: Color.init(hex: "2E2E2E"))).padding(.leading,15)

                            Spacer()
                            Image("next").padding(.trailing,20)
                        }
                    }
                }
                .modifier(ButtonStyle(buttonHeight: 50, buttonColor: Color.init(hex: "F2F2F2"), buttonRadius: 50))
                .padding(.horizontal,30).padding(.top,5)
            }
            
            Spacer()
            //Button
            Button(action: {
                print("CONFIRM BUTTON ACTION CLICKED")
            }){
                Text(Localizable.Continue.localized())
            }
            .modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 16, fontColor: Color.white))
            .modifier(ButtonStyle(buttonHeight: 50, buttonColor: Color.init(hex: "00ADEF"), buttonRadius: 50))
            .padding(.all,30)
        }.showNavigationBarWithBackStyle()
    }
}

struct VerifyIdView_Previews: PreviewProvider {
    static var previews: some View {
        VerifyIdView()
    }
}
