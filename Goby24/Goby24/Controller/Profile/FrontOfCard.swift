//
//  FrontOfCard.swift
//  Goby24
//
//  Created by Jignesh Bhensadadiya on 7/14/21.
//

import SwiftUI

struct FrontOfCard: View {
    @State var isShowPicker: Bool = false
    @State var selectedImage:Image? = Image("camera")
    @State var isLoaderShown:Bool = false
    @Environment(\.presentationMode) var presentationMode

    var body: some View {
        LoadingView(isShowing: $isLoaderShown) {

        VStack{
            Text("Front of card").modifier(CustomTextM(fontName: UIFont.Rubik.medium.fontName, fontSize: 26, fontColor: Color.init(hex: "2E2E2E"))).padding()
            
            VStack{
                selectedImage!.resizable()
            }.frame(width: 200, height: 200, alignment: .center)
            
            Text("Upload a photo of the front of your card").modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 16, fontColor: Color.init(hex: "2E2E2E"))).multilineTextAlignment(.center).padding(.top,50).frame(maxWidth:250)

            
            Spacer()
            
            //Button
            Button(action: {
                print("Take Photo")
                isShowPicker = true
            }){
                Text("Take Photo")
            }
            .modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 16, fontColor: Color.white))
            .modifier(ButtonStyle(buttonHeight: 50, buttonColor: Color.init(hex: "00ADEF"), buttonRadius: 50))
            .padding(.all,30)
            .sheet(isPresented: $isShowPicker) {
               
                
                OpenGallary(isShown: $isShowPicker, image: $selectedImage, sourceType: .camera) { (image) in
                    
                    // PARAM
                    var param:[String:Any] = [String:Any]()
                    param["identity_document"] = image
                    param["identity_document_type"]  = "Nid_Card"
                    isLoaderShown = false
                
                    WebAccess.putDataWithImage(_Url: WebAccess.DOCUMENTS_UPLOAD, _parameters: param) { (result) in
                        isLoaderShown = false
                        switch (result){
                        case .Success(let _data):
                            print(_data)
                            WebCommanMethods.getUserProfileData { (result) in
                                
                            }
                            ShowAlertWithCompletation(title: APPNAME, msg: "documents uploaded sucessfully", view: UIApplication.getTopViewController()!) { (result) in
                                if (result == true){
                                    presentationMode.wrappedValue.dismiss()
                                }
                            }
                            break
                        case .Error(let msg):
                            ShowAlert(title: APPNAME, msg: msg, view: UIApplication.getTopViewController()!)
                            break
                        }
                    }
                    
                    
//                    WebAccess.putDataWithImage(_Url: WebAccess.DOCUMENTS_UPLOAD, _parameters: param) { (result) in
//                        isLoaderShown = false
//                    }
                }
            }
        }.showNavigationBarWithBackStyle()
        }
    }
}

struct FrontOfCard_Previews: PreviewProvider {
    static var previews: some View {
        FrontOfCard()
    }
}
