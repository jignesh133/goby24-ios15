//
//  AboutYouView.swift
//  Goby24
//
//  Created by Jignesh Bhensadadiya on 7/14/21.
//

import SwiftUI

struct AboutYouView: View {
    
    @State var accountClicked: Bool = false
    @State var selection: Int? = nil
    @State  var isViewPresented = false
    @State  var isVerifyAddress = false
    @State  var isDriverClicked:Bool?
    @State  var isRatingViewClick = false
    
    func updateProfile(completion: @escaping(Bool) -> Void) {
        
        var param:[String:Any] = [String:Any]()
        param["is_rider"] = isDriverClicked
        WebAccess.putDataWith(_Url: WebAccess.UPDATEUSER_URL, _parameters: param) { (result) in
            switch (result){
            case .Success(let _data):
                print(_data)
                let responseData = _data["result"] as? [String:Any] ?? [:]
                
                userDefault.set(responseData, forKey: Enum_Login.userProfileData.rawValue)
                appDelegate?.userProfileData = responseData
                userDefault.synchronize()
                appDelegate?.userProfileData = userDefault.value(forKey: Enum_Login.userProfileData.rawValue) as? [String : Any] ?? [:]
                completion(true)
                break
            case .Error(let msg):
                ShowAlert(title: APPNAME, msg: msg, view: UIApplication.getTopViewController()!)
                break
            }
        }
    }
    
    var body: some View {
        VStack(alignment:.leading){
            
          //  NavigationLink(destination: userProfileRatingView(),isActive: self.$isRatingViewClick) {EmptyView()}

            //Button
            Button(action: {
                print("BUTTON ACTION CLICKED")
                isRatingViewClick = true
            }){
                // USER BIO
                Group{
                    ZStack{
                        Image("accountbg").frame(height: 100).padding()
                        HStack{
                            userImageView().padding(.leading,5)

                           // Image("userimage").clipShape(Circle()).padding(.leading,5)
                        //    userImageView().padding(.leading.5)
                            VStack(alignment:.leading){
                                
                                Text(getName(isFullname: true)).modifier(CustomTextM(fontName: UIFont.Montserrat.semiBold.fontName, fontSize: 16, fontColor: Color.init(hex: "2E2E2E")))
                                
                                Text("View and edit your profile").modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 12, fontColor: Color.black))
                            }.padding(.leading,10)
                            Spacer()
                            Image("nextblue").padding()
                            
                        }.padding()//.overlay(RoundedRectangle(cornerRadius: 50).stroke(Color.gray, lineWidth: 0.5))
                        Spacer()
                    }
                }
            }
            .sheet(isPresented: self.$isRatingViewClick) {
                userProfileRatingView()
            }
            HStack{
                Spacer()
                if (isDriverClicked == true){
                    Group{
                    
                        //Button
                        Button(action: {
                            guard isDriverClicked == true else{return}
                            
                            isDriverClicked  = false
                            self.updateProfile { (result) in
                                if (result == true){
                                    let val = getUserIsRider()
                                    if (val == true){
                                        ShowAlert(title: APPNAME, msg: "Now you are driver also", view: UIApplication.getTopViewController()!)
                                    }else {
                                        ShowAlert(title: APPNAME, msg: "Now you are traveller only", view: UIApplication.getTopViewController()!)

                                    }
                                }
                            }
                        }){
                            Text("Traveller")
                        }
                        .modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 16, fontColor: Color.init(hex: "2E2E2E")))
                        .modifier(ButtonStyle(buttonHeight: 50, buttonColor: Color.clear, buttonRadius: 50))
                        .overlay(RoundedRectangle(cornerRadius: 50).stroke(Color.init(hex: "00ADEF"), lineWidth: 1))
                    }.frame(width: 150, height: 50)
                    Spacer()
                    Group{
                        //Button
                        Button(action: {
                            print("BUTTON ACTION CLICKED")
                        }){
                            Text("Driver")
                        }
                        .modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 16, fontColor: Color.white))
                        .modifier(ButtonStyle(buttonHeight: 50, buttonColor: Color.init(hex: "00ADEF"), buttonRadius: 50))
                        
                    }.frame(width: 150, height: 50)
                }else{
                    Group{
                    
                        //Button
                        Button(action: {
                            print("BUTTON ACTION CLICKED")
                            
                        }){
                            Text("Traveller")
                        }
                        .modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 16, fontColor: Color.white))
                        .modifier(ButtonStyle(buttonHeight: 50, buttonColor: Color.init(hex: "00ADEF"), buttonRadius: 50))
                    }.frame(width: 150, height: 50)
                    Spacer()
                    Group{
                        //Button
                        Button(action: {
                            guard isDriverClicked == false else{return}
                            
                            isDriverClicked  = true
                            self.updateProfile { (result) in
                                if (result == true){
                                    let val = getUserIsRider()
                                    if (val == true){
                                        ShowAlert(title: APPNAME, msg: "Now you are driver also", view: UIApplication.getTopViewController()!)
                                    }else {
                                        ShowAlert(title: APPNAME, msg: "Now you are traveller only", view: UIApplication.getTopViewController()!)
                                    }
                                }
                            }
                        }){
                            Text("Driver")
                        }
                        
                        .modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 16, fontColor: Color.init(hex: "2E2E2E")))
                        .modifier(ButtonStyle(buttonHeight: 50, buttonColor: Color.clear, buttonRadius: 50))
                        .overlay(RoundedRectangle(cornerRadius: 50).stroke(Color.init(hex: "00ADEF"), lineWidth: 1))

                    }.frame(width: 150, height: 50)

                }
                
               
                Spacer()
            }.padding(.horizontal,30)
            
            ScrollView{
                Group{
                    NavigationLink(destination: TakePhotoView(), tag: 0, selection: $selection) {
                        Button(action: {
                            selection = 0
                        }){
                            AboutYouViewListCell(text: "Add a profile picture", iconName: "camerablue")
                        }.modifier(ButtonStyle(buttonHeight: 50, buttonColor: Color.init(hex: "F2F2F2"), buttonRadius: 50))
                        .padding(.horizontal,30)
                        .buttonStyle(PlainButtonStyle())

                    }
                }
                // 2
                Group{
                    NavigationLink(destination: AddMiniBioView(), tag: 1, selection: $selection) {
                        Button(action: {
                            selection = 1
                        }){
                            AboutYouViewListCell(text: (Localizable.Add_a_mini_bio.localized()), iconName: "plusicon")
                        }.modifier(ButtonStyle(buttonHeight: 50, buttonColor: Color.init(hex: "F2F2F2"), buttonRadius: 50))
                        .padding(.horizontal,30)
                    }
                }
                Text(Localizable.Edit_personal_details.localized()).modifier(CustomTextM(fontName: UIFont.Montserrat.semiBold.fontName, fontSize: 16, fontColor: Color.init(hex: "2E2E2E")))

                // 3
                Group{
                        Button(action: {
                            self.isViewPresented = true
                        }){
                            AboutYouViewListCell(text: (Localizable.Edit_travel_preferences.localized()), iconName: "plusicon")
                        }.modifier(ButtonStyle(buttonHeight: 50, buttonColor: Color.init(hex: "F2F2F2"), buttonRadius: 50))
                        .padding(.horizontal,30)
                        .sheet(isPresented: self.$isViewPresented) {
                            TravelPreference()
                        }
                }
                // 4
                Group{
                    NavigationLink(destination: VerifyFullNameView(), tag: 3, selection: $selection) {
                        Button(action: {
                            selection = 3
                        }){
                            AboutYouViewListCell(text: (Localizable.Verify_your_Govt_ID.localized()), iconName: "plusicon")
                        }.modifier(ButtonStyle(buttonHeight: 50, buttonColor: Color.init(hex: "F2F2F2"), buttonRadius: 50))
                        .padding(.horizontal,30)
                    }
                }
                // 5
                Group{
                    NavigationLink(destination: FillYourPhoneNumber(), tag: 4, selection: $selection) {
                        Button(action: {
                            selection = 4
                        }){
                            AboutYouViewListCell(text: (Localizable.Verify_your_phone_number.localized()), iconName: ismobilenoverified() ? "selected" : "plusicon")
                        }.modifier(ButtonStyle(buttonHeight: 50, buttonColor: Color.init(hex: "F2F2F2"), buttonRadius: 50))
                        .padding(.horizontal,30)
                    }
                }
                // 6
                Group{
                    
                    // 3
                    Group{
                            Button(action: {
                                self.isVerifyAddress = true
                            }){
                                AboutYouViewListCell(text: (Localizable.Verify_your_Email.localized()), iconName: isEmailVerified() ? "selected" : "plusicon")
                            }.modifier(ButtonStyle(buttonHeight: 50, buttonColor: Color.init(hex: "F2F2F2"), buttonRadius: 50))
                            .padding(.horizontal,30)
                            .sheet(isPresented: self.$isVerifyAddress) {
                                VeryfyEmailCodeView()
                            }
                    }
                    

                }
                if (isDriverClicked == true){
                    // 7
                    Group{
                        NavigationLink(destination: LicensePlateNumberView(), tag: 6, selection: $selection) {
                            Button(action: {
                                selection = 6
                            }){
                                if (getUserVehicleVerified() == false){
                                AboutYouViewListCell(text: (Localizable.Add_vehicle.localized()), iconName: "plusicon")
                                }else{
                                    AboutYouViewListCell(text: (Localizable.Edit_Vehicle.localized()), iconName: "selected")
                                }
                            }.modifier(ButtonStyle(buttonHeight: 50, buttonColor: Color.init(hex: "F2F2F2"), buttonRadius: 50))
                            .padding(.horizontal,30)
                        }
                    }
                }

                

                // Add vehicle
            }.padding(.top,10)
            
            
            Spacer()
        }.onAppear(){
            isDriverClicked = getUserIsRider()
        }
    }
}

struct AboutYouView_Previews: PreviewProvider {
    static var previews: some View {
        AboutYouView()
    }
}
struct AboutYouViewListCell: View {
    @State var text: String = ""
    @State var iconName: String = ""
    @State var tag: Int = -1

    
    var body: some View {
        
        HStack() {
            Image(iconName).padding(.horizontal,20)
            Text(text).modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 16, fontColor: Color.init(hex: "2E2E2E")))

            Spacer()
            Image("next").padding(.trailing,20)
        }
    }
}
