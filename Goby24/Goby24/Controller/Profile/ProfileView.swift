//
//  ProfileView.swift
//  Goby24
//
//  Created by Jignesh Bhensadadiya on 7/12/21.
//

import SwiftUI
import SDWebImageSwiftUI
import Localize_Swift

struct ProfileView: View {
    @State var selection: Int? = nil
    @Binding var isFromTabbar: Bool
    @State var showingSheet: Bool = false
    @State var isNotificationOpen: Bool = false
    @State var userPhoto:userImageView = userImageView()
    
    @EnvironmentObject var settings: UserSettings
    
    var body: some View {
        ScrollView{
            LazyVStack(alignment:.leading){
                NavigationLink(destination: SignUp2(),isActive: $showingSheet) {EmptyView()}.isDetailLink(false)
                NavigationLink(destination: NotificationView(),isActive: $isNotificationOpen) {EmptyView()}.isDetailLink(false)
                
                // USER BIO
                HStack{
                    // userImageView()
                    //  userPhoto
                    
                    VStack(alignment:.leading){
                        Text(getName()).modifier(CustomTextM(fontName: UIFont.Montserrat.semiBold.fontName, fontSize: 16, fontColor: Color.init(hex: "2E2E2E")))
                        
                        Text("View and edit your profile").modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 12, fontColor: Color.black))
                    }.padding(.leading,10)
                    Spacer()
                }.padding()
                
                VStack(alignment:.leading){
                    Button(action: {
                        // NotificationView()
                        isNotificationOpen = true
                    }){
                        Text("Notifications")
                    }
                    .modifier(CustomTextM(fontName: UIFont.Montserrat.semiBold.fontName, fontSize: 16, fontColor: Color.init(hex: "2E2E2E")))
                    
                    Text("Here is whats gong on today").modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 12, fontColor: Color.black))
                }.padding(.leading,25)
                
                ScrollView{
                    Divider().padding()
                    
                    VStack(alignment:.leading){
                        Text("Your current level").modifier(CustomTextM(fontName: UIFont.Montserrat.semiBold.fontName, fontSize: 16, fontColor: Color.init(hex: "2E2E2E")))
                        
                        HStack{
                            ZStack{
                                Text("NEWCOMER").modifier(CustomTextM(fontName: UIFont.Montserrat.semiBold.fontName, fontSize: 16, fontColor: Color.white)).frame(width: 126, height: 32, alignment: .center)
                            }.background(Color.init(hex: "00A761")).cornerRadius(16)
                            
                            ProgressView(value: 0.4).padding(.trailing,20).accentColor(Color.init(hex: "00A761"))
                                .foregroundColor(Color.init(hex: "DADADA"))
                            
                        }.padding(.top,10)
                    }.padding()
                    
                    Divider().padding(.horizontal)
                    
                    NavigationLink(destination: MyRides(), tag: 0, selection: $selection) {
                        Button(action: {
                            selection = 0
                        }){
                            ProfileListItem(title: (Localizable.My_Rides.localized()))
                        }
                    }
                    
                    NavigationLink(destination: Inbox(), tag: 1, selection: $selection) {
                        Button(action: {
                            print("")
                            selection = 1
                        }){
                            ProfileListItem(title: (Localizable.Messages.localized()))
                        }
                        
                    }
                    
                    if(getUserIsRider() == true){
                        NavigationLink(destination: RatingAndReviewView(isUserRider: true), tag: 21, selection: $selection) {
                            Button(action: {
                                print("")
                                selection = 21
                            }){
                                ProfileListItem(title: (Localizable.Ratings.localized()))
                            }
                        }
                    }else{
                        NavigationLink(destination: RatingAndReviewView(), tag: 22, selection: $selection) {
                            Button(action: {
                                print("")
                                selection = 22
                            }){
                                ProfileListItem(title: (Localizable.Ratings.localized()))
                            }
                        }
                    }
                    
                    NavigationLink(destination: ProfileMenu(), tag: 3, selection: $selection) {
                        Button(action: {
                            selection = 3
                        }){
                            ProfileListItem(title: (Localizable.Profile.localized()))
                        }
                    }
                    NavigationLink(destination: SignUp2(), tag: 4, selection: $selection) {
                        Button(action: {
                            selection = 4
                        }){
                            ProfileListItem(title: "Settings")
                        }
                    }
                }
                Spacer()
                //Button
                Button(action: {
                    let alert = UIAlertController(title: APPNAME, message: "Are you sure you want to logout", preferredStyle: .alert)
                    let cancel = UIAlertAction(title:"Cancel", style: .cancel) { (res) in
                    }
                    alert.addAction(cancel)
                    let logout = UIAlertAction(title:(Localizable.Log_Out.localized()).localized(), style: .default) { (res) in
                        print("CONFIRM BUTTON ACTION CLICKED")
                        appDelegate?.resetUserDefautls()
                        self.settings.loggedIn = false
                    }
                    alert.addAction(logout)
                    UIApplication.getTopViewController()!.present(alert, animated: true, completion: nil)
                    
                    
                }){
                    Text(Localizable.Log_Out.localized())
                }
                .modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 16, fontColor: Color.white))
                .modifier(ButtonStyle(buttonHeight: 50, buttonColor: Color.init(hex: "00ADEF"), buttonRadius: 10))
                .padding()
                Spacer()
            }
        }.onAppear(){
            if (appDelegate?.selectedTab == 4){
                print("appear " + "\(isFromTabbar)")
                DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                    if(isFromTabbar == true){
                        isFromTabbar = false
                        showingSheet = true
                    }
                }
            }
            
        }

        .showNavigationBarStyle()
        .onAppear(perform: {
            userPhoto = userImageView(isReload: true)
        })
    }
}

struct ProfileView_Previews: PreviewProvider {
    @State static var isFromTabbar = false
    
    static var previews: some View {
        ProfileView(isFromTabbar: $isFromTabbar)
    }
}
