//
//  LicensePlateNumberView.swift
//  Goby24
//
//  Created by Jignesh Bhensadadiya on 7/14/21.
//

import SwiftUI

struct LicensePlateNumberView: View {
    
    @State var plateNumber: String = ""
    @State var strCountry: String?
    @State var countryIndex: Int = 0
    @State var selection: Int? = nil
    
  
    
    var addVihicle = AddVihicle.shared
    
    var body: some View {
        VStack{
            Text(Localizable.What_is_your_licence_plate_number.localized()).modifier(CustomTextM(fontName: UIFont.Rubik.medium.fontName, fontSize: 26, fontColor: Color.init(hex: "2E2E2E"))).multilineTextAlignment(.center).padding()
            
            //Button
            Button(action: {
                RPicker.selectOption(title: "Choose your country", cancelText: "Cancel", doneText: "Done", dataArray: appDelegate?.countryNames, selectedIndex: countryIndex) { (val, index) in
                    strCountry = val
                    
                }
            }){
                HStack() {
                    Text(strCountry ?? "Choose your country").padding(.leading,20)
                    Spacer()
                    Image("down").padding(.trailing,20)
                }
            }
            .modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 16, fontColor: Color.init(hex: "2E2E2E")))
            .modifier(ButtonStyle(buttonHeight: 50, buttonColor: Color.init(hex: "F2F2F2"), buttonRadius: 50))
            .padding(.horizontal,30)
            
            CustomTextField(placeHolderText: "EX 123-456-789 ",
                            text: self.$plateNumber).padding(.horizontal,30).padding(.top)
            //Button
            Button(action: {
                    selection = 0
            }){
                Text("I don't know my license plate number")
            }
            .modifier(CustomTextM(fontName: UIFont.Poppins.regular.fontName, fontSize: 16, fontColor: Color.init(hex: "00AEEF"))).padding()
            

            
            NavigationLink(destination: VehicaleDetailsView(), tag: 0, selection: $selection) {
                
                //Button
                Button(action: {
                    if (strCountry?.removeWhiteSpace().count == 0){
                        ShowAlert(title: APPNAME, msg: "Please select country", view: UIApplication.getTopViewController()!)
                    }else if (plateNumber.removeWhiteSpace().count == 0){
                        ShowAlert(title: APPNAME, msg: "Please enter licence plate number", view: UIApplication.getTopViewController()!)
                    }
                    else{
                        addVihicle.country = strCountry
                        addVihicle.licenceNo = plateNumber
                        selection = 0
                    }
                }){
                    Text(Localizable.Continue.localized())
                }
                .modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 16, fontColor: Color.white))
                .modifier(ButtonStyle(buttonHeight: 50, buttonColor: Color.init(hex: "00ADEF"), buttonRadius: 50))
                .padding(.all,30)
            }
            Spacer()
        }.showNavigationBarWithBackStyle()
        .onAppear(){
//             SET DATA
            let userData = userDefault.value(forKey: Enum_Login.userProfileData.rawValue) as? [String:Any] ?? [:]
            let vehicleInfo = userData["vehicle_info"] as? [String:Any] ?? [:]
            plateNumber = vehicleInfo["vehicle_no"] as? String ?? ""
            print(userData)
//            strCountry = countr
//                plateNumber
        }
    }
}

struct LicensePlateNumberView_Previews: PreviewProvider {
    static var previews: some View {
        LicensePlateNumberView()
    }
}
