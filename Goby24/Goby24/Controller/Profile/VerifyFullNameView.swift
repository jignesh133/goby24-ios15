//
//  VerifyFullNameView.swift
//  Goby24
//
//  Created by Jignesh Bhensadadiya on 7/14/21.
//

import SwiftUI

struct VerifyFullNameView: View {
    
    //MARK: - PROPERTIES
    @State var firstName: String = getName(isFullname: false)
    @State var lastName: String = getLName()
    @State var selection: Int? = nil

    
    var body: some View {
        VStack{
            Text("Verify").modifier(CustomTextM(fontName: UIFont.Montserrat.semiBold.fontName, fontSize: 16, fontColor: Color.init(hex: "2E2E2E")))
            HStack(spacing:0){
                Text("Full name").modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 11, fontColor: Color.init(hex: "2E2E2E"))).frame(maxWidth:.infinity)
                Text("Id").modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 13, fontColor: Color.init(hex: "2E2E2E"))).frame(maxWidth:.infinity)
                
            }.padding(.horizontal,30).padding(.top)
            ZStack{
                Divider()
                HStack(spacing:0){
                    
                    VStack(alignment: .center, spacing: 0){
                        Circle().fill(Color.init(hex: "FF9500")).frame(width: 5, height: 5)
                    }.frame(maxWidth:.infinity)
                    VStack(alignment: .center, spacing: 0){
                        Circle().fill(Color.init(hex: "FF9500")).frame(width: 5, height: 5)
                    }.frame(maxWidth:.infinity)
                }
            }.background(Color.clear).padding(.horizontal,30)

            Text("First verify your name").modifier(CustomTextM(fontName: UIFont.Montserrat.semiBold.fontName, fontSize: 16, fontColor: Color.init(hex: "2E2E2E"))).padding()

            CustomTextField(placeHolderText: "First Name",
                            text: self.$firstName).padding(.horizontal,30)
            
            CustomTextField(placeHolderText: "Last Name",
                            text: self.$lastName).padding(.horizontal,30).padding(.top,10)
            
            Spacer()
            
            NavigationLink(destination: VerifyIdView(), tag: 0, selection: $selection) {
                //Button
                Button(action: {
                 selection = 0
                }){
                    Text("Continue")
                }
                .modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 16, fontColor: Color.white))
                .modifier(ButtonStyle(buttonHeight: 50, buttonColor: Color.init(hex: "00ADEF"), buttonRadius: 50))
                .padding(.all,30)
            }
            
        }.showNavigationBarWithBackStyle()
    }
}

struct VerifyFullNameView_Previews: PreviewProvider {
    static var previews: some View {
        VerifyFullNameView()
    }
}
