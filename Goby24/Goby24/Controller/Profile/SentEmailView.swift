//
//  SentEmailView.swift
//  Goby24
//
//  Created by Jignesh Bhensadadiya on 7/14/21.
//

import SwiftUI

struct SentEmailView: View {
    var body: some View {
        ZStack{
            Color.init(hex: "00A761").ignoresSafeArea()
            VStack{
                Image("sentemail")
                Text("Sent! you will receive a confirmation emai shortly.").modifier(CustomTextM(fontName: UIFont.Rubik.medium.fontName, fontSize: 26, fontColor: Color.white)).multilineTextAlignment(.center).padding()
            }
        }
    }
}

struct SentEmailView_Previews: PreviewProvider {
    static var previews: some View {
        SentEmailView()
    }
}
