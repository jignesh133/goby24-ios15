//
//  FillYourFourDigitCodeView.swift
//  Goby24
//
//  Created by Jignesh Bhensadadiya on 7/14/21.
//

import SwiftUI

struct FillYourFourDigitCodeView: View {
    
    @State var fourDigitCode: String = ""
    @State var mobileNumber: String = ""
    @State private var isAlertPresent = false
    @State private var alertMessage = ""
    @State var isLoaderShown:Bool = false
    @Environment(\.presentationMode) var presentationMode
    
    func veryfyOtp(){
        if (fourDigitCode.removeWhiteSpace().count == 4 ){
            var param:[String:Any] = [String:Any]()
            param["otp"] = fourDigitCode
            isLoaderShown = false
            WebAccess.postDataWith(_Url: WebAccess.VERIFY_PHONECODE, _parameters: param) { (result) in
                isLoaderShown = false
                switch(result){
                case .Success(let _):
                    
                    ShowAlert(title: APPNAME, msg: "OTP verified successfully", view: UIApplication.getTopViewController()!)
                    break
                case .Error(let error):
                    ShowAlertWithCompletation(title: APPNAME, msg: error, view: UIApplication.getTopViewController()!) { (result) in
                    }
                    break
                    
                }
            }
        }else{
            ShowAlert(title: APPNAME, msg: "Please enter 4 digit otp", view: UIApplication.getTopViewController()!)
        }
        
    }
    func resendOtp(isHideLoading:Bool = false){
        WebAccess.postDataWith(_Url: WebAccess.MOBILE_OTP, _parameters: [:],isHideLoader: isHideLoading) { (result) in
            switch(result){
            case .Success( _):
                if (isHideLoading == false){
                    ShowAlert(title: APPNAME, msg: "OTP sent successfully", view: UIApplication.getTopViewController()!)
                }
                break
            case .Error(let error):
                ShowAlertWithCompletation(title: APPNAME, msg: error, view: UIApplication.getTopViewController()!) { (result) in
                    if (result == true){
                        self.presentationMode.wrappedValue.dismiss()
                    }
                }
                break
                
            }
        }
        
        
    }
    var body: some View {
        LoadingView(isShowing: $isLoaderShown) {
            
            VStack{
                
                Text("Fill your 4 Digit Code").modifier(CustomTextM(fontName: UIFont.Rubik.medium.fontName, fontSize: 26, fontColor: Color.init(hex: "2E2E2E"))).padding()
                
                CustomTextField(placeHolderText: "4-digit code",text: self.$fourDigitCode).padding(.horizontal,30).padding(.top)
                    .keyboardType(.numberPad)
                HStack{
                    //Button
                    Button(action: {
                        print("")
                        self.resendOtp()
                        
                    }){
                        Text(Localizable.Resend_the_OTP.localized())
                    }
                    .modifier(CustomTextM(fontName: UIFont.Poppins.regular.fontName, fontSize: 16, fontColor: Color.init(hex: "00AEEF")))
                    .padding(.horizontal,35)
                    Spacer()
                    Text("00:10:00").modifier(CustomTextM(fontName: UIFont.Poppins.regular.fontName, fontSize: 16, fontColor: Color.init(hex: "00AEEF"))).padding(.horizontal,35)
                    
                }
                
                Spacer()
                
                //Button
                Button(action: {
                    self.veryfyOtp()
                    
                }){
                    Text(Localizable.Done.localized())
                }
                .modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 16, fontColor: Color.white))
                .modifier(ButtonStyle(buttonHeight: 50, buttonColor: Color.init(hex: "00ADEF"), buttonRadius: 50))
                .padding(.all,30)
                
                
            }.showNavigationBarWithBackStyle()
            .onLoad(){
                self.resendOtp(isHideLoading: true)
            }
        }
    }
}

struct FillYourFourDigitCodeView_Previews: PreviewProvider {
    static var previews: some View {
        FillYourFourDigitCodeView()
    }
}
