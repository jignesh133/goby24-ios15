//
//  AddNewCarModelView.swift
//  Goby24
//
//  Created by Jignesh Bhensadadiya on 10/3/21.
//

import SwiftUI

struct AddNewCarModelView: View {
    @State var modelName: String = ""
    let onAddClicked: (String) -> Void
    @Environment(\.viewController) private var viewControllerHolder: UIViewController?

    var body: some View {
        
        VStack{
            VStack{
                HStack{
                    Text("Add Car Model").modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 12, fontColor: Color.init(hex: "00AEEF")))
                    Spacer()
                }
                CustomTextField(placeHolderText: "Write Car Model",text: self.$modelName,fontSize: 12)
                HStack{
                    Button(action: {
                        print("Cancel")
                        self.viewControllerHolder?.dismiss(animated: true, completion: nil)
                    }) {
                        Text(Localizable.Cancel.localized())
                    }
                    .modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 12, fontColor: Color.init(hex: "2E2E2E")))
                    .modifier(ButtonStyle(buttonHeight: 40, buttonColor: Color.init(hex: "D0D0D0"), buttonRadius: 40))
                    .padding(10)
                    
                    Button(action: {
                        print("Add")
                        onAddClicked(modelName)
                        self.viewControllerHolder?.dismiss(animated: true, completion: nil)

                    }) {
                        Text(Localizable.Add.localized())
                    }
                    .modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 12, fontColor: Color.white))
                    .modifier(ButtonStyle(buttonHeight: 40, buttonColor: Color.init(hex: "00AEEF"), buttonRadius: 40))
                    .padding(10)
                }

            }.padding()
            
        }.background(Color.white).cornerRadius(10).frame(width: 250, height: 140, alignment: .center)
        
    }
}
struct AddNewCarBrandView: View {
    @State var brandName: String = ""
    let onAddClicked: (String) -> Void
    @Environment(\.viewController) private var viewControllerHolder: UIViewController?

    var body: some View {
        
        VStack{
            VStack{
                HStack{
                    Text("Add Car Brand").modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 12, fontColor: Color.init(hex: "00AEEF")))
                    Spacer()
                }
                CustomTextField(placeHolderText: "Write Car Brand",text: self.$brandName,fontSize: 12)
                HStack{
                    Button(action: {
                        print("Cancel")
                        self.viewControllerHolder?.dismiss(animated: true, completion: nil)
                    }) {
                        Text(Localizable.Cancel.localized())
                    }
                    .modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 12, fontColor: Color.init(hex: "2E2E2E")))
                    .modifier(ButtonStyle(buttonHeight: 40, buttonColor: Color.init(hex: "D0D0D0"), buttonRadius: 40))
                    .padding(10)
                    
                    Button(action: {
                        print("Add")
                        onAddClicked(brandName)
                        self.viewControllerHolder?.dismiss(animated: true, completion: nil)

                    }) {
                        Text(Localizable.Add.localized())
                    }
                    .modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 12, fontColor: Color.white))
                    .modifier(ButtonStyle(buttonHeight: 40, buttonColor: Color.init(hex: "00AEEF"), buttonRadius: 40))
                    .padding(10)
                }

            }.padding()
            
        }.background(Color.white).cornerRadius(10).frame(width: 250, height: 140, alignment: .center)
        
    }
}
