//
//  AnythingAboutRideView.swift
//  Goby24
//
//  Created by Jignesh Bhensadadiya on 7/18/21.
//

import SwiftUI
import NavigationStack

struct AddMiniBioView: View {
    @State var miniBio: String = ""
    @Environment(\.presentationMode) var presentationMode

    func updateProfile(completion: @escaping(Bool) -> Void) {
        var param:[String:Any] = [String:Any]()
        param["about"] = miniBio
        WebAccess.putDataWith(_Url: WebAccess.UPDATEUSER_URL, _parameters: param) { (result) in
            switch (result){
            case .Success(let _data):
                print(_data)
                let responseData = _data["result"] as? [String:Any] ?? [:]
                
                userDefault.set(responseData, forKey: Enum_Login.userProfileData.rawValue)
                appDelegate?.userProfileData = responseData
                userDefault.synchronize()
                appDelegate?.userProfileData = userDefault.value(forKey: Enum_Login.userProfileData.rawValue) as? [String : Any] ?? [:]
                completion(true)
                break
            case .Error(let msg):
                ShowAlert(title: APPNAME, msg: msg, view: UIApplication.getTopViewController()!)
                break
            }
        }
    }
    var body: some View {
        VStack{
            
            Text("Write something about yourself").modifier(CustomTextM(fontName: UIFont.Rubik.medium.fontName, fontSize: 26, fontColor: Color.init(hex: "2E2E2E"))).multilineTextAlignment(.center).padding()
            
            Group{
                VStack{
                    TextEditor(text: $miniBio).modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 16, fontColor: Color.black)).textContentType(.none)
                    Spacer()
                }.padding()
            }.frame(width: 275, height: 126, alignment: .center).background(Color.init(hex: "F5F5F5")).clipShape(RoundedRectangle(cornerRadius: 14)).padding()
            Spacer()
            Button(action: {
                self.updateProfile { (result) in
                    if (result){
                        ShowAlertWithCompletation(title: APPNAME, msg: "Sucessfully updated", view: UIApplication.getTopViewController()!) { (result) in
                            self.presentationMode.wrappedValue.dismiss()
                        }
                    }
                }

            }){
                Text(Localizable.Save.localized())
            }
            .modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 16, fontColor: Color.white))
            .modifier(ButtonStyle(buttonHeight: 40, buttonColor: Color.init(hex: "00ADEF"), buttonRadius: 40))
            .padding(.horizontal,40)
            Spacer()

        }.showNavigationBarWithBackStyle()
        .onAppear(){

            miniBio = getAboutMe()
        }
    }
}

struct AddMiniBioView_Previews: PreviewProvider {
    static var previews: some View {
        AnythingAboutRideView()
    }
}


 
