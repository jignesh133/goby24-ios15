//
//  TakePhotoView.swift
//  Goby24
//
//  Created by Jignesh Bhensadadiya on 7/14/21.
//

import SwiftUI
import SDWebImageSwiftUI

struct TakePhotoView: View {
    @State var isShowPicker: Bool = false
    @State var isShowPickerWithCamera: Bool = false
    
    
    @State var selectedImage:Image? = Image(systemName: "photo")
    @State var isLoaderShown : Bool = false
    @Environment(\.presentationMode) var presentationMode

    var body: some View {
            
            VStack{
                Spacer()
                ZStack{
                    Image("takephoto").frame(minWidth: 0, maxWidth: .infinity)
                    VStack{
                        Spacer()
                        selectedImage?.resizable()    .aspectRatio(contentMode: .fill).frame(width: 100, height: 100, alignment: .center).clipShape(Circle()).overlay(
                            RoundedRectangle(cornerRadius: 100)
                                .stroke(Color.init(hex: "00AEEF"), lineWidth: 1)
                        )
                        
                        Spacer()
                        Text("Dont wear sunglasses, look straight ahead and make sure you’re alone.").modifier(CustomTextM(fontName: UIFont.Rubik.medium.fontName, fontSize: 26, fontColor: Color.init(hex: "2E2E2E"))).multilineTextAlignment(.center).padding(.horizontal)
                        Spacer()
                        
                    }.padding(.horizontal,10).frame(minWidth: 0, maxWidth: .infinity)
                }.frame(width: UIScreen.main.bounds.size.width - 40 , height: 300, alignment: .center)
                
                Spacer()
                
                //Button
                Button(action: {
                    isShowPicker = true
                    
                }){
                    Text("Take Photo")
                }
                .modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 16, fontColor: Color.white))
                .modifier(ButtonStyle(buttonHeight: 50, buttonColor: Color.init(hex: "00ADEF"), buttonRadius: 50))
                .padding(.horizontal)
                .sheet(isPresented: $isShowPicker) {
                    OpenGallary(isShown: $isShowPicker, image: $selectedImage, sourceType: .camera, onImagePicked: { (image) in
                        var param:[String:Any] = [String:Any]()
                        param["profile_pic"] = image
                        isLoaderShown = false
                        WebAccess.putDataWithImage(_Url: WebAccess.PROFILE_UPDATE_URL, _parameters: param) { (result) in
                            isLoaderShown = false
                            switch (result){
                            case .Success(let _data):
                                print(_data)
                                WebCommanMethods.getUserProfileData { (result) in}
                                ShowAlertWithCompletation(title: APPNAME, msg: "Profile image updated sucessfully", view: UIApplication.getTopViewController()!) { (result) in
                                    if (result == true){
                                        presentationMode.wrappedValue.dismiss()
                                    }
                                }
                                break
                            case .Error(let msg):
                                ShowAlert(title: APPNAME, msg: msg, view: UIApplication.getTopViewController()!)
                                break
                            }
                        }
                    })
                    
                }
                
                //Button
                Button(action: {
                    self.isShowPickerWithCamera = true
                }){
                    Text("Choose a picture")
                }
                .modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 16, fontColor: Color.init(hex: "2E2E2E")))
                .modifier(ButtonStyle(buttonHeight: 50, buttonColor: Color.clear, buttonRadius: 50))
                .overlay(RoundedRectangle(cornerRadius: 50).stroke(Color.init(hex: "00AEEF"), lineWidth: 1))
                .padding()
                .sheet(isPresented: $isShowPickerWithCamera) {
                    OpenGallary(isShown: $isShowPickerWithCamera, image: $selectedImage, sourceType: .photoLibrary, onImagePicked: { (selImage) in
                        var param:[String:Any] = [String:Any]()
                        param["profile_pic"] = selImage
                        isLoaderShown = false
                        WebAccess.putDataWithImage(_Url: WebAccess.PROFILE_UPDATE_URL, _parameters: param) { (result) in
                            isLoaderShown = false
                            switch (result){
                            case .Success(let _data):
                                print(_data)
                               
                                WebCommanMethods.getUserProfileData { (result) in}
                                
                                ShowAlertWithCompletation(title: APPNAME, msg: "Profile image updated sucessfully", view: UIApplication.getTopViewController()!) { (result) in
                                    if (result == true){
                                        presentationMode.wrappedValue.dismiss()
                                    }
                                }
                                break
                                
                            case .Error(let msg):
                                ShowAlert(title: APPNAME, msg: msg, view: UIApplication.getTopViewController()!)
                                break
                            }
                        }
                    })
                    
                }
                
            }.showNavigationBarWithBackStyle()
            .onLoad(){
                
                let data = userDefault.value(forKey: Enum_Login.userProfileData.rawValue) as? [String:Any] ?? [:]
                let urlString = IMAGEURL + getStringFromAny(data["profile_pic"] ?? "")

                SDWebImageManager.shared.loadImage(with: URL(string: urlString), options: .continueInBackground) { (val, value, url) in
                } completed: { (image, data, error, catch, result, url) in
                    if (result == true && image != nil){
                        selectedImage = Image(uiImage: image!)
                    }
                }
            }
        
    }
}

struct TakePhotoView_Previews: PreviewProvider {
    static var previews: some View {
        TakePhotoView()
    }
}
