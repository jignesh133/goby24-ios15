//
//  FillYourPhoneNumber.swift
//  Goby24
//
//  Created by Jignesh Bhensadadiya on 7/14/21.
//

import SwiftUI

struct FillYourPhoneNumber: View {
    @State var phoneNumber: String = ""
    @State var selection: Int? = nil
    
    @State private var isAlertPresent = false
    @State private var alertMessage = ""

    @State var countryArray:[String] = [String]()
    
    @State var countryCode: String = ""
    @State var countryName: String = ""
    @State var countryNameWithCode: String?
    
    func updateProfile(completion: @escaping(Bool) -> Void) {
        
            var param:[String:Any] = [String:Any]()
            param["mobile_no"] = countryNameWithCode//phoneNumber
            WebAccess.putDataWith(_Url: WebAccess.UPDATEUSER_URL, _parameters: param) { (result) in
                switch (result){
                case .Success(let _data):
                    print(_data)
                    let responseData = _data["result"] as? [String:Any] ?? [:]
                    
                    userDefault.set(responseData, forKey: Enum_Login.userProfileData.rawValue)
                    appDelegate?.userProfileData = responseData
                    userDefault.synchronize()
                    appDelegate?.userProfileData = userDefault.value(forKey: Enum_Login.userProfileData.rawValue) as? [String : Any] ?? [:]
                    completion(true)
                    break
                case .Error(let msg):
                    ShowAlert(title: APPNAME, msg: msg, view: UIApplication.getTopViewController()!)
                    break
                }
            }
        }
        
        
    
    
    var body: some View {
        VStack{
            
            Text("Fill your phone number").modifier(CustomTextM(fontName: UIFont.Rubik.medium.fontName, fontSize: 26, fontColor: Color.init(hex: "2E2E2E"))).padding()
            
            //Button
            Button(action: {
                
                RPicker.selectOption(title: "Select country", cancelText: "Cancel", doneText: "Done", dataArray:appDelegate?.countryNamesWithCodes , selectedIndex: 0) { (val, index) in
                    countryNameWithCode = val
                    countryCode = countryNameWithCode?.components(separatedBy: " ").first ?? ""
                    countryName = countryNameWithCode?.components(separatedBy: " ").last ?? ""
                }
                
            }){
                VStack{
                    HStack() {
                        Text(countryNameWithCode ?? "Select country").padding(.leading,20)
                        Spacer()
                        Image("down").padding(.trailing,20)
                    }
                }.frame(minWidth: 10, maxWidth: .infinity)
            }
            .modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 16, fontColor: Color.init(hex: "2E2E2E")))
            .modifier(ButtonStyle(buttonHeight: 50, buttonColor: Color.init(hex: "F2F2F2"), buttonRadius: 50))
            .padding(.horizontal,30)
            .contentShape(Rectangle())

            CustomTextField(placeHolderText: "Phone Number ",text: self.$phoneNumber,keyboardType: .phonePad).padding(.horizontal,30).padding(.top)
            
            Spacer()
            
            NavigationLink(destination: FillYourFourDigitCodeView(mobileNumber: phoneNumber), tag: 4, selection: $selection) {
                //Button
                Button(action: {
                    if (phoneNumber.removeWhiteSpace().count > 0){
                        
                        self.updateProfile { (result) in
                            
                            WebAccess.getDataWith(_Url: WebAccess.MOBILE_OTP) { (result) in
                                
                                switch(result){
                                case .Success(let profiledata):
                                    
                                    if (getBoolFromAny(profiledata["status"] ?? false) == true){
                                        selection = 4
                                    }
                                    break
                                case .Error( _):
                                    break
                                    
                                }
                            }
                        }

                    }else{
                        ShowAlert(title: APPNAME, msg: "Please enter mobile number", view: UIApplication.getTopViewController()!)
                    }
                    
                }){
                    Text("Next")
                }
                .modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 16, fontColor: Color.white))
                .modifier(ButtonStyle(buttonHeight: 50, buttonColor: Color.init(hex: "00ADEF"), buttonRadius: 50))
                .padding(.all,30)
                
            }
            
        }.showNavigationBarWithBackStyle()
        
        
    }
}

struct FillYourPhoneNumber_Previews: PreviewProvider {
    static var previews: some View {
        FillYourPhoneNumber()
    }
}
