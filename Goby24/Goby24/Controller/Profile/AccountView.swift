//
//  AccountView.swift
//  Goby24
//
//  Created by Jignesh Bhensadadiya on 7/14/21.
//

import SwiftUI

struct AccountView: View {
    
    @State var accountClicked: Bool = false
    @State  var isRatingViewClick = false

    @EnvironmentObject var settings: UserSettings

    var body: some View {
        VStack(alignment:.leading){
//            NavigationLink(destination: userProfileRatingView(),isActive: self.$isRatingViewClick) {EmptyView()}

            //Button
            Button(action: {
                isRatingViewClick = true
            }){
                // USER BIO
                ZStack{
                    Image("accountbg").frame(width: .infinity, height: 100).padding()
                    HStack{
                        userImageView().padding(.leading,5)
                        
                        // Image("userimage").clipShape(Circle()).padding(.leading,5)
                        VStack(alignment:.leading){
                            Text(getName(isFullname: true)).modifier(CustomTextM(fontName: UIFont.Montserrat.semiBold.fontName, fontSize: 16, fontColor: Color.init(hex: "2E2E2E")))
                            
                            Text("View and edit your profile").modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 12, fontColor: Color.black))
                        }.padding(.leading,10)
                        Spacer()
                        Image("nextblue").padding()
                        
                    }.padding()//.overlay(RoundedRectangle(cornerRadius: 50).stroke(Color.gray, lineWidth: 0.5))
                    Spacer()
                }
            }
            .sheet(isPresented: self.$isRatingViewClick) {
                userProfileRatingView()
            }
            ScrollView{
                
                if(getUserIsRider() == true){
                    NavigationLink(destination: RatingAndReviewView(isUserRider: true)) {
                        AccountViewCell(text: (Localizable.Ratings_given.localized()))
                    }
                }else{
                    NavigationLink(destination: RatingAndReviewView()) {
                        AccountViewCell(text: (Localizable.Ratings_given.localized()))
                    }
                }
                NavigationLink(destination: ResetPassword()) {
                    AccountViewCell(text: (Localizable.Password.localized()))
                }
                NavigationLink(destination: BankAccountsVC()) {
                    AccountViewCell(text: "Available funds")
                }
                

                
//
                AccountViewCell(text: "Bank details")
                NavigationLink(destination: CompletedTransactionView()) {
                    AccountViewCell(text: (Localizable.Completed_transaction.localized()))
                }
                AccountViewCell(text: "Data protection")
                
                Button(action: {
                    print("BUTTON ACTION CLICKED")
                }){
                    Text(Localizable.Close_my_account.localized())
                }
                .modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 16, fontColor: Color.init(hex: "2E2E2E")))
                .modifier(ButtonStyle(buttonHeight: 50, buttonColor: Color.clear, buttonRadius: 50))
                .overlay(RoundedRectangle(cornerRadius: 50).stroke(Color.init(hex: "00ADEF"), lineWidth: 1))
                .padding(.horizontal,30)
                
                Button(action: {
                    let alert = UIAlertController(title: APPNAME, message: "Are you sure you want to logout", preferredStyle: .alert)
                    let cancel = UIAlertAction(title:(Localizable.Cancel.localized()), style: .cancel) { (res) in
                    }
                    alert.addAction(cancel)
                    let logout = UIAlertAction(title:(Localizable.Log_Out.localized()), style: .default) { (res) in
                        appDelegate?.resetUserDefautls()
                        self.settings.loggedIn = false

                    }
                    alert.addAction(logout)
                    UIApplication.getTopViewController()!.present(alert, animated: true, completion: nil)

                    
                    print("BUTTON ACTION CLICKED")
                }){
                    Text(Localizable.Log_Out.localized())
                }
                .modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 16, fontColor: Color.white))
                .modifier(ButtonStyle(buttonHeight: 50, buttonColor: Color.init(hex: "00ADEF"), buttonRadius: 50))
                .padding(.horizontal,30)
                .padding(.top,5)
                
                // Add vehicle
            }.padding(.top,10)
            
            .padding()
            Spacer()
        }
    }
}

struct AccountView_Previews: PreviewProvider {
    static var previews: some View {
        AccountView()
    }
}
struct AccountViewCell: View {
    @State var text: String = ""
    
    
    var body: some View {
        VStack{
            Group{
                //Button
//                Button(action: {}){
                    HStack() {
                        Text(text).padding(.leading,30)
                        Spacer()
                        Image("next").padding(.trailing,20)
                    }
             //   }
                .modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 16, fontColor: Color.init(hex: "2E2E2E")))
                .modifier(ButtonStyle(buttonHeight: 50, buttonColor: Color.init(hex: "F2F2F2"), buttonRadius: 50))
                .padding(.horizontal,30)
            }
            Spacer()
        }
    }
}
