//
//  VeryfyEmailCodeView.swift
//  Goby24
//
//  Created by Jignesh Bhensadadiya on 7/14/21.
//

import SwiftUI

struct VeryfyEmailCodeView: View {
    
    @State var digit1: String = ""
    @State var digit2: String = ""
    @State var digit3: String = ""
    @State var digit4: String = ""
    @State var isLoaderShown:Bool = false
    @State var isAlertPresent:Bool = false
    @State var alertMessage:String = ""
    @Environment(\.presentationMode) var presentationMode

    func resendOtp(isHideLoading:Bool = false){
        var param:[String:Any] = [String:Any]()
        param["email"] = getEmailId()
        
        WebAccess.postDataWith(_Url: WebAccess.RESEND_EMAIL, _parameters: param,isHideLoader: isHideLoading) { (result) in
                switch(result){
                case .Success( _):
                   // ShowAlertWithCompletation(title: APPNAME, msg: "Mail sent sucessfully", view: UIApplication.getTopViewController()!)
                    break
                case .Error(let error):
                    
                    ShowAlertWithCompletation(title: APPNAME, msg: error, view: UIApplication.getTopViewController()!) { (result) in
                        if (result == true){
                            self.presentationMode.wrappedValue.dismiss()
                        }
                    }
                    break
                    
                }
            }
    }
    func submitOtp(){
        if (digit1.removeWhiteSpace().count == 4){
            var param:[String:Any] = [String:Any]()
            param["email"] = getEmailId()
            param["otp"] = digit1 + digit2 + digit3 + digit4
            isLoaderShown = false
            WebAccess.postDataWith(_Url: WebAccess.VERIFY_EMAIL, _parameters: [:]) { (result) in
                    switch(result){
                    case .Success( _):
                        isLoaderShown = false

                        ShowAlertWithCompletation(title: APPNAME, msg: "OTP Verified sucessfully", view: UIApplication.getTopViewController()!)
                        self.presentationMode.wrappedValue.dismiss()
                        break
                    case .Error(let error):
                        isLoaderShown = false
                        ShowAlertWithCompletation(title: APPNAME, msg: error, view: UIApplication.getTopViewController()!) { (result) in
                            if (result == true){
                                self.presentationMode.wrappedValue.dismiss()
                            }
                        }
                        break
                        
                    }
                }
        }else{
            ShowAlert(title: APPNAME, msg: "Please enter 4 digits code", view: UIApplication.getTopViewController()!)
        }
    }
    
    var body: some View {

        LoadingView(isShowing: $isLoaderShown) {
            
            VStack{
                
                ShowCustomNavigation().padding(.top,15)
                
                Text(Localizable.Verify_your_id.localized()).modifier(CustomTextM(fontName: UIFont.Rubik.medium.fontName, fontSize: 15, fontColor: Color.init(hex: "2E2E2E"))).padding()
                
                Text(getEmailId()).modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 13, fontColor: Color.init(hex: "2E2E2E"))).padding(.horizontal,30)
                
                Text(Localizable.Please_enter_the_verification_code_we.localized()).modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 13, fontColor: Color.init(hex: "2E2E2E"))).multilineTextAlignment(.center).frame(width: 215, height: .infinity, alignment: .center).padding(.top,5)
                
                HStack(spacing:5){
                    TextField("4-digit code", text: $digit1)
                        .textFieldStyle(MyTextFieldStyle())
                        .keyboardType(.numberPad).frame(width: 200, height: 35, alignment: .center).padding(.horizontal,5)
                }
               // Text("00:10:00").modifier(CustomTextM(fontName: UIFont.Poppins.regular.fontName, fontSize: 16, fontColor: Color.init(hex: "00AEEF"))).padding(.horizontal,35).padding(.top,10)
                
                //Button
                Button(action: {
                    print("")
                    resendOtp()
                    
                }){
                    Text(Localizable.Resend_the_OTP.localized())
                }
                .modifier(CustomTextM(fontName: UIFont.Poppins.regular.fontName, fontSize: 16, fontColor: Color.init(hex: "00AEEF")))
                .padding()
                
                //Button
                Button(action: {
                    self.submitOtp()
                    
                }){
                    Text(Localizable.Send.localized())
                }
                .modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 16, fontColor: Color.white))
                .modifier(ButtonStyle(buttonHeight: 50, buttonColor: Color.init(hex: "00ADEF"), buttonRadius: 50))
                .padding(.all,30)
                Spacer()
            }.showNavigationBarWithBackStyle().onAppear(){resendOtp(isHideLoading: true)}
            
        }
    }
}

struct VeryfyEmailCodeView_Previews: PreviewProvider {
    static var previews: some View {
        VeryfyEmailCodeView()
    }
}
