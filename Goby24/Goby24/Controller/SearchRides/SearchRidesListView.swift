//
//  SearchRidesListView.swift
//  Goby24
//
//  Created by Jignesh Bhensadadiya on 8/5/21.
//

import SwiftUI

struct SearchRidesListView: View {
    @State var arrRides:[Model_findRides_Results]
    @State var navigationViewIsActive: Bool = false
    @State var selectedModel : Model_findRides_Results?
    
   
    var body: some View {
       
        NavigationLink(destination:selectedRideDetailsView(offeredRoute: selectedModel!), isActive: $navigationViewIsActive){ EmptyView() }.isDetailLink(false)

        VStack{
            ScrollView(.vertical, showsIndicators: false){
                ForEach(0..<self.arrRides.count, id: \.self) { row in
                    NavigationLink(destination: selectedRideDetailsView(offeredRoute:self.arrRides[row])) {
                        SearchRidesResultsItemView(offeredRoute: self.arrRides[row])
                    }.isDetailLink(false)
                }
            }.padding()
        }
    }
}

