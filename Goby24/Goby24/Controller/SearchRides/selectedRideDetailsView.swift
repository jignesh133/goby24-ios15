//
//  selectedRideDetailsView.swift
//  Goby24
//
//  Created by Jignesh Bhensadadiya on 7/16/21.
//

import SwiftUI
import SDWebImageSwiftUI

struct selectedRideDetailsView: View {

    @State var selection: Int? = nil

    var offeredRoute:Model_findRides_Results
    
    @ObservedObject private var findRide = SearchRouteData.shared

    var body: some View {
        ScrollView{
            NavigationLink(destination: RiderContactView(receiverId:getStringFromAny(offeredRoute.user?.id ?? 0)), tag: 10, selection: $selection) {
             EmptyView()
            }.isDetailLink(false)

            
//            NavigationLink(destination: RiderContactView(receiverId:getStringFromAny(offeredRoute.user?.id)), isActive: $isOpenMessage){EmptyView()}.isDetailLink(false)

            VStack{
                VStack(alignment:.leading,spacing:0){
                    HStack{
                       // Image("timerdiff")
                        
                        VStack(alignment:.leading,spacing:0){
                            
                            VStack(alignment:.leading){
                                // BOTTOM
                                HStack{
                                    VStack(alignment:.leading,spacing:0){
                                        Text(offeredRoute.pickupTime ?? "").modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 16, fontColor: Color.black))
                                        Text(offeredRoute.startLabel ?? "").modifier(CustomTextM(fontName: UIFont.Montserrat.semiBold.fontName, fontSize: 13, fontColor: Color.black)).multilineTextAlignment(.leading)
                                        Text(offeredRoute.startDetail?.secondaryText ?? "").modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 12, fontColor: Color.black))
                                        
                                    }
                                    VStack{
    //                                    Button(action: {
    //                                        print("")
    //                                    }){
    //                                        Text((Localizable.View_map.localized())).modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 15, fontColor: Color.white))
    //                                    }
    //                                    .modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 12, fontColor: Color.white))
    //                                    .modifier(ButtonStyle(buttonHeight: 30, buttonColor: Color.init(hex: "00ADEF"), buttonRadius: 17))
    //                                    .frame(maxWidth:104)
    //                                    .padding([.top],10)
                                        
                                    }
                                }
                                HStack{
                                   // Image("selectedperson")
                                    Text( getStringFromAny(offeredRoute.duration ?? "") + " from your arrival").modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 12, fontColor: Color.black))
                                }
                            }
                           
                            VStack(alignment:.leading){
                                // BOTTOM
                                HStack{
                                    VStack(alignment:.leading,spacing:0){
                                        Text(offeredRoute.dropTime ?? "").modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 16, fontColor: Color.black))
                                        Text(offeredRoute.endLabel ?? "").modifier(CustomTextM(fontName: UIFont.Montserrat.semiBold.fontName, fontSize: 13, fontColor: Color.black)).multilineTextAlignment(.leading)
                                        Text(offeredRoute.endDetail?.secondaryText ?? "").modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 12, fontColor: Color.black))
                                        
                                    }
                                    VStack{
    //                                    Button(action: {
    //                                        print("")
    //                                    }){
    //                                        Text(Localizable.View_map.localized()).modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 15, fontColor: Color.white))
    //                                    }
    //                                    .modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 12, fontColor: Color.white))
    //                                    .modifier(ButtonStyle(buttonHeight: 30, buttonColor: Color.init(hex: "00ADEF"), buttonRadius: 17))
    //                                    .frame(maxWidth:104)
    //                                    .padding([.top],10)
                                    }
                                }
                                HStack{
    //                                Image("selectedperson")
                                    Text(getStringFromAny(offeredRoute.distance ?? "") + " away from distance").modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 12, fontColor: Color.black))
                                }
                            }.padding(.top,30)
                        }
                    }
                    
                }.padding().frame(width:UIScreen.main.bounds.size.width - 70).background(RoundedRectangle(cornerRadius: 25).fill(Color.white).shadow(color: Color.black.opacity(0.12), radius: 3))
                
                VStack{
                    Spacer()
                    HStack{
                        Text(Localizable.Total_price_for.localized()).modifier(CustomTextM(fontName: UIFont.Montserrat.semiBold.fontName, fontSize: 13, fontColor: Color.init(hex: "F57F20")))

                        
                        Text(getStringFromAny(findRide.passenger)).modifier(CustomTextM(fontName: UIFont.Montserrat.semiBold.fontName, fontSize: 13, fontColor: Color.init(hex: "F57F20")))

                        
                        Text((Localizable.passangers.localized())).modifier(CustomTextM(fontName: UIFont.Montserrat.semiBold.fontName, fontSize: 13, fontColor: Color.init(hex: "F57F20")))

                    }
                    Spacer()
                    let total = Double(findRide.passenger) * getDoubleFromAny(offeredRoute.fare ?? "0")
                    let currency = offeredRoute.currency
                    HStack(){
                        Spacer()

                    Text(getStringFromAny(total)).modifier(CustomTextM(fontName: UIFont.Montserrat.semiBold.fontName, fontSize: 26, fontColor: Color.init(hex: "F57F20")))
                        Text(getStringFromAny(currency ?? "")).modifier(CustomTextM(fontName: UIFont.Montserrat.semiBold.fontName, fontSize: 26, fontColor: Color.init(hex: "F57F20")))

                        Spacer()

                    }
                    Spacer()
                }.padding().frame(width:UIScreen.main.bounds.size.width - 70,height: 100).background(RoundedRectangle(cornerRadius: 25).fill(Color.white).shadow(color: Color.black.opacity(0.12), radius: 3))
                
                VStack{
                    HStack{
                        
                        WebImage(url: URL.init(string: (IMAGEURL + (offeredRoute.user?.profilePic ??  "") )))
                            .placeholder(Image(systemName: "photo"))
                            .renderingMode(.original)
                            .resizable()
                            .aspectRatio(contentMode: .fill)
                            .frame(width: 60, height: 60)
                            .clipShape(Circle())
                        
    //                    if ((offeredRoute.user?.profilePic?.count ?? 0) > 0){
    //                        .overlay(Circle().stroke(Color.init(hex: "00AEEF"), lineWidth: 1))
    //                    }
                        
                        VStack(alignment:.leading){
                            Text(offeredRoute.user?.firstName ?? "").modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 13, fontColor: Color.black)).padding(.leading,7)
                            
                            HStack{
                                VStack{
                                    Spacer()
                                    StarsView(rating: Double(offeredRoute.user?.riderRating ?? "0") ?? 0).padding(.top,4)
                                    Spacer()
                                }.frame(width: 80, height: 30)
                                
                                Text(offeredRoute.user?.riderRating ?? "").modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 11, fontColor: Color.black))
                                
                                Spacer()
                                
                                NavigationLink(destination: DriverProfileView(offeredRoute: offeredRoute), tag: 0, selection: $selection) {
                                    // BUTTONS
                                    Button(action: {
                                        selection = 0
                                    }){
                                        Text(Localizable.More.localized()).modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 15, fontColor: Color.white))
                                    }
                                    .modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 12, fontColor: Color.white))
                                    .modifier(ButtonStyle(buttonHeight: 30, buttonColor: Color.init(hex: "00ADEF"), buttonRadius: 17))
                                    .frame(maxWidth:104)
                                }
                            }.padding(.horizontal)
                        }
                    }
                    Group{
                        Button(action: {
                            selection = 10
                        }){
                            HStack{
                                Image("askquection")
                                Text(Localizable.Ask_a_question.localized()).modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 15, fontColor: Color.black))
                            }
                            
                        }
                    }
                    
                    Divider()
                    HStack{
                        Image("nosmoke")
                        Text(getStringFromAny(offeredRoute.user?.travelPreferences?.smoking ?? " -- ")).modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 15, fontColor: Color.black))
                    }
                    HStack{
                        Image("nopet")
                        Text(getStringFromAny(offeredRoute.user?.travelPreferences?.pets ?? " -- ")).modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 15, fontColor: Color.black))
                    }
                    
                }.padding().frame(width:UIScreen.main.bounds.size.width - 70,height: 225).background(RoundedRectangle(cornerRadius: 25).fill(Color.white).shadow(color: Color.black.opacity(0.12), radius: 3))

                NavigationLink(destination: EpaymentOverView(offeredRoute: offeredRoute), tag: 1, selection: $selection) {
                    
                    //Button
                    Button(action: {
                        print("CLICKED")
                        if (getId() == getStringFromAny(offeredRoute.user?.id ?? "")){
                            ShowAlert(title: APPNAME, msg: "You can't book your own ride.", view: UIApplication.getTopViewController()!)
                        }else{
                            selection = 1
                        }
                    }){
                        Text(Localizable.Continue.localized())
                    }
                    .modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 16, fontColor: Color.white))
                    .modifier(ButtonStyle(buttonHeight: 50, buttonColor: Color.init(hex: "00ADEF"), buttonRadius: 50))
                    .padding(.horizontal,30)
                    .padding(.top,30)
                }.isDetailLink(false)
                Spacer()
            }

        }.showNavigationBarWithBackStyle()
    }
}


