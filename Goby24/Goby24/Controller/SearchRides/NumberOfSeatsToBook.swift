//
//  NumberOfSeatsToBook.swift
//  Goby24
//
//  Created by Jignesh Bhensadadiya on 7/11/21.
//

import SwiftUI

struct NumberOfSeatsToBook: View {
    
    //MARK: - PROPERTIES
    @State var strText: String = ""
    @State var maxCount: String = "7"

    @ObservedObject private var findRide = SearchRouteData.shared

    
    var body: some View {
        VStack{
            
            Text(Localizable.Number_of_seats_to_book.localized()).padding(.top, 25).modifier(CustomTextM(fontName: UIFont.Rubik.medium.fontName, fontSize: 26, fontColor: Color.init(hex: "2E2E2E"))).multilineTextAlignment(.center).padding(.horizontal,50)
            
            
            CustomSteper(count: getStringFromAny(self.findRide.passenger),maxCount:maxCount) { (count) in

                self.findRide.passenger = getIntegerFromAny(count)
                print(count)

            }.padding(.top,30)
            Spacer()
            
            
        }.showNavigationBarWithBackStyle()
    }
}

struct NumberOfSeatsToBook_Previews: PreviewProvider {
    static var previews: some View {
        NumberOfSeatsToBook()
    }
}
