//
//  CustomSteper.swift
//  Goby24
//
//  Created by Jignesh Bhensadadiya on 7/11/21.
//

import SwiftUI

struct CustomSteper: View {
    @State var count:String = "1"
    @State var maxCount:String?
    @State var minCount:String = "1"

    @State var customWidth:CGFloat = 210
    let callback: (String) -> ()
    
    var body: some View {
        VStack{
            HStack() {
                
                Button(action: {
                    let countInt = getIntegerFromAny(count) - 1
                    if (countInt >= Int(getDoubleFromAny(minCount))){
                        count = getStringFromAny(countInt)
                        callback(getStringFromAny(countInt))
                    }
                    
                }){
                    Text(" -").foregroundColor(.white).modifier(CustomTextM(fontName: UIFont.Poppins.regular.fontName, fontSize: 40, fontColor: Color.black))
                }.frame(width:$customWidth.wrappedValue / 3,height: 50)
                
                Text(count).modifier(CustomTextM(fontName: UIFont.Rubik.bold.fontName, fontSize: 29, fontColor: Color.black)).frame(minWidth: $customWidth.wrappedValue / 3,minHeight: 50).background(Color.white)
                
                Button(action: {
                    let countInt = getIntegerFromAny(count) + 1
                    if (getIntegerFromAny(maxCount ?? "") > 0 && getIntegerFromAny(countInt) > getIntegerFromAny(maxCount ?? "")) {return}
                    if (countInt > 0){
                        count = getStringFromAny(countInt)
                        callback(getStringFromAny(countInt))
                    }
                }){
                    Text("+ ").foregroundColor(.white).modifier(CustomTextM(fontName: UIFont.Poppins.regular.fontName, fontSize: 40, fontColor: Color.black))
                }.frame(width: $customWidth.wrappedValue / 3,height: 50)
            }
            .frame(width: $customWidth.wrappedValue, height: 50)
            .background(Color.init(hex: "F57F20"))
            .cornerRadius(50)
            .overlay(
                RoundedRectangle(cornerRadius: 50)
                    .stroke(Color.init(hex: "F57F20"), lineWidth: 1)
            )
        }
    }
}

