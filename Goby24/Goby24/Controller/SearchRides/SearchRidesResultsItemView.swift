//
//  SearchRidesResultsItemView.swift
//  Goby24
//
//  Created by Jignesh Bhensadadiya on 7/15/21.
//

import SwiftUI
import SDWebImageSwiftUI
import StarRating

struct SearchRidesResultsItemView: View {
    var offeredRoute:Model_findRides_Results
    @State var customConfig = StarRatingConfiguration(spacing: 8,numberOfStars: 5,stepType: .exact,minRating: 0,borderWidth: 1,borderColor: Color.black,emptyColor: Color.white,shadowRadius: 0,fillColors: [Color.black],starVertices: 5,starWeight: 0.45)

    var body: some View {
        
        VStack{
            HStack{
                VStack{
                    Spacer()
                    WebImage(url: URL.init(string: (IMAGEURL + (offeredRoute.user?.profilePic ??  "") )))
                        .placeholder(Image(systemName: "photo"))
                        .renderingMode(.original)
                        .resizable()
                        .aspectRatio(contentMode: .fill)
                        .frame(width: 60, height: 60)
                        .clipShape(Circle())
                    
                    VStack{
                        Text(offeredRoute.user?.firstName ?? "" ).modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 12, fontColor: Color.black))
                        Spacer()
                        HStack{
                            StarRating(initialRating: Double(offeredRoute.user?.riderRating ?? "") ?? 0,configuration: $customConfig).frame(width: 75, height: 10, alignment: .center)
                            
                            Text(offeredRoute.user?.riderRating ?? "").modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 12, fontColor: Color.black)).frame(height: 30, alignment: .center)
                        }
//                        Button(action: {
//                            print("")
//                        }){
//                            Text(Localizable.Contact.localized())
//                        }
//                        .modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 12, fontColor: Color.white))
//                        .modifier(ButtonStyle(buttonHeight: 18, buttonColor: Color.init(hex: "F57F20"), buttonRadius: 25))
//                        .frame(width:80)
                    }.frame(height:85)
                    
                    Spacer(minLength: 5)
                }.padding(.leading,10)
                
                VStack(alignment: .center, spacing: 0){
                    Text(offeredRoute.pickupTime ?? "").modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 16, fontColor: Color.black))
                    Image("timerdiff")
                    Text(offeredRoute.dropTime ?? "").modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 16, fontColor: Color.black))
                }.padding(.leading,5)
                // ROUTE AND PASSENGER
                
                VStack(alignment:.leading){
                    // WEATHER BUTTON
                    HStack{
                        Spacer()
                        Image("seats").padding(.top,5)
                        Text(((offeredRoute.availableSeats ?? "0") + " seats remaining")).modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 12, fontColor: Color.black)).padding(.horizontal,5).padding(.top,5)

                        Spacer(minLength: 8)
//                        Button(action: {
//                            print("")
//                        }){
//                            Text("Weather infos").modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 12, fontColor: Color.white))
//                            Image("weatherinfos")
//                        }
//                        .modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 12, fontColor: Color.white))
//                        .modifier(ButtonStyle(buttonHeight: 17, buttonColor: Color.init(hex: "00ADEF"), buttonRadius: 17))
//                        .frame(minWidth:125)
//                        .padding([.top],10)
                        
                    }
                    
                    Spacer()
                    VStack(alignment:.leading, spacing:0){
                        Text(offeredRoute.startLabel ?? "").modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 16, fontColor: Color.black))
//                        HStack{
//                            Image("unselectedperson")
//                            Image("unselectedperson")
//                            Image("selectedperson")
//                        }
                    }.padding(.top,5)
                    VStack(alignment:.leading, spacing:0){
                        Text(offeredRoute.endLabel ?? "").modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 16, fontColor: Color.black))
//                        HStack{
//                            Image("unselectedperson")
//                            Image("unselectedperson")
//                            Image("selectedperson")
//                        }
                    }.padding(.top,5)
                    HStack{
                        Spacer()
                        if (offeredRoute.paymentMethod?.lowercased() == "e-payment"){
                            Image("credit-card")//.resizable().frame(width: 30, height: 30, alignment: .center)//.padding([.top],10)
                        }else{
                            Image("cash")//.padding([.top],10)
                        }

                        Text(((offeredRoute.currency ?? "") + " " + (offeredRoute.fare ?? ""))).modifier(CustomTextM(fontName: UIFont.Rubik.bold.fontName, fontSize: 16, fontColor: Color.init(hex: "F57F20")))//.padding(.bottom,20)
                    }
                    Spacer()
                }.padding(.leading,5)
                Spacer()
            }
            .background(Image("Rectangle_rides"))
            //.padding(.bottom,10)
            Spacer(minLength: 15)
        }
            
    }
}

struct StarsViewNew: View {
    
    @Binding var rating:Int
    
    var label = ""
    var maximumRating = 5
    
    var offImage:Image?
    var onImage = Image(systemName:"start.fill")
    
    var offColor = Color.gray
    var onCOlor = Color.yellow
    
    var body: some View {
        HStack{
            if label.isEmpty == false{
                Text(label)
            }
            ForEach(1..<maximumRating + 1){ number in
                self.image(for: number).foregroundColor(number > self.rating ? self.offColor : self.onCOlor)
                
            }
        }
    }
    func image(for number : Int) -> Image{
        if number > rating {
            return offImage ?? onImage
        }else{
            return onImage
        }
    }
}
