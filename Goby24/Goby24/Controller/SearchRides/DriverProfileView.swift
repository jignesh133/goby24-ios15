//
//  DriverProfileView.swift
//  Goby24
//
//  Created by Jignesh Bhensadadiya on 7/16/21.
//

import SwiftUI
import SDWebImageSwiftUI

struct DriverProfileView: View {
//    let driverProfile:Model_findRides_User
    var offeredRoute:Model_findRides_Results

    func getAgeFromDOF(date: String) -> Int {
        let dateFormater = DateFormatter()
        dateFormater.dateFormat = "YYYY-MM-dd"
        let dateOfBirth = dateFormater.date(from: date)
        let calender = Calendar.current
        let dateComponent = calender.dateComponents([.year, .month, .day], from:
        dateOfBirth!, to: Date())
        return dateComponent.year!
    }
    func reportNumber(strReason:String){
        var param:[String:Any] = [String:Any]()
        param["reason"] = strReason
        param["rider"] = getStringFromAny(offeredRoute.user?.id ?? "")
        
        WebAccess.postDataWith(_Url: WebAccess.REPORT_NUMBER, _parameters: param) { (result) in
            appDelegate?.stopLoadingView()
            switch (result){
            case .Success(let _data):
                ShowAlert(title: APPNAME, msg: "Reported Sucessfully", view: UIApplication.getTopViewController()!)
                print(_data)
                break
            case .Error(let msg):
                ShowAlert(title: APPNAME, msg: msg, view: UIApplication.getTopViewController()!)
                break
            }
        }
    }
    var body: some View {
        VStack(){
            
            
            let data = userDefault.value(forKey: Enum_Login.userProfileData.rawValue) as? [String:Any] ?? [:]
            
            let urlString = IMAGEURL + getStringFromAny(offeredRoute.user?.profilePic ?? "")//(driverProfile.profilePic ?? "")
            
            WebImage(url: URL.init(string: urlString ))
                .placeholder(Image(systemName: "photo")) // Placeholder Image
                .renderingMode(.original)
                .resizable()
                .aspectRatio(contentMode: .fill)
                .frame(width: 60, height: 60)
                .clipShape(Circle())
                .overlay(Circle().stroke(Color.init(hex: "00AEEF"), lineWidth: 1)).padding(.vertical)

            Text(offeredRoute.user?.firstName ?? "").modifier(CustomTextM(fontName: UIFont.Rubik.medium.fontName, fontSize: 16, fontColor: Color.black)).padding(.horizontal)
           
            if ((offeredRoute.user?.dob ?? "").count > 0){
                
                let calendar = Calendar.current
                let birthday =  offeredRoute.user?.dob?.stringToDate(strCurrentFormat: "yyyy-MM-dd")
                let dateComponent = calendar.dateComponents([.year], from: birthday!, to: Date())
                let year = dateComponent.year
                Text( getStringFromAny(year ?? "") + " Y/o").modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 12, fontColor: Color.black)).padding(.horizontal)
            }

            ZStack{
                HStack{
                    Text(Localizable.Ratings.localized()).modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 16, fontColor: Color.black)).padding(.horizontal)
                    StarsView(rating: Double(offeredRoute.user?.riderRating ?? "0") ?? 0).padding(.top,10)
                    Text(offeredRoute.user?.riderRating ?? "").modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 11, fontColor: Color.black))
                    Spacer()
                    Image("next")
                    Spacer()

                }
            }.frame(width: 275, height: 35).cornerRadius(10)
            .background(RoundedRectangle(cornerRadius: 25).fill(Color.white).shadow(color: Color.black.opacity(0.12), radius: 3)).padding()
            
            VStack(alignment:.leading){
                HStack{
                    Image("nochat")
                    Text(offeredRoute.user?.travelPreferences?.chattiness ?? " -- ").modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 15, fontColor: Color.black))
                }
                HStack{
                    Image("nosmoke")
                    Text(offeredRoute.user?.travelPreferences?.smoking ?? " -- ").modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 15, fontColor: Color.black))
                }
                HStack{
                    Image("nopet")
                    Text(offeredRoute.user?.travelPreferences?.pets ?? " -- ").modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 15, fontColor: Color.black))
                }
            }
            Divider().padding()
            
            VStack(alignment: .center){
                HStack{
                    if (offeredRoute.user?.isMobileNoVerified ?? false) == true{
                        Image("checkedok")
                    }else{
                        Image("wrong")
                    }

                    Text(Localizable.Phone_Verified.localized()).modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 15, fontColor: Color.black))
                }
                
                let joiningDate = offeredRoute.user?.joiningDate?.stringToDate()
                let sinceDate = joiningDate?.getFormattedDate(formatter: "MMM yyyy")
                Text(getStringFromAny(offeredRoute.totalRides ?? "-") + (Localizable.Rides_published.localized()) + (Localizable.Member_since.localized()) + (sinceDate ?? "") ).modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 15, fontColor: Color.black)).multilineTextAlignment(.center)
            }.frame(width: 200, height: 100, alignment: .center).padding().background(RoundedRectangle(cornerRadius: 25).fill(Color.white).shadow(color: Color.black.opacity(0.12), radius: 3))

            Spacer()
            //Button
            Button(action: {
                print("")
                RPicker.selectOption(title: "Please Select Reason", cancelText: "Cancel", doneText: "Done", dataArray: ["Rash Driving","Bad Behiviour","Fraud","Very late"], selectedIndex: nil) { (val, index) in
                    // REPORT NUMBER
                    self.reportNumber(strReason: val)
                }
                
//                HStack{
//                    Spacer()
//                    Button(action: {
//
//                    }){
//                        Image("paydots")
//                    }.frame(width: 40).padding(.bottom,10)
//
//                }
                
            }){
                    Text("Report this member")
            }
            
            .modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 16, fontColor: Color.init(hex: "00AEEF")))
            .modifier(ButtonStyle(buttonHeight: 50, buttonColor: Color.clear, buttonRadius: 50))
            .overlay(RoundedRectangle(cornerRadius: 50).stroke(Color.init(hex: "00AEEF"), lineWidth: 1))
            .padding()
            Spacer()
        }.showNavigationBarWithBackStyle()
    }
}
