//
//  findRides.swift
//  Goby24
//
//  Created by Jignesh Bhensadadiya on 7/11/21.
//

import SwiftUI

struct FindRides: View {
    
    //MARK: - PROPERTIES
    @State var leavingFrom: String = ""
    @State var goingTo: String = ""
    @State var isExpanded: Bool = true
    @State var selection: Int? = nil
    
    @State var pickupLocation:Model_searchLocation? = nil
    @State var pickOffLocation:Model_searchLocation? = nil
    
    @State var isOpenPickUpLocation:Bool = false
    @State var isOpenPickOffLocation:Bool = false
    @State var advanceOptionClicked: Bool = false
    
    
    
    @ObservedObject private var findRide = SearchRouteData.shared
    @EnvironmentObject var settings: UserSettings
    
    var body: some View {
        ScrollView{
            VStack{
                
                NavigationLink(destination:SearchLocation(strText: self.pickupLocation?.structuredFormatting?.mainText ?? "",isNeedToPopView:true,  onLocationSelect: { (location,isClear) in
                    if (isClear == false){
                        pickupLocation = location
                        leavingFrom = location?.descriptionField ?? ""
                    }else{
                        pickupLocation = nil
                        leavingFrom = ""
                    }
                }),isActive: $isOpenPickUpLocation) {EmptyView()}

                NavigationLink(destination:SearchLocation(strText: self.pickOffLocation?.structuredFormatting?.mainText ?? "",isNeedToPopView:true,  onLocationSelect: { (location,isClear) in
                    if (isClear == false){
                        pickOffLocation = location
                        goingTo = location?.descriptionField ?? ""
                    }else{
                        pickupLocation = nil
                        leavingFrom = ""
                    }
                }),isActive: $isOpenPickOffLocation) {EmptyView()}
                
                Text(Localizable.Find_a_ride.localized()).padding(.top, 25).modifier(CustomTextM(fontName: UIFont.Rubik.medium.fontName, fontSize: 26, fontColor: Color.init(hex: "2E2E2E")))
                
                ZStack{
                    VStack{
                        //  CustomTextField(placeHolderText: "Leaving From ",
                        //                text: self.$leavingFrom).padding(.horizontal,25).padding(.top,5)
                        TextField((Localizable.Leaving_from.localized()), text: $leavingFrom, onEditingChanged: { (change) in
                            if change == true{
                                UIApplication.shared.endEditing()
                                isOpenPickUpLocation = true
                            }
                        })
                        .padding(EdgeInsets(top: 0, leading: 15, bottom: 0, trailing: 15))
                        .frame(width: .infinity, height: 45, alignment: .center)
                        .background(Color(red: 0.95, green: 0.95, blue: 0.95))
                        .cornerRadius(22.5)
                        .modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 16, fontColor: Color.black))
                        .padding(.horizontal,25).padding(.top,5)
                        
                        TextField((Localizable.Going_to.localized()), text: $goingTo, onEditingChanged: { (change) in
                            if change == true{
                                UIApplication.shared.endEditing()
                                
                                isOpenPickOffLocation = true
                            }
                        })
                        .padding(EdgeInsets(top: 0, leading: 15, bottom: 0, trailing: 15))
                        .frame(width: .infinity, height: 45, alignment: .center)
                        .background(Color(red: 0.95, green: 0.95, blue: 0.95))
                        .cornerRadius(22.5)
                        .modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 16, fontColor: Color.black))
                        .padding(.horizontal,25).padding(.top,5)
                        .contentShape(Rectangle())
                        
                        //                    Text("Going to")
                        //                        .modifier(CustomTextM(fontName: UIFont.Rubik.medium.fontName, fontSize: 26, fontColor: Color.init(hex: "2E2E2E")))
                        
                        
                        //  CustomTextField(placeHolderText: "Going to ",
                        //   text: self.$goingTo).padding(.horizontal,25).padding(.top,5)
                    }
                    HStack{
                        Spacer()
                        Button(action: {
                            let swipe = pickupLocation
                            pickupLocation = pickOffLocation
                            pickOffLocation = swipe
                            
                            let swipename = leavingFrom
                            leavingFrom = goingTo
                            goingTo = swipename
                            
                        }){
                            Image("swap")
                        }.frame(width: 50, height: 50)
                        Spacer().frame(width: 55)
                    }
                }
                
                
                VStack{
                    NavigationLink(destination: PickDate(), tag: 0, selection: $selection) {
                        
                        Button(action: {
                            selection = 0
                            
                        }){
                            Text(((self.findRide.journeyDate?.getFormattedDate(formatter: "dd.MM.yyyy")) ?? Localizable.Journey_Date.localized() )).frame(minWidth: 0,  maxWidth: .infinity)
                            
                        }
                        
                        .modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 16, fontColor: Color.black))
                        .modifier(ButtonStyle(buttonHeight: 50, buttonColor: Color.white, buttonRadius: 50))
                        .overlay(RoundedRectangle(cornerRadius: 50).stroke(Color.init(red: 0, green: 0.682, blue: 0.937), lineWidth: 1))
                        .buttonStyle(PlainButtonStyle())
                        .contentShape(Rectangle())
                        
                        
                    }
                    NavigationLink(destination: NumberOfSeatsToBook(), tag: 1, selection: $selection) {
                        Button(action: {
                            selection = 1
                            
                        }){
                            
                            Text(getStringFromAny(findRide.passenger) + " " + (Localizable.passangers.localized()))
                                .frame(minWidth: 0,  maxWidth: .infinity)
                        }
                        
                        
                        .modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 16, fontColor: Color.black))
                        .modifier(ButtonStyle(buttonHeight: 50, buttonColor: Color.white, buttonRadius: 50))
                        .overlay(RoundedRectangle(cornerRadius: 50).stroke(Color.init(red: 0, green: 0.682, blue: 0.937), lineWidth: 1))
                        .padding(.top,20)
                        .buttonStyle(PlainButtonStyle())
                        
                    }
                    
                    if (advanceOptionClicked == false){
                        
                        // ADVANCE Options
                        Button(action: {
                            
                            print("Advanced Options Clicked")
                            advanceOptionClicked.toggle()
                        }){
                            ZStack{
                                Text(Localizable.Advanced_Options.localized())
                                HStack() {
                                    Spacer()
                                    Image("down").padding(.trailing,20)
                                }
                            }
                        }
                        .modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 16, fontColor: Color.black))
                        .modifier(ButtonStyle(buttonHeight: 50, buttonColor: Color.white, buttonRadius: 50))
                        .overlay(RoundedRectangle(cornerRadius: 50).stroke(Color.init(red: 0, green: 0.682, blue: 0.937), lineWidth: 1))
                        .padding(.top,20)
                    }else{
                        VStack{
                            // ADVANCE Options
                            Button(action: {
                                print("Advanced Options Clicked")
                                advanceOptionClicked.toggle()
                            }){
                                ZStack{
                                    Text(Localizable.Advanced_Options.localized())
                                    HStack() {
                                        Spacer()
                                        Image("down").padding(.trailing,20)
                                    }
                                }
                            }
                            .modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 16, fontColor: Color.black))
                            .modifier(ButtonStyle(buttonHeight: 50, buttonColor: Color.white, buttonRadius: 50))
                            HStack{
                                Spacer()
                                // ADVANCE Options
                                Button(action: {
                                    print(" Clicked")
                                    findRide.smokAllowed.toggle()
                                }){
                                    Image(findRide.smokAllowed ? "smoking-selected" : "smoking-unselected")
                                }
                                Spacer()
                                // ADVANCE Options
                                Button(action: {
                                    print("Advanced Options Clicked")
                                    findRide.petAllowed.toggle()
                                }){
                                    Image(findRide.petAllowed ? "pet-selected" : "pet-unselected")
                                    
                                }
                                Spacer()
                            }.padding(.bottom)
                            
                            
                        }.overlay(RoundedRectangle(cornerRadius: 25).stroke(Color.init(red: 0, green: 0.682, blue: 0.937), lineWidth: 1))
                        .padding(.top,20)
                        
                    }
                    
                    
                    
                }.padding(.horizontal,25).padding(.top,20)
                //Spacer()
                VStack{
                    NavigationLink(destination: SearchRidesResultsView(), tag: 2, selection: $selection) {
                        
                        Button(action: {
                            // selection = 2
                            
                            if (self.pickupLocation == nil){
                                ShowAlert(title: APPNAME, msg: "Please enter source address", view: UIApplication.getTopViewController()!)
                            }else if (self.pickOffLocation == nil){
                                ShowAlert(title: APPNAME, msg: "Please enter destination address", view: UIApplication.getTopViewController()!)
                            }else if (self.findRide.journeyDate == nil) {
                                ShowAlert(title: APPNAME, msg: "Please select journey date", view: UIApplication.getTopViewController()!)
                            }else{
                                self.findRide.sourceLocation = self.pickupLocation
                                self.findRide.destinationLocation = self.pickOffLocation
                                selection = 2
                                // ShowAlert(title: APPNAME, msg: "Coming Soon", view: UIApplication.getTopViewController()!)
                            }
                        }){
                            Text(Localizable.Search.localized()).frame(minWidth: 0,  maxWidth: .infinity)
                        }
                        .modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 16, fontColor: Color.white))
                        .modifier(ButtonStyle(buttonHeight: 50, buttonColor: Color.init(hex: "00ADEF"), buttonRadius: 50))
                        .padding()
                        .buttonStyle(PlainButtonStyle())
                        
                    }.isDetailLink(false).padding(.top,20)
                    Spacer()
                    HStack{
                        VStack{
                            Divider()
                        }
                        Text(" OR ").modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 16, fontColor: Color.black))
                        VStack{
                            Divider()
                        }
                        
                    }.padding(.horizontal,20)
                    Spacer()
                    NavigationLink(destination: RequestRideView(), tag: 3, selection: $selection) {
                        
                        Button(action: {
                            selection = 3
                            
                        }){
                            Text("Request Ride").frame(minWidth: 0,  maxWidth: .infinity)
                        }
                        .modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 16, fontColor: Color.white))
                        .modifier(ButtonStyle(buttonHeight: 50, buttonColor: Color.init(hex: "00ADEF"), buttonRadius: 50))
                        .padding()
                        .buttonStyle(PlainButtonStyle())
                        
                    }.isDetailLink(false)
                    
                }
                
            }
        }
        .showNavigationBarStyle()
        .onReceive(self.settings.$isMoveToSearchViewHome) { (isMoveToHome) in
            if isMoveToHome == true{
                pickupLocation = nil
                pickOffLocation = nil
                leavingFrom = ""
                goingTo = ""
                selection = -1
                SearchRouteData.destroy()
                self.settings.isMoveToSearchViewHome = false
            }
        }
    }
}

struct FindRides_Previews: PreviewProvider {
    static var previews: some View {
        FindRides()
    }
}
