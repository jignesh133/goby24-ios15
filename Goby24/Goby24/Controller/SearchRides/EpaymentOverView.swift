//
//  EpaymentOverView.swift
//  Goby24
//
//  Created by Jignesh Bhensadadiya on 7/16/21.
//

import SwiftUI

struct EpaymentOverView: View {
    @State var paymentClicked: Bool = true
    @ObservedObject private var findRide = SearchRouteData.shared
    var offeredRoute:Model_findRides_Results
    
    
    var body: some View {
        VStack{
            HStack{
                //Button
                Button(action: {
                    paymentClicked = true
                }){
                    if(paymentClicked == true){
                        VStack{
                            Text(Localizable.Payments.localized()).modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 13, fontColor: Color.init(hex: "00AEEF")))
                            Divider().background(Color.init(hex: "00AEEF")).frame(height: 1.5)
                        }
                    }else{
                        VStack{
                            Text(Localizable.Payments.localized()).modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 13, fontColor: Color.init(hex: "2E2E2E")))
                            Divider().background(Color.init(hex: "2E2E2E")).frame(height: 1.5)
                        }
                    }
                }.frame(minWidth: 0, maxWidth: .infinity)
                
                
                //ACCOUNT Button 2E2E2E
                Button(action: {
                    paymentClicked = false
                }){
                    if(paymentClicked == false){
                        VStack{
                            Text(Localizable.Profile.localized()).modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 13, fontColor: Color.init(hex: "00AEEF")))
                            Divider().background(Color.init(hex: "00AEEF")).frame( height: 1.5)
                        }
                    }else{
                        VStack{
                            Text(Localizable.Profile.localized()).modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 13, fontColor: Color.init(hex: "2E2E2E")))
                            Divider().background(Color.init(hex: "2E2E2E")).frame( height: 1.5)
                        }
                    }
                }.frame(minWidth: 0, maxWidth: .infinity)
            }.padding()
            
            
            if(paymentClicked == false){
                DriverProfileOverView(offeredRoute: self.offeredRoute)
            }else{
                EpaymentView(offeredRoute: self.offeredRoute)
            }
            Spacer()
        }.showNavigationBarWithBackStyle()
        
    }
}

//struct EpaymentOverView_Previews: PreviewProvider {
//    static var previews: some View {
//        EpaymentOverView()
//    }
//}
