//
//  DriverProfileOverView.swift
//  Goby24
//
//  Created by Jignesh Bhensadadiya on 7/16/21.
//

import SwiftUI
import SDWebImageSwiftUI

struct DriverProfileOverView: View {
    @ObservedObject private var findRide = SearchRouteData.shared
    var offeredRoute:Model_findRides_Results

    var body: some View {
        VStack{
            VStack(alignment: .leading,spacing:0){
                
                Text(Localizable.Profile_Overview.localized()).modifier(CustomTextM(fontName: UIFont.Montserrat.semiBold.fontName, fontSize: 16, fontColor: Color.white)).frame(width: UIScreen.main.bounds.size.width - 40, height: 40, alignment: .center).background(Color.init(hex: "00AEEF")).cornerRadius(radius: 6,corners: [.topLeft, .topRight])
                
                
                HStack{
                    
                    
                    let urlString = IMAGEURL + getStringFromAny(offeredRoute.user?.profilePic ?? "")//(driverProfile.profilePic ?? "")
                    
                    WebImage(url: URL.init(string: urlString ))
                        .placeholder(Image(systemName: "photo")) // Placeholder Image
                        .renderingMode(.original)
                        .resizable()
                        .aspectRatio(contentMode: .fill)
                        .frame(width: 60, height: 60)
                        .clipShape(Circle())
                        .overlay(Circle().stroke(Color.init(hex: "00AEEF"), lineWidth: 1)).padding(.vertical)

                    
//                    Image("userimage").clipShape(Circle()).padding(.all,5).overlay(Circle().stroke(Color.init(hex: "00AEEF"), lineWidth: 1))
                    
                    VStack(alignment:.leading){
                        Text(offeredRoute.user?.firstName ?? "").modifier(CustomTextM(fontName: UIFont.Rubik.bold.fontName, fontSize: 16, fontColor: Color.black)).padding(.leading,10)
                        if ((offeredRoute.user?.dob ?? "").count > 0){
                            
                            let calendar = Calendar.current
                            let birthday =  offeredRoute.user?.dob?.stringToDate(strCurrentFormat: "yyyy-MM-dd")
                            let dateComponent = calendar.dateComponents([.year], from: birthday!, to: Date())
                            let year = dateComponent.year
                            Text(getStringFromAny(year ?? "-") + " Years Old").modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 14, fontColor: Color.black)).padding(.leading,10)

                        }

                    }
                }.padding()
                HStack{
                    Spacer()
                    Text(Localizable.Member_Verification.localized()).modifier(CustomTextM(fontName: UIFont.Montserrat.semiBold.fontName, fontSize: 16, fontColor: Color.black))
                    Spacer()
                }
                
                VStack(){
                    HStack{
                        if ((offeredRoute.user?.isMobileNoVerified ?? false)  == true){
                            Image("right")
                        }else{
                            Image("wrong")
                        }

                        Text(Localizable.Verify_your_phone_number.localized()).modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 15, fontColor: Color.black))
                        Spacer()
                    }
                    HStack{
                        if ((offeredRoute.user?.isEmailVerified ?? false)  == true){
                                Image("right")
                        }else{
                            Image("wrong")

                        }
                        Text(Localizable.Verify_your_Email.localized()).modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 15, fontColor: Color.black))
                        Spacer()
                    }
                }.padding(.horizontal,50).padding(.top)
                Spacer()
            }.frame(width:UIScreen.main.bounds.size.width - 40 ,height: 250).background(RoundedRectangle(cornerRadius: 25).fill(Color.white).shadow(color: Color.black.opacity(0.12), radius: 3))
            
            
            VStack(alignment: .leading,spacing:0){
                
                Text(Localizable.Ride_Overview.localized()).modifier(CustomTextM(fontName: UIFont.Montserrat.semiBold.fontName, fontSize: 16, fontColor: Color.white)).frame(width: UIScreen.main.bounds.size.width - 40, height: 40, alignment: .center).background(Color.init(hex: "00AEEF")).cornerRadius(radius: 6,corners: [.topLeft, .topRight])
                
                VStack(alignment: .center){
                    HStack{
                        Image("clockblack")
                        Text(findRide.journeyDate?.getFormattedDate(formatter: "EEEE dd MMM") ?? "").modifier(CustomTextM(fontName: UIFont.Poppins.regular.fontName, fontSize: 16, fontColor: Color.init(hex: "2E2E2E")))
                        Spacer()
                    }
                    HStack{
                        Image("distance")
                        Text( (offeredRoute.distance ?? "") + " , " + (offeredRoute.duration ?? "") ).modifier(CustomTextM(fontName: UIFont.Poppins.regular.fontName, fontSize: 16, fontColor: Color.init(hex: "2E2E2E")))
                        Spacer()
                    }
                    HStack{
                        Image("car")
                        let series = offeredRoute.vehicleInfo?.model ?? " -- "
                        Text(series ).modifier(CustomTextM(fontName: UIFont.Poppins.regular.fontName, fontSize: 16, fontColor: Color.init(hex: "2E2E2E")))
                        Spacer()
                    }
                }.padding(.horizontal,50).padding(.top)
                HStack{
                    HStack{Image("distancelink")}
                    VStack(spacing:10){
                        HStack{
                            Text(offeredRoute.pickupTime ?? "").modifier(CustomTextM(fontName: UIFont.Poppins.regular.fontName, fontSize: 16, fontColor: Color.init(hex: "2E2E2E")))
                            Text(offeredRoute.startDetail?.mainText ?? "").modifier(CustomTextM(fontName: UIFont.Montserrat.semiBold.fontName, fontSize: 16, fontColor: Color.init(hex: "2E2E2E")))
                        }
                        HStack{
                            Text(offeredRoute.dropTime ?? "").modifier(CustomTextM(fontName: UIFont.Poppins.regular.fontName, fontSize: 16, fontColor: Color.init(hex: "2E2E2E")))
                            Text(offeredRoute.endDetail?.mainText ?? "").modifier(CustomTextM(fontName: UIFont.Montserrat.semiBold.fontName, fontSize: 16, fontColor: Color.init(hex: "2E2E2E")))
                        }
                    }
                }.padding(.horizontal,50).padding(.top,10)
                Spacer()
            }.frame(width:UIScreen.main.bounds.size.width - 40 ,height: 250).background(RoundedRectangle(cornerRadius: 25).fill(Color.white).shadow(color: Color.black.opacity(0.12), radius: 3)).padding(.top,20)
            Spacer()
        }
    }
}

//struct DriverProfileOverView_Previews: PreviewProvider {
//    static var previews: some View {
//        //DriverProfileOverView()
//    }
//}
