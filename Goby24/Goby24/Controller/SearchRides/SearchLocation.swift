//
//  findRides.swift
//  Goby24
//
//  Created by Jignesh Bhensadadiya on 7/11/21.
//

import SwiftUI

struct SearchLocation: View {
    
    //MARK: - PROPERTIES
    @State var strText: String = ""
    @State var arrLocation:[Model_searchLocation] = [Model_searchLocation]()
    @State var isNeedToPopView:Bool = true
    
    let onLocationSelect: (Model_searchLocation?,Bool) -> Void
    
    @Environment(\.presentationMode) var presentationMode
    
    private func textFieldChanged(_ text: String) {
        if (text.removeWhiteSpace().count == 0){
            onLocationSelect(nil,false)
        }
        print(text)
        getDataFromGoogle(str: text)
    }
    
    func getDataFromGoogle(str:String){
        guard str.count > 2 else {
            return
        }
        
        var param:[String:Any] = [String:Any]()
        param["query"] = strText
        let urlComponents = NSURLComponents(string: WebAccess.SEARCH_LOCATION)!
        
        if !param.isEmpty {
            urlComponents.queryItems = [URLQueryItem]()
            for (key, value) in param {
                let queryItem = URLQueryItem(name: key, value: "\(value)")
                urlComponents.queryItems!.append(queryItem)
            }
        }
        let url:String =  BASEURL + (urlComponents.url?.absoluteString ?? "" )
        guard let urlString = url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) else { return  }
        
        WebAccess.getSearchLocationDataWith(
            _Url: urlString, _parameters: [:], isHideLoader: true) { (response) in
            switch (response){
            case .Success(let _data):
                //print(_data)
                do{
                    let result:[String:Any] = UtilsObjs.cleanJson(to: _data ) as? [String : Any] ?? [String:Any]()
                    let data1 =  try JSONSerialization.data(withJSONObject: result["result"] ?? [] , options: JSONSerialization.WritingOptions.prettyPrinted)
                    let convertedString = String(data: data1, encoding: String.Encoding.utf8)
                    let jsonData = Data((convertedString?.utf8)!)
                    self.arrLocation = try JSONDecoder().decode([Model_searchLocation].self, from: jsonData)
                    
                    NSLog("LOG")
                }catch(let error){
                    print(error)
                }
                break
            case .Error(let msg):
                ShowAlert(title: APPNAME, msg: msg, view: UIApplication.getTopViewController()!)
                break
            }
        }
        
    }
    var body: some View {
        
        
        VStack{
            
            
            let binding = Binding<String>(
                get: { self.strText },
                set: { self.strText = $0; self.textFieldChanged($0) }
            )
            
            Text("Search Location").padding(.top, 25).modifier(CustomTextM(fontName: UIFont.Rubik.medium.fontName, fontSize: 26, fontColor: Color.init(hex: "2E2E2E")))
            
            ZStack(alignment: .trailing){
                
                // TextField("Search Location", text: binding).textFieldStyle(MyTextFieldStyle())
                TextField((Localizable.Search.localized()), text: binding).textFieldStyle(MyTextFieldStyle()).contentShape(Rectangle())
                
                
                
                Button(action: {
                    //
                    binding.wrappedValue = ""
                    self.arrLocation.removeAll()
                    onLocationSelect(nil,true)
                    
                }) {
                    Image("smallclose")
                }.padding(.trailing,40)//.padding(.top,10)
            }.padding()
            
            List{
                ForEach(0..<self.$arrLocation.wrappedValue.count, id: \.self) { index in
                    Button(action: {
                        // binding.wrappedValue = (arrLocation[index].descriptionField ?? "")
                        onLocationSelect(arrLocation[index],false)
                        if (isNeedToPopView){
                            self.presentationMode.wrappedValue.dismiss()
                        }
                        
                    }) {
                        VStack(alignment:.leading){
                            Text(self.arrLocation[index].structuredFormatting?.mainText ?? "").modifier(CustomTextM(fontName: UIFont.Poppins.regular.fontName, fontSize: 14, fontColor: Color.init(hex: "2E2E2E"))).multilineTextAlignment(.leading)
                            
                            Text(self.$arrLocation.wrappedValue[index].descriptionField ?? "").modifier(CustomTextM(fontName: UIFont.Rubik.medium.fontName, fontSize: 14, fontColor: Color.init(hex: "707070"))).multilineTextAlignment(.leading)
                            //Divider().background(Color.init(hex: "707070"))
                        }
                    }
                }
            }.padding()
            
            
            Spacer()
                .ignoresSafeArea(.keyboard, edges: .bottom)
            
        }                .ignoresSafeArea(.keyboard, edges: .bottom)
        .onAppear(){
            getDataFromGoogle(str: self.strText)
        }.showNavigationBarWithBackStyle()
        
    }
}

