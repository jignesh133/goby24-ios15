//
//  SortSearchedRideView.swift
//  Goby24
//
//  Created by Jignesh Bhensadadiya on 7/16/21.
//

import SwiftUI

struct SortSearchedRideView: View {
    @State var pricesort: String = ""
    @State var ratingsort: String = ""
    @State var timesort: String = ""
    @Environment(\.presentationMode) var presentationMode
    @ObservedObject private var sortListData = searchRideSort.shared

    var body: some View {
        VStack{
            HStack{
                Button(action: {
                    self.presentationMode.wrappedValue.dismiss()

                }){
                    Image("close")
                }.padding(.horizontal)
                
                Spacer()
                Button(action: {
                    pricesort = ""
                    ratingsort = ""
                    sortListData.isSortNeedToAdd = false
                    searchRideSort.destroy()
                }){
                    Text("Clear All").modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 16, fontColor: Color.init(hex: "2E2E2E")))
                }.padding(.horizontal)
            }.padding(.top)
            
            Text("Sort").modifier(CustomTextM(fontName: UIFont.Rubik.medium.fontName, fontSize: 26, fontColor: Color.init(hex: "2E2E2E"))).padding(.top,20)
            VStack{
                HStack{
                    Text((Localizable.Sort_by.localized() + (Localizable.Price.localized()))).modifier(CustomTextM(fontName: UIFont.Montserrat.bold.fontName, fontSize: 16, fontColor: Color.init(hex: "2E2E2E")))
                    Spacer()
                }
                RadioButtonGroup(items: ["High to Low","Low to Hight"], selectedId: pricesort, isTextFirst: false) { (selecteditem) in
                
                }.padding(.vertical)
            }.padding().padding(.horizontal)
            Divider()
            VStack{
                HStack{
                    Text((Localizable.Sort_by.localized()) + (Localizable.Ratings.localized())).modifier(CustomTextM(fontName: UIFont.Montserrat.bold.fontName, fontSize: 16, fontColor: Color.init(hex: "2E2E2E")))
                    Spacer()
                }
                RadioButtonGroup(items: ["High to low","Low to Hight"], selectedId: ratingsort, isTextFirst: false) { (selecteditem) in
                   
                }.padding(.vertical)
            }.padding().padding(.horizontal)
Divider()
            VStack{
                HStack{
                    Text((Localizable.Sort_by.localized())).modifier(CustomTextM(fontName: UIFont.Montserrat.bold.fontName, fontSize: 16, fontColor: Color.init(hex: "2E2E2E")))
                    Spacer()
                }
                RadioButtonGroup(items: ["Sometimes (time to time","Next (After a few days)","Before (Earlier)"], selectedId: timesort, isTextFirst: false) { (selecteditem) in
                    timesort = selecteditem
                }.padding(.vertical)
            }.padding().padding(.horizontal)
            
            Spacer()
            Button(action: {
                
                if (pricesort == "High to Low"){
                    sortListData.sort_by_price = "asc"
                }else{
                    sortListData.sort_by_price = "desc"
                }
                if (ratingsort == "High to Low"){
                    sortListData.sort_by_rating = "asc"
                }else{
                    sortListData.sort_by_rating = "desc"
                }
                sortListData.isSortNeedToAdd = true
                // POST NOTIFICATION
                NotificationCenter.default.post(name: .reloadFindRides, object: nil, userInfo: nil)
                self.presentationMode.wrappedValue.dismiss()
            }){
                Text("sort")
            }
            .modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 16, fontColor: Color.white))
            .modifier(ButtonStyle(buttonHeight: 50, buttonColor: Color.init(hex: "00ADEF"), buttonRadius: 50))
            .frame(width:200)
            .padding(.top,30)
            Spacer()

        }
    }
}

struct SortSearchedRideView_Previews: PreviewProvider {
    static var previews: some View {
        SortSearchedRideView()
    }
}
