//
//  RadioButtonGroupView.swift
//  Goby24
//
//  Created by Jignesh Bhensadadiya on 7/15/21.
//

import SwiftUI

struct ColorInvert: ViewModifier {

    @Environment(\.colorScheme) var colorScheme

    func body(content: Content) -> some View {
        Group {
            if colorScheme == .dark {
                content.colorInvert()
            } else {
                content
            }
        }
    }
}

struct RadioButton: View {

    @Environment(\.colorScheme) var colorScheme

    let id: String
    let callback: (String)->()
    let selectedID : String
    let size: CGFloat
    let color: Color
    let textSize: CGFloat
    let buttonColor:Color
    let isTextFirst:Bool
    
    init(
        _ id: String,
        callback: @escaping (String)->(),
        _ selectedID: String,
        size: CGFloat = 20,
        color: Color = Color.primary,
        textSize: CGFloat = 14,
        buttonColor:Color = Color.black,
        isTextFirst:Bool = false
        ) {
        self.id = id
        self.size = size
        self.color = color
        self.textSize = textSize
        self.selectedID = selectedID
        self.buttonColor = buttonColor
        self.callback = callback
        self.isTextFirst = isTextFirst
    }

    var body: some View {
        Button(action:{
            self.callback(self.id)
        }) {
            HStack(alignment: .center, spacing: 10) {
                if (isTextFirst == true){
                    Text(id).modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 16, fontColor: self.color)).frame(minWidth:.infinity)
                    Spacer()
                    Image(systemName: self.selectedID == self.id ? "largecircle.fill.circle" : "circle")
                        .foregroundColor(Color.init(hex: "00AEEF"))
                        .aspectRatio(contentMode: .fill)
                        .frame(width: 30, height: 30)
                        .modifier(ColorInvert())
                        

                 
                }else{
                    //print(self.selectedID)
                    Image(systemName: self.selectedID == self.id ? "largecircle.fill.circle" : "circle")
                        .foregroundColor(Color.init(hex: "00AEEF"))
                        .aspectRatio(contentMode: .fill)
                        .frame(width: 30, height: 30)
                        .modifier(ColorInvert())
                        
                    Text(id).modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 16, fontColor: self.color))

                    Spacer()
                }
               
            }.foregroundColor(self.buttonColor)
        }
        .foregroundColor(self.color)
    }
}

struct RadioButtonGroup: View {
    
    let items : [String]

  var selectedId: String = ""

     var isTextFirst: Bool

    let callback: (String) -> ()


    init(items: [String],selectedId:String,isTextFirst:Bool,callback:@escaping (String)->()){
        self.items = items
        self.selectedId = selectedId
        self.isTextFirst = isTextFirst
        self.callback = callback
    }
    
    var body: some View {
        VStack(){
//            print(self.selectedId)
            ForEach(0..<items.count) { index in
                RadioButton(self.items[index], callback: self.radioGroupCallback, self.selectedId,isTextFirst: self.isTextFirst)
            }
        }
    }

    func radioGroupCallback(id: String) {
//        selectedId = id
        callback(id)
    }
}

