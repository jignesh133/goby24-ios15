//
//  EpaymentView.swift
//  Goby24
//
//  Created by Jignesh Bhensadadiya on 7/16/21.
//

import SwiftUI

struct EpaymentView: View {
    @ObservedObject private var findRide = SearchRouteData.shared
    var offeredRoute:Model_findRides_Results
    @State var isPayOnline:Bool = false
    @State var cardUrl:String = ""
    @State var isRatingView:Bool = false
    @State var isWebview:Bool = false
    @State var paymentUrl:String = ""
    @State var isTermAccepted:Bool = false
    @State var bookingId:String = ""
    @EnvironmentObject var settings: UserSettings

//    @EnvironmentObject var settings: UserSettings
    func getServiceCharge() -> Double{
        print(offeredRoute.distance ?? "")
        let km = (offeredRoute.distance?.split(separator:" ").first) ?? "0"
        
        if ((getDoubleFromAny(km) ) <= 100){
           return 2
        }else if ((getDoubleFromAny(km) ) <= 400){
            return 4
         }else{
            return 5
        }
    }
    func getTotal() -> Double{
        let total:Double = Double(findRide.passenger) * getDoubleFromAny(offeredRoute.fare ?? "0")
        return total
    }
    func getTotalWithCharge() -> Double{
        var total:Double = Double(findRide.passenger) * getDoubleFromAny(offeredRoute.fare ?? "0")
        total = total + getServiceCharge()
        return total
    }
    func bookRide(){
        var param:[String:Any] = [String:Any]()
        param["id"] = getStringFromAny(offeredRoute.id ?? "") ///getId()
        param["seats"] = getStringFromAny(findRide.passenger)
        
        WebAccess.postDataWith(_Url: WebAccess.BOOK_RIDE, _parameters: param) { (result) in
            switch (result){
            case .Success(let _data):
                print(_data)
                let result = _data["result"] as? [String:Any] ?? [:]
                if (offeredRoute.paymentMethod?.lowercased() == "cash"){
                    self.bookingId = getStringFromAny(result["id"] ?? "")
                    self.bookRideCashpayment(booking: getStringFromAny(result["id"] ?? ""))
                }else{
                    self.bookingId = getStringFromAny(result["id"] ?? "")

                    self.bookRideEpayment(booking:getStringFromAny(result["id"] ?? ""))
                }
                break
            case .Error(let msg):
                ShowAlert(title: APPNAME, msg: msg, view: UIApplication.getTopViewController()!)
                break
            }
        }
    }
    func bookRideEpayment(booking:String){
        var param:[String:Any] = [String:Any]()
        param["booking"] = booking
        
        WebAccess.postDataWith(_Url: WebAccess.BOOK_RIDE_EPAY, _parameters: param) { (result) in
            switch (result){
            case .Success(let _data):
                print(_data)
                let result = _data["result"] as? [String:Any] ?? [:]
                
                self.paymentUrl = result["payment_page_url"] as? String ?? ""

                self.isWebview  = true

                break
            case .Error(let msg):
                ShowAlert(title: APPNAME, msg: msg, view: UIApplication.getTopViewController()!)
                break
            }
        }
    }
    func bookRideCashpayment(booking:String){
        var param:[String:Any] = [String:Any]()
        param["amount_paid"] = (getStringFromAny(getTotalWithCharge()))
        param["booking"] = booking
        WebAccess.postDataWith(_Url: WebAccess.BOOK_RIDE_CASHPAY, _parameters: param) { (result) in
            switch (result){
            case .Success( _):
                self.isWebview  = false
                ShowAlertWithCompletation(title: APPNAME, msg: "Ride Booked sucessfully", view: UIApplication.getTopViewController()!) { (result) in
                    self.isRatingView = true
                }
                
                break
            case .Error(let msg):
                ShowAlert(title: APPNAME, msg: msg, view: UIApplication.getTopViewController()!)
                break
            }
        }
    }
    var body: some View {
       // NavigationLink(destination:ratingsListView(bookingid:self.bookingId), isActive: $isRatingView){ EmptyView() }.isDetailLink(false)
        NavigationLink(destination: paymentWebview(urlString:paymentUrl, isFinished: { (value) in
            if (value == true){
                self.settings.isMoveToSearchViewHome = true
            }
        }),isActive: $isWebview) {EmptyView()}.isDetailLink(false)
        
        if (isPayOnline == false){
            VStack(alignment: .leading,spacing:0){
                
                if (offeredRoute.paymentMethod?.lowercased() == "cash"){
                    Text("Cash Payment Overview").modifier(CustomTextM(fontName: UIFont.Montserrat.semiBold.fontName, fontSize: 16, fontColor: Color.white)).frame(width: UIScreen.main.bounds.size.width - 40, height: 40, alignment: .center).background(Color.init(hex: "00AEEF")).cornerRadius(radius: 6,corners: [.topLeft, .topRight])

                }else{
                    
                    Text(("E-Payment overview")).modifier(CustomTextM(fontName: UIFont.Montserrat.semiBold.fontName, fontSize: 16, fontColor: Color.white)).frame(width: UIScreen.main.bounds.size.width - 40, height: 40, alignment: .center).background(Color.init(hex: "00AEEF")).cornerRadius(radius: 6,corners: [.topLeft, .topRight])

                }
                HStack(spacing:1){

                    Text(Localizable.Total_price_for.localized() ).modifier(CustomTextM(fontName: UIFont.Montserrat.semiBold.fontName, fontSize: 16, fontColor: Color.init(hex: "707070")))


                    
                    Text("\(findRide.passenger)" + " passangers").modifier(CustomTextM(fontName: UIFont.Montserrat.semiBold.fontName, fontSize: 16, fontColor: Color.init(hex: "707070")))
                    Spacer()
                    
                    Text(getStringFromAny(getTotal()) + " " + (offeredRoute.currency ?? "")).modifier(CustomTextM(fontName: UIFont.Montserrat.semiBold.fontName, fontSize: 16, fontColor: Color.init(hex: "F57F20")))
                }.padding()
                HStack{
                    Text("GOBY24 Services").modifier(CustomTextM(fontName: UIFont.Montserrat.semiBold.fontName, fontSize: 16, fontColor: Color.init(hex: "707070")))
                    Spacer()
                    Text(getStringFromAny(getServiceCharge()) +  " " + (offeredRoute.currency ?? "")).modifier(CustomTextM(fontName: UIFont.Montserrat.semiBold.fontName, fontSize: 16, fontColor: Color.init(hex: "#F57F20")))
                }.padding(.horizontal,10)
                Divider().padding(.horizontal).padding(.top)
                HStack{
                    Text("TOTAL").modifier(CustomTextM(fontName: UIFont.Montserrat.semiBold.fontName, fontSize: 16, fontColor: Color.init(hex: "707070")))
                    Spacer()
                    Text(getStringFromAny(getTotalWithCharge()) + " " + (offeredRoute.currency ?? "")).modifier(CustomTextM(fontName: UIFont.Montserrat.semiBold.fontName, fontSize: 16, fontColor: Color.init(hex: "#F57F20")))
                    
                }.padding(.horizontal).padding(.top,30)
                
                Button(action: {
                    isTermAccepted.toggle()
                }){
                    if (isTermAccepted == true){
                        Image(systemName: "circle").scaledToFit()
                        Text(Localizable.I_agree_to_terms_and_conditions.localized()).modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 13, fontColor: Color.init(hex: "00AEEF")))
                    }else{
                        Image(systemName: "largecircle.fill.circle").scaledToFit()
                        Text((Localizable.I_agree_to_terms_and_conditions.localized())).modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 13, fontColor: Color.init(hex: "00AEEF")))
                    }
                }.padding(.vertical).padding(.horizontal)
                
                HStack{
                    Spacer()
                    Button(action: {
                        self.bookRide()
                    }){
                        if (offeredRoute.paymentMethod?.lowercased() == "cash"){
                            Text("paycash")
                        }else{
                            Text("Pay")
                        }
                        
                    }
                    .modifier(CustomTextM(fontName: UIFont.Poppins.regular.fontName, fontSize: 15, fontColor: Color.white))
                    .modifier(ButtonStyle(buttonHeight: 30, buttonColor: Color.init(hex: "00ADEF"), buttonRadius: 30))
                    .frame(width:140)
                    .padding(.horizontal)
                }
                
                Spacer()
            }.frame(width:UIScreen.main.bounds.size.width - 40 ,height: 290).background(RoundedRectangle(cornerRadius: 25).fill(Color.white).shadow(color: Color.black.opacity(0.12), radius: 3)).padding(.top,20)
        }else{
            VStack{
                //                SwiftUIWebview(url:URL(string: "") ?? )
            }
        }
        
    }
}

//struct EpaymentView_Previews: PreviewProvider {
//    static var previews: some View {
//        EpaymentView()
//    }
//}
