//
//  SearchRidesFilterView.swift
//  Goby24
//
//  Created by Jignesh Bhensadadiya on 7/16/21.
//

import SwiftUI
import StarRating
import Sliders

struct SearchRidesFilterView: View {
    @State var pricesort: String = ""
    @State var ratingsort: String = ""
    @State var timesort: String = ""
    @State var range = 0.1...0.9
    @State var rating:String = "0"
     var initialBounds = 0.45...0.6

    @ObservedObject private var filterListData = searchRideFilter.shared

    @Environment(\.presentationMode) var presentationMode
    @State var customConfig = StarRatingConfiguration(spacing: 8,numberOfStars: 5,stepType: .half,minRating: 0,borderWidth: 1,borderColor: Color.black,emptyColor: Color.white,shadowRadius: 0,fillColors: [Color.black],starVertices: 5,starWeight: 0.4)

    var body: some View {
        VStack{
            HStack{
                Button(action: {    
                    self.presentationMode.wrappedValue.dismiss()

                }){
                    Image("close")
                }.padding(.horizontal)
                
                Spacer()
                Button(action: {
                    range = 0.1...0.9
                    filterListData.isFilterNeedToAdd = false
                    searchRideFilter.destroy()
                }){
                    Text("Clear All").modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 16, fontColor: Color.init(hex: "2E2E2E")))
                }.padding(.horizontal)
            }.padding(.top)
            
            Text(Localizable.Filter.localized()).modifier(CustomTextM(fontName: UIFont.Rubik.medium.fontName, fontSize: 26, fontColor: Color.init(hex: "2E2E2E"))).padding(.top,20)
            
            VStack{
                
                Group{
                    VStack{
                        HStack{
                            Text(Localizable.Price.localized()).modifier(CustomTextM(fontName: UIFont.Montserrat.bold.fontName, fontSize: 16, fontColor: Color.init(hex: "2E2E2E")))
                            Spacer()
                        }
                        HStack{
                            Text(Localizable.Min_price.localized()).modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 10, fontColor: Color.init(hex: "00AEEF")))
                            Spacer()
                            Text(Localizable.Max_price.localized()).modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 10, fontColor: Color.init(hex: "00AEEF")))
                        }.padding(.vertical)
                        
                        
                        HStack {
                            
                            RangeSlider(range: $range)
                               
                                .rangeSliderStyle(
                                    HorizontalRangeSliderStyle(
                                        track:
                                            HorizontalRangeTrack(
                                                view: Capsule().foregroundColor(Color.init(hex: "00AEEF"))
                                            )
                                            .background(Capsule().foregroundColor(Color.init(hex: "00AEEF").opacity(0.25)))
                                            .frame(height: 8),
                                        lowerThumb: Circle().foregroundColor(Color.init(hex: "00AEEF")),
                                        upperThumb: Circle().foregroundColor(Color.init(hex: "00AEEF")),
                                        lowerThumbSize: CGSize(width: 32, height: 32),
                                        upperThumbSize: CGSize(width: 32, height: 32),
                                        options: .forceAdjacentValue
                                    )
                                )
                            
                                
                                
                        }
                        HStack {
                            Text("\(Int(range.lowerBound * 2000))").modifier(CustomTextM(fontName: UIFont.Rubik.medium.fontName, fontSize: 10, fontColor: Color.init(hex: "00AEEF")))
                            Spacer()
                            Text("\(Int(range.upperBound * 2000))").modifier(CustomTextM(fontName: UIFont.Rubik.medium.fontName, fontSize: 10, fontColor: Color.init(hex: "00AEEF")))
                        }
                    }
                }
                
                Group{
                    VStack{
                        HStack{
                            Text(Localizable.Departure_time.localized()).modifier(CustomTextM(fontName: UIFont.Montserrat.bold.fontName, fontSize: 16, fontColor: Color.init(hex: "2E2E2E")))
                            Spacer()
                        }
                        RadioButtonGroup(items: ["04:00 - 12:00","12:01 - 18:00"], selectedId: pricesort, isTextFirst: false) { (selecteditem) in
                            pricesort = selecteditem
                        }
                    }
                }
                
                Group{
                    Divider()
                    HStack{
                        Text("Amenities").modifier(CustomTextM(fontName: UIFont.Montserrat.bold.fontName, fontSize: 16, fontColor: Color.init(hex: "2E2E2E")))
                        Spacer()
                    }
                    
                    HStack{
                        Image("twoperson")
                        Text(Localizable.Max_2_in_the_back.localized()).modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 15, fontColor: Color.black))
                        Spacer()
                        Text("1").modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 15, fontColor: Color.black))
                        
                    }
                    HStack{
                        Image("approval")
                        Text(Localizable.Instant_approval.localized()).modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 15, fontColor: Color.black))
                        Spacer()
                        Text("1").modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 15, fontColor: Color.black))
                        
                    }
                }
                Group{
                    Divider()
                    HStack{
                        Text(Localizable.Filter_by_rating.localized()).modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 15, fontColor: Color.black))
                    }
                    HStack{
                        Spacer()
                        StarRating(initialRating: Double($rating.wrappedValue) ?? 0,configuration: $customConfig, onRatingChanged: { print($0)
                            rating = getStringFromAny($0)
                        })
                        Spacer()
                    }
                }
                Spacer()
                Button(action: {
                    
//                    range.lowerBound = filterListData.fare_lte / 2000
//                    range.upperBound = filterListData.fare_gte / 2000
                //    pricesort = filterListData.pickup_time_lte

                    
                    filterListData.fare_lte = "\(Int(range.lowerBound * 2000))"
                    filterListData.fare_gte = "\(Int(range.upperBound * 2000))"

                    let time = pricesort.components(separatedBy: "-")
                    
                    filterListData.pickup_time_lte = time.first?.removeWhiteSpace() ?? ""
                    filterListData.pickup_time_gte = time.last?.removeWhiteSpace() ?? ""
                    
                    filterListData.rating = self.rating
                    
                    filterListData.isFilterNeedToAdd = true
                    
                    // POST NOTIFICATION
                    NotificationCenter.default.post(name: .reloadFindRides, object: nil, userInfo: nil)
                    
                    self.presentationMode.wrappedValue.dismiss()

                    
                }){
                    Text(Localizable.Filter.localized())
                }
                .modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 16, fontColor: Color.white))
                .modifier(ButtonStyle(buttonHeight: 50, buttonColor: Color.init(hex: "00ADEF"), buttonRadius: 50))
                .frame(width:200)
                .padding(.top,30)
                
            }.padding().padding(.horizontal)
        }.onAppear(){
            print(filterListData.rating)
            print(filterListData.fare_lte)
            print(filterListData.fare_gte)
            
            print(filterListData.pickup_time_lte)
            print(filterListData.pickup_time_gte)
            
            pricesort  = (filterListData.pickup_time_lte ?? "") + " - " + (filterListData.pickup_time_gte ?? "")
            
            rating = filterListData.rating ?? "0"
        }
    }
}

struct SearchRidesFilterView_Previews: PreviewProvider {
    static var previews: some View {
        SearchRidesFilterView()
    }
}
