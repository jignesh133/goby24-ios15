//
//  PickDate.swift
//  Goby24
//
//  Created by Jignesh Bhensadadiya on 7/11/21.
//

import SwiftUI

struct PickDate: View {
    
    //MARK: - PROPERTIES
    @Environment(\.presentationMode) var presentationMode
    @ObservedObject private var findRide = SearchRouteData.shared

    @State var selectedDate = Date()
    var body: some View {
        ScrollView{
            VStack{
                
                Text(Localizable.When_are_you_going.localized()).padding(.top, 25).modifier(CustomTextM(fontName: UIFont.Rubik.medium.fontName, fontSize: 26, fontColor: Color.init(hex: "2E2E2E")))
                
                VStack(alignment: .leading) {
                    Text("PICK DATE").padding(.top, 25).modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 18, fontColor: Color.init(hex: "2E2E2E")))
                    
                    // DatePicker("TEST", selection: $selectedDate)
                    DatePicker("", selection: $selectedDate,in: Date()..., displayedComponents: .date)
                        .datePickerStyle(WheelDatePickerStyle())
                        .clipped()
                        .labelsHidden()
                    //.frame(width: 200)
                    
//                    Text("PICK TIME").padding(.top, 25).modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 18, fontColor: Color.init(hex: "2E2E2E")))
//
//                    // DatePicker("TEST", selection: $selectedDate)
//                    DatePicker("", selection: $selectedDate,in: Date()..., displayedComponents: .hourAndMinute)
//
//                        .datePickerStyle(WheelDatePickerStyle())
//                        .clipped()
                     //   .labelsHidden()
                    // .frame(width: 200)
                    
                }.padding()
                Spacer()
                
                Button(action: {
                    self.findRide.journeyDate = selectedDate
                    self.presentationMode.wrappedValue.dismiss()
                }){
                    Text("Done")
                }
                .modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 16, fontColor: Color.white))
                .modifier(ButtonStyle(buttonHeight: 50, buttonColor: Color.init(hex: "00ADEF"), buttonRadius: 10))
                .padding()
                
            }.showNavigationBarWithBackStyle()
            .onAppear(){selectedDate = self.findRide.journeyDate ?? Date()}
        }
    }
}

struct PickDate_Previews: PreviewProvider {
    static var previews: some View {
        PickDate()
    }
}
