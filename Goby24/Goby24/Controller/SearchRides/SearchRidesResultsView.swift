    //
    //  SearchRidesResultsView.swift
    //  Goby24
    //
    //  Created by Jignesh Bhensadadiya on 7/15/21.
    //
    import SwiftUI
    import Combine
    class searchRides:ObservableObject{
        @Published var arrRides:[Model_findRides_Results] = [Model_findRides_Results]()

        func reload()  {
            objectWillChange.send()
        }
    }
    
    //import StarRating
    struct SearchRidesResultsView: View {
        
        @State var selection: Int? = nil
        @State var isDataFound: Bool = false

        @State var isSortPresented: Bool = false
        @State var isFilterPresented: Bool = false
        @Environment(\.presentationMode) var presentationMode
        
        @ObservedObject private var findRide = SearchRouteData.shared
        @ObservedObject private var filterListData = searchRideFilter.shared
        @ObservedObject private var sortListData = searchRideSort.shared

       // @Published var arrRides:[Model_findRides_Results]?// = [Model_findRides_Results]()
        @ObservedObject var rides:searchRides = searchRides()
        @State var arrRides:[Model_findRides_Results] = [Model_findRides_Results]()
        @State var toggleForRefresh:Bool = false
        
        let NC = NotificationCenter.default

        
        init() {
            //MARK: Disable selection.
            UITableView.appearance().allowsSelection = false
            UITableViewCell.appearance().selectionStyle = .none
            UITableView.appearance().tableHeaderView = UIView(frame: CGRect(x: 0, y: 0, width: 0, height: Double.leastNonzeroMagnitude))
            UITableView.appearance().separatorColor = .clear
            UITableView.appearance().separatorStyle = .none
        
            self.NC.addObserver(forName: .reloadFindRides, object: nil, queue: nil,using: self.reloadData)


        }
        func reloadData(_ notification: Notification) {
            self.getAllRides()

         }
        
        func getAllRides(){
            var param:[String:Any] = [String:Any]()
            param["from_place_id"] = self.findRide.sourceLocation?.placeId ?? ""
            param["to_place_id"] = self.findRide.destinationLocation?.placeId ?? ""
            param["passenger"] = getStringFromAny(self.findRide.passenger )
            param["pickup_date"] =  self.findRide.journeyDate?.getFormattedDate(formatter: "yyyy-MM-dd")
            
            var queryParam:[String:Any] = [String:Any]()
            if (filterListData.isFilterNeedToAdd){
                queryParam["pickup_time_gte"] = filterListData.pickup_time_gte
                queryParam["pickup_time_lte"] = filterListData.pickup_time_lte
                queryParam["fare_gte"] = filterListData.fare_gte
                queryParam["fare_lte"] = filterListData.fare_lte
                queryParam["rating"] = filterListData.rating
            }
            if (sortListData.isSortNeedToAdd){
                queryParam["sort_by_rating"] = sortListData.sort_by_rating
                queryParam["sort_by_price"] = sortListData.sort_by_price
                //queryParam["sort_by_time"] = sortListData.sort_by_time
            }
            print(queryParam)
            var queryString = queryParam.queryString
            if (queryString.count > 0){
                queryString = "?" + queryString
            }
            
            print(param)
            WebAccess.postDataWith(_Url: (WebAccess.FIND_RIDE + queryString), _parameters: param) { (result) in
                appDelegate?.stopLoadingView()
                switch (result){
                case .Success(let _data):
                    
                    do{

                        let result:[String:Any] = UtilsObjs.cleanJson(to: _data ) as? [String : Any] ?? [String:Any]()
                        print(result)
                        let data1 =  try JSONSerialization.data(withJSONObject: result["result"] ?? [] , options: JSONSerialization.WritingOptions.prettyPrinted)
                        let convertedString = String(data: data1, encoding: String.Encoding.utf8)
                        let jsonData = Data((convertedString?.utf8)!)
                        let arrData = try JSONDecoder().decode([Model_findRides_Results].self, from: jsonData)
                        arrRides.removeAll()
                        arrRides = arrData
                        
//                        arrRides.removeAll()
//                        arrRides = arrData
//                        rides.arrRides.removeAll()
                        rides.arrRides = arrData
//                        toggleForRefresh.toggle()
//
                        
                        print(arrRides.count)
                        
                    }catch(let error){
                        print(error)
                    }
                    break
                case .Error(let msg):
                    ShowAlert(title: APPNAME, msg: msg, view: UIApplication.getTopViewController()!)
                    break
                }
            }
        }
        var body: some View {
            VStack{
                
               
                VStack{
                    HStack{
                        Text(self.findRide.sourceLocation?.structuredFormatting?.mainText ?? "").modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 16, fontColor: Color.init(hex: "2E2E2E")))
                        Image("orangehorizontal")
                        Text(self.findRide.destinationLocation?.structuredFormatting?.mainText ?? "").modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 16, fontColor: Color.init(hex: "2E2E2E")))
                    }
                    HStack{
                        // "Thu, 27 May,"
                        Text(self.findRide.journeyDate?.getFormattedDate(formatter: "EEE, dd MMM") ?? "" ).modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 16, fontColor: Color.init(hex: "2E2E2E")))
                        
                        Text(getStringFromAny(self.findRide.passenger)).modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 16, fontColor: Color.init(hex: "2E2E2E")))

                        Text( (Localizable.Passengers.localized())).modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 16, fontColor: Color.init(hex: "2E2E2E")))

                    }
                    
                    HStack{
                        Text( getStringFromAny(arrRides.count ) + " RIDES").modifier(CustomTextM(fontName: UIFont.Rubik.medium.fontName, fontSize: 18, fontColor: Color.init(hex: "F57F20")))
                        
                    }.padding(.top,5)
                    HStack{
                        Text("Available").modifier(CustomTextM(fontName: UIFont.Rubik.medium.fontName, fontSize: 15, fontColor: Color.init(hex: "F57F20")))
                    }
                    
                }.padding(.horizontal,30).frame(height:115).background(Color.init(hex: "F5F5F5")).cornerRadius(30)
                
                
                ZStack{
                    HStack{
                        Text(self.findRide.journeyDate?.getFormattedDate(formatter: "EEE, dd MMM") ?? "" ).modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 16, fontColor: Color.init(hex: "2E2E2E"))).padding(.top,20)
                    }
                    HStack{
                        Spacer()
                        
                        Button(action: { isSortPresented = true}) {Image("sortarrow")}.padding(.trailing,50).padding(.top,20).sheet(isPresented: self.$isSortPresented) {
                            SortSearchedRideView()
                        }
                    }
                }
             
                if ((rides.arrRides.count ) > 0){
                        SearchRidesListView(arrRides: arrRides,selectedModel: arrRides.first)
                    }else{
                        Text(Localizable.No_rides_Available.localized()).modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 16, fontColor: Color.init(hex: "2E2E2E"))).padding(.top,20)
                    }
                    Spacer()

                }
            
            
            .showNavigationBarStyle()
            .navigationBarItems(leading: Button(action: { self.presentationMode.wrappedValue.dismiss()}) {Image("backnav")} , trailing:    Button(action: { isFilterPresented = true}) {Image("filter")}.sheet(isPresented: self.$isFilterPresented) {SearchRidesFilterView()})
            .onAppear(){
                searchRideSort.destroy()
                searchRideFilter.destroy()
//                rides = searchRides()
                self.getAllRides()
            }
//            .onReceive(rides.$arrRides) { (value) in
//                print("**** CHANGE ***")
//            }
        }
        
    }
    
    struct SearchRidesResultsView_Previews: PreviewProvider {
        static var previews: some View {
            SearchRidesResultsView()
        }
    }
