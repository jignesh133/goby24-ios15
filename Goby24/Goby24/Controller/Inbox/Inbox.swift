//
//  Inbox.swift
//  Goby24
//
//  Created by Jignesh Bhensadadiya on 7/12/21.
//

import SwiftUI

struct Inbox: View {
    
    func getData(){
        WebAccess.getDataWith(_Url: WebAccess.INBOX_CHAT_HOME) { (result) in
            switch (result){
            case .Success(let _data):
                print(_data)
                do{
                    let result:[String:Any] = UtilsObjs.cleanJson(to: _data ) as? [String : Any] ?? [String:Any]()
                    print(result)
                    let data1 =  try JSONSerialization.data(withJSONObject: result["result"] ?? [] , options: JSONSerialization.WritingOptions.prettyPrinted)
                    let convertedString = String(data: data1, encoding: String.Encoding.utf8)
                    let jsonData = Data((convertedString?.utf8)!)
                    self.arrMessages = try JSONDecoder().decode([Model_ChatHome].self, from: jsonData)
                    
                    NSLog("LOG")
                    
                }catch(let error){
                    print(error)
                }

                break

            case .Error(let msg):
                ShowAlert(title: APPNAME, msg: msg, view: UIApplication.getTopViewController()!)
                break
            }
        }
    }
    
    @State var selection: Int? = nil
    @State var arrMessages:[Model_ChatHome] = [Model_ChatHome]()
    
    var body: some View {
        VStack{
                        
            Text("INBOX").padding(.top, 25).modifier(CustomTextM(fontName: UIFont.Rubik.medium.fontName, fontSize: 26, fontColor: Color.init(hex: "2E2E2E")))
            
            HStack{
                Text("Inbox " + getStringFromAny(arrMessages.count)).padding(.top, 25).modifier(CustomTextM(fontName: UIFont.Montserrat.semiBold.fontName, fontSize: 12, fontColor: Color.init(hex: "2E2E2E"))).padding(.horizontal)
                Spacer()
            }
            Spacer()
            
            ScrollView{
                ForEach(0..<arrMessages.count,id: \.self) { row in
                    NavigationLink(destination: ChatMessage(objMessages: arrMessages[row])) {
                        InboxListItem(objMessages: arrMessages[row])
                    }
                }
            }
            
            //Button
            Button(action: {
                print("CONFIRM BUTTON ACTION CLICKED")
                
            }){
                Text("View More")
            }
            .modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 16, fontColor: Color.white))
            .modifier(ButtonStyle(buttonHeight: 50, buttonColor: Color.init(hex: "00ADEF"), buttonRadius: 10))
            .padding()
            Spacer()
            
        }
        .showNavigationBarWithBackStyle()
        .onAppear(){
            self.getData()
        }
        
    }
}


struct Inbox_Previews: PreviewProvider {
    static var previews: some View {
        Inbox()
    }
}
