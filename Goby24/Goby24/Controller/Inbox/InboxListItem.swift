//
//  InboxListItem.swift
//  Goby24
//
//  Created by Jignesh Bhensadadiya on 7/12/21.
//

import SwiftUI
import SDWebImageSwiftUI

struct InboxListItem: View {
    
    @State var objMessages:Model_ChatHome?


    
    var body: some View {
       
        VStack(){
            HStack{
                Spacer()
                Text(objMessages?.timestamp?.getDateFromString() ?? "").modifier(CustomTextM(fontName: UIFont.Montserrat.semiBold.fontName, fontSize: 12, fontColor: Color.init(hex: "2E2E2E"))).padding(.trailing,20)
            }
            HStack{
                VStack{
                    let urlString = IMAGEURL + getStringFromAny(objMessages?.user?.profilePic ?? "")//(driverProfile.profilePic ?? "")
                    
                    WebImage(url: URL.init(string: urlString ))
                        .placeholder(Image(systemName: "photo")) // Placeholder Image
                        .renderingMode(.original)
                        .resizable()
                        .aspectRatio(contentMode: .fill)
                        .frame(width: 40, height: 40)
                        .clipShape(Circle())
                        .overlay(Circle().stroke(Color.init(hex: "00AEEF"), lineWidth: 1)).padding(.vertical)


                    
//                    Image("userpic").padding(.leading,20)
                }.padding(.leading,10)
                VStack(alignment:.leading){
                    HStack(spacing:1.5){
                        Text((objMessages?.user?.firstName ?? "")).modifier(CustomTextM(fontName: UIFont.Montserrat.semiBold.fontName, fontSize: 12, fontColor: Color.init(hex: "2E2E2E"))).lineLimit(1)
                        Text((objMessages?.user?.lastName ?? "")).modifier(CustomTextM(fontName: UIFont.Montserrat.semiBold.fontName, fontSize: 12, fontColor: Color.init(hex: "2E2E2E"))).lineLimit(1)

                    }
                    
                    Text(objMessages?.message ?? "").modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 12, fontColor: Color.init(hex: "2E2E2E"))).lineLimit(2)
                    
                }.padding(.leading,5)
                Spacer()
               // Text("2")
                  //  .frame(width: 23, height: 23, alignment: .center)
                   // .multilineTextAlignment(.center).modifier(CustomTextM(fontName: UIFont.Montserrat.semiBold.fontName, fontSize: 12, fontColor: Color.white)).background(Color.init(hex: "F57F20")).cornerRadius(7).padding(.trailing,13.0)

               
            }
            
            Spacer()
        }
    }
}

struct InboxListItem_Previews: PreviewProvider {
    static var previews: some View {
        InboxListItem()
    }
}
