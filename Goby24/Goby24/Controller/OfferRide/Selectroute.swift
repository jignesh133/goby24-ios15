//
//  OfferRideMapView.swift
//  Goby24
//
//  Created by Jignesh Bhensadadiya on 7/18/21.
//

import SwiftUI
import GoogleMaps

struct Selectroute: View {
    @State var city: String = ""
    @State var selectedRoutes: Int = -1
    @State var isPrefered: Bool = false

    
    @State var arrRoutes:[[String:Any]] = []
   var offerRide = OfferRideData.shared

    func getRoutesList(){
        
        var param:[String:Any] = [String:Any]()
        param["origin"] = "place_id:" + getStringFromAny(offerRide.sourceLocation?.placeId ?? "")
        param["destination"] = "place_id:" + getStringFromAny(offerRide.destinationLocation?.placeId ?? "")
        param["mode"] = "driving"
        param["alternatives"] = "true"
        param["key"] = GOOGLE_KEY
        
        let urlComponents = NSURLComponents(string: "https://maps.googleapis.com/maps/api/directions/json")!
        
        if !param.isEmpty {
            urlComponents.queryItems = [URLQueryItem]()
            for (key, value) in param {
                let queryItem = URLQueryItem(name: key, value: "\(value)")
                urlComponents.queryItems!.append(queryItem)
            }
        }
        let url:String =   (urlComponents.url?.absoluteString ?? "" )
        guard let urlString = url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) else { return  }

        WebAccess.getSearchLocationDataWith(
              _Url: urlString, _parameters: [:], isHideLoader: false) { (response) in
            switch (response){
            case .Success(let _data):
                //print(_data)
                do{
                    let result:[String:Any] = UtilsObjs.cleanJson(to: _data ) as? [String : Any] ?? [String:Any]()
                    arrRoutes = result["routes"] as? [[String:Any]] ?? []
                    if (arrRoutes.count > 0){
                        selectedRoutes = 0
                        offerRide.selectedRoutes = arrRoutes[selectedRoutes]
                        let legs = offerRide.selectedRoutes?["legs"] as? [[String:Any]] ?? []
                        let leg = legs.first ?? [:]
                        let duration = leg["duration"] as? [String:Any] ?? [:]
                        offerRide.duration = getStringFromAny(duration["value"] ?? "")
                        print(duration)
                    }else{
                        ShowAlert(title: APPNAME, msg: "No Route Found", view: UIApplication.getTopViewController()!)
                    }
                    NSLog("LOG")
                }catch(let error){
                    print(error)
                }
                break
            case .Error(let msg):
                ShowAlert(title: APPNAME, msg: msg, view: UIApplication.getTopViewController()!)
                break
            }
        }
        
    }

  // BIDY FOR DISPLAY VIEW
    var body: some View {
        
        VStack{
            NavigationLink(destination: PreferedExtraPassengerView(), isActive: $isPrefered){EmptyView()}.isDetailLink(false)
            

            VStack {
                Text(Localizable.Whats_your_route.localized()) .modifier(CustomTextM(fontName: UIFont.Rubik.medium.fontName, fontSize: 26, fontColor: Color.init(hex: "2E2E2E")))
                
                List{
                    ForEach(0..<self.$arrRoutes.wrappedValue.count, id: \.self) { index in
                        Group{
                            HStack{
                                let obj = self.$arrRoutes.wrappedValue[index]
                                VStack{
                                Text(getStringFromAny(obj["summary"] ?? "NO NAME"))
                                    .modifier(CustomTextM(fontName: UIFont.Poppins.regular.fontName, fontSize: 16, fontColor: Color.init(hex: "2E2E2E")))
                                  //  Text(getStringFromAny(obj["summary"] ?? ""))
                                    //    .modifier(CustomTextM(fontName: UIFont.Poppins.regular.fontName, fontSize: 16, fontColor: Color.init(hex: "2E2E2E")))
                                    
                                }
                                Spacer()
                                Button(action: {
                                    selectedRoutes = index
                                    offerRide.selectedRoutes = arrRoutes[selectedRoutes]
                                    
                                    let legs = offerRide.selectedRoutes?["legs"] as? [[String:Any]] ?? []
                                    let leg = legs.first ?? [:]
                                    let duration = leg["duration"] as? [String:Any] ?? [:]
                                    offerRide.duration = getStringFromAny(duration["value"] ?? "")

                                    
                                    print(arrRoutes[selectedRoutes])
                                }){
                                    if (selectedRoutes == index){
                                        Image("selected")
                                    }else{
                                         Image("unselected")
                                    }
                                }
                            }
                        }    .listSeparatorStyle(.none)

                       }
                }.background(Color.white).frame(height:min(CGFloat(self.$arrRoutes.wrappedValue.count * 42),120))
            }
            
            
            if (selectedRoutes >= 0){
                let routesData:[String:Any] = self.arrRoutes[selectedRoutes]
                
                MapViewControllerBridge(mapRoutes:routesData, arrCity: [])
            }
            Spacer()
            
                
                Button(action: {
                    if (selectedRoutes == -1){
                        ShowAlert(title: APPNAME, msg: "Please select route", view: UIApplication.getTopViewController()!)
                    }else{
                        offerRide.arrCityList?.removeAll()
                        isPrefered = true
                        
                    }
                }){
                    Text(Localizable.Continue.localized())
                }
                .modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 16, fontColor: Color.white))
                .modifier(ButtonStyle(buttonHeight: 50, buttonColor: Color.init(hex: "00ADEF"), buttonRadius: 50))
                .padding()
           

        }.showNavigationBarWithBackStyle().onAppear(){
            getRoutesList()
        }
    }
}

struct Selectroute_Previews: PreviewProvider {
    static var previews: some View {
        OfferRideMapView()
    }
}
