//
//  PreferedExtraPassengerView.swift
//  Goby24
//
//  Created by Jignesh Bhensadadiya on 7/18/21.
//

import SwiftUI


struct PreferedExtraPassengerView: View {
    @State var city: String = ""
    //    @State var selection: Int? = nil
    
    @State var isCityOpen:Bool = false
    @State var isMapView:Bool = false
    
    @State var arrCityList:[Model_searchLocation] = [Model_searchLocation]()
    
    
    @ObservedObject var offerRide = OfferRideData.shared
    
    
    
    var body: some View {
        NavigationLink(destination: OfferRideMapView(), isActive: $isMapView){EmptyView()}.isDetailLink(false)
        
        NavigationLink(destination: SearchLocationCITY(onLocationSelect: { (location) in
            isCityOpen = false

            var myLocation = location
            WebAccess.getSearchLocationDataWith(_Url: WebAccess.getPlaceCoordinateUrl(placeId: location.placeId ?? ""), _parameters: [:]) { (result) in
                
                switch (result){
                case .Success(let _data):

                    print(_data)
                    let result = _data["result"] as? [String:Any] ?? [:]
                    let geomatry = result["geometry"] as? [String:Any] ?? [:]
                    let location = geomatry["location"] as? [String:Any] ?? [:]
                    myLocation.lat = getStringFromAny(location["lat"] ?? "")
                    myLocation.lng = getStringFromAny(location["lng"] ?? "")
                    myLocation.isSelected = true
                    if (offerRide.arrCityList == nil){
                        offerRide.arrCityList = []
                    }
                    offerRide.arrCityList?.append(myLocation)
                    self.arrCityList = offerRide.arrCityList ?? []
                    break
                case .Error(let msg):
                    ShowAlert(title: APPNAME, msg: msg, view: UIApplication.getTopViewController()!)
                    break
                }
                
            }
        }),isActive: $isCityOpen) {EmptyView()}.isDetailLink(false)
        ScrollView{
            VStack{
                Text(Localizable.Where_do_you_prefer_to_meet_extra_passengers.localized()).padding(.top, 25).modifier(CustomTextM(fontName: UIFont.Rubik.medium.fontName, fontSize: 26, fontColor: Color.init(hex: "2E2E2E"))).multilineTextAlignment(.center).padding(.horizontal).fixedSize(horizontal: false, vertical: true)                .lineLimit(nil)
                    .lineLimit(nil)
                
                
                
                Text(Localizable.Get_more_with_our_Boost_technology .localized()).padding(.top, 30).modifier(CustomTextM(fontName: UIFont.Poppins.regular.fontName, fontSize: 16, fontColor: Color.init(hex: "2E2E2E"))).multilineTextAlignment(.center)
                
                VStack{
                    Text(Localizable.Add_your_preffered_stopovers_to_aid_Boost.localized()).modifier(CustomTextM(fontName: UIFont.Poppins.regular.fontName, fontSize: 16, fontColor: Color.init(hex: "2E2E2E"))).multilineTextAlignment(.center).padding()
                    
                    //                Text("Add your preferred stopovers to help Boost find extra passengers on your way.").modifier(CustomTextM(fontName: UIFont.Poppins.regular.fontName, fontSize: 16, fontColor: Color.init(hex: "2E2E2E"))).multilineTextAlignment(.center).padding()
                } .frame(width: 300, height: 120)
                .background(Color.init(hex: "F5F5F5"))
                .clipShape(RoundedRectangle(cornerRadius: 25.0, style: .continuous))
                
                VStack{
                    
                    Text(Localizable.Add_city.localized())
                        .modifier(CustomTextM(fontName: UIFont.Poppins.regular.fontName, fontSize: 16, fontColor: Color.init(hex: "2E2E2E")))
                    
                    HStack {
                        Spacer(minLength: 20)
                        HStack (alignment: .center,
                                spacing: 10) {
                            Image("searchgray")
                                .frame(minHeight: 0, maxHeight: 40).padding(.leading,5)
                            TextField("e.g. Manchester picadilly", text: $city) { (editing) in
                                if (editing == true){
                                    
                                    UIApplication.shared.endEditing()
                                    isCityOpen = true
                                    
                                }
                            }
                            //TextField ("e.g. Manchester picadilly", text: $city)
                            .frame(height:40)
                            .modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 16, fontColor: Color.black))
                            
                        }  // HSTack
                        .padding([.top,.bottom], 2)
                        .padding(.leading, 5)
                        .background(Color.init(hex: "F5F5F5"), alignment: .center)
                        .cornerRadius(50)
                        Spacer(minLength: 20)
                    }
                    
                }.padding()
                
                ScrollView{
                    
                    ForEach(0..<(self.arrCityList.count), id: \.self) { index in
                        Group{
                            HStack{
                                Button(action: {
                                    var result = (self.arrCityList[index].isSelected ?? false)
                                    
                                    if result == true{
                                        result = false
                                    }else{
                                        result = true
                                    }
                                    self.arrCityList[index].isSelected = result
//                                    objArrList.arrCityList[index].isSelected = result
                                }){
                                    HStack{
                                        let text = self.arrCityList[index].structuredFormatting?.mainText ?? ""
                                        Text(text) .modifier(CustomTextM(fontName: UIFont.Poppins.regular.fontName, fontSize: 16, fontColor: Color.init(hex: "2E2E2E")))
                                        
                                        Spacer()
                                        
                                        if (self.arrCityList[index].isSelected ?? false){
                                            Image("selected")
                                        }else{
                                            Image("unselected")
                                            
                                        }
                                    }
                                }
                            }
                        }
                        
                    }.padding()
                    //.listSeparatorStyle(.none)
                }
                
                Spacer()
                
                
                Button(action: {
                    print(offerRide.arrCityList?.count ?? "")
                    offerRide.arrCityList = self.arrCityList.filter({ (obj) -> Bool in
                        obj.isSelected == true
                    })
                    // selection = 0
                    self.isMapView = true
                    
                }){
                    Text(Localizable.Continue.localized())
                }
                .modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 16, fontColor: Color.white))
                .modifier(ButtonStyle(buttonHeight: 50, buttonColor: Color.init(hex: "00ADEF"), buttonRadius: 50))
                .padding()
                
                
            }
        }.showNavigationBarWithBackStyle()
        .onAppear(){
//            if (offerRide.arrCityList?.count == 0){
//                offerRide.arrCityList = []
//            }
            if (offerRide.arrCityList?.count ?? 0) > 0 {
                self.arrCityList = offerRide.arrCityList ?? []
            }
            
        }
    }
}

struct PreferedExtraPassengerView_Previews: PreviewProvider {
    static var previews: some View {
        PreferedExtraPassengerView()
    }
}
