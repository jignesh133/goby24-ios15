//
//  SearchLocationCITY.swift
//  Goby24
//
//  Created by Jignesh Bhensadadiya on 8/2/21.
//

import SwiftUI
import GoogleMaps

// SEARCH CITY
struct SearchLocationCITY: View {
    
    //MARK: - PROPERTIES
    @State var strText: String = ""
    @State var arrLocation:[Model_searchLocation] = [Model_searchLocation]()
    let onLocationSelect: (Model_searchLocation) -> Void

    @Environment(\.presentationMode) var presentationMode
    
    @ObservedObject private var offerRide = OfferRideData.shared

    private func textFieldChanged(_ text: String) {
        print(text)
        getDataFromGoogle(str: text)
    }
    func checkCityInPath(coordinate:CLLocationCoordinate2D) -> Bool{
        
       // let cordinate = CLLocationCoordinate2DMake(CLLocationDegrees.init(Double(myLocation.lat)), CLLocationDegrees.init(Double(myLocation.lng)))
        let overview_polyline = offerRide.selectedRoutes?["overview_polyline"] as? [String:Any] ?? [:]
        
    
        let path = GMSPath.init(fromEncodedPath:overview_polyline["points"] as? String ?? "")!
        
        let result = GMSGeometryIsLocationOnPathTolerance(coordinate, path, true, 10000)
        print(result)
        return result
    }
    func getDataFromGoogle(str:String){
        guard str.count > 2 else {
            return
        }
        
        var param:[String:Any] = [String:Any]()
        param["query"] = strText
        let urlComponents = NSURLComponents(string: WebAccess.SEARCH_LOCATION)!
        
        if !param.isEmpty {
            urlComponents.queryItems = [URLQueryItem]()
            for (key, value) in param {
                let queryItem = URLQueryItem(name: key, value: "\(value)")
                urlComponents.queryItems!.append(queryItem)
            }
        }
        let url:String =  BASEURL + (urlComponents.url?.absoluteString ?? "" )
        guard let urlString = url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) else { return  }

        WebAccess.getSearchLocationDataWith(
              _Url: urlString, _parameters: [:], isHideLoader: true) { (response) in
            switch (response){
            case .Success(let _data):
                //print(_data)
                do{
                    let result:[String:Any] = UtilsObjs.cleanJson(to: _data ) as? [String : Any] ?? [String:Any]()
                    let data1 =  try JSONSerialization.data(withJSONObject: result["result"] ?? [] , options: JSONSerialization.WritingOptions.prettyPrinted)
                    let convertedString = String(data: data1, encoding: String.Encoding.utf8)
                    let jsonData = Data((convertedString?.utf8)!)
                    self.$arrLocation.wrappedValue.removeAll()
                    self.arrLocation = try JSONDecoder().decode([Model_searchLocation].self, from: jsonData)
                    NSLog("LOG")
                }catch(let error){
                    print(error)
                }
                break
            case .Error(let msg):
                ShowAlert(title: APPNAME, msg: msg, view: UIApplication.getTopViewController()!)
                break
            }
        }
        
    }
    var body: some View {
        
        
        VStack{
            
            
            
            let binding = Binding<String>(
                get: { self.strText },
                set: { self.strText = $0; self.textFieldChanged($0) }
            )
            
            Text("Search City").padding(.top, 25).modifier(CustomTextM(fontName: UIFont.Rubik.medium.fontName, fontSize: 26, fontColor: Color.init(hex: "2E2E2E")))
            
            ZStack(alignment: .trailing){
                
                TextField("ex. Mumbai", text: binding, onEditingChanged: {_ in
                    
                }, onCommit: {
                    
                }).textFieldStyle(MyTextFieldStyle())
                
                
                Button(action: {
                    self.arrLocation.removeAll()
                    //
                    binding.wrappedValue = ""
                }) {
                    Image("smallclose")
                }.padding(.trailing,40)//.padding(.top,10)
            }.padding()
            
            List{
                ForEach(0..<self.$arrLocation.wrappedValue.count, id: \.self) { index in
                    var myLocation = $arrLocation.wrappedValue[index]
                    Button(action: {
                        binding.wrappedValue = (arrLocation[index].descriptionField ?? "")
                        //onLocationSelect(arrLocation[index])
                        WebAccess.getSearchLocationDataWith(_Url: WebAccess.getPlaceCoordinateUrl(placeId: arrLocation[index].placeId ?? ""), _parameters: [:]) { (result) in
                    
                            switch (result){
                            case .Success(let _data):
                                print(_data)
                                let result = _data["result"] as? [String:Any] ?? [:]
                                let geomatry = result["geometry"] as? [String:Any] ?? [:]
                                let location = geomatry["location"] as? [String:Any] ?? [:]
                               
                                myLocation.lat = getStringFromAny(location["lat"] ?? "")
                                myLocation.lng = getStringFromAny(location["lng"] ?? "")
                                myLocation.isSelected = true
                                
                                let loc_coords = CLLocationCoordinate2D(latitude: getDoubleFromAny(myLocation.lat ?? 0), longitude: getDoubleFromAny(myLocation.lng ?? 0))

                                if(self.checkCityInPath(coordinate: loc_coords) == true){
  
                                    if ((offerRide.arrCityList?.contains(where: {$0.placeId == myLocation.placeId}) == true) && (offerRide.arrCityList?.contains(where: {$0.isSelected == false}) == true)) {
                                       let index =  offerRide.arrCityList?.firstIndex(where: {$0.placeId == myLocation.placeId})
                                        guard ((index != nil) && (index! <= offerRide.arrCityList!.count)) else {return}
                                        offerRide.arrCityList?[index!].isSelected = true
                                        self.presentationMode.wrappedValue.dismiss()

                                    }else if ((offerRide.arrCityList?.contains(where: {$0.placeId == myLocation.placeId}) == true) && (offerRide.arrCityList?.contains(where: {$0.isSelected == true}) == true)) {
                                        ShowAlert(title: APPNAME, msg: "City already added on given route", view: UIApplication.getTopViewController()!)
                                    }else{
                                        onLocationSelect(myLocation)
                                        self.presentationMode.wrappedValue.dismiss()
                                    }
                                }else{
                                    ShowAlert(title: APPNAME, msg: "City Not available on given route", view: UIApplication.getTopViewController()!)
                                    binding.wrappedValue = ""
                                    self.arrLocation.removeAll()
                                }
                                
                                break
                            case .Error(let msg):
                                ShowAlert(title: APPNAME, msg: msg, view: UIApplication.getTopViewController()!)
                                break
                            }
                            
                        }
                        
                    }) {
                        VStack(alignment:.leading){
                            
                            Text(self.arrLocation[index].structuredFormatting?.mainText ?? "").modifier(CustomTextM(fontName: UIFont.Poppins.regular.fontName, fontSize: 14, fontColor: Color.init(hex: "2E2E2E"))).multilineTextAlignment(.leading)
                            
                            Text(self.$arrLocation.wrappedValue[index].descriptionField ?? "").modifier(CustomTextM(fontName: UIFont.Rubik.medium.fontName, fontSize: 14, fontColor: Color.init(hex: "707070"))).multilineTextAlignment(.leading)

                            
                           // Text(self.arrLocation[index].structuredFormatting?.mainText ?? "").modifier(CustomTextM(fontName: UIFont.Poppins.regular.fontName, fontSize: 14, fontColor: Color.init(hex: "2E2E2E"))).multilineTextAlignment(.leading)
                            
                          //  Text(self.$arrLocation.wrappedValue[index].descriptionField ?? "NONE").modifier(CustomTextM(fontName: UIFont.Rubik.medium.fontName, fontSize: 14, fontColor: Color.init(hex: "707070"))).multilineTextAlignment(.leading)
                            //Divider().background(Color.init(hex: "707070"))
                        }
                    }
                }
            }.padding()
            
            
            Spacer()
            
        }.showNavigationBarWithBackStyle().onAppear(){
            
            getDataFromGoogle(str: self.strText)
        }
    }
}
