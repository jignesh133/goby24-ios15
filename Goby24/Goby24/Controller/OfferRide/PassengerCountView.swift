//
//  PassengerCountView.swift
//  Goby24
//
//  Created by Jignesh Bhensadadiya on 7/18/21.
//

import SwiftUI

struct PassengerCountView: View {
    @State var count:String = "1"
    
    //  let callback: (String) -> ()
    
    var body: some View {
        
        VStack{
            Text(Localizable.So_how_many_GoBy24_passengers.localized()).modifier(CustomTextM(fontName: UIFont.Rubik.medium.fontName, fontSize: 22, fontColor: Color.init(hex: "2E2E2E"))).multilineTextAlignment(.center).padding()
            
            
           // ScrollView(.horizontal){
                HStack{
                    //Button
                    Button(action: {
                        print("CONFIRM BUTTON ACTION CLICKED")
                    }){
                        Text("1").modifier(CustomTextM(fontName: UIFont.Rubik.bold.fontName, fontSize: 29, fontColor: Color.init(hex: "2E2E2E")))
                        
                    }.frame(width: 50, height: 50)
                    .overlay(RoundedRectangle(cornerRadius: 30).stroke(Color.init(hex: "00AEEF"), lineWidth: 1))
                    
                    .padding(.horizontal,15)
                    //Button
                    Button(action: {
                        print("CONFIRM BUTTON ACTION CLICKED")
                    }){
                        Text("2").modifier(CustomTextM(fontName: UIFont.Rubik.bold.fontName, fontSize: 29, fontColor: Color.init(hex: "2E2E2E")))
                        
                    }.frame(width: 50, height: 50)
                    .overlay(RoundedRectangle(cornerRadius: 30).stroke(Color.init(hex: "00AEEF"), lineWidth: 1))
                    
                    .padding(.horizontal,15)
                    //Button
                    Button(action: {
                        print("CONFIRM BUTTON ACTION CLICKED")
                    }){
                        Text("3").modifier(CustomTextM(fontName: UIFont.Rubik.bold.fontName, fontSize: 29, fontColor: Color.init(hex: "2E2E2E")))
                        
                    }.frame(width: 50, height: 50)
                    .overlay(RoundedRectangle(cornerRadius: 30).stroke(Color.init(hex: "00AEEF"), lineWidth: 1))
                    .padding(.horizontal,15)
                    //Button
                    Button(action: {
                        print("CONFIRM BUTTON ACTION CLICKED")
                    }){
                        Text("4").modifier(CustomTextM(fontName: UIFont.Rubik.bold.fontName, fontSize: 29, fontColor: Color.init(hex: "2E2E2E")))
                        
                    }.frame(width: 50, height: 50)
                    .overlay(RoundedRectangle(cornerRadius: 30).stroke(Color.init(hex: "00AEEF"), lineWidth: 1))
                    
                    .padding(.horizontal,15)
                }
          //  }.padding()
        }
        
        Spacer()
    }
    
}

struct PassengerCountView_Previews: PreviewProvider {
    static var previews: some View {
        PassengerCountView()
    }
}
