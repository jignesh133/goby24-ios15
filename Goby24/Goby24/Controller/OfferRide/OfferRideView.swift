//
//  OfferRideView.swift
//  Goby24
//
//  Created by Jignesh Bhensadadiya on 7/18/21.
//

import SwiftUI

struct OfferRideView: View {
//    @State var selection: Int? = nil
    @State var isOpenRideHistory:Bool = false
    @State var isSelectroute:Bool = false
    @State var isBoost:Bool = false

    
    @State var pickup: String = ""
    @State var pickoff: String = ""
    @State var isOpenPickUpLocation:Bool = false
    @State var isOpenPickOffLocation:Bool = false
    @State var isOpenAddVehicle:Bool = false

    @State var pickupLocation:Model_searchLocation?
    @State var pickOffLocation:Model_searchLocation?
    
     var offerRide = OfferRideData.shared
    @EnvironmentObject var settings: UserSettings

    init() {
        UITableView.appearance().backgroundColor = UIColor.clear
    }
    
    var body: some View {
        
        VStack{
            
            NavigationLink(destination: MyOfferRideHistory(), isActive: $isOpenRideHistory){EmptyView()}.isDetailLink(false)
//            NavigationLink(destination: Selectroute(), isActive: $isSelectroute){EmptyView()}.isDetailLink(false)
            NavigationLink(destination: OfferRideBoostView(), isActive: $isBoost){EmptyView()}.isDetailLink(false)

            NavigationLink(destination: LicensePlateNumberView(), isActive: $isOpenAddVehicle){EmptyView()}.isDetailLink(false)

            NavigationLink(destination:SearchLocation(strText: self.pickupLocation?.structuredFormatting?.mainText ?? "",isNeedToPopView:true, onLocationSelect: { (location,isClear) in
                if (isClear == false){
                    pickupLocation = location
                    offerRide.sourceLocation = pickupLocation
                    pickup = location?.descriptionField ?? ""
                }else{
                    pickupLocation = nil
                    offerRide.sourceLocation = nil
                    pickup = ""
                }
            }),isActive: $isOpenPickUpLocation) {EmptyView()}.isDetailLink(false)
            
            
            NavigationLink(destination:SearchLocation(strText: self.pickOffLocation?.structuredFormatting?.mainText ?? "",isNeedToPopView:true,  onLocationSelect: { (location,isClear) in
                if (isClear == false){
                    pickOffLocation = location
                    offerRide.destinationLocation = pickOffLocation
                    pickoff = location?.descriptionField ?? ""
                }else{
                    pickOffLocation = nil
                    offerRide.destinationLocation = nil
                    pickoff = ""
                }
            }),isActive: $isOpenPickOffLocation) {EmptyView()}.isDetailLink(false)

            
            VStack{
                Text(Localizable.Pick_up.localized())
                    .modifier(CustomTextM(fontName: UIFont.Rubik.medium.fontName, fontSize: 26, fontColor: Color.init(hex: "2E2E2E")))
                
                HStack {
                    Spacer(minLength: 20)
                    HStack (alignment: .center, spacing: 10) {
                        Image("searchgray")
                            .frame(minHeight: 0, maxHeight: 40).padding(.leading,5)
                        TextField("e.g. Manchester picadilly", text: $pickup, onEditingChanged: { (change) in
                            if change == true{
                                UIApplication.shared.endEditing()

                                isOpenPickUpLocation = true
                            }
                        })
                        
                        .modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 16, fontColor: Color.black))
                    }  // HSTack
                    .padding([.top,.bottom], 2)
                    .padding(.leading, 5)
                    .background(Color.init(hex: "F5F5F5"), alignment: .center)
                    .cornerRadius(50)
                    .contentShape(Rectangle())

                    Spacer(minLength: 20)
                }
                
            }.padding(.vertical)
            
            VStack{
                
                Text(Localizable.Drop_off.localized())
                    .modifier(CustomTextM(fontName: UIFont.Rubik.medium.fontName, fontSize: 26, fontColor: Color.init(hex: "2E2E2E")))
                
                HStack {
                    Spacer(minLength: 20)
                    HStack (alignment: .center,
                            spacing: 10) {
                        Image("searchgray")
                            .frame(minHeight: 0, maxHeight: 40).padding(.leading,5)
                        
                        TextField ("e.g. Manchester picadilly", text: $pickoff, onEditingChanged: { (change) in
                            if change == true{
                                UIApplication.shared.endEditing()
                                isOpenPickOffLocation = true
                            }
                        })
                        .modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 16, fontColor: Color.black))
                    }  // HSTack
                    .padding([.top,.bottom], 2)
                    .padding(.leading, 5)
                    .background(Color.init(hex: "F5F5F5"), alignment: .center)
                    .cornerRadius(50)
                    .contentShape(Rectangle())

                    Spacer(minLength: 20)
                }
                
            }.padding(.vertical)
            
            Divider().padding()
            
            Spacer()
                Button(action: {
                    let data = userDefault.value(forKey: Enum_Login.userProfileData.rawValue) as? [String:Any] ?? [:]
                    print(data)
                    if pickup.count == 0{
                        ShowAlert(title: APPNAME, msg: "Please enter pickup location", view: UIApplication.getTopViewController()!)
                    }else if pickoff.count == 0{
                        ShowAlert(title: APPNAME, msg: "Please enter pickoff location", view: UIApplication.getTopViewController()!)
                    }
                    else if getUserIsRider() == false{
                        ShowAlert(title: APPNAME, msg: "Your position is not driver", view: UIApplication.getTopViewController()!)
                    }
                    else if (((getBoolFromAny(data["is_vehicle_verified"] ?? false)) == false)){
                        let alert = UIAlertController(title: "Do you want be rider", message: "You need to add your vehicle information to be a rider. Would you like to be add vehicle information now?", preferredStyle: .alert)
                        let skip = UIAlertAction(title:"Cancel", style: .cancel) { (res) in
                            
                        }
                        let sure = UIAlertAction(title:"Yes ", style: .default) { (res) in
                            // GIE TRAVELLER RATING
                            isOpenAddVehicle = true
                        }
                        alert.addAction(skip)
                        alert.addAction(sure)
                        
                        UIApplication.getTopViewController()!.present(alert, animated: true, completion: nil)

                        
                        //ShowAlert(title: APPNAME, msg: "Your Vehicle not verified", view: UIApplication.getTopViewController()!)
                    }else{
                        isBoost = true
//                        isSelectroute = true
                    }
                    
                }){
                    Text(Localizable.Continue.localized())
                }
                .modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 16, fontColor: Color.white))
                .modifier(ButtonStyle(buttonHeight: 50, buttonColor: Color.init(hex: "00ADEF"), buttonRadius: 50))
                .padding()
            
        }   

        .showNavigationBarStyle()
        .onReceive(self.settings.$isMoveToOfferViewHome) { (isMoveToHome) in
            if isMoveToHome == true{
                self.pickOffLocation = nil
                self.pickupLocation = nil
                pickup = ""
                pickoff = ""
                OfferRideData.destroy()
                self.settings.isMoveToOfferViewHome = false
                isSelectroute = false
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.75) {
                   // self.isOpenRideHistory = true
                }
            }
        }
    }
}

struct OfferRideView_Previews: PreviewProvider {
    static var previews: some View {
        OfferRideView()
    }
}
