//
//  OfferRideMapView.swift
//  Goby24
//
//  Created by Jignesh Bhensadadiya on 7/18/21.
//

import SwiftUI
import GoogleMaps

struct SelectReturnRoute: View {
    @State var city: String = ""
    @State var selection: Int? = nil

//    @State var pickupLocation:Model_searchLocation?
//    @State var pickOffLocation:Model_searchLocation?

    @State var selectedRoutes: Int = -1

    
    @State var arrRoutes:[[String:Any]] = []
    @ObservedObject private var offerRide = OfferRideReturnRouteData.shared
    
     var offerRideAlready = OfferRideData.shared

    func getRoutesList(){
        
        // SWIPE FOR RETURN ROUTE
        offerRide.sourceLocation = offerRideAlready.destinationLocation
        offerRide.destinationLocation = offerRideAlready.sourceLocation
        

        
        for data in (offerRideAlready.arrCityList ?? []).reversed(){
            offerRide.arrCityList?.append(data)
        }
        
        
        var param:[String:Any] = [String:Any]()
        param["origin"] = "place_id:" + getStringFromAny(offerRide.sourceLocation?.placeId ?? "")
        param["destination"] = "place_id:" + getStringFromAny(offerRide.destinationLocation?.placeId ?? "")
        param["mode"] = "driving"
        param["alternatives"] = "true"
        param["key"] = GOOGLE_KEY
        
        let urlComponents = NSURLComponents(string: "https://maps.googleapis.com/maps/api/directions/json")!
        
        if !param.isEmpty {
            urlComponents.queryItems = [URLQueryItem]()
            for (key, value) in param {
                let queryItem = URLQueryItem(name: key, value: "\(value)")
                urlComponents.queryItems!.append(queryItem)
            }
        }
        let url:String =   (urlComponents.url?.absoluteString ?? "" )
        guard let urlString = url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) else { return  }

        WebAccess.getSearchLocationDataWith(
              _Url: urlString, _parameters: [:], isHideLoader: true) { (response) in
            switch (response){
            case .Success(let _data):
                //print(_data)
                do{
                    let result:[String:Any] = UtilsObjs.cleanJson(to: _data ) as? [String : Any] ?? [String:Any]()
                    arrRoutes = result["routes"] as? [[String:Any]] ?? []
                    if (arrRoutes.count >= 0){
                        selectedRoutes = 0
                        offerRide.selectedRoutes = arrRoutes[selectedRoutes]
                    }
                    NSLog("LOG")
                }catch(let error){
                    print(error)
                }
                break
            case .Error(let msg):
                ShowAlert(title: APPNAME, msg: msg, view: UIApplication.getTopViewController()!)
                break
            }
        }
        
    }


    var body: some View {
        VStack{
            
            VStack {
                Text(Localizable.What_is_your_return_route_.localized()) .modifier(CustomTextM(fontName: UIFont.Rubik.medium.fontName, fontSize: 26, fontColor: Color.init(hex: "2E2E2E")))
                
                List{
                    ForEach(0..<self.$arrRoutes.wrappedValue.count, id: \.self) { index in
                        Group{
                            HStack{
                                let obj = self.$arrRoutes.wrappedValue[index]
                                Text(getStringFromAny(obj["summary"] ?? "NO NAME"))
                                    .modifier(CustomTextM(fontName: UIFont.Poppins.regular.fontName, fontSize: 16, fontColor: Color.init(hex: "2E2E2E")))
                                
                                Spacer()
                                Button(action: {
                                    selectedRoutes = index
                                    offerRide.selectedRoutes = arrRoutes[selectedRoutes]
                                    
                                }){
                                    if (selectedRoutes == index){
                                        Image("selected")
                                    }else{
                                         Image("unselected")
                                    }
                                }
                            }
                        }    .listSeparatorStyle(.none)

                       }
                }.background(Color.white).frame(height:min(CGFloat(self.$arrRoutes.wrappedValue.count * 42),120))
            }
            
            
            if (selectedRoutes >= 0){
                let routesData:[String:Any] = self.arrRoutes[selectedRoutes]
                
                MapViewControllerBridge(mapRoutes:routesData, arrCity: [])
            }
            Spacer()
            
            NavigationLink(destination: ReturnRideDateView(), tag: 0, selection: $selection) {

            Button(action: {
                selection = 0
            }){
                Text(Localizable.Continue.localized())
            }
            .modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 16, fontColor: Color.white))
            .modifier(ButtonStyle(buttonHeight: 50, buttonColor: Color.init(hex: "00ADEF"), buttonRadius: 50))
            .padding()
            }.isDetailLink(false)
        }.showNavigationBarWithBackStyle().onAppear(){
            getRoutesList()
        }
    }
}

struct SelectReturnRoute_Previews: PreviewProvider {
    static var previews: some View {
        SelectReturnRoute()
    }
}
