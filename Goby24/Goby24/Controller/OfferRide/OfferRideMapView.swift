//
//  OfferRideMapView.swift
//  Goby24
//
//  Created by Jignesh Bhensadadiya on 7/18/21.
//

import SwiftUI
import GoogleMaps

struct OfferRideMapView: View {
    @State var city: String = ""
    @State var selection: Int? = nil
    @State var isCityOpen:Bool = false
    @State var isBestPlaceView:Bool = false
   
    @State var arrCityList:[Model_searchLocation] = [Model_searchLocation]()

    @State var arrRoutes:[[String:Any]] = []
    
    var offerRide = OfferRideData.shared
    
    
    var body: some View {
        NavigationLink(destination: bestPlacesView(), isActive: $isBestPlaceView){EmptyView()}.isDetailLink(false)

        VStack{
            NavigationLink(destination: SearchLocationCITY(onLocationSelect: { (location) in
                                                            var myLocation = location
                                                            WebAccess.getSearchLocationDataWith(_Url: WebAccess.getPlaceCoordinateUrl(placeId: location.placeId ?? ""), _parameters: [:]) { (result) in
                                                                
                                                                switch (result){
                                                                case .Success(let _data):
                                                                    print(_data)
                                                                    let result = _data["result"] as? [String:Any] ?? [:]
                                                                    let geomatry = result["geometry"] as? [String:Any] ?? [:]
                                                                    let location = geomatry["location"] as? [String:Any] ?? [:]
                                                                    myLocation.lat = getStringFromAny(location["lat"] ?? "")
                                                                    myLocation.lng = getStringFromAny(location["lng"] ?? "")
//                                                                    offerRide.arrCityList?.append(myLocation)
//                                                                    isCityOpen = false
                                                                    
                                                                    myLocation.isSelected = true
                                                                    if (offerRide.arrCityList == nil){
                                                                        offerRide.arrCityList = []
                                                                    }
                                                                    offerRide.arrCityList?.append(myLocation)
                                                                    self.arrCityList = offerRide.arrCityList ?? []
                                                                    break
                                                                case .Error(let msg):
                                                                    ShowAlert(title: APPNAME, msg: msg, view: UIApplication.getTopViewController()!)
                                                                    break
                                                                }
                                                                
                                                            }                }),isActive: $isCityOpen) {EmptyView()}.isDetailLink(false)
            
            VStack{
                
                Text(Localizable.Add_city.localized()).modifier(CustomTextM(fontName: UIFont.Poppins.regular.fontName, fontSize: 16, fontColor: Color.init(hex: "2E2E2E")))
                
                HStack {
                    Spacer(minLength: 20)
                    HStack (alignment: .center,
                            spacing: 10) {
                        Image("searchgray")
                            .frame(minHeight: 0, maxHeight: 40).padding(.leading,5)
                        TextField("e.g. Manchester picadilly", text: $city) { (editing) in
                            if (editing == true){
                                
                                UIApplication.shared.endEditing()
                                isCityOpen = true
                                
                            }
                        }
                        //TextField ("e.g. Manchester picadilly", text: $city)
                        
                        .modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 16, fontColor: Color.black))
                        
                    }  // HSTack
                    .padding([.top,.bottom], 2)
                    .padding(.leading, 5)
                    .background(Color.init(hex: "F5F5F5"), alignment: .center)
                    .cornerRadius(50)
                    Spacer(minLength: 20)
                }
                
            }.padding()
            
            let routesData = offerRide.selectedRoutes
            
            MapViewControllerBridge(mapRoutes:routesData ?? [:],arrCity: self.arrCityList )
            
            Spacer()
  
            Button(action: {
                
               // selection = 0
                self.isBestPlaceView = true

            }){
                Text(Localizable.Continue.localized())
            }
            .modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 16, fontColor: Color.white))
            .modifier(ButtonStyle(buttonHeight: 50, buttonColor: Color.init(hex: "00ADEF"), buttonRadius: 50))
            .padding()

            
//            NavigationLink(destination: bestPlacesView()) {
//               Text(Localizable.Continue.localized())
//
//             }.isDetailLink(false)
            
        }.showNavigationBarWithBackStyle().onAppear(){
            self.arrCityList = offerRide.arrCityList ?? []
        }
    }
}

struct OfferRideMapView_Previews: PreviewProvider {
    static var previews: some View {
        OfferRideMapView()
    }
}
