//
//  OfferDateView.swift
//  Goby24
//
//  Created by Jignesh Bhensadadiya on 7/18/21.
//

import SwiftUI

struct OfferDateView: View {
    
 var offerRide = OfferRideData.shared
    
    //MARK: - PROPERTIES
    @State var isSeatSelection:Bool = false

    @State var selectedDate = Date()
    
    var body: some View {
        ScrollView{
            
            NavigationLink(destination: OfferRideSeatSelectionVIew(), isActive: $isSeatSelection){EmptyView()}.isDetailLink(false)

            VStack{
                
                Text("When are you going").padding(.top, 25).modifier(CustomTextM(fontName: UIFont.Rubik.medium.fontName, fontSize: 26, fontColor: Color.init(hex: "2E2E2E")))
                
                VStack(alignment: .leading) {
                    Text("PICK DATE").padding(.top, 25).modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 18, fontColor: Color.init(hex: "2E2E2E")))

                    DatePicker("PICK DATE", selection: $selectedDate,in: Date()..., displayedComponents: .date)
                        .datePickerStyle(WheelDatePickerStyle())
                        .clipped()
                        .labelsHidden()

                    Text("PICK TIME").padding(.top, 25).modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 18, fontColor: Color.init(hex: "2E2E2E")))

                    DatePicker("PICK TIME", selection: $selectedDate,in: Date()..., displayedComponents: .hourAndMinute)
                        .datePickerStyle(WheelDatePickerStyle())
                        .clipped()
                        .labelsHidden()

                }.padding()
                
                Spacer()
                    Button(action: {
                        offerRide.pickedDate = selectedDate
                        offerRide.pickupDateAndTime = selectedDate
                        isSeatSelection = true
                    }){
                        Text(Localizable.Continue.localized())
                    }
                    .modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 16, fontColor: Color.white))
                    .modifier(ButtonStyle(buttonHeight: 50, buttonColor: Color.init(hex: "00ADEF"), buttonRadius: 10))
                    .padding()
                
            }
        }.showNavigationBarWithBackStyle()
        .onAppear(){
            offerRide.pickupDateAndTime = selectedDate
        }
        .onDisappear(){
            offerRide.pickupDateAndTime = selectedDate
        }
    }
}

struct OfferDateView_Previews: PreviewProvider {
    static var previews: some View {
        OfferDateView()
    }
}
