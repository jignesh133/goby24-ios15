//
//  OfferRideSeatSelectionVIew.swift
//  Goby24
//
//  Created by Jignesh Bhensadadiya on 7/18/21.
//

import SwiftUI

struct OfferRideSeatSelectionVIew: View {
    @State var yesClicked: Bool = false
    @State var numberOfSeatsSelection:Int = 0
    @State var isCanPassengerBook: Bool = false

    var offerRide = OfferRideData.shared
    @Environment(\.rootPresentationMode) private var rootPresentationMode
    
    var body: some View {
        
        VStack{
            
            NavigationLink(destination: CanPassengerBookInstanlyView(), isActive: $isCanPassengerBook){EmptyView()}.isDetailLink(false)
            
            Text(Localizable.Keep_the_middle_seat_empty_so_that.localized())
                .modifier(CustomTextM(fontName: UIFont.Rubik.medium.fontName, fontSize: 26, fontColor: Color.init(hex: "2E2E2E"))).multilineTextAlignment(.center).padding(30)

            HStack{
                //Button
                Button(action: {
                   
                    yesClicked = false
                    
                }){
                    if(yesClicked == false){
                        VStack{
                            Text(Localizable.No_Ill_squeeze_in_3.localized()).modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 13, fontColor: Color.init(hex: "00AEEF")))
                            Divider().background(Color.init(hex: "00AEEF")).frame(height: 1.5)
                        }
                    }else{
                        VStack{
                            Text(Localizable.No_Ill_squeeze_in_3.localized()).modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 13, fontColor: Color.init(hex: "2E2E2E")))
                            Divider().background(Color.init(hex: "2E2E2E")).frame(height: 1.5)
                            
                        }
                    }
                }.frame(minWidth: 0, maxWidth: .infinity)
                
                
                //ACCOUNT Button
                Button(action: {
                    if (self.offerRide.seats > 3){
                        self.offerRide.seats = 3
                        numberOfSeatsSelection = 3

                    }
                    yesClicked = true
                }){
                    if(yesClicked == true){
                        VStack{
                            Text(Localizable.Yes_sure.localized()).modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 13, fontColor: Color.init(hex: "00AEEF")))
                            Divider().background(Color.init(hex: "00AEEF")).frame( height: 1.5)
                        }
                    }else{
                        VStack{
                            Text(Localizable.Yes_sure.localized()).modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 13, fontColor: Color.init(hex: "2E2E2E")))
                            Divider().background(Color.init(hex: "2E2E2E")).frame( height: 1.5)
                        }
                    }
                }.frame(minWidth: 0, maxWidth: .infinity)
                
                
            }.padding()
            
            if(yesClicked == false){
                
                VStack{
                    Text(Localizable.So_how_many_GoBy24_passengers.localized()).modifier(CustomTextM(fontName: UIFont.Rubik.medium.fontName, fontSize: 22, fontColor: Color.init(hex: "2E2E2E"))).multilineTextAlignment(.center).padding()
                    
                    CustomSteper(count: getStringFromAny(self.offerRide.seats),maxCount:"7") { (count) in
                        self.offerRide.seats = getIntegerFromAny(count)
                        numberOfSeatsSelection = getIntegerFromAny(count)
                    }.padding(.top,10)
                    
                   // ScrollView(.horizontal){
//                        HStack{
//                            if(numberOfSeatsSelection == 2 ){
//                                ZStack{
//                                    Circle().fill(Color.init(hex: "F57F20")).frame(width: 50, height: 50)
//                                    //Button
//                                    Button(action: {
//                                        print("")
//                                       // numberOfSeatsSelection = 3
//                                    }){
//                                        Text("2")
//                                    }
//                                    .frame(width: 50, height: 50)
//                                  // .overlay(RoundedRectangle(cornerRadius: 30))
//                                    .padding(.horizontal,15)
//                                    .modifier(CustomTextM(fontName: UIFont.Rubik.bold.fontName, fontSize: 29, fontColor: Color.white))
//                                }
//                            }
//                            else{
//                                //Button
//                                Button(action: {
//                                    print("")
//                                    numberOfSeatsSelection = 2
//                                    self.offerRide.seats = 2
//                                }){
//                                    Text("2")
//                                }.frame(width: 50, height: 50)
//                                .overlay(RoundedRectangle(cornerRadius: 30).stroke(Color.init(hex: "00AEEF"), lineWidth: 1))
//                                .padding(.horizontal,15)
//                                .modifier(CustomTextM(fontName: UIFont.Rubik.bold.fontName, fontSize: 29, fontColor: Color.init(hex: "2E2E2E")))
//
//                            }
//
//                            if(numberOfSeatsSelection == 3 ){
//                                ZStack{
//                                    Circle().fill(Color.init(hex: "F57F20")).frame(width: 50, height: 50)
//                                    //Button
//                                    Button(action: {
//                                        print("")
//                                       // numberOfSeatsSelection = 3
//                                    }){
//                                        Text("3")
//                                    }
//                                    .frame(width: 50, height: 50)
////                                    .overlay(RoundedRectangle(cornerRadius: 30))
//                                    .padding(.horizontal,15)
//                                    .modifier(CustomTextM(fontName: UIFont.Rubik.bold.fontName, fontSize: 29, fontColor: Color.white))
//                                }
//                            }
//                            else{
//                                //Button
//                                Button(action: {
//                                    print("")
//                                    numberOfSeatsSelection = 3
//                                    self.offerRide.seats = 3
//
//                                }){
//                                    Text("3")
//                                }.frame(width: 50, height: 50)
//                                .overlay(RoundedRectangle(cornerRadius: 30).stroke(Color.init(hex: "00AEEF"), lineWidth: 1))
//                                .padding(.horizontal,15)
//                                .modifier(CustomTextM(fontName: UIFont.Rubik.bold.fontName, fontSize: 29, fontColor: Color.init(hex: "2E2E2E")))
//
//                            }
//                            //Button
//                            if(numberOfSeatsSelection == 4 ){
//                                ZStack{
//                                    Circle().fill(Color.init(hex: "F57F20")).frame(width: 50, height: 50)
//                                    //Button
//                                    Button(action: {
//                                        print("")
//                                       // numberOfSeatsSelection = 3
//                                    }){
//                                        Text("4")
//                                    }
//                                    .frame(width: 50, height: 50)
//                                   // .overlay(RoundedRectangle(cornerRadius: 30))
//                                    .padding(.horizontal,15)
//                                    .modifier(CustomTextM(fontName: UIFont.Rubik.bold.fontName, fontSize: 29, fontColor: Color.white))
//                                }
//                            }
//                            else{
//                                //Button
//                                Button(action: {
//                                    print("")
//                                    numberOfSeatsSelection = 4
//                                    self.offerRide.seats = 4
//                                }){
//                                    Text("4")
//                                }.frame(width: 50, height: 50)
//                                .overlay(RoundedRectangle(cornerRadius: 30).stroke(Color.init(hex: "00AEEF"), lineWidth: 1))
//                                .padding(.horizontal,15)
//                                .modifier(CustomTextM(fontName: UIFont.Rubik.bold.fontName, fontSize: 29, fontColor: Color.init(hex: "2E2E2E")))
//
//                            }                        }
                  //  }.padding()
                }
            }else{
                
                VStack{
                    Text(Localizable.So_how_many_GoBy24_passengers.localized()).modifier(CustomTextM(fontName: UIFont.Rubik.medium.fontName, fontSize: 22, fontColor: Color.init(hex: "2E2E2E"))).multilineTextAlignment(.center).padding()
                    
                    CustomSteper(count: getStringFromAny(self.offerRide.seats ),maxCount:"3") { (count) in
                        self.offerRide.seats = getIntegerFromAny(count)
                        numberOfSeatsSelection = getIntegerFromAny(count)
                    }.padding(.top,10)

                    
                   // ScrollView(.horizontal){
//                        HStack{
//                            if(numberOfSeatsSelection == 1 ){
//                                ZStack{
//                                    Circle().fill(Color.init(hex: "F57F20")).frame(width: 50, height: 50)
//                                    //Button
//                                    Button(action: {
//                                        print("")
//                                       // numberOfSeatsSelection = 3
//                                    }){
//                                        Text("1")
//                                    }
//                                    .frame(width: 50, height: 50)
//                                  // .overlay(RoundedRectangle(cornerRadius: 30))
//                                    .padding(.horizontal,15)
//                                    .modifier(CustomTextM(fontName: UIFont.Rubik.bold.fontName, fontSize: 29, fontColor: Color.white))
//                                }
//                            }
//                            else{
//                                //Button
//                                Button(action: {
//                                    print("")
//                                    numberOfSeatsSelection = 1
//                                    self.offerRide.seats = 1
//
//                                }){
//                                    Text("1")
//                                }.frame(width: 50, height: 50)
//                                .overlay(RoundedRectangle(cornerRadius: 30).stroke(Color.init(hex: "00AEEF"), lineWidth: 1))
//                                .padding(.horizontal,15)
//                                .modifier(CustomTextM(fontName: UIFont.Rubik.bold.fontName, fontSize: 29, fontColor: Color.init(hex: "2E2E2E")))
//
//                            }
//
//                            if(numberOfSeatsSelection == 2 ){
//                                ZStack{
//                                    Circle().fill(Color.init(hex: "F57F20")).frame(width: 50, height: 50)
//                                    //Button
//                                    Button(action: {
//                                        print("")
//                                    }){
//                                        Text("2")
//                                    }
//                                    .frame(width: 50, height: 50)
//                                    .padding(.horizontal,15)
//                                    .modifier(CustomTextM(fontName: UIFont.Rubik.bold.fontName, fontSize: 29, fontColor: Color.white))
//                                }
//                            }
//                            else{
//                                //Button
//                                Button(action: {
//                                    print("")
//                                    numberOfSeatsSelection = 2
//                                    self.offerRide.seats = 2
//
//                                }){
//                                    Text("2")
//                                }.frame(width: 50, height: 50)
//                                .overlay(RoundedRectangle(cornerRadius: 30).stroke(Color.init(hex: "00AEEF"), lineWidth: 1))
//                                .padding(.horizontal,15)
//                                .modifier(CustomTextM(fontName: UIFont.Rubik.bold.fontName, fontSize: 29, fontColor: Color.init(hex: "2E2E2E")))
//
//                            }
//                        }
                  //  }.padding()
                }
            }
            Spacer()
//            NavigationLink(destination: CanPassengerBookInstanlyView(), tag: 0, selection: $selection) {
                
                Button(action: {
                    if(self.offerRide.seats > 0){
                        isCanPassengerBook = true
                    }else{
                        ShowAlert(title: APPNAME, msg: "Please select total seat", view: UIApplication.getTopViewController()!)
                    }
                    // FOR TRY
                    //                self.rootPresentationMode.wrappedValue.dismiss()
                }){
                    Text(Localizable.Continue.localized())
                }
                .modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 16, fontColor: Color.white))
                .modifier(ButtonStyle(buttonHeight: 50, buttonColor: Color.init(hex: "00ADEF"), buttonRadius: 50))
                .padding()
           // }.isDetailLink(false)
        }.showNavigationBarWithBackStyle()
    }
}

struct OfferRideSeatSelectionVIew_Previews: PreviewProvider {
    static var previews: some View {
        OfferRideSeatSelectionVIew()
    }
}
