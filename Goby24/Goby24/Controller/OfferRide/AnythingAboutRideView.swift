//
//  AnythingAboutRideView.swift
//  Goby24
//
//  Created by Jignesh Bhensadadiya on 7/18/21.
//

import SwiftUI
import NavigationStack
struct AnythingAboutRideView: View {
    @State var comment: String = ""
    @State var isReturnRideAdded: Bool = false

    @Environment(\.presentationMode) var presentationMode
    var offerRide = OfferRideData.shared
    @ObservedObject private var offerRideRetuened = OfferRideReturnRouteData.shared
    @EnvironmentObject var settings: UserSettings

    
    func publishMyRide(){
        var param:[String:Any] = [String:Any]()
        var arrData:[[String:Any]] = [[String:Any]]()
        for data in self.offerRide.arrSubRoutes ?? []{
            arrData.append(data.toDictionary())
        }
        param["routes"] =  arrData
        
        param.printJson()
      //  guard arrData.count > 0 else {return}

        WebAccess.postDataWith(_Url: WebAccess.OFFER_RIDE, _parameters: param) { (result) in
            switch (result){
            case .Success(let _data):
                print(_data)
                if self.isReturnRideAdded == true {
                    self.publishMyReturnRide()
                }else{
                    ShowAlertWithCompletation(title: APPNAME, msg: "Ride published sucessfully", view: UIApplication.getTopViewController()!) { (result) in
                        settings.isMoveToOfferViewHome = true
                    }
                }
                print(_data)
                break
            case .Error(let msg):
                ShowAlert(title: APPNAME, msg: msg, view: UIApplication.getTopViewController()!)
                break
            }
        }
    }
    func publishMyReturnRide(){
        var param:[String:Any] = [String:Any]()
        var arrData:[[String:Any]] = [[String:Any]]()
        for data in self.offerRideRetuened.arrSubRoutes ?? []{
            arrData.append(data.toDictionary())
        }
        param["routes"] =  arrData
        print(param)
        WebAccess.postDataWith(_Url: WebAccess.OFFER_RIDE, _parameters: param) { (result) in
            switch (result){
            case .Success(let _data):
                print(_data)
                ShowAlertWithCompletation(title: APPNAME, msg: "Ride published sucessfully", view: UIApplication.getTopViewController()!) { (result) in
                    settings.isMoveToOfferViewHome = true
                  
                }
                break
            case .Error(let msg):
                ShowAlert(title: APPNAME, msg: msg, view: UIApplication.getTopViewController()!)
                break
            }
        }
    }
    var body: some View {
        VStack{
            Text(Localizable.Anything_to_add_about_your_ride.localized()).modifier(CustomTextM(fontName: UIFont.Rubik.medium.fontName, fontSize: 26, fontColor: Color.init(hex: "2E2E2E"))).multilineTextAlignment(.center).padding()
            
            Group{
                VStack{
                    TextField("Hello I’m going to visit.... ", text: $comment).modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 16, fontColor: Color.black))
                        .textContentType(.none)

                    Spacer()
                }.padding()
            }.frame(width: 275, height: 126, alignment: .center).background(Color.init(hex: "F5F5F5")).clipShape(RoundedRectangle(cornerRadius: 14)).padding()
            Spacer()
            Button(action: {
                self.publishMyRide()

            }){
                Text(Localizable.Publish_ride.localized())
            }
            .modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 16, fontColor: Color.white))
            .modifier(ButtonStyle(buttonHeight: 40, buttonColor: Color.init(hex: "00ADEF"), buttonRadius: 40))
            .padding()
        }.showNavigationBarWithBackStyle()
    }
}

struct AnythingAboutRideView_Previews: PreviewProvider {
    static var previews: some View {
        AnythingAboutRideView()
    }
}


 
