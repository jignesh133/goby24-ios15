//
//  bestPlacesView.swift
//  Goby24
//
//  Created by Jignesh Bhensadadiya on 7/18/21.
//

import SwiftUI

struct bestPlacesView: View {
//    @State var selection: Int? = nil
    
    @State var isOfferDateView: Bool = false
    @State var isTemp: Bool = false
    var offerRide = OfferRideData.shared
    @Environment(\.presentationMode) var presentationMode

    func getRoutesListWithCity(){
        var param:[String:Any] = [String:Any]()
        param["origin"] = "place_id:" + getStringFromAny(offerRide.sourceLocation?.placeId ?? "")
        param["destination"] = "place_id:" + getStringFromAny(offerRide.destinationLocation?.placeId ?? "")
        param["mode"] = "driving"
        param["alternatives"] = "true"
        param["key"] = GOOGLE_KEY

        var waypoints:[String] = [String]()
        for city in offerRide.arrCityList ?? [] {
            waypoints.append("place_id:" +  getStringFromAny(city.placeId ?? ""))
        }
        param["waypoints"] = waypoints.joined(separator: "|")

        let urlComponents = NSURLComponents(string: "https://maps.googleapis.com/maps/api/directions/json")!
        
        if !param.isEmpty {
            urlComponents.queryItems = [URLQueryItem]()
            for (key, value) in param {
                let queryItem = URLQueryItem(name: key, value: "\(value)")
                urlComponents.queryItems!.append(queryItem)
            }
        }
        let url:String =   (urlComponents.url?.absoluteString ?? "" )
        guard let urlString = url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) else { return  }
        
        WebAccess.getSearchLocationDataWith(
            _Url: urlString, _parameters: [:], isHideLoader: true) { (response) in
            switch (response){
            case .Success(let _data):
                //print(_data)
                do{
                    let result:[String:Any] = UtilsObjs.cleanJson(to: _data ) as? [String : Any] ?? [String:Any]()
                    //arrRoutes = result["routes"] as? [[String:Any]] ?? []
                    NSLog("LOG")
                }catch(let error){
                    print(error)
                }
                break
            case .Error(let msg):
                ShowAlert(title: APPNAME, msg: msg, view: UIApplication.getTopViewController()!)
                break
            }
        }
    }
    
    var body: some View {
        VStack{
            NavigationLink(destination: OfferDateView(), isActive: $isOfferDateView){EmptyView()}.isDetailLink(false)
          //  NavigationLink(destination: MyRides(), isActive: $isOfferDateView){EmptyView()}.isDetailLink(false)

            Text("These are the best places to stop in those cities. OK for you?")
                .modifier(CustomTextM(fontName: UIFont.Rubik.medium.fontName, fontSize: 26, fontColor: Color.init(hex: "2E2E2E"))).multilineTextAlignment(.center).padding()
            
            VStack(alignment:.leading,spacing:0){
                HStack{
                  
                    VStack(alignment:.leading,spacing:0){
                        HStack{
                            VStack(alignment:.center,spacing:0){
                                Circle().fill(Color.white).frame(width: 10, height: 10)
                                    .border(Color.init(hex: "D0D0D0"), width: 1)
                                HStack{
                                    Divider()
                                }
                                //Rectangle().fill(Color.gray).frame(width: 1)

                            }.fixedSize()
                            VStack(alignment:.leading,spacing:0){
                                Text(offerRide.sourceLocation?.structuredFormatting?.mainText ?? "").modifier(CustomTextM(fontName: UIFont.Montserrat.semiBold.fontName, fontSize: 16, fontColor: Color.black))
                                Text(offerRide.sourceLocation?.descriptionField ?? "").modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 13, fontColor: Color.black)).multilineTextAlignment(.leading).padding(.bottom,5)
                            }
                        }
                        ForEach(0..<getIntegerFromAny(offerRide.arrCityList?.count ?? 0), id: \.self) { index in
                            HStack{
                                VStack(alignment:.center,spacing:0){
                                    Circle().fill(Color.white).frame(width: 10, height: 10)
                                        .border(Color.init(hex: "D0D0D0"), width: 1)
                                         Rectangle().fill(Color.gray).frame(width: 1)
                                    Spacer()
                                }.fixedSize()
                                //Image("timerdiff")
                                VStack(alignment:.leading,spacing:0){
                                    Text(offerRide.arrCityList?[index].structuredFormatting?.mainText ?? "").modifier(CustomTextM(fontName: UIFont.Montserrat.semiBold.fontName, fontSize: 16, fontColor: Color.black))
                                    Text(offerRide.arrCityList?[index].descriptionField ?? "" ).modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 13, fontColor: Color.black)).multilineTextAlignment(.leading).padding(.bottom,5)
                                }
                            }
                        }
                        HStack{
                            VStack(alignment:.center,spacing:0){
                                Circle().fill(Color.white).frame(width: 10, height: 10)
                                    .border(Color.init(hex: "D0D0D0"), width: 1)
                                Rectangle().fill(Color.gray).frame(width: 1)
                                Spacer()
                            }.fixedSize()
                            //Image("timerdiff")
                            VStack(alignment:.leading,spacing:0){
                                Text(offerRide.destinationLocation?.structuredFormatting?.mainText ?? "").modifier(CustomTextM(fontName: UIFont.Montserrat.semiBold.fontName, fontSize: 16, fontColor: Color.black))
                                Text(offerRide.destinationLocation?.descriptionField ?? "").modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 13, fontColor: Color.black)).multilineTextAlignment(.leading).padding(.bottom,5)
                            }
                        }
                    }
                    Spacer()
                    VStack{
                        Button(action: {
                            print("")
                           // isTemp = true
                            self.presentationMode.wrappedValue.dismiss()

                        }){
                            Text("View Map").modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 15, fontColor: Color.white))
                        }
                        .modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 12, fontColor: Color.white))
                        .modifier(ButtonStyle(buttonHeight: 30, buttonColor: Color.init(hex: "00ADEF"), buttonRadius: 17))
                        .frame(maxWidth:104)
                        .padding([.top],10)

                    }
                    
                }
                
                
            }.padding().frame(width:UIScreen.main.bounds.size.width - 70).background(RoundedRectangle(cornerRadius: 25).fill(Color.white).shadow(color: Color.black.opacity(0.12), radius: 3))

            
            Spacer()


                            Button(action: {
                                isOfferDateView = true
                            }){
                                Text(Localizable.Continue.localized())
                            }
                            .modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 16, fontColor: Color.white))
                            .modifier(ButtonStyle(buttonHeight: 50, buttonColor: Color.init(hex: "00ADEF"), buttonRadius: 50))
                            .padding()

        }.showNavigationBarWithBackStyle().onAppear(){
            //getRoutesListWithCity()
        }
    }
}

struct bestPlacesView_Previews: PreviewProvider {
    static var previews: some View {
        bestPlacesView()
    }
}
