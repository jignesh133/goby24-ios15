//
//  CanPassengerBookInstanlyView.swift
//  Goby24
//
//  Created by Jignesh Bhensadadiya on 7/18/21.
//

import SwiftUI

struct CanPassengerBookInstanlyView: View {
    
    @State var selection: Int? = nil

    var offerRide = OfferRideData.shared

    var body: some View {
        GeometryReader { geo in

        VStack{
            VStack{
                Text(Localizable.Can_passengers_contact_instantly.localized()).modifier(CustomTextM(fontName: UIFont.Rubik.medium.fontName, fontSize: 26, fontColor: Color.init(hex: "2E2E2E"))).multilineTextAlignment(.center).padding()
            }.frame(width: UIScreen.main.bounds.size.width , height: 200).background(Color.white)
           
         
            VStack{
                VStack{
                    //Button
                    NavigationLink(destination: recommendedPriceView(), tag: 0, selection: $selection) {
                        Button(action: {
                            offerRide.bookInstantly = "yes"
                            selection = 0
                        }){
                            ZStack() {
                                HStack() {
                                    Text(Localizable.Yes_sure.localized()).modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 16, fontColor: Color.init(hex: "2E2E2E"))).padding(.horizontal)

                                    Spacer()
                                    Image("next").padding(.trailing,20)
                                }
                            }
                        }
                        .modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 16, fontColor: Color.init(hex: "2E2E2E")))
                        .modifier(ButtonStyle(buttonHeight: 60, buttonColor: Color.white, buttonRadius: 40))
                        .padding().padding(.top)

                    }.isDetailLink(false)
                }
                VStack{
                    NavigationLink(destination: recommendedPriceView(), tag: 1, selection: $selection) {
                        //Button
                        Button(action: {
                            offerRide.bookInstantly = "no"
                            selection = 1

                        }){
                            ZStack() {
                                HStack() {
                                    Text(Localizable.No_i_DONT_LIKE_like_to_be_disturbed.localized()).modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 16, fontColor: Color.init(hex: "2E2E2E"))).padding(.horizontal)
                                    
                                    Spacer()
                                    Image("next").padding(.trailing,20)
                                }
                            }
                        }
                        .modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 16, fontColor: Color.init(hex: "2E2E2E")))
                        .modifier(ButtonStyle(buttonHeight: 60, buttonColor: Color.white, buttonRadius: 40))
                        .padding()
                    }.isDetailLink(false)
                }
                Spacer()
            }.frame(width: UIScreen.main.bounds.size.width,height: geo.size.height ).background(Color.init(hex: "F5F5F5")).ignoresSafeArea()

           
        }

        }.showNavigationBarWithBackStyle()
    }
}

struct CanPassengerBookInstanlyView_Previews: PreviewProvider {
    static var previews: some View {
        CanPassengerBookInstanlyView()
    }
}
