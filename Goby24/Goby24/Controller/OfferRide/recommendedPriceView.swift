//
//  recommendedPriceView.swift
//  Goby24
//
//  Created by Jignesh Bhensadadiya on 7/18/21.
//

import SwiftUI

struct recommendedPriceView: View {
    @State var selection: Int? = nil
    @State var priceIsOk:Bool = true
    @State var cardMode:Bool = true
    @State var isReturnRideCommingBack:Bool = false

    @State var recommendedPrice:String = ""
    @State var myRecommendedPrice:String = ""
    @State var availableCurrency:[String] = ["CHF","EURO"]
    @State var selectedCurrency:String = "CHF"
    
    @State var minPrice:String = "5"
    @State var maxPrice:String = "15"
    @State var basePrice:String = "10"

    
    
     var offerRide = OfferRideData.shared
    
    @State var matrix:Model_distanceMatric?
    @State var arrSubRoutes:[Model_SepratedRoutes] = [Model_SepratedRoutes]()
    @State var places:[String] = [String]()
    
    func getFare(newCurrency:String = "CHF"){
        WebAccess.getDataWith(_Url: WebAccess.FARE_PER_KM + "?currency=" + (newCurrency), _parameters: [:]) { (result) in
            switch (result){
            case .Success(let _data):
                print(_data)
                let mainResult = _data["result"] as? [[String:Any]] ?? []
                
                if (mainResult.count == 0){
                    ShowAlert(title: APPNAME, msg: "Fare not available for selected currency", view: UIApplication.getTopViewController()!)
                    return
                }
                let result = mainResult.first ?? [:]
                
                offerRide.currency = getStringFromAny(result["currency"] ?? "")
                offerRide.price_per_km = getStringFromAny(result["price_per_km"] ?? 0)
                selectedCurrency = offerRide.currency ?? ""
                
                DispatchQueue.background(background: {
                    self.methodCalculatePrice()
                }, completion:{
                    print("JSON FILE")
                })
                
                break
            case .Error( _):
                break
            }
            
        }
    }
    func getPriceMatric(){
        
        places.append((offerRide.sourceLocation?.placeId) ?? "")
        
        for data in offerRide.arrCityList ?? [] {
            places.append(data.placeId ?? "")
        }
        places.append((offerRide.destinationLocation?.placeId) ?? "")
        
        
        var param:[String:Any] = [String:Any]()
        param["places"] = places
        
        WebAccess.postDataWith(_Url: WebAccess.DISTANCE_MATRIX, _parameters: param) { (result) in
            switch (result){
            case .Success(let _data):
                print(_data)
                do{
                    let result:[String:Any] = UtilsObjs.cleanJson(to: _data ) as? [String : Any] ?? [String:Any]()
                    let data1 =  try JSONSerialization.data(withJSONObject: result["result"] ?? [] , options: JSONSerialization.WritingOptions.prettyPrinted)
                    let convertedString = String(data: data1, encoding: String.Encoding.utf8)
                    let jsonData = Data((convertedString?.utf8)!)
                    self.matrix = try JSONDecoder().decode(Model_distanceMatric.self, from: jsonData)
                    
                    self.getFare()
                    
                    NSLog("LOG")
                }catch(let error){
                    print(error)
                }
                
                break
            case .Error( _):
                break
            }
            
        }
    }
    
    func methodCalculatePrice(){
        DispatchQueue.main.async {
            // START LOADING
            appDelegate?.startLoadingview()
        }
        let fromPlaceId:[String] = self.places.dropLast()
        var toPlaceId:[String] = []
        
        for data in self.places.reversed(){
            toPlaceId.append(data)
        }
        toPlaceId = toPlaceId.dropLast()
        
        
        
        let fromPlace = self.matrix?.originAddresses//.dropLast()
        let toPlace = self.matrix?.destinationAddresses
        
        arrSubRoutes.removeAll()

        for i in 0..<(fromPlace?.count ?? 0) {
            print("\(i)")
            for j in 0..<(toPlace?.count ?? 0){
                print("\(j)")
                
                if (fromPlace?[i] == toPlace?[j]){
                    continue
                }
                let obj = Model_SepratedRoutes(fromDictionary: [:])
                obj.fromPlaceId = fromPlaceId[i]
                obj.toPlaceId = toPlaceId[j]
                
                obj.startLabel = fromPlace?[i]
                obj.endLabel = toPlace?[j]
                
                obj.seats = offerRide.seats
                print(self.matrix?.rows?[i].elements?[j].duration?.value ?? 0)
                print(offerRide.price_per_km ?? "")
                
                let fare = getDoubleFromAny(offerRide.price_per_km ?? 0) * (getDoubleFromAny(self.matrix?.rows?[i].elements?[j].duration?.value ?? 0)/1000)
                obj.fare = fare
                obj.currency = offerRide.currency
                obj.pickupDate = offerRide.pickupDateAndTime?.getFormattedDate(formatter: "y-MM-dd")
                obj.pickupTime = offerRide.pickupDateAndTime?.getFormattedDate(formatter: "HH:mm")
                obj.timezone = TimeZone.current.abbreviation()
                obj.distance = self.matrix?.rows?[i].elements?[j].distance?.text
                obj.duration = self.matrix?.rows?[i].elements?[j].duration?.text
                obj.durationValue = getStringFromAny(self.matrix?.rows?[i].elements?[j].duration?.value ?? "")
                obj.distanceValue = getStringFromAny(self.matrix?.rows?[i].elements?[j].distance?.value ?? "")
                
                let pickuptimeInterVal = getDoubleFromAny(offerRide.pickupDateAndTime!.timeIntervalSince1970).rounded()
                let durationvalue = self.matrix?.rows?[i].elements?[j].duration?.value ?? 0
                
                
                let dropDteTime = Date(timeIntervalSince1970: Double(durationvalue) + pickuptimeInterVal)
                
                obj.dropDate = dropDteTime.getFormattedDate(formatter: "y-MM-dd")
                obj.dropTime = dropDteTime.getFormattedDate(formatter: "HH:mm")
                
                obj.riderMsg = ""
                obj.bookInstantly = offerRide.bookInstantly
                obj.paymentMethod = offerRide.paymentMode
                
                var start_detail:[String:Any] = [String:Any]()
                start_detail["main_text"] = fromPlace?[i]
                start_detail["secondary_text"] = fromPlace?[j]
                
                var end_detail:[String:Any] = [String:Any]()
                end_detail["main_text"] = toPlace?[j]
                end_detail["secondary_text"] = toPlace?[j]
                
                obj.startDetail = Model_StartDetail(fromDictionary: start_detail)
                obj.endDetail = Model_EndDetail(fromDictionary: end_detail)
                
                
                
                arrSubRoutes.append(obj)
            }
        }
        self.updateRecommenedPrice()
        
        myRecommendedPrice = offerRide.price_per_km ?? "0"
        DispatchQueue.main.async {
            // STOP LOADING
            appDelegate?.stopLoadingView()
        }
        
    }
    func getFareValue(strDuration:String) -> String{
        let fare = getDoubleFromAny(myRecommendedPrice ) * (getDoubleFromAny(strDuration)/1000)
        
        return getStringFromAny(fare)
    }
    func updateRecommenedPrice(){
        if ((Double(self.arrSubRoutes[0].distanceValue) ?? 0 ) <= 25000){
//            let distanceKM = (Double(self.arrSubRoutes[0].distanceValue) ?? 0 ) / 1000
            let getTotal = 5 * (getDoubleFromAny( offerRide.price_per_km ?? "" ) / 100)
//            recommendedPrice = getStringFromAny(getTotal.clean(maximumFractionDigits: 0))
            recommendedPrice =  getStringFromAny(ceil(getTotal))

      }else{
            let distanceKM = (Double(self.arrSubRoutes[0].distanceValue) ?? 0 ) / 1000
            let getTotal = distanceKM * (getDoubleFromAny( offerRide.price_per_km ?? "" ) / 100)
        recommendedPrice =  getStringFromAny(ceil(getTotal))
//            recommendedPrice = getStringFromAny(getTotal.clean(maximumFractionDigits: 0))
        }
    }
    var body: some View {
        
        GeometryReader { geo in
            ScrollView{
                VStack{
                    NavigationLink(destination: returnRideComingBack(),isActive: $isReturnRideCommingBack) {EmptyView()}.isDetailLink(false)

                    Group{
                        VStack{
                            Text(Localizable.This_is_our_recommended_price_per_seat_OK_for_you.localized()).modifier(CustomTextM(fontName: UIFont.Rubik.medium.fontName, fontSize: 26, fontColor: Color.init(hex: "2E2E2E"))).multilineTextAlignment(.center).padding()
                            Spacer()
                            HStack{
                                Text(offerRide.sourceLocation?.descriptionField ?? "").modifier(CustomTextM(fontName: UIFont.Rubik.medium.fontName, fontSize: 12, fontColor: Color.init(hex: "00ADEF")))
                                Text(" - ").modifier(CustomTextM(fontName: UIFont.Rubik.medium.fontName, fontSize: 12, fontColor: Color.init(hex: "00ADEF")))
                                Text(offerRide.destinationLocation?.descriptionField ?? "").modifier(CustomTextM(fontName: UIFont.Rubik.medium.fontName, fontSize: 12, fontColor: Color.init(hex: "00ADEF")))
                                Spacer()
                                
                                Button(action: {
                                    let index = availableCurrency.firstIndex(of: selectedCurrency ) ?? 0
                                    RPicker.selectOption(title: "Please select Currency", cancelText: "Cancel", doneText: "Done", dataArray: availableCurrency, selectedIndex: index) { (val, index) in
                                        if (selectedCurrency != val){
                                            if (val == "CHF"){
                                                minPrice = "5"
                                                maxPrice = "15"
                                                basePrice = "10"
                                                myRecommendedPrice = "10"
                                                offerRide.price_per_km = myRecommendedPrice
                                            }
                                            if (val == "EURO"){
                                                minPrice = "10"
                                                maxPrice = "20"
                                                basePrice = "15"
                                                myRecommendedPrice = "15"
                                                offerRide.price_per_km = myRecommendedPrice
                                            }
                                            self.getFare(newCurrency: val)
                                        }
                                    }
                                }){
                                    HStack{
                                        Text(recommendedPrice).modifier(CustomTextM(fontName: UIFont.Rubik.medium.fontName, fontSize: 16, fontColor: Color.init(hex: "00ADEF")))
                                        // Text(" ").modifier(CustomTextM(fontName: UIFont.Rubik.medium.fontName, fontSize: 12, fontColor: Color.init(hex: "00ADEF")))
                                        Text(selectedCurrency).modifier(CustomTextM(fontName: UIFont.Rubik.medium.fontName, fontSize: 16, fontColor: Color.init(hex: "00ADEF")))
                                        Image("down")
                                    }.frame(minWidth:50)
                                }
                                .modifier(CustomTextM(fontName: UIFont.Poppins.regular.fontName, fontSize: 16, fontColor: Color.init(hex: "2E2E2E")))
                                
                            }.padding()
                            
                            HStack{
                                Spacer()
                                Button(action: {
                                    offerRide.paymentMode = "cash"
                                    cardMode = false
                                }){
                                    HStack{
                                        Image($cardMode.wrappedValue ? "unselected":"selected")
                                        Text(Localizable.I_want_Cash_money.localized())
                                        // Spacer()
                                    }
                                }
                                .modifier(CustomTextM(fontName: UIFont.Poppins.regular.fontName, fontSize: 16, fontColor: Color.init(hex: "2E2E2E")))
                                .padding(.horizontal)
                                Spacer()
                                Button(action: {
                                    offerRide.paymentMode = "E-Payment"
                                    cardMode = true
                                }){
                                    HStack{
                                        Image($cardMode.wrappedValue ? "selected":"unselected")
                                        Text(Localizable.I_want_money_in_my_Card.localized())
                                        //Spacer()
                                    }
                                }
                                .modifier(CustomTextM(fontName: UIFont.Poppins.regular.fontName, fontSize: 16, fontColor: Color.init(hex: "2E2E2E")))
                                .padding(.horizontal)
                                
                                Spacer()
                                // RadioButtonGroup(items: ["Yes, sure","No, I’ll change the price"], selectedId: "", isTextFirst: false) { (item) in }.padding().frame(width: 300, height: 150)
                                
                            }.frame( height: 100).padding()//.background(Color.white).clipShape(RoundedRectangle(cornerRadius: 21, style: .continuous)).shadow(radius: 1).padding(.top)
                            
                            
                        }.frame(width: UIScreen.main.bounds.size.width , height: 420).background(Color.white)
                    }
                    
                    
                    HStack{
                        Spacer()
                        VStack{
                            Spacer()
                            Button(action: {
                                priceIsOk = true
                            }){
                                HStack{
                                    Image($priceIsOk.wrappedValue ? "selected":"unselected")
                                    Text("Yes, sure")
                                    Spacer()
                                }
                            }
                            .modifier(CustomTextM(fontName: UIFont.Poppins.regular.fontName, fontSize: 16, fontColor: Color.init(hex: "2E2E2E")))
                            .padding(.horizontal)
                            
                            Button(action: {
                                priceIsOk = false
                            }){
                                HStack{
                                    Image($priceIsOk.wrappedValue ? "unselected":"selected")
                                    Text(Localizable.No_Ill_change_the_price.localized())
                                    Spacer()
                                }
                            }
                            .modifier(CustomTextM(fontName: UIFont.Poppins.regular.fontName, fontSize: 16, fontColor: Color.init(hex: "2E2E2E")))
                            .padding(.horizontal)
                            
                            Spacer()
                            // RadioButtonGroup(items: ["Yes, sure","No, I’ll change the price"], selectedId: "", isTextFirst: false) { (item) in }.padding().frame(width: 300, height: 150)
                            
                        }.frame(width: 300, height: 100).background(Color.white).clipShape(RoundedRectangle(cornerRadius: 21, style: .continuous)).shadow(radius: 1).padding(.top)
                        Spacer()
                        
                    }
                    if(priceIsOk == false){
                        
                        VStack{
                            
                            Text(Localizable.My_recommended_price_per_seat.localized())
                                // .multilineTextAlignment(.center).lineLimit(2)
                                .modifier(CustomTextM(fontName: UIFont.Rubik.medium.fontName, fontSize: 26, fontColor: Color.init(hex: "2E2E2E")))
                                .padding(.horizontal)
                                .padding(.top)
                            
                            CustomSteper(count: $myRecommendedPrice.wrappedValue,maxCount:$maxPrice.wrappedValue, minCount:$minPrice.wrappedValue,customWidth:300) { (count) in
                                myRecommendedPrice = getStringFromAny(count)
                                offerRide.price_per_km = myRecommendedPrice
                                self.updateRecommenedPrice()
                                
                            }.padding(.top,50).frame(width: 300)
                            
                            List{
                                ForEach(0..<arrSubRoutes.count) { row in
                                    HStack{
                                        Image("orangechain")
                                        VStack(alignment:.leading){
                                            Text(arrSubRoutes[row].startDetail.mainText).modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 13, fontColor: Color.init(hex: "2E2E2E"))).padding(.top,1)
                                            Text(arrSubRoutes[row].endDetail.mainText).modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 13, fontColor: Color.init(hex: "2E2E2E"))).padding(.top,1)
                                        }.padding(.horizontal)
                                        
                                        Spacer()
                                        
                                        let totkm = getFareValue(strDuration: self.arrSubRoutes[row].distanceValue ?? "")
                                        let roundedValue = getDoubleFromAny(totkm).clean(maximumFractionDigits: 0)
                                        Text((getStringFromAny(roundedValue) + " " + selectedCurrency)).modifier(CustomTextM(fontName: UIFont.Montserrat.bold.fontName, fontSize: 18, fontColor: Color.init(hex: "F57F20"))).multilineTextAlignment(.center)
                                    }
                                }
                            }.frame(height:CGFloat(arrSubRoutes.count) * 55).padding(.top,30)
                            
                        }.background(Color.white).clipShape(RoundedRectangle(cornerRadius: 21, style: .continuous)).shadow(radius: 1).padding(.top,50).padding(.horizontal)
                    }
                        Button(action: {
                            for i in 0..<(self.arrSubRoutes.count ) {
                                self.arrSubRoutes[i].paymentMethod = offerRide.paymentMode
                            }
                            offerRide.arrSubRoutes?.removeAll()
                            offerRide.arrSubRoutes = self.arrSubRoutes
                            
                            if ((offerRide.arrSubRoutes?.count ?? 0) > 0 ){
                                isReturnRideCommingBack = true
                            }
                        }){
                            Text("Done")
                        }
                        .modifier(CustomTextM(fontName: UIFont.Poppins.regular.fontName, fontSize: 16, fontColor: Color.white))
                        .modifier(ButtonStyle(buttonHeight: 40, buttonColor: Color.init(hex: "00ADEF"), buttonRadius: 40))
                        .padding()
                        .padding()
                }.background(Color.init(hex: "F5F5F5"))
            }
        }.showNavigationBarWithBackStyle().onAppear(){
            self.getPriceMatric()
        }
    }
}

struct recommendedPriceView_Previews: PreviewProvider {
    static var previews: some View {
        recommendedPriceView()
    }
}
//infix operator /: MultiplicationPrecedence
//public func /<T: FixedWidthInteger>(lhs: T, rhs: T) -> Double {
//    return Double(lhs) / Double(rhs)
//}
