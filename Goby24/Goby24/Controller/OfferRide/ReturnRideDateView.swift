//
//  ReturnRideDateView.swift
//  Goby24
//
//  Created by Jignesh Bhensadadiya on 7/18/21.
//

import SwiftUI

struct ReturnRideDateView: View {
    @State var selection: Int? = nil
    @ObservedObject private var offerRide = OfferRideReturnRouteData.shared
    @ObservedObject private var offerRideAlready = OfferRideData.shared

    @State var places:[String] = [String]()

    //MARK: - PROPERTIES
    
    @State var selectedDate = Date()
    
    func updateRideData()  {
        // SET TIME AMD DATE FOR RETURN ROUTE
        offerRide.pickupDateAndTime = selectedDate
        
        for i in 0..<(self.offerRide.arrSubRoutes?.count ?? 0){
           
            let obj = self.offerRide.arrSubRoutes![i]
            
            // SWIPE PLACE ID
            let swipePlaceId = obj.fromPlaceId
            obj.fromPlaceId = obj.toPlaceId
            obj.toPlaceId = swipePlaceId
            
            let swipeLabel = obj.startLabel
            obj.startLabel = obj.endLabel
            obj.endLabel = swipeLabel

        

            obj.pickupDate = offerRide.pickupDateAndTime?.getFormattedDate(formatter: "y-MM-dd")
            obj.pickupTime = offerRide.pickupDateAndTime?.getFormattedDate(formatter: "HH:mm")

            
            let pickuptimeInterVal = getDoubleFromAny(offerRide.pickupDateAndTime!.timeIntervalSince1970).rounded()
            let durationvalue = getDoubleFromAny(obj.distanceValue ?? "") //self.matrix?.rows?[i].elements?[j].distance?.value ?? 0


//            let dropTime = Date(timeIntervalSince1970: Double(durationvalue) + pickuptimeInterVal)
            let dropDteTime = Date(timeIntervalSince1970: (Double(durationvalue) + pickuptimeInterVal))
//
            obj.dropDate = dropDteTime.getFormattedDate(formatter: "y-MM-dd")
            obj.dropTime = dropDteTime.getFormattedDate(formatter: "HH:mm")
//
//
            
            let swipeDetails = obj.startDetail
            obj.startDetail.mainText = obj.endDetail.mainText
            obj.startDetail.secondaryText = obj.endDetail.secondaryText
            
            obj.endDetail.mainText = swipeDetails?.mainText
            obj.endDetail.secondaryText = swipeDetails?.secondaryText
            
            
            self.offerRide.arrSubRoutes?[i] = obj
            
            
          //  arrSubRoutes.append(obj)
        }
    }
    func getReturnTime() -> Date{
        let calendar = Calendar.current
        let date = calendar.date(byAdding: .second, value: getIntegerFromAny(offerRideAlready.duration), to: offerRideAlready.pickedDate ?? Date())
        print(date)
        print(offerRideAlready.duration)
        return date!
    }
    var body: some View {
        ScrollView{
        VStack{
            
            Text(Localizable.When_are_you_going.localized()).padding(.top, 25).modifier(CustomTextM(fontName: UIFont.Rubik.medium.fontName, fontSize: 26, fontColor: Color.init(hex: "2E2E2E")))
            
            VStack(alignment: .leading) {
                Text("PICK DATE").padding(.top, 25).modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 18, fontColor: Color.init(hex: "2E2E2E")))
                
                // DatePicker("TEST", selection: $selectedDate)
               // var myDate = offerRide.pickedDaet ?? Date()
               //  myDate.addTimeInterval(60)
                //print(myDate)
                DatePicker("", selection: $selectedDate,in: getReturnTime()..., displayedComponents: .date)
                    .datePickerStyle(WheelDatePickerStyle())
                    .clipped()
                    .labelsHidden()
                //.frame(width: 200)
                
                Text("PICK TIME").padding(.top, 25).modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 18, fontColor: Color.init(hex: "2E2E2E")))
                
                // DatePicker("TEST", selection: $selectedDate)
                DatePicker("", selection: $selectedDate,in: getReturnTime()..., displayedComponents: .hourAndMinute)
                    
                    .datePickerStyle(WheelDatePickerStyle())
                    .clipped()
                    .labelsHidden()
                // .frame(width: 200)
                
            }.padding()
            Spacer()
            NavigationLink(destination: AnythingAboutRideView(), tag: 0, selection: $selection) {

            Button(action: {
                self.updateRideData()
                // UPDATE RETURN DATE AND TIME
                selection = 0
            }){
                Text(Localizable.Continue.localized())
            }
            .modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 16, fontColor: Color.white))
            .modifier(ButtonStyle(buttonHeight: 50, buttonColor: Color.init(hex: "00ADEF"), buttonRadius: 10))
            .padding()
            }.isDetailLink(false)
            
        }.showNavigationBarWithBackStyle().onDisappear(){
            offerRide.pickupDateAndTime = selectedDate
        }.onAppear(){
            offerRide.pickupDateAndTime = selectedDate
        }
        }
    }
}

struct ReturnRideDateView_Previews: PreviewProvider {
    static var previews: some View {
        ReturnRideDateView()
    }
}
