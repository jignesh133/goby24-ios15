//
//  returnRideComingBack.swift
//  Goby24
//
//  Created by Jignesh Bhensadadiya on 7/18/21.
//

import SwiftUI

struct returnRideComingBack: View {
    @State var selection: Int? = nil
    
    
    var body: some View {
        GeometryReader { geo in
            
            VStack{
                VStack{
                    Text("Coming back as well? Publish your return ride now!").modifier(CustomTextM(fontName: UIFont.Rubik.medium.fontName, fontSize: 26, fontColor: Color.init(hex: "2E2E2E"))).multilineTextAlignment(.center).padding()
                }.frame(width: UIScreen.main.bounds.size.width , height: 200).background(Color.white)
                
                
                VStack{
                    VStack{
                        //Button
                        NavigationLink(destination: SelectReturnRoute(), tag: 0, selection: $selection) {
                            Button(action: {
                                selection = 0
                            }){
                                ZStack() {
                                    HStack() {
                                        Text("Yes, sure").modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 16, fontColor: Color.init(hex: "2E2E2E"))).padding(.horizontal)
                                        
                                        Spacer()
                                        Image("next").padding(.trailing,20)
                                    }
                                }
                            }
                            .modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 16, fontColor: Color.init(hex: "2E2E2E")))
                            .modifier(ButtonStyle(buttonHeight: 60, buttonColor: Color.white, buttonRadius: 40))
                            .padding().padding(.top)
                            
                        }.isDetailLink(false)
                    }
                    VStack{
                        //Button
                        NavigationLink(destination: AnythingAboutRideView(), tag: 1, selection: $selection) {
                            Button(action: {
                                selection = 1
                            }){
                                ZStack() {
                                    HStack() {
                                        Text(Localizable.Ill_publish_my_return_ride_later.localized()).modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 16, fontColor: Color.init(hex: "2E2E2E"))).padding(.horizontal)
                                        
                                        Spacer()
                                        Image("next").padding(.trailing,20)
                                    }
                                }
                            }
                            .modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 16, fontColor: Color.init(hex: "2E2E2E")))
                            .modifier(ButtonStyle(buttonHeight: 60, buttonColor: Color.white, buttonRadius: 40))
                            .padding()
                        }.isDetailLink(false)
                    }
                    Spacer()
                }.frame(width: UIScreen.main.bounds.size.width,height: geo.size.height ).background(Color.init(hex: "F5F5F5")).ignoresSafeArea()
                
                
            }
            
        }.showNavigationBarWithBackStyle()
    }
}

struct returnRideComingBack_Previews: PreviewProvider {
    static var previews: some View {
        returnRideComingBack()
    }
}
