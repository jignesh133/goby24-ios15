//
//  OfferRideBoostView.swift
//  Goby24
//
//  Created by Jignesh Bhensadadiya on 7/17/21.
//

import SwiftUI

struct OfferRideBoostView: View {
    @State var isSelectroute:Bool = false
    @Environment(\.presentationMode) var presentationMode

    var body: some View {
        
        NavigationLink(destination: Selectroute(), isActive: $isSelectroute){EmptyView()}.isDetailLink(false)
        
        VStack{
            HStack{
                Spacer()
                VStack{
                    Button(action: {
                        self.presentationMode.wrappedValue.dismiss()
                    }){
                        Image("close")
                    }.padding([.top,.trailing],25)
                }
            }
            Spacer()
            Image("offerridecolor")
            VStack(spacing:20){
                Text(Localizable.Get_more_with_our_Boost_technology.localized()).padding(.top, 25).modifier(CustomTextM(fontName: UIFont.Rubik.medium.fontName, fontSize: 26, fontColor: Color.init(hex: "2E2E2E"))).multilineTextAlignment(.center)
                
                Text("Our Boost technology optimises your publication to get you extra passengers for part of your ride.").modifier(CustomTextM(fontName: UIFont.Poppins.regular.fontName, fontSize: 15, fontColor: Color.init(hex: "2E2E2E"))).multilineTextAlignment(.center)
                Text("Don't miss our Boost booking requests to save even more on travel costs!").modifier(CustomTextM(fontName: UIFont.Poppins.regular.fontName, fontSize: 15, fontColor: Color.init(hex: "2E2E2E"))).multilineTextAlignment(.center)
            }.padding(.horizontal,30).padding(.vertical,30).multilineTextAlignment(.center)
            VStack{
                Button(action: {
                    isSelectroute = true
                }){
                    Text("Got it!").frame(minWidth: 0,  maxWidth: .infinity)
                }
                .modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 16, fontColor: Color.white))
                .modifier(ButtonStyle(buttonHeight: 50, buttonColor: Color.init(hex: "00ADEF"), buttonRadius: 50))
                .padding()
                .buttonStyle(PlainButtonStyle())
                
                Button(action: {
                    isSelectroute = true
                }){
                    Text("Skip").frame(minWidth: 0,  maxWidth: .infinity)
                }
                .modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 16, fontColor: Color.white))
                .modifier(ButtonStyle(buttonHeight: 50, buttonColor: Color.init(hex: "F57F20"), buttonRadius: 50))
                .padding(.horizontal)
                .buttonStyle(PlainButtonStyle())
                
            }
            Spacer()
        }.hiddenNavigationBarStyle()
    }
}
    
    struct OfferRideBoostView_Previews: PreviewProvider {
        static var previews: some View {
            OfferRideBoostView()
        }
    }
