//
//  returnRouteMapView.swift
//  Goby24
//
//  Created by Jignesh Bhensadadiya on 7/18/21.
//

import SwiftUI
import MapKit

struct returnRouteMapView: View {
    @State var selection: Int? = nil
    @State var region = MKCoordinateRegion(center: CLLocationCoordinate2D(
    latitude: 53.8862, longitude: 8.6706),
    latitudinalMeters: 250, longitudinalMeters: 250 )

    var body: some View {
        VStack{
            Text("What is your return route ? ") .modifier(CustomTextM(fontName: UIFont.Rubik.medium.fontName, fontSize: 26, fontColor: Color.init(hex: "2E2E2E"))).padding()
            List{
                Group{
                    HStack{
                        Text("London Rd / A6") .modifier(CustomTextM(fontName: UIFont.Poppins.regular.fontName, fontSize: 16, fontColor: Color.init(hex: "2E2E2E")))
                        
                        Spacer()
                        Button(action: {}){
                            Image("selected")
                        }
                    }
                }.listSeparatorStyle(style: .none)
                Group{
                    HStack{
                        Text("London Rd / A6") .modifier(CustomTextM(fontName: UIFont.Poppins.regular.fontName, fontSize: 16, fontColor: Color.init(hex: "2E2E2E")))
                        
                        Spacer()
                        Button(action: {}){
                            Image("unselected")
                        }
                    }
                }.listSeparatorStyle(style: .none)
            }.padding().frame(maxHeight:150)
            
            NavigationLink(destination: AnythingAboutRideView(), tag: 0, selection: $selection) {

            Button(action: {
                selection = 0
            }){
                Text("Continue")
            }
            .modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 16, fontColor: Color.white))
            .modifier(ButtonStyle(buttonHeight: 50, buttonColor: Color.init(hex: "00ADEF"), buttonRadius: 10))
            .padding()
            }
                Map(coordinateRegion: $region)

        }.showNavigationBarWithBackStyle()
    }
}

struct returnRouteMapView_Previews: PreviewProvider {
    static var previews: some View {
        returnRouteMapView()
    }
}
