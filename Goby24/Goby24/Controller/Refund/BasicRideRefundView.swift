//
//  BasicRidesView.swift
//  Goby24
//
//  Created by Jignesh Bhensadadiya on 9/24/21.
//

import SwiftUI

struct BasicRideRefundView: View {
    @State var ridesTaken: Bool = false
    @State var isViewPresented: Bool = false

    var body: some View {
        VStack{
//            HStack{
//                Button(action: {
//                   // self.isViewPresented = true
//                }){
//                    Image("filter")
//                }.sheet(isPresented: self.$isViewPresented) {
//                    FilterRidesView()
//                }
//                Spacer()
//              //  Text(Localizable.My_Rides.localized()).modifier(CustomTextM(fontName: UIFont.Rubik.medium.fontName, fontSize: 26, fontColor: Color.init(hex: "2E2E2E")))
//
//                Spacer()
//                Button(action: {
//                    // SEARCH ICON
//                }){
//                    Image("search")
//                }
//
//
//            }.padding(.horizontal,30).padding(.vertical)
            
            HStack{
                //Button
                Button(action: {
                    ridesTaken = false
                }){
                    if(ridesTaken == false){
                        VStack{
                            Text(Localizable.Rides_Taken.localized()).modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 13, fontColor: Color.init(hex: "00AEEF")))
                            Divider().background(Color.init(hex: "00AEEF")).frame(height: 1.5)
                        }
                    }else{
                        VStack{
                            Text(Localizable.Rides_Taken.localized()).modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 13, fontColor: Color.init(hex: "2E2E2E")))
                            Divider().background(Color.init(hex: "2E2E2E")).frame(height: 1.5)
                            
                        }
                    }
                }.frame(minWidth: 0, maxWidth: .infinity)
                
                
                //ACCOUNT Button
                Button(action: {
                    ridesTaken = true
                }){
                    if(ridesTaken == true){
                        VStack{
                            Text(Localizable.Rides_Offered.localized()).modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 13, fontColor: Color.init(hex: "00AEEF")))
                            Divider().background(Color.init(hex: "00AEEF")).frame( height: 1.5)
                        }
                    }else{
                        VStack{
                            Text(Localizable.Rides_Offered.localized()).modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 13, fontColor: Color.init(hex: "2E2E2E")))
                            Divider().background(Color.init(hex: "2E2E2E")).frame( height: 1.5)
                            
                        }
                    }
                }.frame(minWidth: 0, maxWidth: .infinity)
            }.padding()
            
            
            if(ridesTaken == false){
                RidesTakenView()
            }else{
                RidesOfferedView()
            }
            Spacer()
        }.showNavigationBarWithBackStyle()

    }
}

struct BasicRideRefundView_Previews: PreviewProvider {
    static var previews: some View {
        BasicRideRefundView()
    }
}
