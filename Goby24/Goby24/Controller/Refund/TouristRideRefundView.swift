//
//  RefundView.swift
//  Goby24
//
//  Created by Jignesh Bhensadadiya on 11/30/21.
//

import SwiftUI
struct TouristRideRefundView: View {
    @State var isPackageTaken: Bool = false
    @State var isViewPresented: Bool = false

    var body: some View {
        VStack{
//            HStack{
//                Button(action: {
//                   // self.isViewPresented = true
//                }){
//                    Image("filter")
//                }.sheet(isPresented: self.$isViewPresented) {
//                    FilterRidesView()
//                }
//                Spacer()
//              //  Text(Localizable.My_Rides.localized()).modifier(CustomTextM(fontName: UIFont.Rubik.medium.fontName, fontSize: 26, fontColor: Color.init(hex: "2E2E2E")))
//
//                Spacer()
//                Button(action: {
//                    // SEARCH ICON
//                }){
//                    Image("search")
//                }
//
//
//            }.padding(.horizontal,30).padding(.vertical)
            
            HStack{
                //Button
                Button(action: {
                    isPackageTaken = false
                }){
                    if(isPackageTaken == false){
                        VStack{
                            Text("Package Taken").modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 13, fontColor: Color.init(hex: "00AEEF")))
                            Divider().background(Color.init(hex: "00AEEF")).frame(height: 1.5)
                        }
                    }else{
                        VStack{
                            Text("Package Taken").modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 13, fontColor: Color.init(hex: "2E2E2E")))
                            Divider().background(Color.init(hex: "2E2E2E")).frame(height: 1.5)
                            
                        }
                    }
                }.frame(minWidth: 0, maxWidth: .infinity)
                
                
                //ACCOUNT Button
                Button(action: {
                    isPackageTaken = true
                }){
                    if(isPackageTaken == true){
                        VStack{
                            Text("Package Offered").modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 13, fontColor: Color.init(hex: "00AEEF")))
                            Divider().background(Color.init(hex: "00AEEF")).frame( height: 1.5)
                        }
                    }else{
                        VStack{
                            Text("Package Offered").modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 13, fontColor: Color.init(hex: "2E2E2E")))
                            Divider().background(Color.init(hex: "2E2E2E")).frame( height: 1.5)
                            
                        }
                    }
                }.frame(minWidth: 0, maxWidth: .infinity)
            }.padding()
            
            
            if(isPackageTaken == false){
               // RidesOfferedView()
                TouristRide_PackageTakenView()
            }else{
               // RidesOfferedView()
                TouristRide_PackageOfferedView()
            }
            Spacer()
        }.showNavigationBarWithBackStyle()

    }
}
