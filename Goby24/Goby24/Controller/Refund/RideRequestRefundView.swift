//
//  RideRequestView.swift
//  Goby24
//
//  Created by Jignesh Bhensadadiya on 9/24/21.
//

import SwiftUI

struct RideRequestRefundView: View {
    @State var isTraveller: Bool = false
    @State var isViewPresented: Bool = false

    var body: some View {
        VStack{
            
            HStack{
                //Button
                Button(action: {
                    isTraveller = false
                }){
                    if(isTraveller == false){
                        VStack{
                            Text("Traveller").modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 13, fontColor: Color.init(hex: "00AEEF")))
                            Divider().background(Color.init(hex: "00AEEF")).frame(height: 1.5)
                        }
                    }else{
                        VStack{
                            Text("Traveller").modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 13, fontColor: Color.init(hex: "2E2E2E")))
                            Divider().background(Color.init(hex: "2E2E2E")).frame(height: 1.5)
                            
                        }
                    }
                }.frame(minWidth: 0, maxWidth: .infinity)
                
                
                //ACCOUNT Button
                Button(action: {
                    isTraveller = true
                }){
                    if(isTraveller == true){
                        VStack{
                            Text("Rider").modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 13, fontColor: Color.init(hex: "00AEEF")))
                            Divider().background(Color.init(hex: "00AEEF")).frame( height: 1.5)
                        }
                    }else{
                        VStack{
                            Text("Rider").modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 13, fontColor: Color.init(hex: "2E2E2E")))
                            Divider().background(Color.init(hex: "2E2E2E")).frame( height: 1.5)
                            
                        }
                    }
                }.frame(minWidth: 0, maxWidth: .infinity)
            }.padding()
            
            
            if(isTraveller == false){
                RideRequest_TravellerView()
            }else{
                RideRequest_RiderView()
            }
            Spacer()
        }.showNavigationBarWithBackStyle()

    }
}

//struct RideRequestListView_Previews: PreviewProvider {
//    static var previews: some View {
//        RideRequestView()
//    }
//}
