//
//  ContactUsView.swift
//  Goby24
//
//  Created by Jignesh Bhensadadiya on 7/21/21.
//

import SwiftUI

struct ContactUsView: View {
    @State var emailId:String = ""
    @State var comment:String = ""
    
    
    var body: some View {
        VStack{
            Spacer()
            Group{
                Text("Contact Us").modifier(CustomTextM(fontName: UIFont.Rubik.medium.fontName, fontSize: 26, fontColor: Color.init(hex: "2E2E2E")))
                
                CustomTextField(placeHolderText: "Email address ",
                                text: self.$emailId).padding(.bottom,20)
                //Button
                Button(action: {}){
                    HStack() {
                        Text("  Choose country")
                        Spacer()
                        Image("down").padding(.trailing,20)
                    }
                }
                .modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 16, fontColor: Color.init(hex: "2E2E2E")))
                .modifier(ButtonStyle(buttonHeight: 50, buttonColor: Color.init(hex: "F2F2F2"), buttonRadius: 60))
                .padding(.bottom,20)
                
                TextField(" Enter description here....", text: $comment).modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 16, fontColor: Color.black))
                    .textContentType(.none).frame( height: 126, alignment: .center).background(Color.init(hex: "F2F2F2")).cornerRadius(14)
            }.padding(.horizontal)
            
            Group{
                Button(action: {
                    print("CONFIRM BUTTON ACTION CLICKED")
                }){
                    Text("Continue")
                }
                .modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 16, fontColor: Color.white))
                .modifier(ButtonStyle(buttonHeight: 50, buttonColor: Color.init(hex: "00ADEF"), buttonRadius: 50))
                .padding()
                
            }
            Spacer()
            HStack{
                Spacer()
                Button(action: {
                    print("CONFIRM BUTTON ACTION CLICKED")
                }){
                    Image("callRound")
                }.padding(.trailing)
            }
            
            
        }.showNavigationBarWithBackStyle()
    }
}

struct ContactUsView_Previews: PreviewProvider {
    static var previews: some View {
        ContactUsView()
    }
}
