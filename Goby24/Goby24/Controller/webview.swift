//
//  webview.swift
//  Goby24
//
//  Created by Jignesh Bhensadadiya on 7/21/21.
//

import SwiftUI

struct paymentWebview: View {
    @State var urlString: String = "https://www.apple.com"
    let isFinished: (Bool) -> Void
    @State var bookingId: String = ""
    @State var isRatingView: Bool = false
    
    var body: some View {
        VStack{
            //NavigationLink(destination:ratingsListView(bookingid:self.bookingId), isActive: $isRatingView){ EmptyView() }.isDetailLink(false)

            SwiftUIWebview(url:URL(string: urlString)!,needToGoBack: { (value) in
                if (value == true){
                    if (self.isRatingView == false){
                        isRatingView = true
                        isFinished(true)
                    }
                }
            })
        }.showNavigationBarWithBackStyle()
    }
}

struct RequestRidepaymentWebview: View {
    @State var urlString: String = "https://www.apple.com"
    let isFinished: (Bool) -> Void
    @State var bookingId: String = ""
    
    var body: some View {
        VStack{
//            NavigationLink(destination:ratingsListView(bookingid:self.bookingId), isActive: $isRatingView){ EmptyView() }.isDetailLink(false)

            SwiftUIWebview(url:URL(string: urlString)!,needToGoBack: { (value) in
                if (value == true){
                    
                }
            })
        }.showNavigationBarWithBackStyle()
    }
}
struct OfferPackagePaymentWebview: View {
    @State var urlString: String = "https://www.apple.com"
    let isFinished: (Bool) -> Void
    @State var bookingId: String = ""
    
    var body: some View {
        VStack{
//            NavigationLink(destination:ratingsListView(bookingid:self.bookingId), isActive: $isRatingView){ EmptyView() }.isDetailLink(false)

            SwiftUIWebview(url:URL(string: urlString)!,needToGoBack: { (value) in
                if (value == true){
                    
                }
            })
        }.showNavigationBarWithBackStyle()
    }
}
struct webview: View {
    @State var urlString: String = "https://www.apple.com"

    var body: some View {
        VStack{
           
            SwiftUIWebview(url:URL(string: urlString)!,needToGoBack: { (value) in
                
            })
        }.showNavigationBarWithBackStyle()
    }
}


