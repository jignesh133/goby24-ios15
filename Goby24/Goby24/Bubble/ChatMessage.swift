//
//  ChatMessage.swift
//  Goby24
//
//  Created by Jignesh Bhensadadiya on 7/12/21.
//

import SwiftUI

struct ChatMessage: View{
    @State var messageText: String = ""
    
    var body: some View {
        VStack{
            VStack(){
                HStack{
                    VStack{
                        Image("userpic").padding(.leading,20)
                    }
                    VStack(alignment:.leading){
                        Text("Lorem ipsum dolor").modifier(CustomTextM(fontName: UIFont.Montserrat.semiBold.fontName, fontSize: 12, fontColor: Color.init(hex: "2E2E2E"))).lineLimit(1)
                        HStack{
                            Circle().fill(Color.init(hex: "16CB28")).frame(width: 5, height: 5, alignment: .center)
                            Text("Online").modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 8, fontColor: Color.init(hex: "2E2E2E"))).lineLimit(2)
                        }
                    }.padding(.leading,5)
                    Spacer()
                }
                HStack {
                    VStack { Divider().background(Color.gray) }.padding(.leading,20)  .layoutPriority(0)
                    Text("Wednesday, 24 March 2021").foregroundColor(Color.init(hex: "00AEEF")).lineLimit(1).modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 8, fontColor: Color.init(hex: "2E2E2E"))).frame(width: .infinity, height: .infinity)  .layoutPriority(1)
                    VStack { Divider().background(Color.gray) }.padding(.trailing,20)  .layoutPriority(0)
                }
               
                ScrollView{
                    BlueBubble()
                    BlueBubble()
                    GrayBubble()
                }.padding()

                Spacer()
            }
            
            ZStack {
                ScrollView(.vertical, showsIndicators: false) { }
                VStack {
                    Spacer()
                    HStack {
                        TextField("Message", text: $messageText).modifier(CustomTextM(fontName: UIFont.Poppins.regular.fontName, fontSize: 15, fontColor: Color.init(hex: "2E2E2E"))).padding(.leading,10)
                        
                        Button(action: {}) {
                            Image("pinlogo")
                        }
                        Button(action: {}) {
                            HStack{
                                Text("Send").modifier(CustomTextM(fontName: UIFont.Poppins.regular.fontName, fontSize: 15, fontColor: Color.white))
                                Image("send")
                            }.frame(width: 79, height: 31).background(Color.init(hex: "00AEEF")).cornerRadius(35).padding(.trailing,10)
                            
                        }.disabled(messageText.count == 0)
                    }
                    .padding([.top, .bottom], 10)
                    .background(Color.clear)
                    .overlay( RoundedRectangle(cornerRadius: 35).stroke(Color.init(hex: "707070"), lineWidth: 1))
                    
                    .padding([.leading, .trailing], 15)
                }
            }
            Spacer()
        }.showNavigationBarWithBackStyle()
    }
}

struct ChatMessage_Previews: PreviewProvider {
    static var previews: some View {
        ChatMessage()
    }
}
