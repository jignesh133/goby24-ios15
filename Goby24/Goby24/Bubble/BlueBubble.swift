//
//  BlueBubble.swift
//  Goby24
//
//  Created by Jignesh Bhensadadiya on 7/12/21.
//

import SwiftUI

struct BlueBubble: View {
    
    @State var messageText: String = "Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci"
    @State var time: String = "08:20"
    
    var body: some View {
        VStack(alignment:.trailing){
            HStack{
                Spacer()
                VStack(alignment:.trailing){
                    ZStack {
                        Image("Rectangleblue")
                            .renderingMode(.template)
                            .foregroundColor(Color.init(hex: "00AEEF"))
                        Text(messageText).modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 11, fontColor: Color.white))
                            .foregroundColor(.white)
                            .padding(.horizontal, 12)
                            .padding(.vertical,012)
                            .layoutPriority(1)
                    }.padding(.trailing,10)
                    
                    HStack{
                        Spacer()
                        Text(time).modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 8, fontColor: Color.init(hex: "2E2E2E")))
                    }.padding(.trailing,10)
                    Spacer()
                }
            }
            Spacer()
        }
    }
}

struct BlueBubble_Previews: PreviewProvider {
    static var previews: some View {
        BlueBubble()
    }
}
