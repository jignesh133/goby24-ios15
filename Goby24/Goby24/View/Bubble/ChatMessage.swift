//
//  ChatMessage.swift
//  Goby24
//
//  Created by Jignesh Bhensadadiya on 7/12/21.
//

import SwiftUI
import SDWebImageSwiftUI

struct ChatMessage: View{
    @State var objMessages:Model_ChatHome?
    @State var arrMessages:[Model_ChatDetailsItem] = [Model_ChatDetailsItem]()
    
    func getData(){
        WebAccess.getDataWith(_Url: (WebAccess.CHAT_DETAILS_HOME + "?chat_with=" + getStringFromAny(objMessages?.user?.id ?? ""))) { (result) in
            switch (result){
            case .Success(let _data):
                print(_data)
                do{
                    let result:[String:Any] = UtilsObjs.cleanJson(to: _data ) as? [String : Any] ?? [String:Any]()
                    print(result)
                    let data1 =  try JSONSerialization.data(withJSONObject: result["result"] ?? [] , options: JSONSerialization.WritingOptions.prettyPrinted)
                    let convertedString = String(data: data1, encoding: String.Encoding.utf8)
                    let jsonData = Data((convertedString?.utf8)!)
                    self.arrMessages = try JSONDecoder().decode([Model_ChatDetailsItem].self, from: jsonData)
                    self.listCount = self.arrMessages.count
                    NSLog("LOG")
                    
                }catch(let error){
                    print(error)
                }
                
                break
                
            case .Error(let msg):
                ShowAlert(title: APPNAME, msg: msg, view: UIApplication.getTopViewController()!)
                break
            }
        }
    }
    
    func sendData(){
        var param:[String:Any] = [String:Any]()
        param["receiver"] = getStringFromAny(objMessages?.user?.id ?? "")
        param["message"] = messageText
        
        WebAccess.postDataWith(_Url: WebAccess.CHAT_DETAILS_HOME, _parameters: param) { (result) in
            switch (result){
            case .Success(let _data):
                print(_data)
                messageText = ""
                self.getData()
                break
            case .Error(let msg):
                ShowAlert(title: APPNAME, msg: msg, view: UIApplication.getTopViewController()!)
                break
            }
        }
    }
    @State var messageText: String = ""
    @State var listCount : Int = 0

    var body: some View {
        
        VStack(){
            HStack{
                VStack{
                        let urlString = IMAGEURL + getStringFromAny(objMessages?.user?.profilePic ?? "")//(driverProfile.profilePic ?? "")
                         WebImage(url: URL.init(string: urlString ))
                        .placeholder(Image(systemName: "photo")) // Placeholder Image
                        .renderingMode(.original)
                        .resizable()
                        .aspectRatio(contentMode: .fill)
                        .frame(width: 40, height: 40)
                        .clipShape(Circle())
                        .overlay(Circle().stroke(Color.init(hex: "00AEEF"), lineWidth: 1))
                        .padding(.vertical)
                    
                }.padding(.leading,20)
                
                VStack(alignment:.leading){
                    HStack(spacing:1.5){
                        Text((objMessages?.user?.firstName ?? "")).modifier(CustomTextM(fontName: UIFont.Montserrat.semiBold.fontName, fontSize: 12, fontColor: Color.init(hex: "2E2E2E"))).lineLimit(1)
                        Text((objMessages?.user?.lastName ?? "")).modifier(CustomTextM(fontName: UIFont.Montserrat.semiBold.fontName, fontSize: 12, fontColor: Color.init(hex: "2E2E2E"))).lineLimit(1)
                        
                    }
                    //                        Text(objMessages?.sender?.firstName ?? "").modifier(CustomTextM(fontName: UIFont.Montserrat.semiBold.fontName, fontSize: 12, fontColor: Color.init(hex: "2E2E2E"))).lineLimit(1)
                    HStack{
                        Circle().fill(Color.init(hex: "16CB28")).frame(width: 5, height: 5, alignment: .center)
                        Text("Online").modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 8, fontColor: Color.init(hex: "2E2E2E"))).lineLimit(2)
                    }
                }.padding(.leading,5)
                Spacer()
            }
            HStack {
                VStack { Divider().background(Color.gray) }.padding(.leading,20)  .layoutPriority(0)
                // Text("Wednesday, 24 March 2021").foregroundColor(Color.init(hex: "00AEEF")).lineLimit(1).modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 8, fontColor: Color.init(hex: "2E2E2E"))).frame(width: .infinity, height: .infinity)  .layoutPriority(1)
                VStack { Divider().background(Color.gray) }.padding(.trailing,20)  .layoutPriority(0)
            }
            CustomScrollView(scrollToEnd: true){
                LazyVStack{
                    ForEach(0..<(arrMessages.count),id: \.self) { row in
                        VStack{
                            let objMessages:Model_ChatDetailsItem? = self.arrMessages[row]
                            if (getId() == getStringFromAny(objMessages?.sender?.id ?? 0)){
                                GrayBubble(objMessages: self.arrMessages[row])
                            }else{
                                BlueBubble(objMessages: self.arrMessages[row])
                            }
                        }
                    }
                }
            }
            
//            ScrollViewReader { scrollview in
//                ScrollView{
//                    ForEach(0..<(arrMessages.count),id: \.self) { row in
//                        VStack{
//                            let objMessages:Model_ChatDetailsItem? = self.arrMessages[row]
//
//                            if (getId() == getStringFromAny(objMessages?.sender?.id ?? 0)){
//                                GrayBubble(objMessages: self.arrMessages[row])
//                            }else{
//                                BlueBubble(objMessages: self.arrMessages[row])
//                            }
//                        }
//                        .id(row)  //Set the Id
//
//                    }
//                }
//                .onChange(of: listCount, perform: { value in
//                    DispatchQueue.main.async {
//                        scrollview.scrollTo(value)
//                    }
//                })
//            } .frame(maxHeight: .infinity)

            
            
            
            Spacer()

            /// BOTTIOM VIEW
            ZStack {
                VStack {
                    Spacer()
                    HStack {
                        TextField("Message", text: $messageText).modifier(CustomTextM(fontName: UIFont.Poppins.regular.fontName, fontSize: 15, fontColor: Color.init(hex: "2E2E2E"))).padding(.leading,10)
                        
//                        Button(action: {
//                            if(messageText.removeWhiteSpace().count > 0){
//                                self.sendData()
//                            }
//                        }) {
//                            Image("pinlogo")
//                        }
                        Button(action: {
                            if(messageText.removeWhiteSpace().count > 0){
                                self.sendData()
                            }
                        }) {
                            HStack{
                                Text("Send").modifier(CustomTextM(fontName: UIFont.Poppins.regular.fontName, fontSize: 15, fontColor: Color.white))
                                Image("send")
                            }.frame(width: 79, height: 31).background(Color.init(hex: "00AEEF")).cornerRadius(35).padding(.trailing,10)
                            
                        }.disabled(messageText.count == 0)
                    }
                    .padding([.top, .bottom], 10)
                    .background(Color.clear)
                    .overlay( RoundedRectangle(cornerRadius: 35).stroke(Color.init(hex: "707070"), lineWidth: 1))
                    
                    .padding([.leading, .trailing], 15)
                }
            }.frame(height: 75)
            
        }
        .showNavigationBarWithBackStyle().onAppear(){
            getData()
        }
    }
}

struct ChatMessage_Previews: PreviewProvider {
    static var previews: some View {
        ChatMessage()
    }
}


struct CustomScrollView<Content>: View where Content: View {
    var axes: Axis.Set = .vertical
    var reversed: Bool = false
    var scrollToEnd: Bool = false
    var content: () -> Content

    @State private var contentHeight: CGFloat = .zero
    @State private var contentOffset: CGFloat = .zero
    @State private var scrollOffset: CGFloat = .zero

    var body: some View {
        GeometryReader { geometry in
            if self.axes == .vertical {
                self.vertical(geometry: geometry)
            } else {
                // implement same for horizontal orientation
            }
        }
        .clipped()
    }

    private func vertical(geometry: GeometryProxy) -> some View {
        VStack {
            content()
        }
        .modifier(ViewHeightKey())
        .onPreferenceChange(ViewHeightKey.self) {
            self.updateHeight(with: $0, outerHeight: geometry.size.height)
        }
        .frame(height: geometry.size.height, alignment: (reversed ? .bottom : .top))
        .offset(y: contentOffset + scrollOffset)
        .animation(.easeInOut)
        .background(Color.white)
        .gesture(DragGesture()
            .onChanged { self.onDragChanged($0) }
            .onEnded { self.onDragEnded($0, outerHeight: geometry.size.height) }
        )
    }

    private func onDragChanged(_ value: DragGesture.Value) {
        self.scrollOffset = value.location.y - value.startLocation.y
    }

    private func onDragEnded(_ value: DragGesture.Value, outerHeight: CGFloat) {
        let scrollOffset = value.predictedEndLocation.y - value.startLocation.y

        self.updateOffset(with: scrollOffset, outerHeight: outerHeight)
        self.scrollOffset = 0
    }

    private func updateHeight(with height: CGFloat, outerHeight: CGFloat) {
        let delta = self.contentHeight - height
        self.contentHeight = height
        if scrollToEnd {
            self.contentOffset = self.reversed ? height - outerHeight - delta : outerHeight - height
        }
        if abs(self.contentOffset) > .zero {
            self.updateOffset(with: delta, outerHeight: outerHeight)
        }
    }

    private func updateOffset(with delta: CGFloat, outerHeight: CGFloat) {
        let topLimit = self.contentHeight - outerHeight

        if topLimit < .zero {
             self.contentOffset = .zero
        } else {
            var proposedOffset = self.contentOffset + delta
            if (self.reversed ? proposedOffset : -proposedOffset) < .zero {
                proposedOffset = 0
            } else if (self.reversed ? proposedOffset : -proposedOffset) > topLimit {
                proposedOffset = (self.reversed ? topLimit : -topLimit)
            }
            self.contentOffset = proposedOffset
        }
    }
}



struct ViewHeightKey: PreferenceKey {
    static var defaultValue: CGFloat { 0 }
    static func reduce(value: inout Value, nextValue: () -> Value) {
        value = value + nextValue()
    }
}

extension ViewHeightKey: ViewModifier {
    func body(content: Content) -> some View {
        return content.background(GeometryReader { proxy in
            Color.clear.preference(key: Self.self, value: proxy.size.height)
        })
    }
}
