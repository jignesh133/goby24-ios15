//
//  GrayBubble.swift
//  Goby24
//
//  Created by Jignesh Bhensadadiya on 7/12/21.
//

import SwiftUI

struct GrayBubble: View {
    
//    @State var messageText: String = "Neque porro quisquam est qui dolorem"
//    @State var time: String = "12:20"
//    @State var objMessages:Model_ChatHome?
    @State var objMessages:Model_ChatDetailsItem?

    var body: some View {
        VStack(alignment:.leading){
            HStack{
                VStack(alignment:.leading){
                    ZStack {
                        Image("Rectanglegray")
                            .renderingMode(.template)
                            .foregroundColor(Color.init(hex: "F3F3F3"))
                            .layoutPriority(-1)

                        Text((objMessages?.message ?? "")).modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 11, fontColor: Color.black))
                            .foregroundColor(.white)
                            .padding(12)
                            .layoutPriority(1)
                    }.padding(.leading,10)
                    
                    HStack{
                        let date =  ((objMessages?.timestamp ?? "").stringToDate(strCurrentFormat: "yyyy-MM-dd'T'HH:mm:ss").getFormattedDate(formatter: "dd MMM yyyy hh:mm:ss"))
                        Text(date).modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 8, fontColor: Color.init(hex: "2E2E2E")))
                        Spacer()
                    }.padding(.leading,10)
                    Spacer()
                }
                Spacer()
            }
            Spacer()
        }
    }
}

struct GrayBubble_Previews: PreviewProvider {
    static var previews: some View {
        GrayBubble()
    }
}
