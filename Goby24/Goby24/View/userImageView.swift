//
//  userImageView.swift
//  Goby24
//
//  Created by Jignesh Bhensadadiya on 7/23/21.
//

import SwiftUI
import SDWebImageSwiftUI

struct userImageView: View {
    let isBlueBorderNeed:Bool = false
    @State var isReload:Bool = false
    
    var body: some View {
        VStack{
            if isReload == true {
                
                let data = userDefault.value(forKey: Enum_Login.userProfileData.rawValue) as? [String:Any] ?? [:]
                let urlString = IMAGEURL + getStringFromAny(data["profile_pic"] ?? "")
                self.Print(urlString)
                
                WebImage(url: URL.init(string: urlString ))
                    .placeholder(Image(systemName: "photo")) // Placeholder Image
                    .renderingMode(.original)
                    .resizable()
                    .aspectRatio(contentMode: .fill)
                    .frame(width: 60, height: 60)
                    .clipShape(Circle())
                if (isBlueBorderNeed == true){
                    overlay(Circle().stroke(Color.init(hex: "00AEEF"), lineWidth: 1))
                }
                
            }else{
                let data = userDefault.value(forKey: Enum_Login.userProfileData.rawValue) as? [String:Any] ?? [:]
                let urlString = IMAGEURL + getStringFromAny(data["profile_pic"] ?? "")
                self.Print(urlString)
                
                WebImage(url: URL.init(string: urlString ))
                    .placeholder(Image(systemName: "photo")) // Placeholder Image
                    .renderingMode(.original)
                    .resizable()
                    .aspectRatio(contentMode: .fill)
                    .frame(width: 60, height: 60)
                    .clipShape(Circle())
                if (isBlueBorderNeed == true){
                    overlay(Circle().stroke(Color.init(hex: "00AEEF"), lineWidth: 1))
                }
            }
        }.onAppear(){
            self.isReload.toggle()
        }
    }
}

struct userImageView_Previews: PreviewProvider {
    static var previews: some View {
        userImageView()
    }
}
struct driverImageView: View {
    let strUrl:String
    let isBlueBorderNeed:Bool

    var body: some View {
        VStack{
            let urlString = (IMAGEURL + strUrl)
            self.Print(urlString)
            
            WebImage(url: URL.init(string: urlString ))
                .placeholder(Image(systemName: "photo"))
                .renderingMode(.original)
                .resizable()
                .aspectRatio(contentMode: .fill)
                .frame(width: 60, height: 60)
                .clipShape(Circle())
            if (isBlueBorderNeed == true){
                overlay(Circle().stroke(Color.init(hex: "00AEEF"), lineWidth: 1))
            }
            
            
        }
    }
}
