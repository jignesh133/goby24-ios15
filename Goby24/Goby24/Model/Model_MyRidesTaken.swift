//
//  Model_MyRidesTaken.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on August 8, 2021

import Foundation

struct Model_MyRidesTaken : Codable {
    
    let booking : Int?
    let currency : String?
    let date : String?
    let distance : String?
    let fare : String?
    let journeyTime : String?
    let rideStatus : String?
    let route : [String]?
    let routeId : Int?
    let time : String?
    let rider:Model_Rider?
    
    let isriderrated : Bool?
    let istravellerrated : Bool?
    let paymentMethod : String?
    
    let book_instantly : String?
    
    
    enum CodingKeys: String, CodingKey {
        case booking = "booking"
        case currency = "currency"
        case date = "date"
        case distance = "distance"
        case fare = "fare"
        case journeyTime = "journeyTime"
        case rideStatus = "rideStatus"
        case route = "route"
        case routeId = "route_id"
        case time = "time"
        case isriderrated = "is_rider_rated"
        case istravellerrated = "is_traveller_rated"
        case rider = "rider"
        case paymentMethod = "payment_method"
        
        case book_instantly = "book_instantly"
        
        
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        booking = try values.decodeIfPresent(Int.self, forKey: .booking)
        currency = try values.decodeIfPresent(String.self, forKey: .currency)
        date = try values.decodeIfPresent(String.self, forKey: .date)
        distance = try values.decodeIfPresent(String.self, forKey: .distance)
        fare = try values.decodeIfPresent(String.self, forKey: .fare)
        journeyTime = try values.decodeIfPresent(String.self, forKey: .journeyTime)
        rideStatus = try values.decodeIfPresent(String.self, forKey: .rideStatus)
        route = try values.decodeIfPresent([String].self, forKey: .route)
        routeId = try values.decodeIfPresent(Int.self, forKey: .routeId)
        time = try values.decodeIfPresent(String.self, forKey: .time)
        
        isriderrated = try values.decodeIfPresent(Bool.self, forKey: .isriderrated)
        istravellerrated = try values.decodeIfPresent(Bool.self, forKey: .istravellerrated) 
        
        rider = try values.decodeIfPresent(Model_Rider.self, forKey: .rider)
        paymentMethod = try values.decodeIfPresent(String.self, forKey: .paymentMethod)
        
        book_instantly = try values.decodeIfPresent(String.self, forKey: .book_instantly)
        
    }
    
}
struct Model_MyRidesOffered : Codable {
    
    let booking : Int?
    let currency : String?
    let date : String?
    let distance : String?
    let fare : String?
    let journeyTime : String?
    let rideStatus : String?
    let route : [String]?
    let routeId : Int?
    let time : String?
    let isriderrated : Bool?
    let istravellerrated : Bool?
    let rider:Model_Rider?
    
    let paymentMethod : String?
    
    let book_instantly : String?
    
    
    enum CodingKeys: String, CodingKey {
        case booking = "booking"
        case currency = "currency"
        case date = "date"
        case distance = "distance"
        case fare = "fare"
        case journeyTime = "journeyTime"
        case rideStatus = "rideStatus"
        case route = "route"
        case routeId = "route_id"
        case time = "time"
        
        case isriderrated = "is_rider_rated"
        case istravellerrated = "is_traveller_rated"
        
        case rider = "rider"
        case paymentMethod = "payment_method"
        
        case book_instantly = "book_instantly"
        
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        booking = try values.decodeIfPresent(Int.self, forKey: .booking)
        currency = try values.decodeIfPresent(String.self, forKey: .currency)
        date = try values.decodeIfPresent(String.self, forKey: .date)
        distance = try values.decodeIfPresent(String.self, forKey: .distance)
        fare = try values.decodeIfPresent(String.self, forKey: .fare)
        journeyTime = try values.decodeIfPresent(String.self, forKey: .journeyTime)
        rideStatus = try values.decodeIfPresent(String.self, forKey: .rideStatus)
        route = try values.decodeIfPresent([String].self, forKey: .route)
        routeId = try values.decodeIfPresent(Int.self, forKey: .routeId)
        time = try values.decodeIfPresent(String.self, forKey: .time)
        isriderrated = try values.decodeIfPresent(Bool.self, forKey: .isriderrated)
        istravellerrated = try values.decodeIfPresent(Bool.self, forKey: .istravellerrated)
        
        rider = try values.decodeIfPresent(Model_Rider.self, forKey: .rider)
        paymentMethod = try values.decodeIfPresent(String.self, forKey: .paymentMethod)
        
        book_instantly = try values.decodeIfPresent(String.self, forKey: .book_instantly)
        
    }
    
}



