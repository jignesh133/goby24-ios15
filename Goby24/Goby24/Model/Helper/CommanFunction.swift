//
//  CommanFunction.swift
//  Smart Fitness
//
//  Created by MCA 2 on 18/03/19.
//  Copyright © 2019 stratecore. All rights reserved.
//

import UIKit


func getName(isFullname:Bool = false) -> String {
    let data = userDefault.value(forKey: Enum_Login.userProfileData.rawValue) as? [String:Any] ?? [:]
    let firstName = getStringFromAny(data["first_name"] ?? "" as Any)
    let lastname = getStringFromAny(data["last_name"] ?? "" as Any)
    if isFullname == false{
        return firstName
    }else{
        return firstName + " " + lastname
    }
}
func getLName(isFullname:Bool = false) -> String {
    let data = userDefault.value(forKey: Enum_Login.userProfileData.rawValue) as? [String:Any] ?? [:]
    
    let firstName = getStringFromAny(data["first_name"] ?? "" as Any)
    let lastname = getStringFromAny(data["last_name"] ?? "" as Any)
    
    if isFullname == false{
        return lastname
    }else{
        return firstName + " " + lastname
    }
}
func getRating() -> String{
    let data = userDefault.value(forKey: Enum_Login.userProfileData.rawValue) as? [String:Any] ?? [:]
    let rider_rating = getStringFromAny(data["rider_rating"] as Any)
    return rider_rating
}
func getAboutMe() -> String{
    let data = userDefault.value(forKey: Enum_Login.userProfileData.rawValue) as? [String:Any] ?? [:]
    let about = getStringFromAny(data["about"] as Any)
    return about
}
func getEmailId() -> String{
    let data = userDefault.value(forKey: Enum_Login.userProfileData.rawValue) as? [String:Any] ?? [:]
    let email = getStringFromAny(data["email"] as Any)
    return email

}
func getId() -> String{
    let data = userDefault.value(forKey: Enum_Login.userProfileData.rawValue) as? [String:Any] ?? [:]
    let userid = data["id"]//"\(String(describing: ))"
    return getStringFromAny(userid ?? "")
}
func getUserIsRider() -> Bool{
    let data = userDefault.value(forKey: Enum_Login.userProfileData.rawValue) as? [String:Any] ?? [:]
    let is_rider = data["is_rider"] as? Bool ?? false
    return is_rider//getBoolFromAny(is_rider ?? false)
}
func getUserVehicleVerified() -> Bool{
    let data = userDefault.value(forKey: Enum_Login.userProfileData.rawValue) as? [String:Any] ?? [:]
    let is_verified = data["is_vehicle_verified"] as? Bool ?? false
    return is_verified//getBoolFromAny(is_rider ?? false)
}
func isEmailVerified() -> Bool{
    let data = userDefault.value(forKey: Enum_Login.userProfileData.rawValue) as? [String:Any] ?? [:]
    let is_email_verified = getBoolFromAny(data["is_email_verified"] as Any) ?? false
    return is_email_verified
}
func isIdentityVerified() -> Bool{
    let data = userDefault.value(forKey: Enum_Login.userProfileData.rawValue) as? [String:Any] ?? [:]
    let is_identity_verified = getBoolFromAny(data["is_identity_verified"] as Any) ?? false
    return is_identity_verified
}
func ismobilenoverified() -> Bool{
    let data = userDefault.value(forKey: Enum_Login.userProfileData.rawValue) as? [String:Any] ?? [:]
    let is_mobile_no_verified = getBoolFromAny(data["is_mobile_no_verified"] as Any) ?? false
    return is_mobile_no_verified
}

func getPhone() -> String{
    let data = userDefault.value(forKey: Enum_Login.userProfileData.rawValue) as? [String:Any] ?? [:]
    let mobile_no = getStringFromAny(data["mobile_no"] as Any)
    return mobile_no
}

// DEVICE CHECK
struct ScreenSize {
    static let width = UIScreen.main.bounds.size.width
    static let height = UIScreen.main.bounds.size.height
    static let frame = CGRect(x: 0, y: 0, width: ScreenSize.width, height: ScreenSize.height)
    static let maxWH = max(ScreenSize.width, ScreenSize.height)
}

// DEVICE CHECK
struct DeviceType {
    static let iPhone4orLess  = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.maxWH < 568.0
    static let iPhone5orSE    = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.maxWH == 568.0
    static let iPhone678      = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.maxWH == 667.0
    static let iPhone678p     = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.maxWH == 736.0
    static let iPhoneX        = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.maxWH == 812.0
    static let iPhoneXMAX     = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.maxWH == 896.0
}

// CHECK NOTCH DISPLAY
var hasTopNotch: Bool {
    if #available(iOS 11.0,  *) {
        return UIApplication.shared.delegate?.window??.safeAreaInsets.top ?? 0 > 20
    }
    return false
}

// CHECK NOTCH DISPLAY BOTTOM
var hasBottomSafeAreaInsets: Bool {
    if #available(iOS 11.0, tvOS 11.0, *) {
        return UIApplication.shared.delegate?.window??.safeAreaInsets.bottom ?? 0 > 0
    }
    return false
}


//MARK:- custom alert
func ShowAlertWithCompletation(title:String?,msg:String,view:UIViewController,completion: ((Bool) -> ())? = nil) {
    let alert = UIAlertController(title: title, message: msg, preferredStyle: .alert)
    let ok = UIAlertAction(title: "OK", style: .cancel) { (res) in
        completion!(true)
    }
    alert.addAction(ok)
    view.present(alert, animated: true, completion: nil)
}
func ShowAlert(title:String?,msg:String,view:UIViewController) {
    let alert = UIAlertController(title: title, message: msg, preferredStyle: .alert)
    let ok = UIAlertAction(title: "Ok", style: .cancel, handler: nil)
    alert.addAction(ok)
    view.present(alert, animated: true, completion: nil)
}

//MARK:- custom alert
func ShowAlertWithCompletation(title:String?,msg:String,view:UIViewController,buttonTitle:String,completion: ((Bool) -> ())? = nil) {
    let alert = UIAlertController(title: title, message: msg, preferredStyle: .alert)
    let ok = UIAlertAction(title:buttonTitle, style: .cancel) { (res) in
        completion!(true)
    }
    alert.addAction(ok)
    view.present(alert, animated: true, completion: nil)
}
func ShowAlert(title:String?,msg:String,view:UIViewController,buttonTitle:String) {
    let alert = UIAlertController(title: title, message: msg, preferredStyle: .alert)
    let ok = UIAlertAction(title: buttonTitle, style: .cancel, handler: nil)
    alert.addAction(ok)
    view.present(alert, animated: true, completion: nil)
}


func jsonToData(json: Any) -> Data? {
    do {
        return try JSONSerialization.data(withJSONObject: json, options: JSONSerialization.WritingOptions.prettyPrinted)
    } catch _ {
        //print(myJSONError)
    }
    return nil;
}
//MARK:- GET VALUE FROM ANY
func getStringFromAny(_ obj : Any = "") -> String {
    if let value = obj as? String{
        return value
    }
    else if obj is NSNull {
        return ""
    }else{
        return "\(obj)"
    }
}
func getIntegerFromAny(_ obj:Any) ->Int{
    if let value:NSString = obj as? NSString{
        if value.length <= 0{
            return 0
        }
        return value.integerValue
    }
    else if let value:Int = obj as? Int{
        return value
    }else if let value:NSNumber = obj as? NSNumber{
        return Int.init(truncating: value)
    }
    else{
        return 0
    }
}
func getFloatFromAny(_ obj:Any) ->Float{
    if let value:NSString = obj as? NSString{
        if value.length <= 0{
            return 0
        }
        return value.floatValue
    }
    else if let value:Float = obj as? Float{
        return value
    }
    else{
        return 0
    }
}

func getDoubleFromAny(_ obj:Any) ->Double{
    if let value:NSString = obj as? NSString{
        if value.length <= 0{
            return 0
        }
        return value.doubleValue
    }
    else if let value:Double = obj as? Double{
        return value
    }
    else if let value = obj as? Int{
        return Double(value)
    }
    else if let value = obj as? Float{
        return Double(value)
    }
    else{
        return 0
    }
}

func getRoundedDoubleFromAny(_ obj:Any) ->Double{
    if let value:NSString = obj as? NSString{
        if value.length <= 0{
            return 0
        }
        return value.doubleValue
    }
    else if let value:Double = obj as? Double{
        return value
    }
    else{
        return 0
    }
}

func getInt64FromAny(_ obj:Any) ->Int64{
    
    if let value:String = obj as? String , let newValue = Int64(value){
        return newValue
    } else if let value:Int64 = obj as? Int64{
        return value
    } else if let value:NSNumber = obj as? NSNumber{
        return Int64.init(truncating: value)
    } else if let value:Int = obj as? Int {
        return Int64(value)
    } else {
        return 0
    }
}
func getBoolFromAny(_ obj:Any)->Bool?{
    if let value:Int = obj as? Int {
        if (value == 0){
            return false
        }else{
            return true
        }
    }else if let value:Int64 = obj as? Int64{
        if (value == 0){
            return false
        }else{
            return true
        }
    }else if let value:String = obj as? String{
        if (value == "0"){
            return false
        }else if (value == ""){
            return false
        }
        else{
            return true
        }
    }
    return false
}
func convertToDictionary(text: String) -> [String: Any]? {
    if let data = text.data(using: .utf8) {
        do {
            return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
        } catch {
            //print(error.localizedDescription)
        }
    }
    return nil
}

func methodCalculateDifferentDates(startDate:String,endDate:String) -> Int{
    let start = startDate.stringToDate(strCurrentFormat: "yyyy-MM-dd")
    let end = endDate.stringToDate(strCurrentFormat: "yyyy-MM-dd")
    
    let secondsBetween: TimeInterval = end.timeIntervalSince(start)
    let numberOfDays: Int = Int(secondsBetween / 86400)
    return numberOfDays
}

extension String{
    func stringToDate(strCurrentFormat:String="yyyy-MM-dd") -> Date {
        let olDateFormatter = DateFormatter()
        if strCurrentFormat == "" {
            olDateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        }else{
            olDateFormatter.dateFormat = "yyyy-MM-dd"//"yyyy-MM-dd'T'HH:mm:ss"
        }
        let convertDateFormatter = DateFormatter()
        convertDateFormatter.dateFormat = "yyyy-MM-dd"
        let newDate = convertDateFormatter.date(from: self)
        return newDate ?? Date()
    }
    func getDateFromString(strCurrentFormat:String="yyyy-MM-dd") -> String {
        let olDateFormatter = DateFormatter()
        if strCurrentFormat == "" {
            olDateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        }
        
        let convertDateFormatter = DateFormatter()
        convertDateFormatter.dateFormat = "yyyy-MM-dd"
        let newDate = convertDateFormatter.date(from: self) ?? Date()
        let newDateString = convertDateFormatter.string(from: newDate)
        return newDateString
    }
}

extension Double {
    func clean(maximumFractionDigits: Int = 2) -> String {
        let s = String(format: "%.\(maximumFractionDigits)f", self)
        var offset = -maximumFractionDigits - 1
        for i in stride(from: 0, to: -maximumFractionDigits, by: -1) {
            if s[s.index(s.endIndex, offsetBy: i - 1)] != "0" {
                offset = i
                break
            }
        }
        return String(s[..<s.index(s.endIndex, offsetBy: offset)])
    }
}

