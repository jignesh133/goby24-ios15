//
//  Constant.swift
//  Smart Fitness
//
//  Created by MCA 2 on 15/03/19.
//  Copyright © 2019 stratecore. All rights reserved.
//

import UIKit

class Constant: NSObject {

}


let BASEURLSOCIAL          = "http://dev.api.goby24.ch/"
let BASEURL                = "http://dev.api.goby24.ch/api/"
let IMAGEURL               = ""


//let BASEURLSOCIAL          = "http://api.goby24.ch/"
//let BASEURL          = "http://api.goby24.ch/api/"
//let IMAGEURL   = "http://api.goby24.ch/"

let GOOGLE_API = "673594831440-s6kmencdckjpfb5peppckcenla3ahjs9.apps.googleusercontent.com"//"203486544721-r7510h3kplcvekfv3vpdcsr1dr15hvrh.apps.googleusercontent.com"
let GOOGLE_KEY = "AIzaSyD2NVhCGOQphMEo2ay2kBS15Whqki-hjHo"

enum ImageSource {
    case photoLibrary
    case camera
}
enum Json_Result <T>{
    case Success(T)
    case Error(String)
}
enum Json_Upload_Result <T>{
    case UploadWithSuccess(T)
    case UploadWithError(String)
}

enum languageCode:String{
    case English = "en"
    case Italian = "it"
    case German = "de"
    case French = "fr"
}

enum Localizable {
    
    /// HomePage
    static let HomePage = "HomePage"

    /// YOUR RIDE . YOUR CHOICE
    static let YOUR_RIDE_YOUR_CHOICE = "YOUR_RIDE_YOUR_CHOICE"

    /// Leaving from
    static let Leaving_from = "Leaving_from"

    /// Going to
    static let Going_to = "Going_to"

    /// Passenger
    static let Passenger = "Passenger"

    /// Passengers
    static let Passengers = "Passengers"

    /// Search
    static let Search = "Search"

    /// Driving in your car soon?
    static let Driving_in_your_car_soon = "Driving_in_your_car_soon"

    /// Lets make this your least expensive journey ever.
    static let Lets_make_this_your_least_expensive_journey_ever = "Lets_make_this_your_least_expensive_journey_ever"

    /// Offer a ride
    static let Offer_a_ride = "Offer_a_ride"

    /// Tourist packages
    static let Tourist_packages = "Tourist_packages"

    /// Go literally anywhere.
    static let Go_literally_anywhere = "Go_literally_anywhere"

    /// From anywhere.
    static let From_anywhere = "From_anywhere"

    /// Smart
    static let Smart = "Smart"

    /// With access to millions of journeys, you can quickly find people nearby travelling your way.
    static let With_access_to_millions_of_journeys_you_can_quickly_find = "With_access_to_millions_of_journeys_you_can_quickly_find"

    /// Simple
    static let Simple = "Simple"

    /// Enter your exact address to find the perfect ride. Choose who you’d like to travel with. And book!
    static let Enter_your_exact_address_to_find_the_perfect_ride_Choose = "Enter_your_exact_address_to_find_the_perfect_ride_Choose"

    /// Seamless
    static let Seamless = "Seamless"

    /// Get to your exact destination, without the hassle. No queues. No waiting around.
    static let Get_to_your_exact_destination_without_the_hassle_No_queues_No_waiting_around = "Get_to_your_exact_destination_without_the_hassle_No_queues_No_waiting_around"

    /// Where do you want to go?
    static let Where_do_you_want_to_go = "Where_do_you_want_to_go"

    /// Log Out
    static let Log_Out = "Log_Out"

    /// Login
    static let Login = "Login"

    /// Sign up
    static let Sign_up = "Sign_up"

    /// Dashboard
    static let Dashboard = "Dashboard"

    /// Your current level
    static let Your_current_level = "Your_current_level"

    /// What's a newbie?
    static let Whats_a_newbie = "Whats_a_newbie"

    /// Newbie
    static let Newbie = "Newbie"

    /// Intermediate
    static let Intermediate = "Intermediate"

    /// Experienced
    static let Experienced = "Experienced"

    /// Expert
    static let Expert = "Expert"

    /// Ambassador
    static let Ambassador = "Ambassador"

    /// Edit Profile
    static let Edit_Profile = "Edit_Profile"

    /// View Public Profile
    static let View_Public_Profile = "View_Public_Profile"

    /// Profile
    static let Profile = "Profile"

    /// Experience Level:
    static let Experience_Level = "Experience_Level"

    /// out of
    static let out_of = "out_of"

    /// Email is verified
    static let Email_is_verified = "Email_is_verified"

    /// Email is not verified
    static let Email_is_not_verified = "Email_is_not_verified"

    /// Phone is verified
    static let Phone_is_verified = "Phone_is_verified"

    /// Phone is not verified
    static let Phone_is_not_verified = "Phone_is_not_verified"

    /// ID is verified
    static let ID_is_verified = "ID_is_verified"

    /// ID is not verified
    static let ID_is_not_verified = "ID_is_not_verified"

    /// Car share preferences
    static let Car_share_preferences = "Car_share_preferences"

    /// Edit your car share preferences
    static let Edit_your_car_share_preferences = "Edit_your_car_share_preferences"

    /// Be a driver also
    static let Be_a_driver_also = "Be_a_driver_also"

    /// New Messages
    static let New_Messages = "New_Messages"

    /// See all messages
    static let See_all_messages = "See_all_messages"

    /// No new message available
    static let No_new_message_available = "No_new_message_available"

    /// Member Verification
    static let Member_Verification = "Member_Verification"

    /// Verify your Email
    static let Verify_your_Email = "Verify_your_Email"

    /// Verify your phone number
    static let Verify_your_phone_number = "Verify_your_phone_number"

    /// Verify your id
    static let Verify_your_id = "Verify_your_id"

    /// Id is verified
    static let Id_is_verified = "Id_is_verified"

    /// Verify your vehicle
    static let Verify_your_vehicle = "Verify_your_vehicle"

    /// Vehicle is verified
    static let Vehicle_is_verified = "Vehicle_is_verified"

    /// Complete your member verification
    static let Complete_your_member_verification = "Complete_your_member_verification"

    /// Member Activity
    static let Member_Activity = "Member_Activity"

    /// Member since:
    static let Member_since = "Member_since"

    /// years old
    static let years_old = "years_old"

    /// About You
    static let About_You = "About_You"

    /// Account
    static let Account = "Account"

    /// Add a mini bio
    static let Add_a_mini_bio = "Add_a_mini_bio"

    /// Write something about yourself
    static let Write_something_about_yourself = "Write_something_about_yourself"

    /// Save
    static let Save = "Save"

    /// Edit personal details
    static let Edit_personal_details = "Edit_personal_details"

    /// Personal Details
    static let Personal_Details = "Personal_Details"

    /// First Name
    static let First_Name = "First_Name"

    /// Last Name
    static let Last_Name = "Last_Name"

    /// Birthdate
    static let Birthdate = "Birthdate"

    /// Email
    static let Email = "Email"

    /// Phone
    static let Phone = "Phone"

    /// Gender
    static let Gender = "Gender"

    /// Edit travel preferences
    static let Edit_travel_preferences = "Edit_travel_preferences"

    /// Choose Travel Preferences
    static let Choose_Travel_Preferences = "Choose_Travel_Preferences"

    /// Chattiness
    static let Chattiness = "Chattiness"

    /// I'm a Chatbox
    static let Im_a_Chatbox = "Im_a_Chatbox"

    /// I'm chatty when I feel comfortable
    static let Im_chatty_when_I_feel_comfortable = "Im_chatty_when_I_feel_comfortable"

    /// I'm the quite type
    static let Im_the_quite_type = "Im_the_quite_type"

    /// Music
    static let Music = "Music"

    /// It's all about the playlist
    static let Its_all_about_the_playlist = "Its_all_about_the_playlist"

    /// I'll jam depending on the mood
    static let Ill_jam_depending_on_the_mood = "Ill_jam_depending_on_the_mood"

    /// Silence is golden
    static let Silence_is_golden = "Silence_is_golden"

    /// Smoking
    static let Smoking = "Smoking"

    /// I’m fine with smoking!
    static let Im_fine_with_smoking = "Im_fine_with_smoking"

    /// Cigarette breaks outside the car are ok
    static let Cigarette_breaks_outside_the_car_are_ok = "Cigarette_breaks_outside_the_car_are_ok"

    /// Please, no smoking in the car
    static let Please_no_smoking_in_the_car = "Please_no_smoking_in_the_car"

    /// Pets
    static let Pets = "Pets"

    /// I love pets. Woof!
    static let I_love_pets_Woof = "I_love_pets_Woof"

    /// I’ll travel with pets depending on the animal
    static let Ill_travel_with_pets_depending_on_the_animal = "Ill_travel_with_pets_depending_on_the_animal"

    /// Sorry, not a pet person
    static let Sorry_not_a_pet_person = "Sorry_not_a_pet_person"

    /// Verify your Govt. ID
    static let Verify_your_Govt_ID = "Verify_your_Govt_ID"

    /// Identity Verification
    static let Identity_Verification = "Identity_Verification"

    /// First, Verify your name
    static let First_Verify_your_name = "First_Verify_your_name"

    /// Lets verify your ID
    static let Lets_verify_your_ID = "Lets_verify_your_ID"

    /// Select the type of document you want to upload
    static let Select_the_type_of_document_you_want_to_upload = "Select_the_type_of_document_you_want_to_upload"

    /// Passport
    static let Passport = "Passport"

    /// Attach Front Page
    static let Attach_Front_Page = "Attach_Front_Page"

    /// Attach Back Page
    static let Attach_Back_Page = "Attach_Back_Page"

    /// Upload
    static let Upload = "Upload"

    /// NID Card
    static let NID_Card = "NID_Card"

    /// Verify ID
    static let Verify_ID = "Verify_ID"

    /// Verify your phone no
    static let Verify_your_phone_no = "Verify_your_phone_no"

    /// Enter phone no
    static let Enter_phone_no = "Enter_phone_no"

    /// Cancel
    static let Cancel = "Cancel"

    /// Please, enter the verification code we sent to your phone.
    static let Please_enter_the_verification_code_we_sent_to_your_phone = "Please_enter_the_verification_code_we_sent_to_your_phone"

    /// Resend the OTP
    static let Resend_the_OTP = "Resend_the_OTP"

    /// Submit OTP
    static let Submit_OTP = "Submit_OTP"

    /// Please, enter the verification code we sent to your email :
    static let Please_enter_the_verification_code_we = "Please_enter_the_verification_code_we"

    /// Add vehicle
    static let Add_vehicle = "Add_vehicle"

    /// Edit Vehicle
    static let Edit_Vehicle = "Edit_Vehicle"

    /// What is your licence plate number?
    static let What_is_your_licence_plate_number = "What_is_your_licence_plate_number"

    /// Choose Country
    static let Choose_Country = "Choose_Country"

    /// Enter Licence Plate/ Vehicle Number
    static let Enter_Licence_Plate_Vehicle_Number = "Enter_Licence_Plate_Vehicle_Number"

    /// Verify Car
    static let Verify_Car = "Verify_Car"

    /// Add Vehicle Information
    static let Add_Vehicle_Information = "Add_Vehicle_Information"

    /// What is your Car Brand?
    static let What_is_your_Car_Brand = "What_is_your_Car_Brand"

    /// Car Brand
    static let Car_Brand = "Car_Brand"

    /// Car Brand doesn't exist?
    static let Car_Brand_doesnt_exist = "Car_Brand_doesnt_exist"

    /// What is your Car Model?
    static let What_is_your_Car_Model = "What_is_your_Car_Model"

    /// Car Model
    static let Car_Model = "Car_Model"

    /// Car Model doesn't exist?
    static let Car_Model_doesnt_exist = "Car_Model_doesnt_exist"

    /// Registration Card
    static let Registration_Card = "Registration_Card"

    /// Choose File
    static let Choose_File = "Choose_File"

    /// No file chosen
    static let No_file_chosen = "No_file_chosen"

    /// Driving licence
    static let Driving_licence = "Driving_licence"

    /// Submit
    static let Submit = "Submit"

    /// Ratings given
    static let Ratings_given = "Ratings_given"

    /// Ratings
    static let Ratings = "Ratings"

    /// customers rating
    static let customers_rating = "customers_rating"

    /// Excellent
    static let Excellent = "Excellent"

    /// Very Good
    static let Very_Good = "Very_Good"

    /// Good
    static let Good = "Good"

    /// Disapointing
    static let Disapointing = "Disapointing"

    /// Very disapointing
    static let Very_disapointing = "Very_disapointing"

    /// Reviews
    static let Reviews = "Reviews"

    /// Show Details
    static let Show_Details = "Show_Details"

    /// Messages
    static let Messages = "Messages"

    /// Chat Members
    static let Chat_Members = "Chat_Members"

    /// Chat and Messages
    static let Chat_and_Messages = "Chat_and_Messages"

    /// Search here...
    static let Search_here = "Search_here"

    /// No Messages
    static let No_Messages = "No_Messages"

    /// Write something ...
    static let Write_something_ = "Write_something_"

    /// Change Password
    static let Change_Password = "Change_Password"

    /// Reset Your Password
    static let Reset_Your_Password = "Reset_Your_Password"

    /// Password
    static let Password = "Password"

    /// Current Password
    static let Current_Password = "Current_Password"

    /// Old Password
    static let Old_Password = "Old_Password"

    /// New Password
    static let New_Password = "New_Password"

    /// Update Password
    static let Update_Password = "Update_Password"

    /// Privacy Policy
    static let Privacy_Policy = "Privacy_Policy"

    /// Finance
    static let Finance = "Finance"

    /// Bank Payment
    static let Bank_Payment = "Bank_Payment"

    /// Transfer your Bank funds
    static let Transfer_your_Bank_funds = "Transfer_your_Bank_funds"

    /// Amount to pay
    static let Amount_to_pay = "Amount_to_pay"

    /// Add Bank Account
    static let Add_Bank_Account = "Add_Bank_Account"

    /// Enter Bank Details
    static let Enter_Bank_Details = "Enter_Bank_Details"

    /// Country
    static let Country = "Country"

    /// Choose your country
    static let Choose_your_country = "Choose_your_country"

    /// Currency
    static let Currency = "Currency"

    /// Choose Currency
    static let Choose_Currency = "Choose_Currency"

    /// Personal
    static let Personal = "Personal"

    /// Business
    static let Business = "Business"

    /// Account Details
    static let Account_Details = "Account_Details"

    /// Bank Name
    static let Bank_Name = "Bank_Name"

    /// Choose Bank Name
    static let Choose_Bank_Name = "Choose_Bank_Name"

    /// Bank Branch
    static let Bank_Branch = "Bank_Branch"

    /// Choose Bank Branch
    static let Choose_Bank_Branch = "Choose_Bank_Branch"

    /// Bank Code
    static let Bank_Code = "Bank_Code"

    /// Account holder name
    static let Account_holder_name = "Account_holder_name"

    /// Account Number
    static let Account_Number = "Account_Number"

    /// Write Account number
    static let Write_Account_number = "Write_Account_number"

    /// I agree to terms and conditions
    static let I_agree_to_terms_and_conditions = "I_agree_to_terms_and_conditions"

    /// I confirm the bank details above
    static let I_confirm_the_bank_details_above = "I_confirm_the_bank_details_above"

    /// Need help to fill the details?
    static let Need_help_to_fill_the_details = "Need_help_to_fill_the_details"

    /// you can add upto 2 accounts
    static let you_can_add_upto_2_accounts = "you_can_add_upto_2_accounts"

    /// Acc No:
    static let Acc_No = "Acc_No"

    /// Approved
    static let Approved = "Approved"

    /// Make Payment
    static let Make_Payment = "Make_Payment"

    /// Delete Account
    static let Delete_Account = "Delete_Account"

    /// Completed transaction
    static let Completed_transaction = "Completed_transaction"

    /// Close my account
    static let Close_my_account = "Close_my_account"

    /// Are you sure want to close your account?
    static let Are_you_sure_want_to_close_your_account = "Are_you_sure_want_to_close_your_account"

    /// Please tell us the reason for closing your account
    static let Please_tell_us_the_reason_for_closing_your_account = "Please_tell_us_the_reason_for_closing_your_account"

    /// Yes, I am Sure
    static let Yes_I_am_Sure = "Yes_I_am_Sure"

    /// No, Cancel it
    static let No_Cancel_it = "No_Cancel_it"

    /// My Rides
    static let My_Rides = "My_Rides"

    /// Rides Taken
    static let Rides_Taken = "Rides_Taken"

    /// Rides Offered
    static let Rides_Offered = "Rides_Offered"

    /// Filter
    static let Filter = "Filter"

    /// All Rides
    static let All_Rides = "All_Rides"

    /// Upcoming Ride
    static let Upcoming_Ride = "Upcoming_Ride"

    /// Rides Completed
    static let Rides_Completed = "Rides_Completed"

    /// Ride Completed
    static let Ride_Completed = "Ride_Completed"

    /// Rides Cancelled
    static let Rides_Cancelled = "Rides_Cancelled"

    /// Ride Cancelled
    static let Ride_Cancelled = "Ride_Cancelled"

    /// My Ride
    static let My_Ride = "My_Ride"

    /// No rides Available
    static let No_rides_Available = "No_rides_Available"

    /// Cash
    static let Cash = "Cash"

    /// Card
    static let Card = "Card"

    /// Payment Method
    static let Payment_Method = "Payment_Method"

    /// Booking
    static let Booking = "Booking"

    /// Instant
    static let Instant = "Instant"

    /// Distance
    static let Distance = "Distance"

    /// Duration
    static let Duration = "Duration"

    /// Journey Date
    static let Journey_Date = "Journey_Date"

    /// Start Time
    static let Start_Time = "Start_Time"

    /// Vehicle
    static let Vehicle = "Vehicle"

    /// Info not provided
    static let Info_not_provided = "Info_not_provided"

    /// Email Verification
    static let Email_Verification = "Email_Verification"

    /// Phone Verification
    static let Phone_Verification = "Phone_Verification"

    /// Cancel Ride
    static let Cancel_Ride = "Cancel_Ride"

    /// Why do you want to cancel the ride?
    static let Why_do_you_want_to_cancel_the_ride = "Why_do_you_want_to_cancel_the_ride"

    /// Personal Reason
    static let Personal_Reason = "Personal_Reason"

    /// Change of mind
    static let Change_of_mind = "Change_of_mind"

    /// Emergency situation
    static let Emergency_situation = "Emergency_situation"

    /// Want to book new ride
    static let Want_to_book_new_ride = "Want_to_book_new_ride"

    /// Payments & Refunds
    static let Payments__Refunds = "Payments__Refunds"

    /// Payments
    static let Payments = "Payments"

    /// Refund
    static let Refund = "Refund"

    /// Choose Payment Method
    static let Choose_Payment_Method = "Choose_Payment_Method"

    /// Credit Card
    static let Credit_Card = "Credit_Card"

    /// No cancelled rides available
    static let No_cancelled_rides_available = "No_cancelled_rides_available"

    /// Find a ride
    static let Find_a_ride = "Find_a_ride"

    /// When are you going
    static let When_are_you_going = "When_are_you_going"

    /// Continue
    static let Continue = "Continue"

    /// No Ride history...
    static let No_Ride_history = "No_Ride_history"

    /// Number of seats to book
    static let Number_of_seats_to_book = "Number_of_seats_to_book"

    /// Advanced Options
    static let Advanced_Options = "Advanced_Options"

    /// Smoker
    static let Smoker = "Smoker"

    /// Ride with pet
    static let Ride_with_pet = "Ride_with_pet"

    /// Price
    static let Price = "Price"

    /// Departure time
    static let Departure_time = "Departure_time"

    /// Amendities
    static let Amendities = "Amendities"

    /// Max 2 in the back
    static let Max_2_in_the_back = "Max_2_in_the_back"

    /// Instant approval
    static let Instant_approval = "Instant_approval"

    /// Filter by rating
    static let Filter_by_rating = "Filter_by_rating"

    /// Sort by
    static let Sort_by = "Sort_by"

    /// Price Increasing
    static let Price_Increasing = "Price_Increasing"

    /// Price Decreasing
    static let Price_Decreasing = "Price_Decreasing"

    /// Rating Increasing
    static let Rating_Increasing = "Rating_Increasing"

    /// Rating Decreasing
    static let Rating_Decreasing = "Rating_Decreasing"

    /// Time Increasing
    static let Time_Increasing = "Time_Increasing"

    /// Time Decreasing
    static let Time_Decreasing = "Time_Decreasing"

    /// rides available
    static let rides_available = "rides_available"

    /// View map
    static let View_map = "View_map"

    /// Total price for
    static let Total_price_for = "Total_price_for"

    /// passangers
    static let passangers = "passangers"

    /// Ask a question
    static let Ask_a_question = "Ask_a_question"

    /// Rides published
    static let Rides_published = "Rides_published"

    /// Report this member
    static let Report_this_member = "Report_this_member"

    /// Start Chatting with our rider
    static let Start_Chatting_with_our_rider = "Start_Chatting_with_our_rider"

    /// Ask the rider anything you want
    static let Ask_the_rider_anything_you_want = "Ask_the_rider_anything_you_want"

    /// Send
    static let Send = "Send"

    /// Card Payment Overview
    static let Card_Payment_Overview = "Card_Payment_Overview"

    /// Total price for 1 passangers
    static let Total_price_for_1_passangers = "Total_price_for_1_passangers"

    /// GOBY24 Services charge
    static let GOBY24_Services_charge = "GOBY24_Services_charge"

    /// Tax charge
    static let Tax_charge = "Tax_charge"

    /// Total charge
    static let Total_charge = "Total_charge"

    /// I agree to the terms and conditions
    static let I_agree_to_the_terms_and_conditions = "I_agree_to_the_terms_and_conditions"

    /// Pay
    static let Pay = "Pay"

    /// Profile Overview
    static let Profile_Overview = "Profile_Overview"

    /// Verification of phone no
    static let Verification_of_phone_no = "Verification_of_phone_no"

    /// Verification of email
    static let Verification_of_email = "Verification_of_email"

    /// Ride Overview
    static let Ride_Overview = "Ride_Overview"

    /// Cash Payment Overview
    static let Cash_Payment_Overview = "Cash_Payment_Overview"

    /// Pick-up
    static let Pick_up = "Pick_up"

    /// Drop-off
    static let Drop_off = "Drop_off"

    /// Whats your route?
    static let Whats_your_route = "Whats_your_route"

    /// Get more with our Boost technology
    static let Get_more_with_our_Boost_technology = "Get_more_with_our_Boost_technology"

    /// Add your preffered stopovers to aid Boost find extra passangers on your way.
    static let Add_your_preffered_stopovers_to_aid_Boost = "Add_your_preffered_stopovers_to_aid_Boost"

    /// Where do you prefer to meet extra passengers?
    static let Where_do_you_prefer_to_meet_extra_passengers = "Where_do_you_prefer_to_meet_extra_passengers"

    /// Add city
    static let Add_city = "Add_city"

    /// Sorry, the city is not on the route.
    static let Sorry_the_city_is_not_on_the_route = "Sorry_the_city_is_not_on_the_route"

    /// Overview of your city selection
    static let Overview_of_your_city_selection = "Overview_of_your_city_selection"

    /// These are the best places to stop in those cities. OK for you?
    static let These_are_the_best_places_to_stop_in_those = "These_are_the_best_places_to_stop_in_those"

    /// At what time will you pick passengers up?
    static let At_what_time_will_you_pick_passengers_up = "At_what_time_will_you_pick_passengers_up"

    /// Keep the middle seat empty so that your passengers are comfortable
    static let Keep_the_middle_seat_empty_so_that = "Keep_the_middle_seat_empty_so_that"

    /// No, I'll squeeze in 3
    static let No_Ill_squeeze_in_3 = "No_Ill_squeeze_in_3"

    /// Yes, sure!
    static let Yes_sure = "Yes_sure"

    /// So how many GoBy24 passengers can you take?
    static let So_how_many_GoBy24_passengers = "So_how_many_GoBy24_passengers"

    /// Can passengers book instantly?
    static let Can_passengers_book_instantly = "Can_passengers_book_instantly"

    /// Can passengers book instantly?
    static let Can_passengers_contact_instantly = "Can_passengers_contact_instantly"

    /// No, I’ll reply to each request myself
    static let No_Ill_reply_to_each_request_myself = "No_Ill_reply_to_each_request_myself"

    /// No, I’ll reply to each request myself
    static let No_i_DONT_LIKE_like_to_be_disturbed = "No_i_DONT_LIKE_like_to_be_disturbed"

    
    /// This is our recommended price per seat. OK for you?
    static let This_is_our_recommended_price_per_seat_OK_for_you = "This_is_our_recommended_price_per_seat_OK_for_you"

    /// I want Cash money
    static let I_want_Cash_money = "I_want_Cash_money"

    /// I want money in my Card
    static let I_want_money_in_my_Card = "I_want_money_in_my_Card"

    /// No, I’ll change the price
    static let No_Ill_change_the_price = "No_Ill_change_the_price"

    /// My recommended price per seat
    static let My_recommended_price_per_seat = "My_recommended_price_per_seat"

    /// Done
    static let Done = "Done"

    /// Coming back as well? Publish your return ride now!
    static let Coming_back_as_well_Publish_your_return_ride_now = "Coming_back_as_well_Publish_your_return_ride_now"

    /// I’ll publish my return ride later
    static let Ill_publish_my_return_ride_later = "Ill_publish_my_return_ride_later"

    /// What is your return route ?
    static let What_is_your_return_route_ = "What_is_your_return_route_"

    /// Anything to add about your ride?
    static let Anything_to_add_about_your_ride = "Anything_to_add_about_your_ride"

    /// Publish ride
    static let Publish_ride = "Publish_ride"

    /// Your Rides
    static let Your_Rides = "Your_Rides"

    /// Want to see details or cancel a ride?
    static let Want_to_see_details_or_cancel_a_ride = "Want_to_see_details_or_cancel_a_ride"

    /// Tourist Package
    static let Tourist_Package = "Tourist_Package"

    /// Tourist Package offers
    static let Tourist_Package_offers = "Tourist_Package_offers"

    /// Request tourist package
    static let Request_tourist_package = "Request_tourist_package"

    /// Add tourist package
    static let Add_tourist_package = "Add_tourist_package"

    /// Days
    static let Days = "Days"

    /// Min price
    static let Min_price = "Min_price"

    /// Max price
    static let Max_price = "Max_price"

    /// No fo tourists
    static let No_fo_tourists = "No_fo_tourists"

    /// Choose country
    static let Choose_country = "Choose_country"

    /// Choose your city
    static let Choose_your_city = "Choose_your_city"

    /// Request from tourist
    static let Request_from_tourist = "Request_from_tourist"

    /// Create Request
    static let Create_Request = "Create_Request"

    /// Choose an existing tourist spot
    static let Choose_an_existing_tourist_spot = "Choose_an_existing_tourist_spot"

    /// Add a new spot
    static let Add_a_new_spot = "Add_a_new_spot"

    /// Start journey from
    static let Start_journey_from = "Start_journey_from"

    /// End journey to
    static let End_journey_to = "End_journey_to"

    /// Journey Start date
    static let Journey_Start_date = "Journey_Start_date"

    /// Journey End date
    static let Journey_End_date = "Journey_End_date"

    /// No of passengers
    static let No_of_passengers = "No_of_passengers"

    /// Package Budget
    static let Package_Budget = "Package_Budget"

    /// Write description about the tourist package
    static let Write_description_about_the_tourist_package = "Write_description_about_the_tourist_package"

    /// Submit Request
    static let Submit_Request = "Submit_Request"

    /// All Requests
    static let All_Requests = "All_Requests"

    /// Requests from tourist
    static let Requests_from_tourist = "Requests_from_tourist"

    /// ID
    static let ID = "ID"

    /// Tourist Spot
    static let Tourist_Spot = "Tourist_Spot"

    /// Start Location
    static let Start_Location = "Start_Location"

    /// End Location
    static let End_Location = "End_Location"

    /// Start Date
    static let Start_Date = "Start_Date"

    /// End Date
    static let End_Date = "End_Date"

    /// Budget
    static let Budget = "Budget"

    /// Edit Request
    static let Edit_Request = "Edit_Request"

    /// Delete Request
    static let Delete_Request = "Delete_Request"

    /// Details
    static let Details = "Details"

    /// Accept Request
    static let Accept_Request = "Accept_Request"

    /// Contact tourist
    static let Contact_tourist = "Contact_tourist"

    /// Already Accepted
    static let Already_Accepted = "Already_Accepted"

    /// I am only a driver
    static let I_am_only_a_driver = "I_am_only_a_driver"

    /// Choose Tourist Spot
    static let Choose_Tourist_Spot = "Choose_Tourist_Spot"

    /// Choose no of passengers
    static let Choose_no_of_passengers = "Choose_no_of_passengers"

    /// Choose number of days you want to spent
    static let Choose_number_of_days_you_want_to_spent = "Choose_number_of_days_you_want_to_spent"

    /// Choose your available date range
    static let Choose_your_available_date_range = "Choose_your_available_date_range"

    /// Choose your available time range
    static let Choose_your_available_time_range = "Choose_your_available_time_range"

    /// I am also a guide
    static let I_am_also_a_guide = "I_am_also_a_guide"

    /// Name of the package
    static let Name_of_the_package = "Name_of_the_package"

    /// Package Description
    static let Package_Description = "Package_Description"

    /// Choose city
    static let Choose_city = "Choose_city"

    /// Select Region
    static let Select_Region = "Select_Region"

    /// Upload Banner Image
    static let Upload_Banner_Image = "Upload_Banner_Image"

    /// Upload Cover Image
    static let Upload_Cover_Image = "Upload_Cover_Image"

    /// Footer
    static let Footer = "Footer"

    /// About Us
    static let About_Us = "About_Us"

    /// Contact Us
    static let Contact_Us = "Contact_Us"

    /// How it works
    static let How_it_works = "How_it_works"

    /// Frequently asked questions
    static let Frequently_asked_questions = "Frequently_asked_questions"

    /// Help
    static let Help = "Help"

    /// Terms & Conditions
    static let Terms__Conditions = "Terms__Conditions"

    /// Completed Transactions
    static let Completed_Transactions = "Completed_Transactions"

    /// Showing 5 completed transactions
    static let Showing_5_completed_transactions = "Showing_5_completed_transactions"

    /// Based on your preferences
    static let Based_on_your_preferences = "Based_on_your_preferences"

    /// Date
    static let Date = "Date"

    /// Rider's Name
    static let Riders_Name = "Riders_Name"

    /// Payment_method
    static let Payment_method = "Payment_method"

    /// Fare
    static let Fare = "Fare"

    /// Status
    static let Status = "Status"

    /// More
    static let More = "More"

    /// Phone Verified
    static let Phone_Verified = "Phone_Verified"

    /// Phone not verified
    static let Phone_not_verified = "Phone_not_verified"

    /// Rash Driving
    static let Rash_Driving = "Rash_Driving"

    /// Bad Behaviour
    static let Bad_Behaviour = "Bad_Behaviour"

    /// Fraud
    static let Fraud = "Fraud"

    /// Very Late
    static let Very_Late = "Very_Late"

    /// Confirm
    static let Confirm = "Confirm"

    /// Your offer will be confirmed by the driver
    static let Your_offer_will_be_confirmed_by_the_driver = "Your_offer_will_be_confirmed_by_the_driver"

    /// Contact the driver
    static let Contact_the_driver = "Contact_the_driver"

    /// Check the queue
    static let Check_the_queue = "Check_the_queue"

    /// Add your preferred stopovers to help Boost find extra passengers on your way.
    static let Add_your_preferred_stopovers_to_help_Boost_find = "Add_your_preferred_stopovers_to_help_Boost_find"

    /// Ride is successfully published
    static let Ride_is_successfully_published = "Ride_is_successfully_published"

    /// Ride cancelled successfully by Rider
    static let Ride_cancelled_successfully_by_Rider = "Ride_cancelled_successfully_by_Rider"

    /// You can't cancel your ride from here
    static let You_cant_cancel_your_ride_from_here = "You_cant_cancel_your_ride_from_here"

    /// No Tourist Package Offers available...
    static let No_Tourist_Package_Offers_available = "No_Tourist_Package_Offers_available"

    /// Add Tourist Package
    static let Add_Tourist_Package = "Add_Tourist_Package"

    /// Please contact with company to add more passengers
    static let Please_contact_with_company_to_add_more_passengers = "Please_contact_with_company_to_add_more_passengers"

    /// Tourist (Max)
    static let Tourist_Max = "Tourist_Max"

    /// Only
    static let Only = "Only"

    /// Tourist information
    static let Tourist_information = "Tourist_information"

    /// End Journey to
    static let End_Journey_to = "End_Journey_to"

    /// Number of Tourists
    static let Number_of_Tourists = "Number_of_Tourists"

    /// Loading...
    static let Loading = "Loading"

    /// Add
    static let Add = "Add"

    /// Add New Spot
    static let Add_New_Spot = "Add_New_Spot"

    /// Descriptions
    static let Descriptions = "Descriptions"

    /// Write a new spot name
    static let Write_a_new_spot_name = "Write_a_new_spot_name"

    /// Request Accepted Successfully
    static let Request_Accepted_Successfully = "Request_Accepted_Successfully"

    /// Request Acceptance Failed
    static let Request_Acceptance_Failed = "Request_Acceptance_Failed"

    /// Start
    static let Start = "Start"

    /// End
    static let End = "End"

    /// Package Name
    static let Package_Name = "Package_Name"

    /// Package Details
    static let Package_Details = "Package_Details"

    /// Package Price
    static let Package_Price = "Package_Price"

    /// Update
    static let Update = "Update"

    /// Edit
    static let Edit = "Edit"

    /// Accept
    static let Accept = "Accept"

    /// Delete
    static let Delete = "Delete"

    /// Contact
    static let Contact = "Contact"

    /// Start Chatting with our tourist
    static let Start_Chatting_with_our_tourist = "Start_Chatting_with_our_tourist"

    /// Ask the tourist anything you want
    static let Ask_the_tourist_anything_you_want = "Ask_the_tourist_anything_you_want"

    /// is an online platform connecting carpoolers in Switzerland and abroad by promoting optimized journeys and on-demand shared transportation with an easy-to-use application. Our iOS and Android apps allow you to conveniently arrange ridesharing, schedule trips and share the cost of the journey.
    static let is_an_online_platform_connecting_carpoolers = "is_an_online_platform_connecting_carpoolers"

    /// Goby24 for travelers
    static let Goby24_for_travelers = "Goby24_for_travelers"

    /// offers unique touristic itineraries to discover a city or a region at low cost. Travel through Schengen countries, book a ride with a local, visit new places and enjoy sharing knowledge with other travelers.
    static let offers_unique_touristic_itineraries_to_discover_a_city_or_a_region_at_low = "offers_unique_touristic_itineraries_to_discover_a_city_or_a_region_at_low"

    /// Our Missions
    static let Our_Missions = "Our_Missions"

    /// A win-win service
    static let A_win_win_service = "A_win_win_service"

    /// allowing travelers to save money and drivers to reduce the cost of their journeys by putting their empty seats to work.
    static let allowing_travelers_to_save_money_and_drivers_to_reduce_the = "allowing_travelers_to_save_money_and_drivers_to_reduce_the"

    /// Sharing.
    static let Sharing = "Sharing"

    /// No more dull journeys! Carpooling creates a social bond that offers the opportunity to meet new people.
    static let No_more_dull_journeys_Carpooling = "No_more_dull_journeys_Carpooling"

    /// Promoting sustainable mobility
    static let Promoting_sustainable_mobility = "Promoting_sustainable_mobility"

    /// and participating actively in reducing of our carbon footprint. Ridesharing is an eco-friendly way of commuting.
    static let and_participating_actively_in_reducing_of_our = "and_participating_actively_in_reducing_of_our"

    /// Greater flexibility
    static let Greater_flexibility = "Greater_flexibility"

    /// with on-demand shared transport. By allowing users to select a route and a schedule, Goby24 offers a greater flexibility than public transportation. It also gives the possibility for people who do not have a driving license to get around easily and inexpensively.
    static let with_on_demand_shared_transport_By_allowing_users_to_select_a_route_and_a_schedule_Goby24_offers = "with_on_demand_shared_transport_By_allowing_users_to_select_a_route_and_a_schedule_Goby24_offers"

    /// Together for a better future
    static let Together_for_a_better_future = "Together_for_a_better_future"

    /// At
    static let At = "At"

    /// we listen to our users and take all feedback into account to improve our platform and our offers. Do not hesitate to contact us to share your experience.
    static let we_listen_to_our_users_and_take_all_feedback_into_account_to_improve_our = "we_listen_to_our_users_and_take_all_feedback_into_account_to_improve_our"

    /// The Saving App for Savvy Travelers
    static let The_Saving_App_for_Savvy_Travelers = "The_Saving_App_for_Savvy_Travelers"

    /// Add New Vehicle Brand?
    static let Add_New_Vehicle_Brand = "Add_New_Vehicle_Brand"

    /// Add a new vehicle brand that doesn't exist in the dropdown
    static let Add_a_new_vehicle_brand_that_doesnt_exist_in_the_dropdown = "Add_a_new_vehicle_brand_that_doesnt_exist_in_the_dropdown"

    /// Add New Vehicle Model?
    static let Add_New_Vehicle_Model = "Add_New_Vehicle_Model"

    /// Add a new vehicle Model that doesn't exist in the dropdown
    static let Add_a_new_vehicle_Model_that_doesnt_exist_in_the_dropdown = "Add_a_new_vehicle_Model_that_doesnt_exist_in_the_dropdown"

    /// Booked
    static let Booked = "Booked"

    /// Please upload necessary images
    static let Please_upload_necessary_images = "Please_upload_necessary_images"

    /// Successfully uploaded vehicle informations
    static let Successfully_uploaded_vehicle_informations = "Successfully_uploaded_vehicle_informations"

}
