//
//  Helper.swift
//  Smart Fitness
//
//  Created by MCA 2 on 18/03/19.
//  Copyright © 2019 stratecore. All rights reserved.
//


import UIKit
import SDWebImage

// MARK: - UIApplication
extension Date {
    func getFormattedDate(formatter:String? ) -> String{
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = formatter
        dateFormatterPrint.locale = NSLocale.current
        return dateFormatterPrint.string(from: self);
    }
}
extension UIApplication {
    class func getTopViewController(base: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        
        if let nav = base as? UINavigationController {
            return getTopViewController(base: nav.visibleViewController)
            
        } else if let tab = base as? UITabBarController, let selected = tab.selectedViewController {
            return getTopViewController(base: selected)
            
        } else if let presented = base?.presentedViewController {
            return getTopViewController(base: presented)
        }
//        else if let sidemenu = base as? SideMenuController{
//            if let centerVC = sidemenu.contentViewController as? UINavigationController{
//                return centerVC.viewControllers.last
//            }
//            return sidemenu.contentViewController
//        }
        return base
    }
}
// MARK: - Encodable
extension Encodable {
    func asDictionary() throws -> [String: Any] {
        let data = try JSONEncoder().encode(self)
        guard let dictionary = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String: Any] else {
            throw NSError()
        }
        return dictionary
    }
    func asString() throws -> String{
        let encoder = JSONEncoder()
        encoder.outputFormatting = .prettyPrinted
        let data = try! encoder.encode(self)
        guard let str = String(data: data, encoding: .utf8) else {
            throw NSError()
        }
        return str
    }
}


// MARK: - CALayer
extension CALayer {
    func addBorderTo(edge: UIRectEdge, color: UIColor, thickness: CGFloat) {
        let border = CALayer();
        switch edge {
        case UIRectEdge.top:
            border.frame = CGRect(x: 0, y: 0, width: self.frame.width, height: thickness)
            break
        case UIRectEdge.bottom:
            border.frame = CGRect(x:0, y:self.frame.height - thickness, width:self.frame.width, height:thickness)
            break
        case UIRectEdge.left:
            border.frame = CGRect(x:0, y:0, width: thickness, height: self.frame.height)
            break
        case UIRectEdge.right:
            border.frame = CGRect(x:self.bounds.width - thickness, y: 0, width: thickness, height:self.frame.height)
            break
        default:
            break
        }
        border.backgroundColor = color.cgColor;
        
        self.addSublayer(border)
    }
    func addBorderToBounds(edge: UIRectEdge, color: UIColor, thickness: CGFloat) {
        
        let border = CALayer();
        
        switch edge {
        case UIRectEdge.top:
            border.frame = CGRect(x: 0, y: 0, width: self.bounds.width, height: thickness)
            break
        case UIRectEdge.bottom:
            border.frame = CGRect(x:0, y:self.bounds.height - thickness, width:self.bounds.width, height:thickness)
            break
        case UIRectEdge.left:
            border.frame = CGRect(x:0, y:0, width: thickness, height: self.bounds.height)
            break
        case UIRectEdge.right:
            border.frame = CGRect(x:self.bounds.width - thickness, y: 0, width: thickness, height:self.bounds.height)
            break
        default:
            break
        }
        
        border.backgroundColor = color.cgColor;
        
        self.addSublayer(border)
    }
    
}
// MARK: - UIView
extension UIView {
    func methodCenterLocationInWindow()->CGPoint{
        if let window = UIApplication.shared.windows.first {
            let locationInWindow : CGPoint = self.convert(self.bounds.origin, to: window)
            return locationInWindow
        }
        return CGPoint.init()
    }
    func methodLocationInWindow()->CGRect{
        if let window = UIApplication.shared.windows.first {
            let locationInWindow : CGRect = self.convert(self.bounds, to: window)
            return locationInWindow
        }
        return CGRect.init()
    }
    func methodSetCorner(val:CGFloat){
        self.layer.cornerRadius = val
        self.clipsToBounds = true
        self.layer.masksToBounds = true
    }
    func addDropShadowToView(){
        self.layer.masksToBounds =  false
        self.layer.shadowColor = UIColor.lightGray.cgColor;
        self.layer.shadowOffset = CGSize.init(width: 0.5, height: 0.5)
        self.layer.shadowOpacity = 0.1
    }
    class func fromNib<T: UIView>(frame:CGRect = CGRect.zero) -> T {
            let view = Bundle.main.loadNibNamed(String(describing: T.self), owner: nil, options: nil)![0] as! T
        if (frame != CGRect.zero){
            view.frame = frame
        }
        return view
    }
    func roundCorners(corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        layer.mask = mask
    }
}
// MARK: - STRING
extension String {

    /// CONVERT THE HTML STRING TO ATTRIBUTED DISPLAYING STRING
    var htmlToAttributedString: NSAttributedString? {
        guard let data = data(using: .utf8) else { return NSAttributedString() }
        do {
            return try NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding:String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            return NSAttributedString()
        }
    }
    // CONVERT HTML STRING TO NON HTML STRING
    var htmlToString: String {
        return htmlToAttributedString?.string ?? ""
    }
    
//    var localized: String {
//        // GET PROJECT LANGUAGE
//        let val = userDefault.value(forKey: "SELECTEDLANGUAGE")
//        let path = Bundle.main.path(forResource:UserDefaults.standard.value(forKey: "en") as? String, ofType: "lproj")
//        var bundle = Bundle.main
//        if(path != nil){
//            bundle = Bundle(path: path!)!
//        }
//        return NSLocalizedString(self, tableName: nil, bundle: Bundle.main, value: "", comment: "")
//    }
    func isValidPassword() -> Bool {
        let passwordRegex = "^(?=.*[A-Za-z])(?=.*\\d)[A-Za-z\\d]{6,}$"
        return NSPredicate(format: "SELF MATCHES %@", passwordRegex).evaluate(with: self)
    }
    func isValidEmail() -> Bool {
        let emailRegex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
        var valid = NSPredicate(format: "SELF MATCHES %@", emailRegex).evaluate(with: self)
        if valid {
            valid = !self.contains("Invalid email id")
        }
        return valid
    }
    func isValidUsername() -> Bool {
        let emailRegex = "^[a-zA-Z0-9]{1,}$"
        var valid = NSPredicate(format: "SELF MATCHES %@", emailRegex).evaluate(with: self)
        if valid {
            valid = !self.contains("Invalid user id")
        }
        return valid
    }
    func removeWhiteSpace() -> String{
        return  self.trimmingCharacters(in: .whitespaces)
    
    }
}
// MARK: - UINAVIGATIONBAR
extension UINavigationBar{
    
    // METHOD TO CHANGE TITLE FONTS
    func methodSetFonts(font:UIFont,color:UIColor) {
        self.titleTextAttributes = [NSAttributedString.Key.font: font,NSAttributedString.Key.foregroundColor: color]
    }
  
    
    /// Applies a background gradient with the given colors
    func applyNavigationGradient( colors : [UIColor]) {
        var frameAndStatusBar: CGRect = self.bounds
        frameAndStatusBar.size.height += 20 // add 20 to account for the status bar
        //print(frameAndStatusBar.size)
        setBackgroundImage(UINavigationBar.gradient(size: frameAndStatusBar.size, colors: colors), for: .default)
    }
    
    /// Creates a gradient image with the given settings
    static func gradient(size : CGSize, colors : [UIColor]) -> UIImage?{
        // Turn the colors into CGColors
        let cgcolors = colors.map { $0.cgColor }
        
        // Begin the graphics context
        UIGraphicsBeginImageContextWithOptions(size, true, 0.0)
        
        // If no context was retrieved, then it failed
        guard let context = UIGraphicsGetCurrentContext() else { return nil }
        
        // From now on, the context gets ended if any return happens
        defer { UIGraphicsEndImageContext() }
        
        // Create the Coregraphics gradient
        var locations : [CGFloat] = [0.0,0.5,1.0]
        guard let gradient = CGGradient(colorsSpace: CGColorSpaceCreateDeviceRGB(), colors: cgcolors as NSArray as CFArray, locations: &locations) else { return nil }
        
        // Draw the gradient
        context.drawLinearGradient(gradient, start: CGPoint(x: 0.0, y: 1.0), end: CGPoint(x: size.width, y: 1.0), options: [])
        
        // Generate the image (the defer takes care of closing the context)
        return UIGraphicsGetImageFromCurrentImageContext()
    }
}
//extension UIImageView{
//    func loadImageFromServerWithCompletion(imgPath:String,palceholder:String="placeholder") {
//        if let _urlNew = imgPath.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed){
//            self.setShowActivityIndicator(true)
//            self.setIndicatorStyle(.gray)
//            self.sd_setImage(with: URL.init(string: _urlNew)) { (image, error, type, url) in
//                if (error == nil){
//                }
//            }
//        }
//    }
//}

// MARK: - UIIMAGEVIEW
extension UIImageView{
    func ImageRound(img:UIImage){
        self.layer.cornerRadius = self.frame.size.width / 2
        self.layer.masksToBounds = true
        self.clipsToBounds = true
        self.image = img
    }
    // IMAGE SQUARE
    func ImageSequare(img:UIImage) {
        self.layer.cornerRadius = 5.0
        self.layer.masksToBounds = true
        self.clipsToBounds = true
        self.image = img
    }

}

class ImageViewRound: UIView {
    override func draw(_ rect: CGRect) {
        self.layer.cornerRadius = self.frame.size.width / 2
        self.layer.masksToBounds = true
        self.clipsToBounds = true
        self.layoutIfNeeded()
        
    }
}

class ImageViewSequare: UIImageView {
    override func draw(_ rect: CGRect) {
        self.layer.cornerRadius = 5.0
        self.layer.masksToBounds = true
        self.clipsToBounds = true
    }
}



// MARK: - NOTIFICATION.NAME
extension Notification.Name {
    static let favouritesButtonClicked = Notification.Name("favouritesButtonClicked")
}
// MARK: - UIVIEWCONTROLLER
extension UIViewController {
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action:#selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}
// MARK: - UIPickerView
extension UIPickerView {
    func hideSelectionIndicator() {
        for i in [1, 2] {
            self.subviews[i].isHidden = true
        }
    }
    @IBInspectable var hideSelectionView: Bool {
        get{
            return self.hideSelectionView
        }
        set{
            for i in [1, 2] {
                self.subviews[i].isHidden = newValue
            }
        }
    }
}

class redButton: UIButton {
    override func awakeFromNib() {
       // self.setBackgroundColor(color: UIColor.REDColor, forState: .normal)
       // self.setBackgroundColor(color: UIColor.BLACKColor, forState: .highlighted)
      //  self.methodSetButtonCorner(val: 6)
       // self.addCharactersSpacing()
    }
}
class greyButton: UIButton {
    override func awakeFromNib() {
      //  self.setBackgroundColor(color: UIColor.GREYButtonColor, forState: .normal)
       // self.setBackgroundColor(color: UIColor.BLACKColor, forState: .highlighted)
      //  self.methodSetButtonCorner(val: 6)
      //  self.addCharactersSpacing()
    }
}
class normalredButton: UIButton {
    override func awakeFromNib() {
       // self.setBackgroundColor(color: UIColor.REDColor, forState: .normal)
        //self.setBackgroundColor(color: UIColor.BLACKColor, forState: .highlighted)
        //self.addCharactersSpacing()

    }
}
extension NSObject {
    class func fromClassName(className : String) -> NSObject {
//        let className = Bundle.main.infoDictionary!["CFBundleName"] as! String + "." + className
//        let className =   className

        let aClass = NSClassFromString(className) as! UIViewController.Type
        return aClass.init()
    }
}
