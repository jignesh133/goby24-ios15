//
//  UtilsObjs.m
//  FLIK
//
//  Created by MAC 2 on 30/09/19.
//  Copyright © 2019 MAC 2. All rights reserved.
//

#import "UtilsObjs.h"

@implementation UtilsObjs
#pragma mark - REMOVE NULL FROM DICT
+(id)cleanJsonToObjectWithSkip:(id)data :(NSArray *)arrSkipKey{
    NSError* error;
    if (data == (id)[NSNull null]){
        return [[NSObject alloc] init];
    }
    id jsonObject;
    if ([data isKindOfClass:[NSData class]]){
        jsonObject = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
    } else {
        jsonObject = data;
    }
    if ([jsonObject isKindOfClass:[NSArray class]]) {
        NSMutableArray *array = [jsonObject mutableCopy];
        for (int i = (int)array.count-1; i >= 0; i--) {
            id a = array[i];
            if (a == (id)[NSNull null]){
                [array removeObjectAtIndex:i];
            } else {
                array[i] = [self cleanJsonToObjectWithSkip:a :arrSkipKey];
            }
        }
        return array;
    } else if ([jsonObject isKindOfClass:[NSDictionary class]]) {
        NSMutableDictionary *dictionary = [jsonObject mutableCopy];
        for(NSString *key in [dictionary allKeys]) {
            id d = dictionary[key];
            if ([arrSkipKey containsObject:key]){
                continue;
            }
            if ([d isKindOfClass:[NSNumber class]]) {
                if (d == (id)[NSNull null]){
                    dictionary[key] = @0;
                } else {
                    dictionary[key] = [self cleanJsonToObjectWithSkip:d :arrSkipKey];
                }
            }else{
                if (d == (id)[NSNull null]){
                    dictionary[key] = @"";
                } else {
                    dictionary[key] = [self cleanJsonToObjectWithSkip:d   :arrSkipKey];
                }
                
            }
        }
        return dictionary;
    } else {
        return jsonObject;
    }
}

+(id)cleanJsonToObject:(id)data {
    NSError* error;
    if (data == (id)[NSNull null]){
        return [[NSObject alloc] init];
    }
    id jsonObject;
    if ([data isKindOfClass:[NSData class]]){
        jsonObject = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
    } else {
        jsonObject = data;
    }
    if ([jsonObject isKindOfClass:[NSArray class]]) {
        NSMutableArray *array = [jsonObject mutableCopy];
        for (int i = (int)array.count-1; i >= 0; i--) {
            id a = array[i];
            if (a == (id)[NSNull null]){
                [array removeObjectAtIndex:i];
            } else {
                array[i] = [self cleanJsonToObject:a];
            }
        }
        return array;
    } else if ([jsonObject isKindOfClass:[NSDictionary class]]) {
        NSMutableDictionary *dictionary = [jsonObject mutableCopy];
        for(NSString *key in [dictionary allKeys]) {
            id d = dictionary[key];
          
            if ([d isKindOfClass:[NSNumber class]]) {
                if (d == (id)[NSNull null]){
                    dictionary[key] = @0;
                } else {
                    dictionary[key] = [self cleanJsonToObject:d];
                }
            }else{
                if (d == (id)[NSNull null]){
                    dictionary[key] = @"";
                } else {
                    dictionary[key] = [self cleanJsonToObject:d];
                }
                
            }
        }
        return dictionary;
    } else {
        return jsonObject;
    }
}
@end
