//
//  WebCommanMethods.swift
//  Goby24
//
//  Created by Jignesh Bhensadadiya on 7/24/21.
//

import Foundation

class WebCommanMethods: NSObject {
    // MARK:- CUSTOM METHODS
    static func refreshToken(){
        var param:[String:Any] = [String:Any]()
        param["refresh"] = userDefault.value(forKey: Enum_Login.token.rawValue) as? String ?? ""
        
        WebAccess.postDataWith(_Url: WebAccess.REFRESH_TOKEN, _parameters: param) { (result) in
            switch (result){
            case .Success(let _data):
                print(_data)
                let responseData = _data//["data"] as? [String:Any] ?? [:]
                let token:String = "Bearer " + getStringFromAny((responseData["access"] ?? ""))
                // userDefault.set(getStringFromAny(token), forKey: Enum_Login.token.rawValue)
                // userDefault.synchronize()
                print(token)
                
                break
            case .Error( _):
                // ShowAlert(title: APPNAME, msg: msg, view: nil)
                break
            }
        }
    }
    static func getUserProfileData(completion: @escaping(Bool) -> Void){
        WebAccess.getDataWith(_Url: WebAccess.PROFILE_URL,isHideLoader: true) { (result) in
            switch(result){
            case .Success(let profiledata):
                let responseData = UtilsObjs.cleanJson(to: profiledata["result"] )  as? [String:Any] ?? [:]

//                let responseData = profiledata["result"] as? [String:Any] ?? [:]
                userDefault.set(responseData, forKey: Enum_Login.userProfileData.rawValue)
                userDefault.setValue(responseData["profile_pic"], forKey: Enum_Login.profilePic.rawValue)
                userDefault.setValue(true, forKey: Enum_Login.isLogin.rawValue)
                
                appDelegate?.userProfileData = responseData
                userDefault.synchronize()
                appDelegate?.userProfileData = userDefault.value(forKey: Enum_Login.userProfileData.rawValue) as? [String : Any] ?? [:]
                completion(true)
                break
            case .Error( _):
                completion(false)
                break
                
            }
            
        }
    }
    
}
