//
//  UtilsObjs.h
//  FLIK
//
//  Created by MAC 2 on 30/09/19.
//  Copyright © 2019 MAC 2. All rights reserved.
//

#import <Foundation/Foundation.h>
@interface UtilsObjs : NSObject

+(id)cleanJsonToObject:(id)data;
+(id)cleanJsonToObjectWithSkip:(id)data :(NSArray *)arrSkipKey;

@end


