
//
//  appApiConstant.swift
//  OrangeWill
//
//  Created by Hupp Technologies on 27/05/17.
//  Copyright © 2017 Hupp Technologies. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import SwiftUI


class WebAccess: NSObject {
    
    
    static let PASSWORD_RESET:String = "password_reset/"
    static let LOGIN_URL:String = "authentication/token/"
    static let SIGNUP_URL:String = "users/register-newuser/"
    static let UPDATEUSER_URL:String = "users/new-user/"
   
    
    static let LOGIN_FACEBOOK_URL:String = "social_auth/facebook/"
    static let LOGIN_GOOGLE_URL:String = "social_auth/google/"
    static let LOGIN_APPLE_URL:String = "social_auth/apple/"
    
    static let NEW_USER:String = "users/new-user/"
    
    static let REFRESH_TOKEN:String = "authentication/token/refresh/"
    static let MOBILE_OTP:String = "users/send-otp/"
    
    
    static let VERIFY_EMAIL:String = "users/verify-email/"
    static let RESEND_EMAIL:String = "users/send-email/"
    static let VERIFY_PHONECODE:String = "users/verify-otp/"
    
    static let DOCUMENTS_UPLOAD:String = "users/identity-document/"
    
    static let PROFILE_URL:String = "users/new-user/"
    static let PROFILE_UPDATE_URL:String = "users/new-user/"
    static let CHANGE_PASSWORD:String = "change-password/"
    
    //
    static let CAR_BRANDS:String = "ride/car-brands/"
    static let CAR_BRANDMODELS:String = "ride/car-models/"
    static let ADD_Vehicle:String = "users/rider-info/"
    
    
    static let SEARCH_LOCATION:String = "ride/search-location/"
    static let DISTANCE_MATRIX:String = "ride/distance-matrix/"
    static let FARE_PER_KM:String = "ride/ride-fare-per-km"

    static let OFFER_RIDE:String = "ride/offer-ride/"
    static let FIND_RIDE:String = "ride/find-ride/"
    
    static let BOOK_RIDE:String = "ride/book-ride/"
    static let BOOK_RIDE_EPAY:String = "payment/ride-epayment/"
    static let BOOK_RIDE_CASHPAY:String = "payment/ride-payment/"

    
    static let MY_RIDE_:String = "ride/my-ride/"
    static let RIDER_RIDE:String = "ride/rider-ride/"

    
    static let TOURIST_REQUEST:String = "ride/tourist-spot-request/"
    static let TOURIST_PACKAGES:String = "ride/tourist-packages/"
   
    static let TOURIST_PACKAGES_BOOKING:String = "ride/tourist-package-booking/"
    static let TOURIST_SPOT_LIST:String = "ride/tourist-spots/"
    static let TOURIST_SPOT_PAYMENT:String = "payment/tourist-package-payment/"
    static let TOURIST_REQUEST_PAYMENT:String = "payment/tourist-package-request-payment/"
    static let TOURIST_RIDE_BASE_FARE:String = "ride/get-ride-fare-details-with-base-fare/"

    
    static let TOURIST_SPOT_REQUEST:String = "ride/tourist-spot-request/"

    static let TOURIST_SPOT_REQUEST_ACCEPT:String = "tourist-spot-request-accept/"
    
    static let TRENDING_ROUTES:String = "other/trending-routes/"
    static let SLIDERIMAGES:String = "other/slider-image/"
   
    static let FEEDBACK_RIDER:String = "ride/feedback-rider/"
    static let FEEDBACK_RIDER_SUMMERY:String = "ride/feedback-rider/rating-summary"

    static let RIDE_REQUEST:String = "ride/ride-request/"
    static let RIDE_REQUEST_LIST:String = "ride/ride-request/"

    static let REQUEST_RIDE_ACCEPT:String = "ride/ride-request-accept/"
    static let REQUEST_RIDE_REJECT:String = "ride/ride-request-reject/"

    static let REQUEST_RIDE_EPAY:String = "payment/ride-request-epayment/"
    static let REQUEST_RIDE_CASHPAY:String = "payment/ride-request-payment/"

    static let NOTIFICATION:String = "other/notification/"
    

    static let FARE_DETAILS_WITH_BASE_FARE:String = "ride/get-ride-fare-details-with-base-fare?"

    static let TRAVELLER_RIDE_HISTORY:String = "ride/my-ride-request/"
    static let RIDER_RIDE_HISTORY:String = "ride/rider-ride-request/"

    static let TOURIST_RIDE_TAKEN_HISTORY:String = "ride/tourist-package-booking/"
    static let TOURIST_RIDE_OFFERED_HISTORY:String = "ride/rider-offered-tourist-packages/"

    static let BASIC_RIDE_TRANSACTION_HISTORY:String = "payment/transaction-history/"
    static let RIDE_REQUEST_TRANSACTION_HISTORY:String = "payment/ride-request-transaction-history/"
    static let TOURISTPACKAGE_TRANSACTION_HISTORY:String = "payment/tourist-package-transaction-history/"
    static let TOURIST_PACKAGE_REQUEST_TRANSACTION_HISTORY:String = "payment/tourist-package-request-payment-history/"

    

    static let FEEDBACK_TRAVELLER:String = "ride/feedback-traveller/"
    static let FEEDBACK_TRAVELLER_SUMMERY:String = "ride/feedback-traveller/rating-summary/"

    static let INBOX_CHAT_HOME:String = "inbox/chat-home/"
    static let CHAT_DETAILS_HOME:String = "inbox/inbox-messages/"

    
    static let RIDE_CANCEL_BY_TRAVELLER:String = "ride/cancel-ride-traveller/"
    static let RIDE_CANCEL_BY_RIDER:String = "ride/cancel-ride/"

    static let PACKAGE_TAKEN_CANCELLED:String = "ride/tourist-package-ride-cancel/"
    static let PACKAGE_OFFER_CANCELLED:String = "ride/tourist-package-ride-cancel/"

    
    static let TOURIST_PACKAGE_TAKEN_CANCEL:String = "ride/tourist-package-booking-cancel/"
    static let TOURIST_PACKAGE_Offered_CANCEL:String = ""

    static let REPORT_NUMBER:String = "/users/report-rider/"

    
    // MARK: - No Internet Connection
    static func checkInternetConnection() -> NetworkConnection {
        if Connectivity.isConnectedToInternet == true {
            return .available
        }
        return .notAvailable
    }
    // MARk: UPLOAD IMAGE WITH DATA
    static func putDataWithImage(_Url:String,_parameters:[String:Any],isHideLoader:Bool = false, completion: @escaping(Json_Result<[String:Any]>) -> Void) {
        
        if !Connectivity.isConnectedToInternet {
            completion(.Error(INTERNET_MESSAGE))
            return
        }
        
        
        
        var headers: HTTPHeaders = [
            "Content-Type":"application/json",
            "Accept": "application/json",
        ]
        if (getStringFromAny(userDefault.value(forKey: Enum_Login.token.rawValue) ?? "") != ""){
            headers["Authorization"] = userDefault.value(forKey: Enum_Login.token.rawValue) as? String ?? ""
        }
        guard URL.init(string: BASEURL + _Url) != nil else {
            return completion(.Error("Url not valid."))
        }
        
        
        // CLEARE CATCH
        URLCache.shared.removeAllCachedResponses()
        appDelegate?.stopLoadingView()

        if (isHideLoader == false){
              appDelegate?.startLoadingview()
        }else{
             appDelegate?.stopLoadingView()
        }
        
        AF.upload(multipartFormData: { multipart in
            for key in _parameters.keys{
                let name = String(key)
                
                if let val = _parameters[name] as? Image{
                    let randomInt = Int.random(in: 1..<1000)
                    let seconds = "\(Int64(NSDate().timeIntervalSince1970 * 1000))" + getStringFromAny(randomInt)
                    let fileData:Data = val.asUIImage().jpegData(compressionQuality: 0.35)!
                    multipart.append(fileData, withName: name, fileName: seconds + ".jpg", mimeType: "image/jpeg")
                }else if let val = _parameters[name] as? UIImage{
                    let randomInt = Int.random(in: 1..<1000)
                    let seconds = "\(Int64(NSDate().timeIntervalSince1970 * 1000))" + getStringFromAny(randomInt)
                    let fileData:Data = val.jpegData(compressionQuality: 0.35)!
                    multipart.append(fileData, withName: name, fileName: seconds + ".jpg", mimeType: "image/jpeg")
                }else if let val = _parameters[name] as? String{
                    multipart.append(val.data(using: .utf8)!, withName :name)
                }
            }
        }, to: BASEURL + _Url, method: .put, headers: headers).responseJSON(completionHandler: { (response) in
            
            var responseDic = response.value as? [String : Any]
            appDelegate?.stopLoadingView()

            switch(response.response?.statusCode){
            case 200,201:
                responseDic = UtilsObjs.cleanJson(to: responseDic) as? [String : Any] ?? [:]
                DispatchQueue.main.async {
                    completion(.Success(responseDic!))
                }
                break
            case 204:
                DispatchQueue.main.async {
                    completion(.Error("240 No Content"))
                }
                break
            case 400,401,403,415:
                DispatchQueue.main.async {
                    completion(.Error(getStringFromAny(responseDic?["detail"] ?? responseDic?["result"] ??  getStringFromAny(response.response?.statusCode ?? 0))))
                }
                break
            case 500,501:
                DispatchQueue.main.async {
                    completion(.Error("500 Internal Server Error"))
                }
                break
            case .none: break
            case .some(_): break
            }
            //
            
            
        })
    }
    // MARk: UPLOAD IMAGE WITH DATA
    static func postDataWithImage(_Url:String,_parameters:[String:Any],isHideLoader:Bool = false, completion: @escaping(Json_Result<[String:Any]>) -> Void) {
        
        if !Connectivity.isConnectedToInternet {
            completion(.Error(INTERNET_MESSAGE))
            return
        }
        
        var headers: HTTPHeaders = [
            "Content-Type":"application/json",
            "Accept": "application/json",
        ]
        if (getStringFromAny(userDefault.value(forKey: Enum_Login.token.rawValue) ?? "") != ""){
            headers["Authorization"] = userDefault.value(forKey: Enum_Login.token.rawValue) as? String ?? ""
        }
        guard URL.init(string: BASEURL + _Url) != nil else {
            return completion(.Error("Url not valid."))
        }
        
        // CLEARE CATCH
        URLCache.shared.removeAllCachedResponses()
        

        if (isHideLoader == false){
            DispatchQueue.main.async {
                appDelegate?.startLoadingview()
            }
        }else{
             appDelegate?.stopLoadingView()
        }
        
        AF.upload(multipartFormData: { multipart in
            
            for key in _parameters.keys{
                let name = String(key)
                
                if let val = _parameters[name] as? Image{
                    let randomInt = Int.random(in: 1..<1000)
                    let seconds = "\(Int64(NSDate().timeIntervalSince1970 * 1000))" + getStringFromAny(randomInt)
                    let fileData:Data = val.asUIImage().jpegData(compressionQuality: 0.35)!
                    multipart.append(fileData, withName: name, fileName: seconds + ".jpg", mimeType: "image/jpeg")
                }else  if let val = _parameters[name] as? UIImage{
                    let randomInt = Int.random(in: 1..<1000)
                    let seconds = "\(Int64(NSDate().timeIntervalSince1970 * 1000))" + getStringFromAny(randomInt)
                    let fileData:Data = val.jpegData(compressionQuality: 0.35)!
                    multipart.append(fileData, withName: name, fileName: seconds + ".jpg", mimeType: "image/jpeg")
                }
                else if let val = _parameters[name] as? String{
                    multipart.append(val.data(using: .utf8)!, withName :name)
                }
            }
        }, to: BASEURL + _Url, method: .post, headers: headers).responseJSON(completionHandler: { (response) in
            
            var responseDic = response.value as? [String : Any]
            
            switch(response.response?.statusCode){
            case 200,201:
                responseDic = UtilsObjs.cleanJson(to: responseDic) as? [String : Any] ?? [:]
                
                if (((responseDic?["result"] as? String) != nil)){
                    
                    if ((responseDic?["result"] as? String)?.lowercased() == "success") {
                        DispatchQueue.main.async {
                            completion(.Success(responseDic!))
                        }
                    }else{
                        DispatchQueue.main.async {
                            completion(.Error(responseDic?["result"] as? String ?? ""))
                        }
                    }
                }else{
                    DispatchQueue.main.async {
                        completion(.Success(responseDic!))
                    }
                }
                break
            case 204:
                DispatchQueue.main.async {
                    completion(.Error("240 No Content"))
                }
                break
            case 400,401,403,415:
                
                DispatchQueue.main.async {
                    completion(.Error(getStringFromAny(responseDic?["detail"] ?? responseDic?["result"] ??  getStringFromAny(response.response?.statusCode ?? 0))))
                }
                break
            case 500,501:
                
                DispatchQueue.main.async {
                    completion(.Error("500 Internal Server Error"))
                }
                break
            case .none:
                break
            case .some(_):
                break
            }
        })
    }
    //MARK:- Post method
    static func putDataWith(_Url:String,_parameters:[String:Any],isHideLoader:Bool = false, completion: @escaping(Json_Result<[String:Any]>) -> Void) {
        
        if !Connectivity.isConnectedToInternet {
            completion(.Error(INTERNET_MESSAGE))
            return
        }
        
        var headers: HTTPHeaders = [
            "Content-Type":"application/json",
            "Accept": "application/json",
        ]
        if (getStringFromAny(userDefault.value(forKey: Enum_Login.token.rawValue) ?? "") != ""){
            headers["Authorization"] = userDefault.value(forKey: Enum_Login.token.rawValue) as? String ?? ""
        }
        
        guard let url = URL.init(string: BASEURL + _Url) else {
            return completion(.Error("Url not valid."))
        }
        
        // CLEARE CATCH
        URLCache.shared.removeAllCachedResponses()
        if (isHideLoader == false){
              appDelegate?.startLoadingview()
        }else{
             appDelegate?.stopLoadingView()
        }
        
        AF.request(url, method: .put, parameters: _parameters, encoding:JSONEncoding.default, headers: headers).responseJSON { (response) in
            
            var responseDic = response.value as? [String : Any]
            
             appDelegate?.stopLoadingView()
            
            switch(response.response?.statusCode){
            case 200,201:
                responseDic = UtilsObjs.cleanJson(to: responseDic) as? [String : Any] ?? [:]
                DispatchQueue.main.async {
                    completion(.Success(responseDic!))
                }
                break
            case 204:
                DispatchQueue.main.async {
                    completion(.Error("240 No Content"))
                }
                break
            case 400,401,403,405,415:
                DispatchQueue.main.async {
                    completion(.Error(getStringFromAny(responseDic?["detail"] ?? responseDic?["result"] ??  getStringFromAny(response.response?.statusCode ?? 0))))
                }
                break
            case 500,501:
                DispatchQueue.main.async {
                    completion(.Error("500 Internal Server Error"))
                }
                break            case .none: break
            case .some(_): break
            }
        }
        
        
        
    }
    //MARK:- Post method
    static func postDataWith(_Url:String,_parameters:[String:Any],isHideLoader:Bool = false,baseUrl:String = BASEURL, completion: @escaping(Json_Result<[String:Any]>) -> Void) {
        
        if !Connectivity.isConnectedToInternet {
            completion(.Error(INTERNET_MESSAGE))
            return
        }
        
        var headers: HTTPHeaders = [
            "Content-Type":"application/json",
            "Accept": "application/json",
        ]
        let userTokenValue = getStringFromAny(userDefault.value(forKey: Enum_Login.token.rawValue) ?? "")
        if ((userTokenValue.removeWhiteSpace() != "Bearer") && userTokenValue.count > 0 ){
            headers["Authorization"] = userDefault.value(forKey: Enum_Login.token.rawValue) as? String ?? ""
        }
        
        guard let url = URL.init(string: baseUrl + _Url) else {
            return completion(.Error("Url not valid."))
        }
        
        // CLEARE CATCH
        URLCache.shared.removeAllCachedResponses()
        if (isHideLoader == false){
              appDelegate?.startLoadingview()
        }else{
             appDelegate?.stopLoadingView()
        }
        
        AF.request(url, method: .post, parameters: _parameters, encoding:JSONEncoding.default, headers: headers).responseJSON { (response) in
            
            var responseDic = response.value as? [String : Any]
            
             appDelegate?.stopLoadingView()
            print("%d",response.response?.statusCode ?? 0)
            switch(response.response?.statusCode){
            case 200,201:
                responseDic = UtilsObjs.cleanJson(to: responseDic) as? [String : Any] ?? [:]
                DispatchQueue.main.async {
                    completion(.Success(responseDic!))
                }
                break
            case 204:
                DispatchQueue.main.async {
                    completion(.Error("240 No Content"))
                }
                break
            case 400,401,403,404,415:
                DispatchQueue.main.async {
                    let statusCode = getStringFromAny(response.response?.statusCode ?? "")
                    completion(.Error(getStringFromAny(responseDic?["detail"] ?? responseDic?["result"] ??  ("SOMETHING WENT WRONG " + statusCode ))))
                }
                break
            case 500:
                DispatchQueue.main.async {
                    completion(.Error("500 Internal Server Error"))
                }
                break
            case .none: break
            case .some(_): break
                
            }
        }
        
        
        
    }
    
    //MARK:- Post method
    static func postDataWithString(_Url:String,_parameters:[String:Any],isHideLoader:Bool = false,baseUrl:String = BASEURL, completion: @escaping(Json_Result<[String:Any]>) -> Void) {
        
        if !Connectivity.isConnectedToInternet {
            completion(.Error(INTERNET_MESSAGE))
            return
        }
        
        var headers: HTTPHeaders = [
            "Content-Type":"application/json",
            "Accept": "application/json",
        ]
        if (getStringFromAny(userDefault.value(forKey: Enum_Login.token.rawValue) ?? "") != ""){
            headers["Authorization"] = userDefault.value(forKey: Enum_Login.token.rawValue) as? String ?? ""
        }
        
        guard let url = URL.init(string: baseUrl + _Url) else {
            return completion(.Error("Url not valid."))
        }
        
        // CLEARE CATCH
        URLCache.shared.removeAllCachedResponses()
        if (isHideLoader == false){
              appDelegate?.startLoadingview()
        }else{
             appDelegate?.stopLoadingView()
        }
        
        AF.request(url, method: .post, parameters: _parameters, encoding:JSONEncoding.default, headers: headers).responseString { (response) in
            
            var responseDic = response.value as? [String : Any]
            
             appDelegate?.stopLoadingView()
            print("%d",response.response?.statusCode ?? 0)
            switch(response.response?.statusCode){
            case 200,201:
                responseDic = UtilsObjs.cleanJson(to: responseDic) as? [String : Any] ?? [:]
                DispatchQueue.main.async {
                    completion(.Success(responseDic!))
                }
                break
            case 400,401,404,415:
                DispatchQueue.main.async {
                    completion(.Error(getStringFromAny(responseDic?["detail"] ?? responseDic?["result"] ??  "SOMETHING WENT WRONG ")))
                }
                break
            case 500:
                DispatchQueue.main.async {
                    completion(.Error("500 Internal Server Error"))
                }
                break
            case .none: break
            case .some(_): break
                
            }
        }
        
        
        
    }
    //    MARK:- GET
    static func deleteDataWith(_Url:String,_parameters:[String:Any] = [:],isHideLoader:Bool = false,baseUrl:String = BASEURL, completion: @escaping(Json_Result<[String:Any]>) -> Void) {
        
        
        switch checkInternetConnection() {
        
        case .notAvailable:
            completion(.Error(INTERNET_MESSAGE))
            return
        case .available:
            break
        }
        
        var headers: HTTPHeaders = [
            "Content-Type":"application/json",
            "Accept": "application/json",
        ]
        if (getStringFromAny(userDefault.value(forKey: Enum_Login.token.rawValue) ?? "") != ""){
            headers["Authorization"] = userDefault.value(forKey: Enum_Login.token.rawValue) as? String ?? ""
        }
        
        guard let url = URL.init(string: BASEURL + _Url) else {
            print("NOT A URL")
            return completion(.Error("Url not valid."))
        }
        let urlRequest = URLRequest(url: url)
        URLCache.shared.removeCachedResponse(for: urlRequest)
        
        // CLEARE CATCH
        URLCache.shared.removeAllCachedResponses()
        if (isHideLoader == false){
              appDelegate?.startLoadingview()
        }else{
             appDelegate?.stopLoadingView()
        }
        

        AF.request(url, method: .delete, parameters: _parameters, encoding:URLEncoding.default, headers: headers).responseJSON { (response) in
            var responseDic = response.value as? [String : Any]
           
            appDelegate?.stopLoadingView()

            switch(response.result) {
            case .success(let json):
                appDelegate?.stopLoadingView()
                
                switch(response.response?.statusCode){
                case 200,201:
                    responseDic = UtilsObjs.cleanJson(to: responseDic) as? [String : Any] ?? [:]
                    DispatchQueue.main.async {
                        completion(.Success(responseDic!))
                    }
                    break
                case 400,401,415:
                    DispatchQueue.main.async {
                        completion(.Error(getStringFromAny(responseDic?["detail"] ?? responseDic?["result"] ??  getStringFromAny(response.response?.statusCode ?? 0))))
                    }
                    break
                case 500,501:
                    DispatchQueue.main.async {
                        completion(.Error("500 Internal Server Error"))
                    }
                    break
                case .none: break
                case .some(_): break
                }
                break
            case .failure(let err):
                appDelegate?.stopLoadingView()
                DispatchQueue.main.async {
                    completion(.Error(err.localizedDescription))
                }
                break
            }
            

        }
    }
    //    MARK:- GET
    static func getDataWith(_Url:String,_parameters:[String:Any] = [:],isHideLoader:Bool = false,baseUrl:String = BASEURL, completion: @escaping(Json_Result<[String:Any]>) -> Void) {
        
        
        switch checkInternetConnection() {
        
        case .notAvailable:
            completion(.Error(INTERNET_MESSAGE))
            return
        case .available:
            break
        }
        
        var headers: HTTPHeaders = [
            "Content-Type":"application/json",
            "Accept": "application/json",
        ]
        if (getStringFromAny(userDefault.value(forKey: Enum_Login.token.rawValue) ?? "") != ""){
            headers["Authorization"] = userDefault.value(forKey: Enum_Login.token.rawValue) as? String ?? ""
        }
        
        guard let url = URL.init(string: BASEURL + _Url) else {
            print("NOT A URL")
            return completion(.Error("Url not valid."))
        }
        let urlRequest = URLRequest(url: url)
        URLCache.shared.removeCachedResponse(for: urlRequest)
        
        // CLEARE CATCH
        URLCache.shared.removeAllCachedResponses()
        if (isHideLoader == false){
              appDelegate?.startLoadingview()
        }else{
             appDelegate?.stopLoadingView()
        }
        

        AF.request(url, method: .get, parameters: _parameters, encoding:URLEncoding.default, headers: headers).responseJSON { (response) in
            var responseDic = response.value as? [String : Any]
           
            appDelegate?.stopLoadingView()

            switch(response.result) {
            case .success(let json):
                appDelegate?.stopLoadingView()
                
                switch(response.response?.statusCode){
                case 200,201:
                   // responseDic =  //UtilsObjs.cleanJson(to: responseDic) as? [String : Any] ?? [:]
                    DispatchQueue.main.async {
                        completion(.Success(responseDic!))
                    }
                    break
                case 400,405,401,415:
                    DispatchQueue.main.async {
                        completion(.Error(getStringFromAny(responseDic?["detail"] ?? responseDic?["result"] ??  getStringFromAny(response.response?.statusCode ?? 0))))
                    }
                    break
                case 500,501:
                    DispatchQueue.main.async {
                        completion(.Error("500 Internal Server Error"))
                    }
                    break
                case 204:
                    DispatchQueue.main.async {
                        completion(.Error("No Content Available "))
                    }
                    break
                case .none: break
                case .some(_): break
                }
                break
            case .failure(let err):
                appDelegate?.stopLoadingView()
                DispatchQueue.main.async {
                    completion(.Error(err.localizedDescription))
                }
                break
            }
            

        }
    }
    //    MARK:- GET
    static func getSearchLocationDataWith(_Url:String,_parameters:[String:Any] = [:],isHideLoader:Bool = false, completion: @escaping(Json_Result<[String:Any]>) -> Void) {
        
        
        switch checkInternetConnection() {
        
        case .notAvailable:
            completion(.Error(INTERNET_MESSAGE))
            return
        case .available:
            break
        }
        
        var headers: HTTPHeaders = [
            "Content-Type":"application/json",
            "Accept": "application/json",
        ]
//        if (getStringFromAny(userDefault.value(forKey: Enum_Login.token.rawValue) ?? "") != ""){
//            headers["Authorization"] = userDefault.value(forKey: Enum_Login.token.rawValue) as? String ?? ""
//        }
//        
        guard let url = URL.init(string: _Url) else {
            print("NOT A URL")
            return completion(.Error("Url not valid."))
        }
        let urlRequest = URLRequest(url: url)
        URLCache.shared.removeCachedResponse(for: urlRequest)
        
        // CLEARE CATCH
        URLCache.shared.removeAllCachedResponses()
        if (isHideLoader == false){
              appDelegate?.startLoadingview()
        }else{
             appDelegate?.stopLoadingView()
        }
        

        AF.request(url, method: .get, parameters: _parameters, encoding:URLEncoding.default, headers: headers).responseJSON { (response) in
            var responseDic = response.value as? [String : Any]
            
            switch(response.response?.statusCode){
            case 200:
                appDelegate?.stopLoadingView()

                responseDic = UtilsObjs.cleanJson(to: responseDic) as? [String : Any] ?? [:]
                DispatchQueue.main.async {
                    completion(.Success(responseDic!))
                }
                break
            case 400,401,403,415:
                appDelegate?.stopLoadingView()

                DispatchQueue.main.async {
                    completion(.Error(getStringFromAny(responseDic?["detail"] ?? responseDic?["result"] ??  getStringFromAny(response.response?.statusCode ?? 0))))
                }
                break
            case 500,501:
                appDelegate?.stopLoadingView()

                DispatchQueue.main.async {
                    completion(.Error("500 Internal Server Error"))
                }
                break
            case .none:
                appDelegate?.stopLoadingView()
                break
            case .some(_):
                appDelegate?.stopLoadingView()

                break
            }
        }
    }
    
//    func getPlaceInfo(placeId:String,completionHandler: @escaping CompletionHandler){
//
//
//        WebAccess.getSearchLocationDataWith(
//              _Url: urlString, _parameters: [:], isHideLoader: true) { (response) in
//            switch (response){
//            case .Success( _):
//                completionHandler("","")
//                break
//            case .Error(let msg):
//                completionHandler("","")
//                ShowAlert(title: APPNAME, msg: msg, view: UIApplication.getTopViewController()!)
//                break
//            }
//        }
//
//    }
    static func getPlaceCoordinateUrl(placeId:String) -> String {
        var param:[String:Any] = [String:Any]()
        param["place_id"] =   placeId
        param["key"] = GOOGLE_KEY
        
        let urlComponents = NSURLComponents(string: "https://maps.googleapis.com/maps/api/place/details/json?")!
        
        if !param.isEmpty {
            urlComponents.queryItems = [URLQueryItem]()
            for (key, value) in param {
                let queryItem = URLQueryItem(name: key, value: "\(value)")
                urlComponents.queryItems!.append(queryItem)
            }
        }
        let url:String =   (urlComponents.url?.absoluteString ?? "" )
        guard let urlString = url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) else { return  ""}
        
        return urlString
    }
    
}

