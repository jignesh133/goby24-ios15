//
//  Global.swift
//  Smart Fitness
//
//  Created by MCA 2 on 13/03/19.
//  Copyright © 2019 stratecore. All rights reserved.
//

import UIKit
import Foundation
import Alamofire
//import CRNotifications


// struct CustomCRNotification: CRNotificationType {
//    var textColor: UIColor
//    var backgroundColor: UIColor
//    var image: UIImage?
//}


struct Connectivity {
    static let sharedInstance = NetworkReachabilityManager()!
    static var isConnectedToInternet:Bool {
        return self.sharedInstance.isReachable
    }
}

// Definition:
extension Notification.Name {
    static let refereshTopbarIcons = Notification.Name("refereshTopbarIcons")

}



// DECLARE APP NAME
let APPNAME                                 = "Goby24"//"LiveBand Tipping"

// WINDOW OBJECT OF THE SCREEN
let window = UIApplication.shared.windows.first


let INTERNET_MESSAGE                        = "Connection Failled. Please check connection"



let bundleName = Bundle.main.infoDictionary!["CFBundleName"] as! String


/// USER DEFAULT
let userDefault = UserDefaults.standard

//////////// Define the Global Variable ////////////
let appDelegate = UIApplication.shared.delegate as? AppDelegate

//// GET FULL SCREEN RECT
let fullScreenRect:CGRect = CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height)

class Global: NSObject {

}

/// DECLARE ENUM FOR USER DEFAULTS
enum  Enum_Login:String{
    case isLogin="isLogin",userId="userId",userProfileData="userProfileData",token="token",profilePic="profilePic",rememberMe="rememberMe",subscriptionType="subscriptionType",is_rider="is_rider"
}

enum Enum_fontSize:CGFloat{
    case DROPDOWN_TITLE=14,none=0
}

enum Enum_userDefaults:String{
    case language = "language"
    case languageCode = "languagecode"
    case country = "country"
}
