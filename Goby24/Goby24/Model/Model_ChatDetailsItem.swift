//
//  Model_ChatDetailsItem.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on October 9, 2021

import Foundation

struct Model_ChatDetailsItem : Codable {

        let id : Int?
        let message : String?
        let receiver : Model_Receiver?
        let sender : Model_Sender?
        let timestamp : String?

        enum CodingKeys: String, CodingKey {
                case id = "id"
                case message = "message"
                case receiver = "receiver"
                case sender = "sender"
                case timestamp = "timestamp"
        }
    
        init(from decoder: Decoder) throws {
                let values = try decoder.container(keyedBy: CodingKeys.self)
                id = try values.decodeIfPresent(Int.self, forKey: .id)
                message = try values.decodeIfPresent(String.self, forKey: .message)
                timestamp = try values.decodeIfPresent(String.self, forKey: .timestamp)
            
                receiver = try values.decodeIfPresent(Model_Receiver.self, forKey: .receiver)
                sender = try values.decodeIfPresent(Model_Sender.self, forKey: .sender)
            
        }

}
