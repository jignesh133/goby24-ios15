// Copyright 2021 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

//
//  MapViewController.swift
//  GoogleMapsSwiftUI
//
//  Created by Chris Arriola on 2/5/21.
//
typealias CompletionHandler = (_ lat:String, _ lng:String) -> Void

import GoogleMaps
import SwiftUI
import UIKit

class MapViewController: UIViewController {

  var map =  GMSMapView(frame: .zero)
  var isAnimating: Bool = false
  var routes:[String:Any] = [String:Any]()
  var arrCity:[Model_searchLocation] = [Model_searchLocation]()
    var tempArrCity:[Model_searchLocation] = [Model_searchLocation]()

  override func loadView() {
    super.loadView()
    self.view = map
    
    self.drawRoutes()
    
  }
    func drawRoutes()  {
        self.map.clear()
        let overview_polyline = routes["overview_polyline"] as? [String:Any]  ?? [:]
        let points = overview_polyline["points"] as? String ?? ""
        
        let path = GMSPath(fromEncodedPath: points)
        let polyLine = GMSPolyline(path: path)
        polyLine.strokeWidth = 2
        polyLine.strokeColor = UIColor.red
        polyLine.map = self.map

        let legs = routes["legs"] as? [[String:Any]] ?? []
        var leg:[String:Any] = [String:Any]()
        
        if legs.count > 0 {
            leg = legs.first!
            let start_location = leg["start_location"] as? [String:Any] ?? [:]
            let end_location = leg["end_location"] as? [String:Any] ?? [:]

            let start_locationName = leg["start_address"] as? String ?? ""
            let end_locationName = leg["end_address"] as? String ?? ""

            
            let startPosition = CLLocationCoordinate2D(latitude: getDoubleFromAny(start_location["lat"] ?? "0"), longitude: getDoubleFromAny(start_location["lng"] ?? "0"))
            let endPosition = CLLocationCoordinate2D(latitude: getDoubleFromAny(end_location["lat"] ?? "0"), longitude: getDoubleFromAny(end_location["lng"] ?? "0"))

            let marker1 = GMSMarker(position: startPosition)
            marker1.title = start_locationName
            marker1.map = self.map

            
            let marker2 = GMSMarker(position: endPosition)
            marker2.title = end_locationName
            marker2.map = self.map
  
            for data in arrCity{
                guard (data.lat?.count ?? 0  > 0 && data.lng?.count ?? 0  > 0) else {
                    continue
                }
                let position = CLLocationCoordinate2D(latitude: getDoubleFromAny(data.lat!), longitude: getDoubleFromAny(data.lng!))

                let marker = GMSMarker(position: position)
                marker.title = data.descriptionField
                marker.map = self.map

            }
//            tempArrCity = self.arrCity
            
//            DispatchQueue.main.async { [self] in
//                if self.tempArrCity.count > 0{
//                    repeat {
//
//                        self.getPlaceInfo(placeId: self.tempArrCity.first?.placeId ?? "") { (result) in
//                            self.tempArrCity.removeFirst()
//                        }
//
//
//                    } while self.tempArrCity.count > 0
//                }
//            }

           
            
            let bounds = GMSCoordinateBounds(coordinate: startPosition, coordinate: endPosition)
            let update = GMSCameraUpdate.fit(bounds, with: UIEdgeInsets(top: 170, left: 30, bottom: 30, right: 30))
            self.map.moveCamera(update)

        }


    }
    func getPlaceInfo(placeId:String,completionHandler: @escaping CompletionHandler){
        
        var param:[String:Any] = [String:Any]()
        param["place_id"] = "place_id:" + getStringFromAny(placeId)
        param["key"] = GOOGLE_KEY
        
        let urlComponents = NSURLComponents(string: "https://maps.googleapis.com/maps/api/place/details/json?")!
        
        if !param.isEmpty {
            urlComponents.queryItems = [URLQueryItem]()
            for (key, value) in param {
                let queryItem = URLQueryItem(name: key, value: "\(value)")
                urlComponents.queryItems!.append(queryItem)
            }
        }
        let url:String =   (urlComponents.url?.absoluteString ?? "" )
        guard let urlString = url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) else { return  }

        WebAccess.getSearchLocationDataWith(
              _Url: urlString, _parameters: [:], isHideLoader: true) { (response) in
            switch (response){
            case .Success( _):
                completionHandler("","")
                break
            case .Error(let msg):
                ShowAlert(title: APPNAME, msg: msg, view: UIApplication.getTopViewController()!)
                completionHandler("","")
                break
            }
        }
        
    }
}
