//
//  Model_Sender.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on October 9, 2021

import Foundation

struct Model_Sender : Codable {

        let firstName : String?
        let id : Int?
        let lastName : String?
        let profilePic : String?

        enum CodingKeys: String, CodingKey {
                case firstName = "first_name"
                case id = "id"
                case lastName = "last_name"
                case profilePic = "profile_pic"
        }
    
        init(from decoder: Decoder) throws {
                let values = try decoder.container(keyedBy: CodingKeys.self)
                firstName = try values.decodeIfPresent(String.self, forKey: .firstName)
                id = try values.decodeIfPresent(Int.self, forKey: .id)
                lastName = try values.decodeIfPresent(String.self, forKey: .lastName)
                profilePic = try values.decodeIfPresent(String.self, forKey: .profilePic)
        }

}
