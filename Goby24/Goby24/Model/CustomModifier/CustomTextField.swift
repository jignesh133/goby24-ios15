//
//  CustomTextField.swift
//  FirebaseStarterSwiftUIApp
//
//  Created by Duy Bui on 8/16/20.
//  Copyright © 2020 iOS App Templates. All rights reserved.
//

import SwiftUI
struct phoneNumberTextView: View {
    
    @Binding var text: String
    @Binding var dialCode: String
    private let placeHolderText: String
    private var fontSize: CGFloat = 16

    init(text: Binding<String>, dialCode: Binding<String>,placeHolderText: String){
        _text = text
        _dialCode = dialCode
        self.placeHolderText = placeHolderText
    }
    
    var body: some View {
        ZStack(alignment: .trailing) {
            
            TextField(placeHolderText, text: $text)
                .textFieldStyle(MyTextFieldStyle())
                .keyboardType(.numberPad)
                
            Button(action: {
                RPicker.selectOption(title: "Please select Country", cancelText: "Cancel", doneText: "Done", dataArray: appDelegate?.countryNamesWithCodes, selectedIndex: nil) { (val, index) in
                    dialCode = appDelegate?.arrCountryWithCode[index].dial_code ?? ""
                }
            }) {
                Image("down")
                Text(dialCode)
                    .accentColor(.gray)
            }.padding(.trailing,20)
            .modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: fontSize, fontColor: .black))

            
            
        }
    }
}

struct priceTextField: View {
    
    @Binding var text: String
    @Binding var currency: String
    private let placeHolderText: String
    var fontSize: CGFloat = 16
    let arrLanguage: [String] = ["CHF","EUR","USD","RUB"]
    
    init(text: Binding<String>, currency: Binding<String>,placeHolderText: String,fontSize: CGFloat){
        _text = text
        _currency = currency
        self.placeHolderText = placeHolderText
        self.fontSize = fontSize
    }
    
    var body: some View {
        ZStack(alignment: .trailing) {
            
            TextField(placeHolderText, text: $text)
                .textFieldStyle(MyTextFieldStyle(fontSize: fontSize))
                .keyboardType(.decimalPad)
                
            Button(action: {
                let index = self.arrLanguage.firstIndex(of: currency)
                
                RPicker.selectOption(title: "Please select Currency", cancelText: "Cancel", doneText: "Done", dataArray: arrLanguage, selectedIndex: index) { (val, index) in
                    currency = val
                }
            }) {
                Text(currency)
                Image("down")
                    .accentColor(.gray)
            }.padding(.trailing,20)
            .modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: fontSize, fontColor: .black))

            
            
        }
    }
}
//import Sliders
struct CustomTextField: View {
    
    @Binding var text: String
    private let isPasswordType: Bool
    private let placeHolderText: String
    private var fontSize: CGFloat = 16
    
    private let textFieldkeyboardType:UIKeyboardType
    @State private var isSecured: Bool = true
    
    init(placeHolderText: String, text: Binding<String>, isPasswordType: Bool = false,keyboardType:UIKeyboardType = .default,fontSize:CGFloat = 16) {
        _text = text
        self.isPasswordType = isPasswordType
        self.placeHolderText = placeHolderText
        self.textFieldkeyboardType = keyboardType
        self.fontSize = fontSize
    }
    
    var body: some View {
        ZStack(alignment: .trailing) {
            if isPasswordType == true && isSecured == true{
                SecureField(placeHolderText, text: $text)
                    .textFieldStyle(MyTextFieldStyle())
                    .keyboardType(.default)
                Button(action: {
                    isSecured.toggle()
                }) {
                    Image(systemName: self.isSecured ? "eye" : "eye")
                        .accentColor(.gray)
                }.padding(.trailing,20)
                
            } else if isPasswordType == true && isSecured == false{
                TextField(placeHolderText, text: $text)
                    .textFieldStyle(MyTextFieldStyle())
                    .keyboardType(.default)
                    
                Button(action: {
                    isSecured.toggle()
                }) {
                    Image(systemName: self.isSecured ? "eye" : "eye")
                        .accentColor(.gray)
                }.padding(.trailing,20)
                
            }
            else {
                TextField(placeHolderText, text: $text)
                    .textFieldStyle(MyTextFieldStyle(fontSize: fontSize))
                    .keyboardType(textFieldkeyboardType)
                
            }
            
            
        }
    }
}

struct MyTextFieldStyle: TextFieldStyle {
    var fontSize:CGFloat = 16
    var fontColor:Color = .black
    
    func _body(configuration: TextField<Self._Label>) -> some View {
        configuration
            .padding(EdgeInsets(top: 0, leading: 15, bottom: 0, trailing: 15))
            .frame(width: .infinity, height: 45, alignment: .center)
            .background(Color(red: 0.95, green: 0.95, blue: 0.95))
            .cornerRadius(22.5)
            .modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: fontSize, fontColor: fontColor))
            .textContentType(.none)
    }
}
struct MyTextFielWithImagedStyle: TextFieldStyle {
    func _body(configuration: TextField<Self._Label>) -> some View {
        configuration
            .padding(EdgeInsets(top: 0, leading: 15, bottom: 0, trailing: 15))
            .frame(width: .infinity, height: 45, alignment: .center)
            .background(Color(red: 0.95, green: 0.95, blue: 0.95))
            .cornerRadius(22.5)
            .modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 16, fontColor: Color.black))
            .textContentType(.none)
    }
}
struct CustomTextFieldWithDownButton: View {
    
    @Binding var text: String
    let onButtonClicked: (Bool) -> Void
    private let placeHolderText: String
    private var fontSize: CGFloat = 16
    
    private let textFieldkeyboardType:UIKeyboardType
    @State private var isSecured: Bool = true
    
    init(placeHolderText: String, text: Binding<String>, onButtonClicked: @escaping (Bool) -> Void,keyboardType:UIKeyboardType = .default,fontSize:CGFloat = 16) {
        _text = text
        self.onButtonClicked = onButtonClicked
        self.placeHolderText = placeHolderText
        self.textFieldkeyboardType = keyboardType
        self.fontSize = fontSize
    }
    
    var body: some View {
        ZStack(alignment: .trailing) {
            
            TextField(placeHolderText, text: $text)
                .textFieldStyle(MyTextFieldStyle())
                .keyboardType(.default)
            
            Button(action: {
                onButtonClicked(true)
            }) {
                Image("down")
                    .accentColor(.gray)
            }.padding(.trailing,20)
            
        }
    }
}
//struct MySliderText: RangeSliderStyle{
//    func makeBody(configuration: Configuration) -> some View {
//        configuration
//        (
//            HorizontalRangeSliderStyle(
//                track:
//                    HorizontalRangeTrack(
//                        view: Capsule().foregroundColor(.purple)
//                    )
//                    .background(Capsule().foregroundColor(Color.init(hex: "")))
//                    .frame(height: 8),
//                lowerThumb: Circle().foregroundColor(Color.init(hex: "00AEEF")),
//                upperThumb: Circle().foregroundColor(Color.init(hex: "00AEEF")),
//                lowerThumbSize: CGSize(width: 15, height: 15),
//                upperThumbSize: CGSize(width: 15, height: 15),
//                options: .forceAdjacentValue
//            )
//        )
//        
//    }
//}
