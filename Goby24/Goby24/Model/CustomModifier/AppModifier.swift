//
//  AppModifier.swift
//  GOBY24
//
//  Created by Jignesh Bhensadadiya on 7/9/21.
//

import Foundation
import SwiftUI
import UIKit


struct CustomTextM: ViewModifier {
    //MARK:- PROPERTIES
    let fontName: String
    let fontSize: CGFloat
    let fontColor: Color
    
    func body(content: Content) -> some View {
        content
            .font(.custom(fontName, size: fontSize))
            .foregroundColor(fontColor)
    }
}
struct ButtonStyle: ViewModifier {
    //MARK:- PROPERTIES
    let buttonHeight: CGFloat
    let buttonColor: Color
    let buttonRadius: CGFloat
    
    func body(content: Content) -> some View {
        content
            
          //  .frame(width: .infinity)
            .frame(minWidth: 0, maxWidth: .infinity)
            .frame(height: buttonHeight)
            .background(buttonColor)
            .cornerRadius(buttonRadius)
            .buttonStyle(PlainButtonStyle())
        
    }
}

struct HiddenNavigationBar: ViewModifier {
    func body(content: Content) -> some View {
        content
            .navigationBarTitle("", displayMode: .inline)
            .navigationBarHidden(true)
            .navigationBarBackButtonHidden(true)
    }
}
struct ShowNavigationBar: ViewModifier {
    func body(content: Content) -> some View {
        content
            .navigationBarTitle("", displayMode: .inline)
            .navigationBarHidden(false)
            .navigationBarBackButtonHidden(true)
            .toolbar { // <2>
                ToolbarItem(placement: .principal) {Image("appLogo")}
            }
    }
}
struct ShowNavigationBarClose: ViewModifier {
    @Environment(\.presentationMode) var presentationMode
    
    func body(content: Content) -> some View {
        content
            .navigationBarTitle("", displayMode: .inline)
            .navigationBarHidden(false)
            .navigationBarBackButtonHidden(true)
            .toolbar { // <2>
                //ToolbarItem(placement: .principal) {Image("appLogo")}
            }
            // Add your custom back button here
            .navigationBarItems(leading: Button(action: { self.presentationMode.wrappedValue.dismiss() }) { Image("close")})
        
    }
}
struct ShowNavigationBarWithClose: ViewModifier {
    @Environment(\.presentationMode) var presentationMode
    
    func body(content: Content) -> some View {
        content
            .navigationBarTitle("", displayMode: .inline)
            .navigationBarHidden(false)
            .navigationBarBackButtonHidden(true)
            .toolbar { // <2>
                ToolbarItem(placement: .principal) {Image("appLogo")}
            }
            // Add your custom back button here
            .navigationBarItems(leading: Button(action: { self.presentationMode.wrappedValue.dismiss() }) { Image("close")})
        
    }
}
struct ShowNavigationBarWithBack: ViewModifier {
    @Environment(\.presentationMode) var presentationMode
    
    
    func body(content: Content) -> some View {
        content
            .navigationBarTitle("", displayMode: .inline)
            .navigationBarHidden(false)
            .navigationBarBackButtonHidden(true)
            .toolbar {ToolbarItem(placement: .principal) {Image("appLogo")}}
            // Add your custom back button here
            .navigationBarItems(leading: Button(action: { self.presentationMode.wrappedValue.dismiss()}) {Image("backnav")}.frame(width: 40, height: 40))
    }
}
//struct ShowProfilePic: ViewModifier {
//    typealias Body = Image
//    
//
//    func body(content: Content) -> some View {
//        content
//            .clipShape(Circle())
//    }
//}
struct ShowCustomNavigation:View {
    @Environment(\.presentationMode) var presentationMode
    
    var body: some View {
        ZStack{
            VStack{
                Image("appLogo").padding(.top,0)
                Spacer()
            }
            VStack(alignment:.leading){
                HStack{
                    Button(action: {
                        self.presentationMode.wrappedValue.dismiss()
                    }){
                        Image("close").padding(.leading,025).padding(.top,10)
                    }
                    
                    Spacer()
                }
                Spacer()
            }
        }.frame(width: UIScreen.main.bounds.size.width, height: 40, alignment: .center)
        
    }
    
}

struct NavigationConfigurator: UIViewControllerRepresentable {
    var configure: (UINavigationController) -> Void = { _ in }
    
    func makeUIViewController(context: UIViewControllerRepresentableContext<NavigationConfigurator>) -> UIViewController {
        UIViewController()
    }
    func updateUIViewController(_ uiViewController: UIViewController, context: UIViewControllerRepresentableContext<NavigationConfigurator>) {
        if let nc = uiViewController.navigationController {
            self.configure(nc)
        }
    }
    
}

//struct ListSeparatorStyle: ViewModifier {
//    
//    let style: UITableViewCell.SeparatorStyle
//    
//    func body(content: Content) -> some View {
//        content
//            .onAppear() {
//                UITableView.appearance().separatorStyle = self.style
//            }
//    }
//}
struct NavigationUtil {
  static func popToRootView() {
    findNavigationController(viewController: UIApplication.shared.windows.filter { $0.isKeyWindow }.first?.rootViewController)?
      .popToRootViewController(animated: true)
  }
    
  static func findNavigationController(viewController: UIViewController?) -> UINavigationController? {
   
    guard let viewController = viewController else {
      return nil
    }

    print(viewController)
    if let navigationController = viewController as? UITabBarController {
        print(navigationController)
        return navigationController.selectedViewController?.navigationController
    }

    
    if let navigationController = viewController as? UINavigationController {
      return navigationController
    }

    for childViewController in viewController.children {
      return findNavigationController(viewController: childViewController)
    }

    return nil
  }
}
struct ViewDidLoadModifier: ViewModifier {

    @State private var didLoad = false
    private let action: (() -> Void)?

    init(perform action: (() -> Void)? = nil) {
        self.action = action
    }

    func body(content: Content) -> some View {
        content.onAppear {
            if didLoad == false {
                didLoad = true
                action?()
            }
        }
    }

}


//public class KeyboardInfo: ObservableObject {
//
//    public static var shared = KeyboardInfo()
//
//    @Published public var height: CGFloat = 0
//
//    private init() {
//        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardChanged), name: UIApplication.keyboardWillShowNotification, object: nil)
//        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardChanged), name: UIResponder.keyboardWillHideNotification, object: nil)
//        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardChanged), name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
//    }
//
//    @objc func keyboardChanged(notification: Notification) {
//        if notification.name == UIApplication.keyboardWillHideNotification {
//            self.height = 0
//        } else {
//            self.height = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? CGRect)?.height ?? 0
//        }
//    }
//
//}
//
//struct KeyboardAware: ViewModifier {
//    @ObservedObject private var keyboard = KeyboardInfo.shared
//
//    func body(content: Content) -> some View {
//        content
//            .padding(.bottom, self.keyboard.height)
//            .edgesIgnoringSafeArea(self.keyboard.height > 0 ? .bottom : [])
//            .animation(.easeOut)
//    }
//}
//
//extension View {
//    public func keyboardAware() -> some View {
//        ModifiedContent(content: self, modifier: KeyboardAware())
//    }
//}
