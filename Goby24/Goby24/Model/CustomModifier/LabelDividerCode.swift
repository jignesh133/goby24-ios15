//
//  LabelDividerCode.swift
//  Goby24
//
//  Created by Jignesh Bhensadadiya on 7/9/21.
//

import Foundation
import UIKit
import SwiftUI

struct LabelDividerCode: View {

    let label: String
    let horizontalPadding: CGFloat
    let color: Color
    
    init(label: String, horizontalPadding: CGFloat = 20, color: Color = .gray) {
        self.label = label
        self.horizontalPadding = horizontalPadding
        self.color = color
    }

    var body: some View {
        HStack {
            line
            Text(label).foregroundColor(color)
            line
        }
    }

    var line: some View {
        VStack { Divider().background(color) }.padding(horizontalPadding)
    }
}
