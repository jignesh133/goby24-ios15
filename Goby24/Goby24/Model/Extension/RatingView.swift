//
//  RatingView.swift
//  Goby24
//
//  Created by Jignesh Bhensadadiya on 7/15/21.
//

import SwiftUI
import StarRating

struct StarsView: View {
    
    let rating: Double
    var color:Color = .black
    var  stepType: StarRating.StepType = .half

    @State var customConfig = StarRatingConfiguration(spacing: 8,numberOfStars: 5,stepType: .half,minRating: 0,borderWidth: 1,borderColor: Color.black,emptyColor: Color.white,shadowRadius: 0,fillColors: [Color.black],starVertices: 5,starWeight: 0.45)

    func updateConfig()  {
        customConfig.stepType = self.stepType
    }

    var body: some View{
        StarRating(initialRating: rating,configuration: $customConfig)
    }
}

struct StarsViewWithDrag: View {
    
    let rating: Double
    var color:Color? = nil
    let onValueUpdated: (Double) -> Void
     
    @State var customConfig = StarRatingConfiguration(spacing: 8,numberOfStars: 5,stepType: .half,minRating: 0,borderWidth: 1,borderColor: Color.black,emptyColor: Color.white,shadowRadius: 0,fillColors: [Color.black],starVertices: 5,starWeight: 0.45)

    var body: some View{
        
        VStack(alignment: .center, spacing: 0){
            Spacer()
            StarRating(initialRating: rating,configuration: $customConfig, onRatingChanged: {
                print($0)
                onValueUpdated($0)
            })
            Spacer()
        }.frame(width: .infinity, height: .infinity, alignment: .center)
        .onAppear(){
            if (color != nil){
                customConfig.borderColor = color!
                customConfig.fillColors = [color!]
            }
        }
    }
}
 
