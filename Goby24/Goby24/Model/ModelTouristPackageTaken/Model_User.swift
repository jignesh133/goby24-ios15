//
//  Model_User.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on September 24, 2021

import Foundation

struct Model_User : Codable {

        let about : AnyObject?
        let authProvider : String?
        let city : String?
        let country : String?
        let dob : String?
        let email : String?
        let firebaseDeviceId : AnyObject?
        let firstName : String?
        let gender : String?
        let id : Int?
        let identityDocument : AnyObject?
        let identityDocumentType : AnyObject?
        let isActive : Bool?
        let isEmailVerified : Bool?
        let isIdentityVerified : Bool?
        let isMobileNoVerified : Bool?
        let isPayementMethodVerified : Bool?
        let isRider : Bool?
        let isVehicleVerified : Bool?
        let joiningDate : String?
        let lastLogin : AnyObject?
        let lastName : String?
        let mobileNo : String?
        let profilePic : AnyObject?
        let riderRating : String?
        let travelPreferences : Model_TravelPreference?
        let userLevel : Int?

        enum CodingKeys: String, CodingKey {
                case about = "about"
                case authProvider = "auth_provider"
                case city = "city"
                case country = "country"
                case dob = "dob"
                case email = "email"
                case firebaseDeviceId = "firebase_device_id"
                case firstName = "first_name"
                case gender = "gender"
                case id = "id"
                case identityDocument = "identity_document"
                case identityDocumentType = "identity_document_type"
                case isActive = "is_active"
                case isEmailVerified = "is_email_verified"
                case isIdentityVerified = "is_identity_verified"
                case isMobileNoVerified = "is_mobile_no_verified"
                case isPayementMethodVerified = "is_payement_method_verified"
                case isRider = "is_rider"
                case isVehicleVerified = "is_vehicle_verified"
                case joiningDate = "joining_date"
                case lastLogin = "last_login"
                case lastName = "last_name"
                case mobileNo = "mobile_no"
                case profilePic = "profile_pic"
                case riderRating = "rider_rating"
                case travelPreferences = "travel_preferences"
                case userLevel = "user_level"
        }
    
        init(from decoder: Decoder) throws {
                let values = try decoder.container(keyedBy: CodingKeys.self)
                about = try values.decodeIfPresent(AnyObject.self, forKey: .about)
                authProvider = try values.decodeIfPresent(String.self, forKey: .authProvider)
                city = try values.decodeIfPresent(String.self, forKey: .city)
                country = try values.decodeIfPresent(String.self, forKey: .country)
                dob = try values.decodeIfPresent(String.self, forKey: .dob)
                email = try values.decodeIfPresent(String.self, forKey: .email)
                firebaseDeviceId = try values.decodeIfPresent(AnyObject.self, forKey: .firebaseDeviceId)
                firstName = try values.decodeIfPresent(String.self, forKey: .firstName)
                gender = try values.decodeIfPresent(String.self, forKey: .gender)
                id = try values.decodeIfPresent(Int.self, forKey: .id)
                identityDocument = try values.decodeIfPresent(AnyObject.self, forKey: .identityDocument)
                identityDocumentType = try values.decodeIfPresent(AnyObject.self, forKey: .identityDocumentType)
                isActive = try values.decodeIfPresent(Bool.self, forKey: .isActive)
                isEmailVerified = try values.decodeIfPresent(Bool.self, forKey: .isEmailVerified)
                isIdentityVerified = try values.decodeIfPresent(Bool.self, forKey: .isIdentityVerified)
                isMobileNoVerified = try values.decodeIfPresent(Bool.self, forKey: .isMobileNoVerified)
                isPayementMethodVerified = try values.decodeIfPresent(Bool.self, forKey: .isPayementMethodVerified)
                isRider = try values.decodeIfPresent(Bool.self, forKey: .isRider)
                isVehicleVerified = try values.decodeIfPresent(Bool.self, forKey: .isVehicleVerified)
                joiningDate = try values.decodeIfPresent(String.self, forKey: .joiningDate)
                lastLogin = try values.decodeIfPresent(AnyObject.self, forKey: .lastLogin)
                lastName = try values.decodeIfPresent(String.self, forKey: .lastName)
                mobileNo = try values.decodeIfPresent(String.self, forKey: .mobileNo)
                profilePic = try values.decodeIfPresent(AnyObject.self, forKey: .profilePic)
                riderRating = try values.decodeIfPresent(String.self, forKey: .riderRating)
                travelPreferences = Model_TravelPreference(from: decoder)
                userLevel = try values.decodeIfPresent(Int.self, forKey: .userLevel)
        }

}
