//
//  Model_TouristPackage.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on September 24, 2021

import Foundation

struct Model_TouristPackage : Codable {

        let city : String?
        let country : String?
        let currency : String?
        let dateFrom : String?
        let dateTo : String?
        let id : Int?
        let isGuide : Bool?
        let noOfDays : String?
        let noOfTourist : String?
        let packageBanner : String?
        let packageBrief : String?
        let packageCover : String?
        let packageName : String?
        let packagePrice : String?
        let packageStatus : String?
        let paymentStatus : Bool?
        let provider : Int?
        let spot : Int?
        let timeFrom : String?
        let timeTo : String?

        enum CodingKeys: String, CodingKey {
                case city = "city"
                case country = "country"
                case currency = "currency"
                case dateFrom = "date_from"
                case dateTo = "date_to"
                case id = "id"
                case isGuide = "is_guide"
                case noOfDays = "no_of_days"
                case noOfTourist = "no_of_tourist"
                case packageBanner = "package_banner"
                case packageBrief = "package_brief"
                case packageCover = "package_cover"
                case packageName = "package_name"
                case packagePrice = "package_price"
                case packageStatus = "package_status"
                case paymentStatus = "payment_status"
                case provider = "provider"
                case spot = "spot"
                case timeFrom = "time_from"
                case timeTo = "time_to"
        }
    
        init(from decoder: Decoder) throws {
                let values = try decoder.container(keyedBy: CodingKeys.self)
                city = try values.decodeIfPresent(String.self, forKey: .city)
                country = try values.decodeIfPresent(String.self, forKey: .country)
                currency = try values.decodeIfPresent(String.self, forKey: .currency)
                dateFrom = try values.decodeIfPresent(String.self, forKey: .dateFrom)
                dateTo = try values.decodeIfPresent(String.self, forKey: .dateTo)
                id = try values.decodeIfPresent(Int.self, forKey: .id)
                isGuide = try values.decodeIfPresent(Bool.self, forKey: .isGuide)
                noOfDays = try values.decodeIfPresent(String.self, forKey: .noOfDays)
                noOfTourist = try values.decodeIfPresent(String.self, forKey: .noOfTourist)
                packageBanner = try values.decodeIfPresent(String.self, forKey: .packageBanner)
                packageBrief = try values.decodeIfPresent(String.self, forKey: .packageBrief)
                packageCover = try values.decodeIfPresent(String.self, forKey: .packageCover)
                packageName = try values.decodeIfPresent(String.self, forKey: .packageName)
                packagePrice = try values.decodeIfPresent(String.self, forKey: .packagePrice)
                packageStatus = try values.decodeIfPresent(String.self, forKey: .packageStatus)
                paymentStatus = try values.decodeIfPresent(Bool.self, forKey: .paymentStatus)
                provider = try values.decodeIfPresent(Int.self, forKey: .provider)
                spot = try values.decodeIfPresent(Int.self, forKey: .spot)
                timeFrom = try values.decodeIfPresent(String.self, forKey: .timeFrom)
                timeTo = try values.decodeIfPresent(String.self, forKey: .timeTo)
        }

}
