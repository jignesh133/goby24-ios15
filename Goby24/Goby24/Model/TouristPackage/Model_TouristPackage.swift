//
//  Model_TouristPackage.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on August 9, 2021

import Foundation

struct Model_TouristPackage : Codable {
    
    let city : String?
    let country : String?
    let dateFrom : String?
    let dateTo : String?
    let id : Int?
    let noOfDays : String?
    let noOfTourist : String?
    let packageBanner : String?
    let packageBrief : String?
    let packageCover : String?
    let packageName : String?
    let packagePrice : String?
    let packageStatus : String?
    
    let provider : Model_Provider?
    
    let spot : Model_Spot?
    let timeFrom : String?
    let timeTo : String?
    let vehicleInfo : Model_VehicleInfo?
    let currency : String?
    
    
    enum CodingKeys: String, CodingKey {
        case currency = "currency"
        case city = "city"
        case country = "country"
        case dateFrom = "date_from"
        case dateTo = "date_to"
        case id = "id"
        case noOfDays = "no_of_days"
        case noOfTourist = "no_of_tourist"
        case packageBanner = "package_banner"
        case packageBrief = "package_brief"
        case packageCover = "package_cover"
        case packageName = "package_name"
        case packagePrice = "package_price"
        case packageStatus = "package_status"
        case provider = "provider"
        case spot = "spot"
        case timeFrom = "time_from"
        case timeTo = "time_to"
        case vehicleInfo = "vehicle_info"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        
        currency = try values.decodeIfPresent(String.self, forKey: .currency)
        
        
        city = try values.decodeIfPresent(String.self, forKey: .city)
        country = try values.decodeIfPresent(String.self, forKey: .country)
        dateFrom = try values.decodeIfPresent(String.self, forKey: .dateFrom)
        dateTo = try values.decodeIfPresent(String.self, forKey: .dateTo)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        noOfDays = getStringFromAny(try values.decodeIfPresent(Int.self, forKey: .noOfDays) ?? "0")
        noOfTourist = getStringFromAny(try values.decodeIfPresent(Int.self, forKey: .noOfTourist) ?? "0")
        packageBanner = try values.decodeIfPresent(String.self, forKey: .packageBanner)
        packageBrief = try values.decodeIfPresent(String.self, forKey: .packageBrief)
        packageCover = try values.decodeIfPresent(String.self, forKey: .packageCover)
        packageName = try values.decodeIfPresent(String.self, forKey: .packageName)
        packagePrice = try values.decodeIfPresent(String.self, forKey: .packagePrice)
        packageStatus = try values.decodeIfPresent(String.self, forKey: .packageStatus)
        timeFrom = try values.decodeIfPresent(String.self, forKey: .timeFrom)
        timeTo = try values.decodeIfPresent(String.self, forKey: .timeTo)
        
        if let d = try? values.decodeIfPresent(Model_VehicleInfo.self, forKey: .vehicleInfo) {
            vehicleInfo = d
        }else{
            vehicleInfo = nil
        }
        
        if let s = try values.decodeIfPresent(Model_Spot.self, forKey: .spot) {
            spot = s
        }else{
            spot = nil
        }
        
//        if let p = try values.decodeIfPresent(String.self, forKey: .provider) {
//            provider = nil
//        }else
        if let p = try values.decodeIfPresent(Model_Provider?.self, forKey: .provider) {
            provider = p
        }else{
            provider = nil
        }
    }
}
