//
//  Model_VehicleInfo.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on August 9, 2021

import Foundation

struct Model_VehicleInfo : Codable {

        let brand : String?
        let model : String?
        let vehicleNo : String?

        enum CodingKeys: String, CodingKey {
                case brand = "brand"
                case model = "model"
                case vehicleNo = "vehicle_no"
        }
    
        init(from decoder: Decoder) throws {
                let values = try decoder.container(keyedBy: CodingKeys.self)
                brand = try values.decodeIfPresent(String.self, forKey: .brand)
                model = try values.decodeIfPresent(String.self, forKey: .model)
                vehicleNo = try values.decodeIfPresent(String.self, forKey: .vehicleNo)
        }

}
