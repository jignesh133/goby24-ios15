//
//  DatepickerView.swift
//  Goby24
//
//  Created by Jignesh Bhensadadiya on 7/25/21.
//

import Foundation
import SwiftUI

struct DatePickerWithButtons: View {
    
    @State var showDatePicker: Bool = true
    @Binding var selectedDate: Date
    let pickedDate: (Date) -> Void

    @Environment(\.presentationMode) var presentationMode

    var body: some View {
        ZStack {
            
            Color.black.opacity(0.3)
                .edgesIgnoringSafeArea(.all)
            
            
            VStack {
                DatePicker("Select Date", selection: $selectedDate,in: ...Date(), displayedComponents: [.date] )
                    .datePickerStyle(GraphicalDatePickerStyle()).foregroundColor(.yellow)
                
                Divider()
                HStack {
                    
//                    Button(action: {
//                        showDatePicker = false
//                    }, label: {
//                        Text("Cancel")
//                    })
                    
                    Spacer()
                    
                    Button(action: {
                       // savedDate = selectedDate
                        showDatePicker = false
                        self.pickedDate(selectedDate)
                        self.presentationMode.wrappedValue.dismiss()
                    }, label: {
                        Text("Save")
                            .bold()
                    })
                    
                }
                .padding(.horizontal)
                
            }
            .padding()
            .background(
                Color.white
                    .cornerRadius(30)
            )
           // .frame(height: 300)
            
        }.showNavigationCloseButton()
        
    }
}

//struct DatePickerWithButtons_Previews: PreviewProvider {
//    static var previews: some View {
//        DatePickerWithButtons()
//
//        // /*@START_MENU_TOKEN@*/Text("Hello, World!")/*@END_MENU_TOKEN@*/
//    }
//}
