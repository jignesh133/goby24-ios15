
import Foundation
import SwiftUI


class AddVihicle :     ObservableObject{
    
    static var shared = AddVihicle()
    
    private init() {}

    @Published var country:String?
    @Published var licenceNo:String?
    
    @Published var licenceFImage:Image? = nil
    @Published var licenceRImage:Image? = nil

    @Published var carBrand:String?
    @Published var carModel:String?

    deinit {
        
    }
    class func destroy() {
        shared = AddVihicle.init()
        
    }
}

class OfferRideData:ObservableObject {
    
    static var shared = OfferRideData()
    
    private init() {}

    @Published var sourceLocation:Model_searchLocation?
    @Published var destinationLocation:Model_searchLocation?
    @Published var selectedRoutes:[String:Any]?
    
    @Published var arrCityList:[Model_searchLocation]?
    @Published var pickupDateAndTime:Date?
    @Published var matrix:Model_distanceMatric?
    @Published var arrRoutes:[Model_subRoutes]?

    @Published var seats:Int = 0
    
    @Published var currency:String?
    @Published var price_per_km:String?
    
    @Published var paymentMode:String = "E-Payment"
    @Published var bookInstantly:String = "yes"

    @Published var distance:String = ""
    @Published var duration:String = ""

    
    // USED FOR SEND DATA TO SERVER
    @Published var arrSubRoutes:[Model_SepratedRoutes]?
   
    @Published var pickedDate:Date?
    
    deinit {
        
    }
    class func destroy() {
        shared = OfferRideData.init()
        
    }
//     func destroy() {
//
////        sourceLocation = nil
////        destinationLocation = nil
////        selectedRoutes = [:]
////        arrCityList = nil
////        pickupDateAndTime = nil
////        matrix = nil
////        arrRoutes = []
////        seats = nil
////        currency = ""
////        price_per_km = ""
////        paymentMode = "E-Payment"
////        bookInstantly = "yes"
////        arrSubRoutes = nil
////        pickedDate = nil
//    }

}

//class OfferRideData: ObservableObject {
//    static var shared = OfferRideData()
//    private init() {}
//
//    @Published var sourceLocation:Model_searchLocation?
//    @Published var destinationLocation:Model_searchLocation?
//    @Published var selectedRoutes:[String:Any]?
//
//    @Published var arrCityList:[Model_searchLocation]?
//    @Published var pickupDateAndTime:Date?
//    @Published var matrix:Model_distanceMatric?
//    @Published var arrRoutes:[Model_subRoutes]?
//
//    @Published var seats:Int?
//
//    @Published var currency:String?
//    @Published var price_per_km:String?
//
//    @Published var paymentMode:String = "E-Payment"
//    @Published var bookInstantly:String = "yes"
//
//    // USED FOR SEND DATA TO SERVER
//    @Published var arrSubRoutes:[Model_SepratedRoutes]?
//
//    @Published var pickedDate:Date?
//
//    deinit {
//
//    }
//    class func destroy() {
//        shared = OfferRideData.init()
//
//    }
////     func destroy() {
////
//////        sourceLocation = nil
//////        destinationLocation = nil
//////        selectedRoutes = [:]
//////        arrCityList = nil
//////        pickupDateAndTime = nil
//////        matrix = nil
//////        arrRoutes = []
//////        seats = nil
//////        currency = ""
//////        price_per_km = ""
//////        paymentMode = "E-Payment"
//////        bookInstantly = "yes"
//////        arrSubRoutes = nil
//////        pickedDate = nil
////    }
//
//}
class OfferRideReturnRouteData: ObservableObject {
    
    static var shared = OfferRideReturnRouteData()
    
    private init() {}

    @Published var sourceLocation:Model_searchLocation?
    @Published var destinationLocation:Model_searchLocation?
    @Published var selectedRoutes:[String:Any]?
    
    @Published var arrCityList:[Model_searchLocation]?
    @Published var pickupDateAndTime:Date?
    @Published var matrix:Model_distanceMatric?
    @Published var arrRoutes:[Model_subRoutes]?

    @Published var seats:Int?
    
    @Published var currency:String?
    @Published var price_per_km:String?
    
    @Published var paymentMode:String = "cash"
    @Published var bookInstantly:String = "can"

    @Published var arrSubRoutes:[Model_SepratedRoutes]?

    @Published var pickedDaet:Date?
    
    @Published var distance:String = ""
    @Published var duration:String = ""

    class func destroy() {
        shared = OfferRideReturnRouteData.init()
    }
    
}
class SearchRouteData: ObservableObject {
    static var shared = SearchRouteData()
    private init() {
    }
    
    @Published var petAllowed:Bool = false
    @Published var smokAllowed:Bool = false
    
    @Published var passenger:Int = 1

    @Published var sourceLocation:Model_searchLocation?
    @Published var destinationLocation:Model_searchLocation?

    @Published var journeyDate:Date?

    @Published var totalFare:Double?
    @Published var currency:String?

    
    class func destroy() {
        //print(shared.journeyDate)
        //shared = SearchRouteData()
        shared = SearchRouteData.init()
       // print(shared.journeyDate)
        //shared = SearchRouteData()
    }
}
class TouristPackageOffer: ObservableObject {
    static var shared = TouristPackageOffer()
    
    private init() {}
    
    @Published var strCountry:String?
    @Published var touristPackage:Model_TouristPackage?
    @Published var passanger:String?
    @Published var days:String?

    @Published var availableStartDate:String = Date().getFormattedDate(formatter: "YYYY-MM-dd")
    @Published var availableEndDate:String = Date().getFormattedDate(formatter: "YYYY-MM-dd")

    @Published var availableStartTime:String = Date().getFormattedDate(formatter: "HH:mm")
    @Published var availableEndTime:String = Date().getFormattedDate(formatter: "HH:mm")

    @Published var price:String?

    class func destroy() {
        shared = TouristPackageOffer.init()
    }
    
}
class TouristPackageOfferEdit: ObservableObject {
    static var shared = TouristPackageOfferEdit()
    
    private init() {}
    
    @Published var strCountry:String?
    @Published var touristPackage:Model_TouristPackage?
    @Published var passanger:String?
    @Published var days:String?

    @Published var availableStartDate:String = Date().getFormattedDate(formatter: "dd.M.yy")
    @Published var availableEndDate:String = Date().getFormattedDate(formatter: "dd.M.yy")

    @Published var availableStartTime:String = Date().getFormattedDate(formatter: "hh:mm a")
    @Published var availableEndTime:String = Date().getFormattedDate(formatter: "hh:mm a")

    @Published var price:String?

    class func destroy() {
        shared = TouristPackageOfferEdit.init()
    }
    
}
class TouristOfferBook: ObservableObject {
    static var shared = TouristOfferBook()
    
    private init() {}
    
    @Published var jouneyDate:String?
    @Published var passanger:String?
    @Published var startJournyLocation:String = ""
    @Published var endJournyLocation:String = ""
    
    class func destroy() {
        shared = TouristOfferBook.init()
    }
    
}
class searchRideFilter: ObservableObject {
    static var shared = searchRideFilter()

    @Published var pickup_time_gte:String?
    @Published var pickup_time_lte:String?

    
    @Published var fare_gte:String?
    @Published var fare_lte:String?

    @Published var rating:String?
    @Published var isFilterNeedToAdd:Bool = false

    
    private init() {}
    
    
    
    class func destroy() {
     //   shared = searchRideFilter.init()
        shared.pickup_time_lte = ""
        shared.pickup_time_gte = ""
        shared.fare_gte = ""
        shared.fare_lte = ""
        shared.isFilterNeedToAdd = false
        
    }
}

class searchRideSort: ObservableObject {
    static var shared = searchRideSort()

    @Published var sort_by_rating:String = "asc"
    @Published var sort_by_price:String = "asc"
    @Published var sort_by_time:String = "asc"
    @Published var isSortNeedToAdd:Bool = false

    private init() {}
    
    
    
    class func destroy() {
        //shared = searchRideSort.init()
        shared.sort_by_rating = ""
        shared.sort_by_price = ""
        shared.sort_by_time = ""
        shared.isSortNeedToAdd = false
    }
}
