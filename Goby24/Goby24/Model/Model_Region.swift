//
//  Model_Region.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on August 26, 2021

import Foundation

struct Model_Region : Codable {

        let name : String?
        let shortCode : String?

        enum CodingKeys: String, CodingKey {
                case name = "name"
                case shortCode = "shortCode"
        }
    
        init(from decoder: Decoder) throws {
                let values = try decoder.container(keyedBy: CodingKeys.self)
                name = try values.decodeIfPresent(String.self, forKey: .name)
                shortCode = try values.decodeIfPresent(String.self, forKey: .shortCode)
        }

}
