//
//  Model_Result.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on October 3, 2021

import Foundation

struct Model_feedback_rider : Codable {

        let noOfRatings : Int?
        let ratingDetail : [Model_RatingDetail]?
        let totalRating : Float?

        enum CodingKeys: String, CodingKey {
                case noOfRatings = "no_of_ratings"
                case ratingDetail = "rating_detail"
                case totalRating = "total_rating"
        }
    
        init(from decoder: Decoder) throws {
                let values = try decoder.container(keyedBy: CodingKeys.self)
                noOfRatings = try values.decodeIfPresent(Int.self, forKey: .noOfRatings)
                ratingDetail = try values.decodeIfPresent([Model_RatingDetail].self, forKey: .ratingDetail)
                totalRating = try values.decodeIfPresent(Float.self, forKey: .totalRating)
        }

}
