//
//  Model_RatingDetail.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on October 3, 2021

import Foundation

struct Model_RatingDetail : Codable {

        let rating : Float?
        let remark : String?
        let user : Model_User?

        enum CodingKeys: String, CodingKey {
                case rating = "rating"
                case remark = "remark"
                case user = "user"
        }
        init(from decoder: Decoder) throws {
                let values = try decoder.container(keyedBy: CodingKeys.self)
                rating = try values.decodeIfPresent(Float.self, forKey: .rating)
                remark = try values.decodeIfPresent(String.self, forKey: .remark)
                user = try values.decodeIfPresent(Model_User.self, forKey: .user)
        }
}
