//
//  Model_ChatHome.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on October 9, 2021

import Foundation

struct Model_ChatHome : Codable {

        let message : String?
        let user : Model_Sender?
        let timestamp : String?

        enum CodingKeys: String, CodingKey {
                case message = "message"
                case user = "user"
                case timestamp = "timestamp"
        }
    
        init(from decoder: Decoder) throws {
                let values = try decoder.container(keyedBy: CodingKeys.self)
                message = try values.decodeIfPresent(String.self, forKey: .message)
            user = try values.decodeIfPresent(Model_Sender.self, forKey: .user)
            timestamp = try values.decodeIfPresent(String.self, forKey: .timestamp)
        }

}
