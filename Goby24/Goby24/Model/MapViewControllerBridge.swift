// Copyright 2021 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

//
//  MapViewControllerBridge.swift
//  GoogleMapsSwiftUI
//
//  Created by Chris Arriola on 2/5/21.
//

import GoogleMaps
import SwiftUI

struct MapViewControllerBridge: UIViewControllerRepresentable {
    
    var mapRoutes:[String:Any]?
    var arrCity:[Model_searchLocation]?

    init(mapRoutes:[String:Any],arrCity:[Model_searchLocation]) {
        self.mapRoutes = mapRoutes
        self.arrCity = arrCity
    }
    func makeUIViewController(context: Context) -> MapViewController {
        let controller = MapViewController()
        controller.routes = mapRoutes ?? [:]
        controller.arrCity = arrCity ?? []

        return controller
    }
    
    func updateUIViewController(_ uiViewController: MapViewController, context: Context) {
        
        uiViewController.routes = mapRoutes ?? [:]
        uiViewController.arrCity = arrCity ?? []
            uiViewController.drawRoutes()
        


    }
}

