//
//  Model_Rider.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on August 14, 2021

import Foundation

struct Model_Rider : Codable {

        let firstName : AnyObject?
        let lastName : AnyObject?
        let profilePic : AnyObject?

        enum CodingKeys: String, CodingKey {
                case firstName = "first_name"
                case lastName = "last_name"
                case profilePic = "profile_pic"
        }
    
        init(from decoder: Decoder) throws {
                let values = try decoder.container(keyedBy: CodingKeys.self)
                firstName = try values.decodeIfPresent(AnyObject.self, forKey: .firstName)
                lastName = try values.decodeIfPresent(AnyObject.self, forKey: .lastName)
                profilePic = try values.decodeIfPresent(AnyObject.self, forKey: .profilePic)
        }

}
