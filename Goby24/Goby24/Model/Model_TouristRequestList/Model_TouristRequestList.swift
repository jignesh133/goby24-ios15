//
//  Model_TouristRequestList.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on August 14, 2021

import Foundation

struct Model_TouristRequestList : Codable {

    
    
        let currency : String?
        let descriptionField : String?
        let id : Int?
        let journeyEndDate : String?
        let journeyFrom : String?
        let journeyStartDate : String?
        let journeyTo : String?
        let newTouristSpot : String?
        let noOfPassengers : String?
        let packageBudget : String?
        let requestStatus : String?
        let ispaid : Bool?
        let rider : Rider_WITH_NULL?
        let touristspot : Model_Touristspot?
        let user : Model_User?

        enum CodingKeys: String, CodingKey {
                case ispaid = "is_paid"
                case currency = "currency"
                case descriptionField = "description"
                case id = "id"
                case journeyEndDate = "journey_end_date"
                case journeyFrom = "journey_from"
                case journeyStartDate = "journey_start_date"
                case journeyTo = "journey_to"
                case newTouristSpot = "new_tourist_spot"
                case noOfPassengers = "no_of_passengers"
                case packageBudget = "package_budget"
                case requestStatus = "request_status"
                case rider = "rider"
                case touristspot = "touristspot"
                case user = "user"
        }
    
        init(from decoder: Decoder) throws {
                let values = try decoder.container(keyedBy: CodingKeys.self)
                currency = try values.decodeIfPresent(String.self, forKey: .currency)
                descriptionField = try values.decodeIfPresent(String.self, forKey: .descriptionField)
                id = try values.decodeIfPresent(Int.self, forKey: .id)
                journeyEndDate = try values.decodeIfPresent(String.self, forKey: .journeyEndDate)
                journeyFrom = try values.decodeIfPresent(String.self, forKey: .journeyFrom)
                journeyStartDate = try values.decodeIfPresent(String.self, forKey: .journeyStartDate)
                journeyTo = try values.decodeIfPresent(String.self, forKey: .journeyTo)
                newTouristSpot = try values.decodeIfPresent(String.self, forKey: .newTouristSpot)
                noOfPassengers = try values.decodeIfPresent(String.self, forKey: .noOfPassengers)
                packageBudget = try values.decodeIfPresent(String.self, forKey: .packageBudget)
                requestStatus = try values.decodeIfPresent(String.self, forKey: .requestStatus)
            
            ispaid = try values.decodeIfPresent(Bool.self, forKey: .ispaid)
            
                rider = try values.decodeIfPresent(Rider_WITH_NULL.self, forKey: .rider)
            
                if let d = try? values.decodeIfPresent(Model_Touristspot.self, forKey: .touristspot) {
                    touristspot = d
                }else{
                    touristspot = nil
                }

//                touristspot = try values.decodeIfPresent(Model_Touristspot_NULL.self, forKey: .touristspot)
                user = try values.decodeIfPresent(Model_User.self, forKey: .user)
        
        }

}
