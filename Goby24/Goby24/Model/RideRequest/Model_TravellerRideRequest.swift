//
//  Model_rideRequest.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on September 24, 2021

import Foundation

struct Model_TravellerRideRequest : Codable {
    
    let country : String?
    let currency : String?
    let date : String?
    let fare : String?
    let id : Int?
    let isRiderRated : Bool?
    let isTravellerRated : Bool?
    let paymentMethod : String?
    let requestStatus : String?
    let route : [String]?
    let seats : String?
    let time : String?
    
    let traveller : Model_Traveller?
    let rider : Model_Rider?
    
    enum CodingKeys: String, CodingKey {
        case country = "country"
        case currency = "currency"
        case date = "date"
        case fare = "fare"
        case id = "id"
        case isRiderRated = "is_rider_rated"
        case isTravellerRated = "is_traveller_rated"
        case paymentMethod = "paymentMethod"
        case requestStatus = "requestStatus"
        case rider = "rider"
        case route = "route"
        case seats = "seats"
        case time = "time"
        case traveller = "traveller"
    }
    
    init(from decoder: Decoder) throws {
        
        let values = try decoder.container(keyedBy: CodingKeys.self)
        
        country = try values.decodeIfPresent(String.self, forKey: .country)
        currency = try values.decodeIfPresent(String.self, forKey: .currency)
        date = try values.decodeIfPresent(String.self, forKey: .date)
        fare = try values.decodeIfPresent(String.self, forKey: .fare)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        isRiderRated = try values.decodeIfPresent(Bool.self, forKey: .isRiderRated)
        isTravellerRated = try values.decodeIfPresent(Bool.self, forKey: .isTravellerRated)
        paymentMethod = try values.decodeIfPresent(String.self, forKey: .paymentMethod)
        requestStatus = try values.decodeIfPresent(String.self, forKey: .requestStatus)
        
        route = try values.decodeIfPresent([String].self, forKey: .route)
        seats = try values.decodeIfPresent(String.self, forKey: .seats)
        time = try values.decodeIfPresent(String.self, forKey: .time)
        
        if let x = try values.decodeIfPresent(Model_Traveller.self, forKey: .traveller){
            traveller = x
        }else{
            traveller = nil
        }
        
        rider =  try values.decodeIfPresent(Model_Rider.self, forKey: .rider)
//        if let riderData =   try values.decodeIfPresent(Model_Rider?.self, forKey: .rider){
//            rider = riderData
//        }else{
//            rider = nil
//        }
        
    }
}
struct Model_RiderRideRequest : Codable {
    
    let country : String?
    let currency : String?
    let date : String?
    let fare : String?
    let id : Int?
    let isRiderRated : Bool?
    let isTravellerRated : Bool?
    let paymentMethod : String?
    let requestStatus : String?
    let rider : Model_Rider?
    let route : [String]?
    let seats : String?
    let time : String?
    let traveller : Model_Traveller?
    
    enum CodingKeys: String, CodingKey {
        case country = "country"
        case currency = "currency"
        case date = "date"
        case fare = "fare"
        case id = "id"
        case isRiderRated = "is_rider_rated"
        case isTravellerRated = "is_traveller_rated"
        case paymentMethod = "paymentMethod"
        case requestStatus = "requestStatus"
        case rider = "rider"
        case route = "route"
        case seats = "seats"
        case time = "time"
        case traveller = "traveller"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        country = try values.decodeIfPresent(String.self, forKey: .country)
        currency = try values.decodeIfPresent(String.self, forKey: .currency)
        date = try values.decodeIfPresent(String.self, forKey: .date)
        fare = try values.decodeIfPresent(String.self, forKey: .fare)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        isRiderRated = try values.decodeIfPresent(Bool.self, forKey: .isRiderRated)
        isTravellerRated = try values.decodeIfPresent(Bool.self, forKey: .isTravellerRated)
        paymentMethod = try values.decodeIfPresent(String.self, forKey: .paymentMethod)
        requestStatus = try values.decodeIfPresent(String.self, forKey: .requestStatus)
        
        if let x = try values.decodeIfPresent(Model_Traveller.self, forKey: .traveller){
            traveller = x
        }else{
            traveller = nil
        }
        if let rid = try values.decodeIfPresent(Model_Rider.self, forKey: .rider){
            rider = rid
        }else{
            rider = nil
        }
        
        
        
        route = try values.decodeIfPresent([String].self, forKey: .route)
        seats = try values.decodeIfPresent(String.self, forKey: .seats)
        time = try values.decodeIfPresent(String.self, forKey: .time)
    }
    
}
