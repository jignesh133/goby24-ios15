//
//  DropdownView.swift
//  Goby24
//
//  Created by Jignesh Bhensadadiya on 7/26/21.
//

import SwiftUI

struct DropdownView: View {
    @State var isExpanded:Bool = false
    @State var value:String = ""
    var arrList:[String] = ["A","B","C","D","E"]
      let onItemSelectedIndex: (Int) -> Void
    
    var body: some View {
        VStack(alignment:.leading,spacing:0){
            DisclosureGroup(isExpanded: $isExpanded) {
                ForEach(0..<arrList.count) { row in
                    Group{
                        let itemIndex = row
                        HStack{
                            
                            Button(action: {
                                withAnimation{
                                    value = arrList[itemIndex]
                                       onItemSelectedIndex(itemIndex)
                                    if (arrList.count > 0){
                                        isExpanded.toggle()
                                    }
                                }
                            }){
                                HStack{
                                    Text(arrList[itemIndex]).contentShape(Rectangle())

                                    Spacer()
                                }
                            }
                            .modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 16, fontColor: Color.init(hex: "2E2E2E")))
                            .modifier(ButtonStyle(buttonHeight: 40, buttonColor: Color.clear, buttonRadius: 50))
                            .onTapGesture(perform: {
                                value = arrList[itemIndex]
                                onItemSelectedIndex(itemIndex)
                                if (arrList.count > 0){
                                    isExpanded.toggle()
                                }
                            })
                            //Spacer()
                        }.padding(.leading,15).padding(.bottom,5)
                        
                    }
                }
                // Spacer(height:10)
            } label: {
                HStack{
                    Text(value).padding(.horizontal).modifier(CustomTextM(fontName: UIFont.Montserrat.regular.fontName, fontSize: 15, fontColor: Color.init(hex: "2E2E2E"))) .contentShape(Rectangle())

                    Spacer()
                    //                            Image("down").padding(.trailing,-10)
                    Image(isExpanded ? "up":"down").padding(.trailing,-10)
                    
                }
                .padding(.trailing)
              //  .frame(minWidth: 0, maxWidth: .infinity)
                .frame(height: 40)
                .onTapGesture {
                    withAnimation{
                        if (arrList.count > 0){
                            isExpanded.toggle()
                        }
                    }
                }
            }
            .accentColor(Color.clear)
            .background(Color.init(hex: "F2F2F2"))
            .cornerRadius(24)
            .modifier(CustomTextM(fontName: UIFont.Montserrat.medium.fontName, fontSize: 15, fontColor: Color.init(hex: "2E2E2E"))).multilineTextAlignment(.center)
            .padding()
            
        }
    }
}

//struct DropdownView_Previews: PreviewProvider {
//    static var previews: some View {
//        DropdownView()
//    }
//}
//struct SampleDropDown: View {
//
//    @State var arrLocation:[Model_searchLocation] = [Model_searchLocation]()
//     let onLocationSelect1: (Model_searchLocation) -> Void
//    
//    
//    var body: some View {
//
//        VStack(alignment: .leading, spacing: 4){
//            List{
//                ForEach(0..<self.$arrLocation.wrappedValue.count, id: \.self) { index in
//                    Button(action: {
//                        onLocationSelect1(arrLocation[index])
//                    }) {
//                        VStack{
//                            Text(self.$arrLocation.wrappedValue[index].descriptionField ?? "NONE").modifier(CustomTextM(fontName: UIFont.Poppins.regular.fontName, fontSize: 14, fontColor: Color.init(hex: "2E2E2E"))).multilineTextAlignment(.leading)
//                            
//                            Text(self.$arrLocation.wrappedValue[index].descriptionField ?? "NONE").modifier(CustomTextM(fontName: UIFont.Rubik.medium.fontName, fontSize: 14, fontColor: Color.init(hex: "707070"))).multilineTextAlignment(.leading)
//                            Divider().background(Color.init(hex: "707070"))
//                        }
//                    }
//                }
//            }
//        }.padding(.all, 12)
//        .background(RoundedRectangle(cornerRadius: 6).foregroundColor(.white).shadow(radius: 2))
//
//    }
//}


//struct DropdownOptionElement: View {
//    var val: String
//    var key: String
//    var onSelect: ((_ key: String) -> Void)?
//
//    var body: some View {
//        Button(action: {
//            if let onSelect = self.onSelect {
//                onSelect(self.key)
//            }
//        }) {
//            Text(self.val)
//        }
//        .padding(.horizontal, 20)
//        .padding(.vertical, 5)
//    }
//}
