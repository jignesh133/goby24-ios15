//
//  Model_Countrys.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on August 26, 2021

import Foundation

struct Model_Countrys : Codable {

        let countryName : String?
        let countryShortCode : String?
        let regions : [Model_Region]?

        enum CodingKeys: String, CodingKey {
                case countryName = "countryName"
                case countryShortCode = "countryShortCode"
                case regions = "regions"
        }
    
        init(from decoder: Decoder) throws {
                let values = try decoder.container(keyedBy: CodingKeys.self)
                countryName = try values.decodeIfPresent(String.self, forKey: .countryName)
                countryShortCode = try values.decodeIfPresent(String.self, forKey: .countryShortCode)
                regions = try values.decodeIfPresent([Model_Region].self, forKey: .regions)
        }

}

struct Model_CountrysWithDialCode : Codable {

        let name : String?
        let dial_code : String?
        let code : String?

        enum CodingKeys: String, CodingKey {
                case name = "name"
                case dial_code = "dial_code"
                case code = "code"
        }
    
        init(from decoder: Decoder) throws {
           let values = try decoder.container(keyedBy: CodingKeys.self)
            name = try values.decodeIfPresent(String.self, forKey: .name)
            dial_code = try values.decodeIfPresent(String.self, forKey: .dial_code)
            code = try values.decodeIfPresent(String.self, forKey: .code)
        }

}
