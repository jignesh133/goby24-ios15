//
//  Model_BasicRide_CompletedTransaction.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on September 25, 2021

import Foundation

struct Model_BasicRide_CompletedTransaction : Codable {

        let amountPaid : String?
        let booking : Int?
        let date : String?
        let endLocation : String?
        let paymentMethod : String?
        let paymentStatus : String?
        let rider : Model_Rider?
        let startLocation : String?
        let totalFare : String?
        let transactionId : String?
        let traveller : Model_Traveller?
        let currency : String?
    
    
        enum CodingKeys: String, CodingKey {
                case currency = "currency"
       
                case amountPaid = "amount_paid"
                case booking = "booking"
                case date = "date"
                case endLocation = "end_location"
                case paymentMethod = "payment_method"
                case paymentStatus = "payment_status"
                case rider = "rider"
                case startLocation = "start_location"
                case totalFare = "total_fare"
                case transactionId = "transaction_id"
                case traveller = "traveller"
        }
    
        init(from decoder: Decoder) throws {
                let values = try decoder.container(keyedBy: CodingKeys.self)
                amountPaid = try values.decodeIfPresent(String.self, forKey: .amountPaid)
                booking = try values.decodeIfPresent(Int.self, forKey: .booking)
                date = try values.decodeIfPresent(String.self, forKey: .date)
                endLocation = try values.decodeIfPresent(String.self, forKey: .endLocation)
                paymentMethod = try values.decodeIfPresent(String.self, forKey: .paymentMethod)
                paymentStatus = try values.decodeIfPresent(String.self, forKey: .paymentStatus)
                startLocation = try values.decodeIfPresent(String.self, forKey: .startLocation)
                totalFare = try values.decodeIfPresent(String.self, forKey: .totalFare)
                transactionId = try values.decodeIfPresent(String.self, forKey: .transactionId)
        
                currency = try values.decodeIfPresent(String.self, forKey: .currency)
    
            
            
            if let t =   try values.decodeIfPresent(Model_Traveller.self, forKey: .traveller){
                traveller = t
            }else{
                traveller = nil
            }
            
            
            if let r =   try values.decodeIfPresent(Model_Rider.self, forKey: .rider){
                rider = r
            }else{
                rider = nil
            }
            
          
        }

}
struct Model_Ride_Request_CompletedTransaction : Codable {

        let amountPaid : String?
        let booking : Int?
        let date : String?
        let endLocation : String?
        let paymentMethod : String?
        let paymentStatus : String?
        let rider : Model_Rider?
        let startLocation : String?
        let totalFare : String?
        let transactionId : String?
        let traveller : Model_Traveller?
        let rideRequest : Model_RideRequest?
        let currency : String?

    
        enum CodingKeys: String, CodingKey {
            
                case currency = "currency"

                case amountPaid = "amount_paid"
                case booking = "booking"
                case date = "date"
                case endLocation = "end_location"
                case paymentMethod = "payment_method"
                case paymentStatus = "payment_status"
                case rider = "rider"
                case startLocation = "start_location"
                case totalFare = "total_fare"
                case transactionId = "transaction_id"
                case traveller = "traveller"
            case rideRequest = "ride_request"
        }
    
        init(from decoder: Decoder) throws {
                let values = try decoder.container(keyedBy: CodingKeys.self)
                currency = try values.decodeIfPresent(String.self, forKey: .currency)
            
                amountPaid = try values.decodeIfPresent(String.self, forKey: .amountPaid)
                booking = try values.decodeIfPresent(Int.self, forKey: .booking)
                date = try values.decodeIfPresent(String.self, forKey: .date)
                endLocation = try values.decodeIfPresent(String.self, forKey: .endLocation)
                paymentMethod = try values.decodeIfPresent(String.self, forKey: .paymentMethod)
                paymentStatus = try values.decodeIfPresent(String.self, forKey: .paymentStatus)
                startLocation = try values.decodeIfPresent(String.self, forKey: .startLocation)
                totalFare = try values.decodeIfPresent(String.self, forKey: .totalFare)
                transactionId = try values.decodeIfPresent(String.self, forKey: .transactionId)
        
            if let t =   try values.decodeIfPresent(Model_Traveller.self, forKey: .traveller){
                traveller = t
            }else{
                traveller = nil
            }
            rideRequest = try values.decodeIfPresent(Model_RideRequest.self, forKey: .rideRequest)

            
            if let r =   try values.decodeIfPresent(Model_Rider.self, forKey: .rider){
                rider = r
            }else{
                rider = nil
            }
            
          
        }

}
struct Model_Tourist_Package_CompletedTransaction : Codable {

        let amountPaid : String?
        let booking : Int?
        let date : String?
        let endLocation : String?
        let paymentMethod : String?
        let paymentStatus : String?
        let rider : Model_Rider?
        let startLocation : String?
        let totalFare : String?
        let transactionId : String?
        let traveller : Model_Traveller?
        let currency : String?
    let package_name : String?

    
        enum CodingKeys: String, CodingKey {
            case currency = "currency"
            case package_name = "package_name"
                case amountPaid = "amount_paid"
                case booking = "booking"
                case date = "date"
                case endLocation = "end_location"
                case paymentMethod = "payment_method"
                case paymentStatus = "payment_status"
                case rider = "rider"
                case startLocation = "start_location"
                case totalFare = "total_fare"
                case transactionId = "transaction_id"
                case traveller = "traveller"
        }
    
        init(from decoder: Decoder) throws {
                let values = try decoder.container(keyedBy: CodingKeys.self)
                amountPaid = try values.decodeIfPresent(String.self, forKey: .amountPaid)
                booking = try values.decodeIfPresent(Int.self, forKey: .booking)
                date = try values.decodeIfPresent(String.self, forKey: .date)
                endLocation = try values.decodeIfPresent(String.self, forKey: .endLocation)
                paymentMethod = try values.decodeIfPresent(String.self, forKey: .paymentMethod)
                paymentStatus = try values.decodeIfPresent(String.self, forKey: .paymentStatus)
                startLocation = try values.decodeIfPresent(String.self, forKey: .startLocation)
                totalFare = try values.decodeIfPresent(String.self, forKey: .totalFare)
                transactionId = try values.decodeIfPresent(String.self, forKey: .transactionId)
            package_name = try  values.decodeIfPresent(String.self, forKey: .package_name)
            currency = try values.decodeIfPresent(String.self, forKey: .currency)
            if let t =   try values.decodeIfPresent(Model_Traveller.self, forKey: .traveller){
                traveller = t
            }else{
                traveller = nil
            }
            
            
            if let r =   try values.decodeIfPresent(Model_Rider.self, forKey: .rider){
                rider = r
            }else{
                rider = nil
            }
            
          
        }

}
struct Model_Tourist_Package_Request_CompletedTransaction : Codable {

        let amountPaid : String?
        let booking : Int?
        let date : String?
        let endLocation : String?
        let paymentMethod : String?
        let paymentStatus : String?
        let rider : Model_Rider?
        let startLocation : String?
        let totalFare : String?
        let transactionId : String?
        let traveller : Model_Traveller?
        let currency : String?
    let package_name : String?

    let tourist_package_request:Model_TouristRequestList?
    
        enum CodingKeys: String, CodingKey {
            case tourist_package_request = "tourist_package_request"
            case package_name = "package_name"
                case amountPaid = "amount_paid"
                case booking = "booking"
                case date = "date"
                case endLocation = "end_location"
                case paymentMethod = "payment_method"
                case paymentStatus = "payment_status"
                case rider = "rider"
                case startLocation = "start_location"
                case totalFare = "total_fare"
                case transactionId = "transaction_id"
                case traveller = "traveller"
                case currency = "currency"

        }
    
        init(from decoder: Decoder) throws {
                let values = try decoder.container(keyedBy: CodingKeys.self)
                amountPaid = try values.decodeIfPresent(String.self, forKey: .amountPaid)
                booking = try values.decodeIfPresent(Int.self, forKey: .booking)
                date = try values.decodeIfPresent(String.self, forKey: .date)
                endLocation = try values.decodeIfPresent(String.self, forKey: .endLocation)
                paymentMethod = try values.decodeIfPresent(String.self, forKey: .paymentMethod)
                paymentStatus = try values.decodeIfPresent(String.self, forKey: .paymentStatus)
                startLocation = try values.decodeIfPresent(String.self, forKey: .startLocation)
                totalFare = try values.decodeIfPresent(String.self, forKey: .totalFare)
                transactionId = try values.decodeIfPresent(String.self, forKey: .transactionId)
        
            currency = try values.decodeIfPresent(String.self, forKey: .currency)
            package_name = try values.decodeIfPresent(String.self, forKey: .package_name)
            if let t =   try values.decodeIfPresent(Model_Traveller.self, forKey: .traveller){
                traveller = t
            }else{
                traveller = nil
            }
            
            
            if let r =   try values.decodeIfPresent(Model_Rider.self, forKey: .rider){
                rider = r
            }else{
                rider = nil
            }
            
            tourist_package_request = try values.decodeIfPresent(Model_TouristRequestList.self, forKey: .tourist_package_request)
        }

}

struct Model_Ride_Request : Codable {

        let firstName : String?
        let id : Int?
        let lastName : String?
        let profilePic : String?

        enum CodingKeys: String, CodingKey {
                case firstName = "first_name"
                case id = "id"
                case lastName = "last_name"
                case profilePic = "profile_pic"
        }
    
        init(from decoder: Decoder) throws {
                let values = try decoder.container(keyedBy: CodingKeys.self)
                firstName = try values.decodeIfPresent(String.self, forKey: .firstName)
                id = try values.decodeIfPresent(Int.self, forKey: .id)
                lastName = try values.decodeIfPresent(String.self, forKey: .lastName)
                profilePic = try values.decodeIfPresent(String.self, forKey: .profilePic)
        }

}

//struct Model_BasicRide_CompletedTransaction : Codable {
//
//        let amountPaid : String?
//        let booking : Int?
//        let date : String?
//        let endLocation : String?
//        let paymentMethod : String?
//        let paymentStatus : String?
//        let rider : Model_Rider?
//        let startLocation : String?
//        let totalFare : String?
//        let transactionId : String?
//        let traveller : Model_Traveller?
//
//        enum CodingKeys: String, CodingKey {
//                case amountPaid = "amount_paid"
//                case booking = "booking"
//                case date = "date"
//                case endLocation = "end_location"
//                case paymentMethod = "payment_method"
//                case paymentStatus = "payment_status"
//                case rider = "rider"
//                case startLocation = "start_location"
//                case totalFare = "total_fare"
//                case transactionId = "transaction_id"
//                case traveller = "traveller"
//        }
//    
//        init(from decoder: Decoder) throws {
//                let values = try decoder.container(keyedBy: CodingKeys.self)
//                amountPaid = try values.decodeIfPresent(String.self, forKey: .amountPaid)
//                booking = try values.decodeIfPresent(Int.self, forKey: .booking)
//                date = try values.decodeIfPresent(String.self, forKey: .date)
//                endLocation = try values.decodeIfPresent(String.self, forKey: .endLocation)
//                paymentMethod = try values.decodeIfPresent(String.self, forKey: .paymentMethod)
//                paymentStatus = try values.decodeIfPresent(String.self, forKey: .paymentStatus)
//                startLocation = try values.decodeIfPresent(String.self, forKey: .startLocation)
//                totalFare = try values.decodeIfPresent(String.self, forKey: .totalFare)
//                transactionId = try values.decodeIfPresent(String.self, forKey: .transactionId)
//        
//            if let t =   try values.decodeIfPresent(Model_Traveller.self, forKey: .traveller){
//                traveller = t
//            }else{
//                traveller = nil
//            }
//            
//            
//            if let r =   try values.decodeIfPresent(Model_Rider.self, forKey: .rider){
//                rider = r
//            }else{
//                rider = nil
//            }
//            
//          
//        }
//
//}
