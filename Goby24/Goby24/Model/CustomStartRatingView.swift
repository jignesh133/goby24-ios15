//
//  CustomStartRatingView.swift
//  Goby24
//
//  Created by Jignesh Bhensadadiya on 8/15/21.
//

import SwiftUI
import StarRating

struct CustomStartRatingView: View {
    let onRatingChanged: (Float) -> Void

    @State var customConfig = StarRatingConfiguration(spacing: 8,numberOfStars: 5,stepType: .half,minRating: 0,borderWidth: 1,borderColor: Color.black,emptyColor: Color.white,shadowRadius: 0,fillColors: [Color.black],starVertices: 5,starWeight: 0)

    var body: some View {

        StarRating(initialRating: 0,configuration: $customConfig, onRatingChanged: {
                    print($0)
            
        })

    }
}

//struct CustomStartRatingView_Previews: PreviewProvider {
//    static var previews: some View {
//        //CustomStartRatingView()
//    }
//}
