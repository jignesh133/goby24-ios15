//
//  Model_SepratedRoutes.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on July 30, 2021

import Foundation


class Model_SepratedRoutes : NSObject, NSCoding{

    var bookInstantly : String!
    var currency : String!
    var distance : String!
    var distanceValue : String!

    var dropDate : String!
    var dropTime : String!
    var duration : String!
    var durationValue : String!
    var endDetail : Model_EndDetail!
    var endLabel : String!
    var fare : Double!
    var fromPlaceId : String!
    var paymentMethod : String!
    var pickupDate : String!
    var pickupTime : String!
    var riderMsg : String!
    var seats : Int!
    var startDetail : Model_StartDetail!
    var startLabel : String!
    var timezone : String!
    var toPlaceId : String!


    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        bookInstantly = dictionary["book_instantly"] as? String
        currency = dictionary["currency"] as? String
        distance = dictionary["distance"] as? String
        distanceValue = dictionary["distanceValue"] as? String

        dropDate = dictionary["drop_date"] as? String
        dropTime = dictionary["drop_time"] as? String
        duration = dictionary["duration"] as? String
        durationValue = dictionary["durationValue"] as? String

        endLabel = dictionary["end_label"] as? String
        fare = dictionary["fare"] as? Double
        fromPlaceId = dictionary["from_place_id"] as? String
        paymentMethod = dictionary["payment_method"] as? String
        pickupDate = dictionary["pickup_date"] as? String
        pickupTime = dictionary["pickup_time"] as? String
        riderMsg = dictionary["rider_msg"] as? String
        seats = dictionary["seats"] as? Int
        startLabel = dictionary["start_label"] as? String
        timezone = dictionary["timezone"] as? String
        toPlaceId = dictionary["to_place_id"] as? String
        if let endDetailData = dictionary["end_detail"] as? [String:Any]{
            endDetail = Model_EndDetail(fromDictionary: endDetailData)
        }
        if let startDetailData = dictionary["start_detail"] as? [String:Any]{
            startDetail = Model_StartDetail(fromDictionary: startDetailData)
        }
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if bookInstantly != nil{
            dictionary["book_instantly"] = bookInstantly
        }
        if currency != nil{
            dictionary["currency"] = currency
        }
        if distance != nil{
            dictionary["distance"] = distance
        }
        if dropDate != nil{
            dictionary["drop_date"] = dropDate
        }
        if dropTime != nil{
            dictionary["drop_time"] = dropTime
        }
        if duration != nil{
            dictionary["duration"] = duration
        }
        if endLabel != nil{
            dictionary["end_label"] = endLabel
        }
        if fare != nil{
            dictionary["fare"] = fare
        }
        if fromPlaceId != nil{
            dictionary["from_place_id"] = fromPlaceId
        }
        if paymentMethod != nil{
            dictionary["payment_method"] = paymentMethod
        }
        if pickupDate != nil{
            dictionary["pickup_date"] = pickupDate
        }
        if pickupTime != nil{
            dictionary["pickup_time"] = pickupTime
        }
        if riderMsg != nil{
            dictionary["rider_msg"] = riderMsg
        }
        if seats != nil{
            dictionary["seats"] = seats
        }
        if startLabel != nil{
            dictionary["start_label"] = startLabel
        }
        if timezone != nil{
            dictionary["timezone"] = timezone
        }
        if toPlaceId != nil{
            dictionary["to_place_id"] = toPlaceId
        }
        if endDetail != nil{
            dictionary["end_detail"] = endDetail.toDictionary()
        }
        if startDetail != nil{
            dictionary["start_detail"] = startDetail.toDictionary()
        }
        return dictionary
    }

    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        bookInstantly = aDecoder.decodeObject(forKey: "book_instantly") as? String
        currency = aDecoder.decodeObject(forKey: "currency") as? String
        distance = aDecoder.decodeObject(forKey: "distance") as? String
        dropDate = aDecoder.decodeObject(forKey: "drop_date") as? String
        dropTime = aDecoder.decodeObject(forKey: "drop_time") as? String
        duration = aDecoder.decodeObject(forKey: "duration") as? String
        endDetail = aDecoder.decodeObject(forKey: "end_detail") as? Model_EndDetail
        endLabel = aDecoder.decodeObject(forKey: "end_label") as? String
        fare = aDecoder.decodeObject(forKey: "fare") as? Double
        fromPlaceId = aDecoder.decodeObject(forKey: "from_place_id") as? String
        paymentMethod = aDecoder.decodeObject(forKey: "payment_method") as? String
        pickupDate = aDecoder.decodeObject(forKey: "pickup_date") as? String
        pickupTime = aDecoder.decodeObject(forKey: "pickup_time") as? String
        riderMsg = aDecoder.decodeObject(forKey: "rider_msg") as? String
        seats = aDecoder.decodeObject(forKey: "seats") as? Int
        startDetail = aDecoder.decodeObject(forKey: "start_detail") as? Model_StartDetail
        startLabel = aDecoder.decodeObject(forKey: "start_label") as? String
        timezone = aDecoder.decodeObject(forKey: "timezone") as? String
        toPlaceId = aDecoder.decodeObject(forKey: "to_place_id") as? String
    }

    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if bookInstantly != nil{
            aCoder.encode(bookInstantly, forKey: "book_instantly")
        }
        if currency != nil{
            aCoder.encode(currency, forKey: "currency")
        }
        if distance != nil{
            aCoder.encode(distance, forKey: "distance")
        }
        if dropDate != nil{
            aCoder.encode(dropDate, forKey: "drop_date")
        }
        if dropTime != nil{
            aCoder.encode(dropTime, forKey: "drop_time")
        }
        if duration != nil{
            aCoder.encode(duration, forKey: "duration")
        }
        if endDetail != nil{
            aCoder.encode(endDetail, forKey: "end_detail")
        }
        if endLabel != nil{
            aCoder.encode(endLabel, forKey: "end_label")
        }
        if fare != nil{
            aCoder.encode(fare, forKey: "fare")
        }
        if fromPlaceId != nil{
            aCoder.encode(fromPlaceId, forKey: "from_place_id")
        }
        if paymentMethod != nil{
            aCoder.encode(paymentMethod, forKey: "payment_method")
        }
        if pickupDate != nil{
            aCoder.encode(pickupDate, forKey: "pickup_date")
        }
        if pickupTime != nil{
            aCoder.encode(pickupTime, forKey: "pickup_time")
        }
        if riderMsg != nil{
            aCoder.encode(riderMsg, forKey: "rider_msg")
        }
        if seats != nil{
            aCoder.encode(seats, forKey: "seats")
        }
        if startDetail != nil{
            aCoder.encode(startDetail, forKey: "start_detail")
        }
        if startLabel != nil{
            aCoder.encode(startLabel, forKey: "start_label")
        }
        if timezone != nil{
            aCoder.encode(timezone, forKey: "timezone")
        }
        if toPlaceId != nil{
            aCoder.encode(toPlaceId, forKey: "to_place_id")
        }
    }
}
class Model_StartDetail : NSObject, NSCoding{

    var mainText : String!
    var secondaryText : String!


    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        mainText = dictionary["main_text"] as? String
        secondaryText = dictionary["secondary_text"] as? String
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if mainText != nil{
            dictionary["main_text"] = mainText
        }
        if secondaryText != nil{
            dictionary["secondary_text"] = secondaryText
        }
        return dictionary
    }

    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        mainText = aDecoder.decodeObject(forKey: "main_text") as? String
        secondaryText = aDecoder.decodeObject(forKey: "secondary_text") as? String
    }

    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder){
        if mainText != nil{
            aCoder.encode(mainText, forKey: "main_text")
        }
        if secondaryText != nil{
            aCoder.encode(secondaryText, forKey: "secondary_text")
        }
    }
}
class Model_EndDetail : NSObject, NSCoding{

    var mainText : String!
    var secondaryText : String!


    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        mainText = dictionary["main_text"] as? String
        secondaryText = dictionary["secondary_text"] as? String
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if mainText != nil{
            dictionary["main_text"] = mainText
        }
        if secondaryText != nil{
            dictionary["secondary_text"] = secondaryText
        }
        return dictionary
    }

    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        mainText = aDecoder.decodeObject(forKey: "main_text") as? String
        secondaryText = aDecoder.decodeObject(forKey: "secondary_text") as? String
    }

    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if mainText != nil{
            aCoder.encode(mainText, forKey: "main_text")
        }
        if secondaryText != nil{
            aCoder.encode(secondaryText, forKey: "secondary_text")
        }
    }
}
