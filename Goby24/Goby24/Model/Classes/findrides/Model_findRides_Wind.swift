//
//  Model_findRides_Wind.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on August 5, 2021

import Foundation

struct Model_findRides_Wind : Codable {

    var deg : Int?
        let gust : Float?
        let speed : Int?

        enum CodingKeys: String, CodingKey {
                case deg = "deg"
                case gust = "gust"
                case speed = "speed"
        }
    
        init(from decoder: Decoder) throws {
                let values = try decoder.container(keyedBy: CodingKeys.self)
            if let degUbt = try values.decodeIfPresent(Int.self, forKey: .deg){
                deg = degUbt
            }
            if let degUbt = try values.decodeIfPresent(Float.self, forKey: .deg){
                deg = getIntegerFromAny(degUbt)
            }
            

            
                gust = try values.decodeIfPresent(Float.self, forKey: .gust)
                speed = try values.decodeIfPresent(Int.self, forKey: .speed)
        }

}
