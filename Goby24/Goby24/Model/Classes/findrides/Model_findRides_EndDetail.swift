//
//  Model_findRides_EndDetail.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on August 5, 2021

import Foundation

struct Model_findRides_EndDetail : Codable {

        let clouds : Int?
        let detailedStatus : String?
        let heatIndex : String?
        let humidity : Int?
        let mainText : String?
       // let rain : Model_findRides_Rain?
        let secondaryText : String?
      //  var temperature : Float?
     //   let wind : Model_findRides_Wind?

        enum CodingKeys: String, CodingKey {
                case clouds = "clouds"
                case detailedStatus = "detailed_status"
                case heatIndex = "heat_index"
                case humidity = "humidity"
                case mainText = "main_text"
              //  case rain = "rain"
                case secondaryText = "secondary_text"
            //    case temperature = "temperature"
             //   case wind = "wind"
        }
    
        init(from decoder: Decoder) throws {
                let values = try decoder.container(keyedBy: CodingKeys.self)
                clouds = try values.decodeIfPresent(Int.self, forKey: .clouds)
                detailedStatus = try values.decodeIfPresent(String.self, forKey: .detailedStatus)
                heatIndex = try values.decodeIfPresent(String.self, forKey: .heatIndex)
                humidity = try values.decodeIfPresent(Int.self, forKey: .humidity)
                mainText = try values.decodeIfPresent(String.self, forKey: .mainText)
              //  rain = try Model_findRides_Rain(from: decoder)
                secondaryText = try values.decodeIfPresent(String.self, forKey: .secondaryText)
//            if let temp = try values.decodeIfPresent(Float.self, forKey: .temperature){
//                temperature = temp
//            }
//            if let temp = try values.decodeIfPresent(Int.self, forKey: .temperature){
//                temperature = getFloatFromAny(temp)
//            }
//            if let temp = try values.decodeIfPresent(String.self, forKey: .temperature){
//                temperature = getFloatFromAny(temp)
//            }
//               wind = try values.decodeIfPresent(Model_findRides_Wind.self, forKey: .wind)
        
        }

}
