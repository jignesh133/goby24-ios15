//
//  Model_findRides_TravelPreference.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on August 5, 2021

import Foundation

struct Model_findRides_TravelPreference : Codable {

        let chattiness : String?
        let music : String?
        let pets : String?
        let smoking : String?

        enum CodingKeys: String, CodingKey {
                case chattiness = "Chattiness"
                case music = "Music"
                case pets = "Pets"
                case smoking = "Smoking"
        }
    
        init(from decoder: Decoder) throws {
                let values = try decoder.container(keyedBy: CodingKeys.self)
                chattiness = try values.decodeIfPresent(String.self, forKey: .chattiness)
                music = try values.decodeIfPresent(String.self, forKey: .music)
                pets = try values.decodeIfPresent(String.self, forKey: .pets)
                smoking = try values.decodeIfPresent(String.self, forKey: .smoking)
        }

}
