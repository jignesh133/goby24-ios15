//
//  Model_findRides_StartDetail.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on August 5, 2021

import Foundation

struct Model_findRides_StartDetail : Codable {

        let clouds : Int?
        let detailedStatus : String?
        let heatIndex : String?
        let humidity : Int?
        let mainText : String?
     //   let rain : Model_findRides_Rain?
        let secondaryText : String?
     //   let temperature : Int?
       // let wind : Model_findRides_Wind?

        enum CodingKeys: String, CodingKey {
                case clouds = "clouds"
                case detailedStatus = "detailed_status"
                case heatIndex = "heat_index"
                case humidity = "humidity"
                case mainText = "main_text"
              //  case rain = "rain"
                case secondaryText = "secondary_text"
          //      case temperature = "temperature"
              //  case wind = "wind"
        }
    
        init(from decoder: Decoder) throws {
                let values = try decoder.container(keyedBy: CodingKeys.self)
                clouds = try values.decodeIfPresent(Int.self, forKey: .clouds)
                detailedStatus = try values.decodeIfPresent(String.self, forKey: .detailedStatus)
                heatIndex = try values.decodeIfPresent(String.self, forKey: .heatIndex)
                humidity = try values.decodeIfPresent(Int.self, forKey: .humidity)
                mainText = try values.decodeIfPresent(String.self, forKey: .mainText)
           //     rain = try Model_findRides_Rain(from: decoder)
                secondaryText = try values.decodeIfPresent(String.self, forKey: .secondaryText)
            //    temperature = try values.decodeIfPresent(Int.self, forKey: .temperature)
              //  wind = try values.decodeIfPresent(Model_findRides_Wind.self, forKey: .wind)

        }

}
