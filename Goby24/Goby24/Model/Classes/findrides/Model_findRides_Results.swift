//
//  Model_findRides_Results.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on August 5, 2021

import Foundation

struct Model_findRides_Results : Codable,Identifiable {
    
    let bookInstantly : String?
    let currency : String?
    let distance : String?
    let dropDate : String?
    let dropTime : String?
    let duration : String?
    let endDetail : Model_findRides_EndDetail?
    let endLabel : String?
    let fare : String?
    let fromPlaceId : String?
    let id : Int?
    let paymentMethod : String?
    let pickupDate : String?
    let pickupTime : String?
    let rideStatus : String?
    let riderMsg : String?
    let startDetail : Model_findRides_StartDetail?
    let startLabel : String?
    let toPlaceId : String?
    let totalRides : Int?
    let user : Model_findRides_User?
    let vehicleInfo : Model_findRides_VehicleInfo?
    
    let availableSeats : String?
    
    var totalSeats : String?
    
    enum CodingKeys: String, CodingKey {
        case availableSeats = "available_seats"
        case totalSeats = "total_seats"
        
        case bookInstantly = "book_instantly"
        case currency = "currency"
        case distance = "distance"
        case dropDate = "drop_date"
        case dropTime = "drop_time"
        case duration = "duration"
        case endDetail = "end_detail"
        case endLabel = "end_label"
        case fare = "fare"
        case fromPlaceId = "from_place_id"
        case id = "id"
        case paymentMethod = "payment_method"
        case pickupDate = "pickup_date"
        case pickupTime = "pickup_time"
        case rideStatus = "ride_status"
        case riderMsg = "rider_msg"
        case startDetail = "start_detail"
        case startLabel = "start_label"
        case toPlaceId = "to_place_id"
        case totalRides = "total_rides"
        case user = "user"
        case vehicleInfo = "vehicle_info"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        bookInstantly = try values.decodeIfPresent(String.self, forKey: .bookInstantly)
        currency = try values.decodeIfPresent(String.self, forKey: .currency)
        distance = try values.decodeIfPresent(String.self, forKey: .distance)
        dropDate = try values.decodeIfPresent(String.self, forKey: .dropDate)
        dropTime = try values.decodeIfPresent(String.self, forKey: .dropTime)
        duration = try values.decodeIfPresent(String.self, forKey: .duration)
        endDetail = try values.decodeIfPresent(Model_findRides_EndDetail.self, forKey: .endDetail)
        endLabel = try values.decodeIfPresent(String.self, forKey: .endLabel)
        fare = try values.decodeIfPresent(String.self, forKey: .fare)
        fromPlaceId = try values.decodeIfPresent(String.self, forKey: .fromPlaceId)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        paymentMethod = try values.decodeIfPresent(String.self, forKey: .paymentMethod)
        pickupDate = try values.decodeIfPresent(String.self, forKey: .pickupDate)
        pickupTime = try values.decodeIfPresent(String.self, forKey: .pickupTime)
        rideStatus = try values.decodeIfPresent(String.self, forKey: .rideStatus)
        riderMsg = try values.decodeIfPresent(String.self, forKey: .riderMsg)
        startDetail = try values.decodeIfPresent(Model_findRides_StartDetail.self, forKey: .startDetail)
        
        
        startLabel = try values.decodeIfPresent(String.self, forKey: .startLabel)
        toPlaceId = try values.decodeIfPresent(String.self, forKey: .toPlaceId)
        totalRides = try values.decodeIfPresent(Int.self, forKey: .totalRides)
        
//        totalSeats = try values.decodeIfPresent(String.self, forKey: .totalSeats)
       
        
        if let seats = try? values.decodeIfPresent(String.self, forKey: .totalSeats) {
            totalSeats = seats
        }else  if let seats = try? values.decodeIfPresent(Int.self, forKey: .totalSeats) {
            totalSeats = getStringFromAny(seats)
        }else{
            totalSeats = ""
        }
        
        
        if let seats = try? values.decodeIfPresent(String.self, forKey: .availableSeats) {
            availableSeats = seats
        }else  if let seats = try? values.decodeIfPresent(Int.self, forKey: .availableSeats) {
            availableSeats = getStringFromAny(seats)
        }else{
            availableSeats = ""
        }
        
        if let d = try? values.decodeIfPresent(Model_findRides_User.self, forKey: .user) {
            user = d
        }else{
            user = nil
        }
        if let d = try? values.decodeIfPresent(Model_findRides_VehicleInfo.self, forKey: .vehicleInfo) {
            vehicleInfo = d
        }else{
            vehicleInfo = nil
        }
    }
}
