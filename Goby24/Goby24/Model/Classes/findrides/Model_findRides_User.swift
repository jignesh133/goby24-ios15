//
//  Model_findRides_User.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on August 5, 2021

import Foundation

struct Model_findRides_User : Codable {

        let dob : String?
        let firstName : String?
        let id : Int?
        let isEmailVerified : Bool?
        let isMobileNoVerified : Bool?
        let joiningDate : String?
        let lastName : String?
        let profilePic : String?
        let riderRating : String?
        let travelPreferences : Model_findRides_TravelPreference?

        enum CodingKeys: String, CodingKey {
                case dob = "dob"
                case firstName = "first_name"
                case id = "id"
                case isEmailVerified = "is_email_verified"
                case isMobileNoVerified = "is_mobile_no_verified"
                case joiningDate = "joining_date"
                case lastName = "last_name"
                case profilePic = "profile_pic"
                case riderRating = "rider_rating"
                case travelPreferences = "travel_preferences"
        }
    
        init(from decoder: Decoder) throws {
                let values = try decoder.container(keyedBy: CodingKeys.self)
                dob = try values.decodeIfPresent(String.self, forKey: .dob)
                firstName = try values.decodeIfPresent(String.self, forKey: .firstName)
                id = try values.decodeIfPresent(Int.self, forKey: .id)
                isEmailVerified = try values.decodeIfPresent(Bool.self, forKey: .isEmailVerified)
                isMobileNoVerified = try values.decodeIfPresent(Bool.self, forKey: .isMobileNoVerified)
                joiningDate = try values.decodeIfPresent(String.self, forKey: .joiningDate)
                lastName = try values.decodeIfPresent(String.self, forKey: .lastName)
                profilePic = try values.decodeIfPresent(String.self, forKey: .profilePic)
                riderRating = try values.decodeIfPresent(String.self, forKey: .riderRating)
                travelPreferences = try values.decodeIfPresent(Model_findRides_TravelPreference.self, forKey: .travelPreferences)

            
        }

}
