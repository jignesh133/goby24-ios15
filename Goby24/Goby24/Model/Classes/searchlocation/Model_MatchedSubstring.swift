//
//  Model_MatchedSubstring.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on July 27, 2021

import Foundation

struct Model_MatchedSubstring : Codable {

        let length : Int?
        let offset : Int?

        enum CodingKeys: String, CodingKey {
                case length = "length"
                case offset = "offset"
        }
    
        init(from decoder: Decoder) throws {
                let values = try decoder.container(keyedBy: CodingKeys.self)
                length = try values.decodeIfPresent(Int.self, forKey: .length)
                offset = try values.decodeIfPresent(Int.self, forKey: .offset)
        }

}
