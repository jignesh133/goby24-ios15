//
//  Model_StructuredFormatting.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on July 27, 2021

import Foundation

struct Model_StructuredFormatting : Codable {

        let mainText : String?
        let mainTextMatchedSubstrings : [Model_MainTextMatchedSubstring]?
        let secondaryText : String?

        enum CodingKeys: String, CodingKey {
                case mainText = "main_text"
                case mainTextMatchedSubstrings = "main_text_matched_substrings"
                case secondaryText = "secondary_text"
        }
    
        init(from decoder: Decoder) throws {
                let values = try decoder.container(keyedBy: CodingKeys.self)
                mainText = try values.decodeIfPresent(String.self, forKey: .mainText)
                mainTextMatchedSubstrings = try values.decodeIfPresent([Model_MainTextMatchedSubstring].self, forKey: .mainTextMatchedSubstrings)
                secondaryText = try values.decodeIfPresent(String.self, forKey: .secondaryText)
        }

}
