//
//  Model_Term.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on July 27, 2021

import Foundation

struct Model_Term : Codable {

        let offset : Int?
        let value : String?

        enum CodingKeys: String, CodingKey {
                case offset = "offset"
                case value = "value"
        }
    
        init(from decoder: Decoder) throws {
                let values = try decoder.container(keyedBy: CodingKeys.self)
                offset = try values.decodeIfPresent(Int.self, forKey: .offset)
                value = try values.decodeIfPresent(String.self, forKey: .value)
        }

}
