//
//  Model_searchLocation.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on July 27, 2021

import Foundation

struct Model_searchLocation : Codable,Identifiable {
    let id = UUID()
    let descriptionField : String?
    let matchedSubstrings : [Model_MatchedSubstring]?
    let placeId : String?
    let reference : String?
    let structuredFormatting : Model_StructuredFormatting?
    let terms : [Model_Term]?
    let types : [String]?
    var lat:String?
    var lng:String?
    var isSelected:Bool?
    
    enum CodingKeys: String, CodingKey {
        case descriptionField = "description"
        case matchedSubstrings = "matched_substrings"
        case placeId = "place_id"
        case reference = "reference"
        case structuredFormatting = "structured_formatting"
        case terms = "terms"
        case types = "types"
        case lat = "lat"
        case lng = "lng"
        case isSelected = "isSelected"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        descriptionField = try values.decodeIfPresent(String.self, forKey: .descriptionField)
        matchedSubstrings = try values.decodeIfPresent([Model_MatchedSubstring].self, forKey: .matchedSubstrings)
        placeId = try values.decodeIfPresent(String.self, forKey: .placeId)
        reference = try values.decodeIfPresent(String.self, forKey: .reference)
        terms = try values.decodeIfPresent([Model_Term].self, forKey: .terms)
        types = try values.decodeIfPresent([String].self, forKey: .types)
        structuredFormatting = try values.decodeIfPresent(Model_StructuredFormatting.self, forKey: .structuredFormatting)
        lat = try values.decodeIfPresent(String.self, forKey: .lat)
        lng = try values.decodeIfPresent(String.self, forKey: .lng)
        isSelected = try values.decodeIfPresent(Bool.self,forKey: .isSelected)
    }
    
}
