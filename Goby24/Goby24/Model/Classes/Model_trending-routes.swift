//
//  Model_trending-routes.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on September 2, 2021

import Foundation

struct Model_trending_routes : Codable {

        let id : Int?
        let journeyFrom : String?
        let journeyTo : String?
        let price : String?

        enum CodingKeys: String, CodingKey {
                case id = "id"
                case journeyFrom = "journey_from"
                case journeyTo = "journey_to"
                case price = "price"
        }
    
        init(from decoder: Decoder) throws {
                let values = try decoder.container(keyedBy: CodingKeys.self)
                id = try values.decodeIfPresent(Int.self, forKey: .id)
                journeyFrom = try values.decodeIfPresent(String.self, forKey: .journeyFrom)
                journeyTo = try values.decodeIfPresent(String.self, forKey: .journeyTo)
                price = try values.decodeIfPresent(String.self, forKey: .price)
        }

}


struct Model_SliderImages : Codable {

        let id : Int?
        let image : String?
        let isActive : Bool?

        enum CodingKeys: String, CodingKey {
                case id = "id"
                case image = "image"
                case isActive = "is_active"
        }
    
        init(from decoder: Decoder) throws {
                let values = try decoder.container(keyedBy: CodingKeys.self)
                id = try values.decodeIfPresent(Int.self, forKey: .id)
                image = try values.decodeIfPresent(String.self, forKey: .image)
                isActive = try values.decodeIfPresent(Bool.self, forKey: .isActive)
        }

}
