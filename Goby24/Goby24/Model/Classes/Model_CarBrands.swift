//
//  Model_CarBrands.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on July 27, 2021

import Foundation

struct Model_CarBrands : Codable {

        let id : Int?
        let logo : String?
    var name : String?

        enum CodingKeys: String, CodingKey {
                case id = "id"
                case logo = "logo"
                case name = "name"
        }
    
        init(from decoder: Decoder) throws {
                let values = try decoder.container(keyedBy: CodingKeys.self)
                id = try values.decodeIfPresent(Int.self, forKey: .id)
                logo = try values.decodeIfPresent(String.self, forKey: .logo)
                name = try values.decodeIfPresent(String.self, forKey: .name)
        }

}
struct Model_BrandModel : Codable {

        let brand : Model_CarBrands?
        let id : Int?
        var name : String?

        enum CodingKeys: String, CodingKey {
                case brand = "brand"
                case id = "id"
                case name = "name"
        }
    
        init(from decoder: Decoder) throws {
                let values = try decoder.container(keyedBy: CodingKeys.self)
            brand = try Model_CarBrands(from: decoder)
                id = try values.decodeIfPresent(Int.self, forKey: .id)
                name = try values.decodeIfPresent(String.self, forKey: .name)
        }

}
