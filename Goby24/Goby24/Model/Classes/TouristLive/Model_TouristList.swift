//
//  Model_TouristList.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on August 9, 2021

import Foundation
enum Rider_WITH_NULL: Codable {
    case stringValue(String)
    case objectValue(Model_Rider)

    init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        if let x = try? container.decode(String.self) {
            self = .stringValue(x)
            return
        }
        if let x = try? container.decode(Model_Rider.self) {
            self = .objectValue(x)
            return
        }
        throw DecodingError.typeMismatch(Rider_WITH_NULL.self, DecodingError.Context(codingPath: decoder.codingPath, debugDescription: "Wrong type for MyValue"))
    }

    func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        switch self {
        case .stringValue(let x):
            try container.encode(x)
        case .objectValue(let x):
            try container.encode(x)
        }
    }
}
enum Model_Touristspot_NULL: Codable {
    case stringValue(String)
    case objectValue(Model_Touristspot)
    
    
    init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        if let x = try? container.decode(String.self) {
            self = .stringValue(x)
            return
        }
        if let x = try? container.decode(Model_Touristspot.self) {
            self = .objectValue(x)
            return
        }
        throw DecodingError.typeMismatch(Model_Touristspot_NULL.self, DecodingError.Context(codingPath: decoder.codingPath, debugDescription: "Wrong type for MyValue"))
    }

    func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        switch self {
        case .stringValue(let x):
            try container.encode(x)
        case .objectValue(let x):
            try container.encode(x)
        }
    }
}
enum Model_User_NULL: Codable {
    case stringValue(String)
    case objectValue(Model_User)

    init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        if let x = try? container.decode(String.self) {
            self = .stringValue(x)
            return
        }
        if let x = try? container.decode(Model_User.self) {
            self = .objectValue(x)
            return
        }
        throw DecodingError.typeMismatch(Model_User_NULL.self, DecodingError.Context(codingPath: decoder.codingPath, debugDescription: "Wrong type for MyValue"))
    }

    func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        switch self {
        case .stringValue(let x):
            try container.encode(x)
        case .objectValue(let x):
            try container.encode(x)
        }
    }
}


struct Model_TouristList : Codable {
    
    let currency : String?
    let descriptionField : String?
    let id : Int?
    let journeyEndDate : String?
    let journeyFrom : String?
    let journeyStartDate : String?
    let journeyTo : String?
    let newTouristSpot : String?
    let noOfPassengers : String?
    let packageBudget : String?
    let requestStatus : String?
    var rider : Rider_WITH_NULL//Model_Rider?
    let touristspot : Model_Touristspot_NULL
    let user : Model_User_NULL
    
    enum CodingKeys: String, CodingKey {
        case currency = "currency"
        case descriptionField = "description"
        case id = "id"
        case journeyEndDate = "journey_end_date"
        case journeyFrom = "journey_from"
        case journeyStartDate = "journey_start_date"
        case journeyTo = "journey_to"
        case newTouristSpot = "new_tourist_spot"
        case noOfPassengers = "no_of_passengers"
        case packageBudget = "package_budget"
        case requestStatus = "request_status"
        case rider = "rider"
        case touristspot = "touristspot"
        case user = "user"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        currency = try values.decodeIfPresent(String.self, forKey: .currency)
        descriptionField = try values.decodeIfPresent(String.self, forKey: .descriptionField)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        journeyEndDate = try values.decodeIfPresent(String.self, forKey: .journeyEndDate)
        journeyFrom = try values.decodeIfPresent(String.self, forKey: .journeyFrom)
        journeyStartDate = try values.decodeIfPresent(String.self, forKey: .journeyStartDate)
        journeyTo = try values.decodeIfPresent(String.self, forKey: .journeyTo)
        newTouristSpot = try values.decodeIfPresent(String.self, forKey: .newTouristSpot)
        noOfPassengers = try values.decodeIfPresent(String.self, forKey: .noOfPassengers)
        packageBudget = try values.decodeIfPresent(String.self, forKey: .packageBudget)
        requestStatus = try values.decodeIfPresent(String.self, forKey: .requestStatus)
        rider = try values.decodeIfPresent(Rider_WITH_NULL.self, forKey: .rider)!

        
//        if let riderCheck = try values.decodeIfPresent(Model_Rider.self, forKey: .rider){
//            rider.objectValue = riderCheck
//        }else {
//            rider.stringValue  = try values.decodeIfPresent(String.self, forKey: .rider)
//        }
        
        //        if let touristspotCheck = try values.decodeIfPresent(Model_Touristspot.self, forKey: .touristspot){
        //            touristspot = touristspotCheck
        //        }else {
        //            touristspot  = nil
        //        }
        //        if let usercheck = try values.decodeIfPresent(Model_User.self, forKey: .user){
        //            user = usercheck
        //        }else {
        //            user  = nil
        //        }
        
        // rider = try values.decodeIfPresent(Model_Rider.self, forKey: .rider)
        touristspot = try values.decodeIfPresent(Model_Touristspot_NULL.self, forKey: .touristspot)!
        user = try values.decodeIfPresent(Model_User_NULL.self, forKey: .user)!
        
    }
    
}
