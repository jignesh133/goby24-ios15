//
//  Model_Touristspot.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on August 9, 2021

import Foundation

struct Model_Touristspot : Codable {

        let city : String?
        let country : String?
        let id : Int?
        let spotBanner : String?
        let spotBrief : String?
        let spotName : String?
        let status : String?

        enum CodingKeys: String, CodingKey {
                case city = "city"
                case country = "country"
                case id = "id"
                case spotBanner = "spot_banner"
                case spotBrief = "spot_brief"
                case spotName = "spot_name"
                case status = "status"
        }
    
        init(from decoder: Decoder) throws {
                let values = try decoder.container(keyedBy: CodingKeys.self)
                city = try values.decodeIfPresent(String.self, forKey: .city)
                country = try values.decodeIfPresent(String.self, forKey: .country)
                id = try values.decodeIfPresent(Int.self, forKey: .id)
                spotBanner = try values.decodeIfPresent(String.self, forKey: .spotBanner)
                spotBrief = try values.decodeIfPresent(String.self, forKey: .spotBrief)
                spotName = try values.decodeIfPresent(String.self, forKey: .spotName)
                status = try values.decodeIfPresent(String.self, forKey: .status)
        }

}
