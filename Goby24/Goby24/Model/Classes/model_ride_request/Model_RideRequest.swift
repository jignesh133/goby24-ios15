//
//  Model_Result.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on September 17, 2021

import Foundation

struct Model_RideRequest : Codable {

        let budget : String?
        let country : String?
        let currency : String?
        let id : Int?
        let journeyDate : String?
        let journeyFrom : String?
        let journeyTo : String?
        let paymentMethod : String?
        let paymentStatus : Bool?
        let pickupTime : String?
        let refundStatus : String?
        let remark : String?
        let requestStatus : String?
        let rider : Model_RequestRideRider?
        let seats : String?
        let timestamp : String?
        let user : Model_User?

        enum CodingKeys: String, CodingKey {
                case budget = "budget"
                case country = "country"
                case currency = "currency"
                case id = "id"
                case journeyDate = "journey_date"
                case journeyFrom = "journey_from"
                case journeyTo = "journey_to"
                case paymentMethod = "payment_method"
                case paymentStatus = "payment_status"
                case pickupTime = "pickup_time"
                case refundStatus = "refund_status"
                case remark = "remark"
                case requestStatus = "request_status"
                case rider = "rider"
                case seats = "seats"
                case timestamp = "timestamp"
                case user = "user"
        }
    
        init(from decoder: Decoder) throws {
                let values = try decoder.container(keyedBy: CodingKeys.self)
                budget = try values.decodeIfPresent(String.self, forKey: .budget)
                country = try values.decodeIfPresent(String.self, forKey: .country)
                currency = try values.decodeIfPresent(String.self, forKey: .currency)
                id = try values.decodeIfPresent(Int.self, forKey: .id)
                journeyDate = try values.decodeIfPresent(String.self, forKey: .journeyDate)
                journeyFrom = try values.decodeIfPresent(String.self, forKey: .journeyFrom)
                journeyTo = try values.decodeIfPresent(String.self, forKey: .journeyTo)
                paymentMethod = try values.decodeIfPresent(String.self, forKey: .paymentMethod)
                paymentStatus = try values.decodeIfPresent(Bool.self, forKey: .paymentStatus)
                pickupTime = try values.decodeIfPresent(String.self, forKey: .pickupTime)
                refundStatus = try values.decodeIfPresent(String.self, forKey: .refundStatus)
                remark = try values.decodeIfPresent(String.self, forKey: .remark)
                requestStatus = try values.decodeIfPresent(String.self, forKey: .requestStatus)
                seats = try values.decodeIfPresent(String.self, forKey: .seats)
                timestamp = try values.decodeIfPresent(String.self, forKey: .timestamp)
            
//                user = try values.decodeIfPresent(Model_User.self, forKey: .user)
            
            if let d = try? values.decodeIfPresent(Model_User.self, forKey: .user) {
                user = d
            }else{
                user = nil
            }
            if let d = try? values.decodeIfPresent(Model_RequestRideRider.self, forKey: .rider) {
                rider = d
            }else{
                rider = nil
            }
            
//            rider = try values.decodeIfPresent(Rider_WITH_NULL.self, forKey: .rider)
//                    if let riderCheck = try values.decodeIfPresent(Model_Rider.self, forKey: .rider){
//                        rider.objectValue = riderCheck
//                    }else {
//                        rider.stringValue  = try values.decodeIfPresent(String.self, forKey: .rider)
//                    }

        }

}
