//
//  Model_Element.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on July 30, 2021

import Foundation

struct Model_Element : Codable {

        let distance : Model_Distance?
        let duration : Model_Duration?
        let durationInTraffic : Model_DurationInTraffic?
        let status : String?

        enum CodingKeys: String, CodingKey {
                case distance = "distance"
                case duration = "duration"
                case durationInTraffic = "duration_in_traffic"
                case status = "status"
        }
    
        init(from decoder: Decoder) throws {
                let values = try decoder.container(keyedBy: CodingKeys.self)
            
            distance = try values.decodeIfPresent(Model_Distance.self, forKey: .distance)
            duration = try values.decodeIfPresent(Model_Duration.self, forKey: .duration)
            
            
            
//            distance = try Model_Distance(from: decoder)
//            duration = try Model_Duration(from: decoder)
            durationInTraffic = try Model_DurationInTraffic(from: decoder)
                status = try values.decodeIfPresent(String.self, forKey: .status)
        }

}
