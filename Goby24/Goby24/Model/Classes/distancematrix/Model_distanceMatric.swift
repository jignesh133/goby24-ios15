//
//  Model_distanceMatric.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on July 30, 2021

import Foundation

struct Model_distanceMatric : Codable {

        let destinationAddresses : [String]?
    var originAddresses : [String]?
        let rows : [Model_Row]?
        let status : String?

        enum CodingKeys: String, CodingKey {
                case destinationAddresses = "destination_addresses"
                case originAddresses = "origin_addresses"
                case rows = "rows"
                case status = "status"
        }
    
        init(from decoder: Decoder) throws {
                let values = try decoder.container(keyedBy: CodingKeys.self)
                destinationAddresses = try values.decodeIfPresent([String].self, forKey: .destinationAddresses)
                originAddresses = try values.decodeIfPresent([String].self, forKey: .originAddresses)
                rows = try values.decodeIfPresent([Model_Row].self, forKey: .rows)
                status = try values.decodeIfPresent(String.self, forKey: .status)
        }

}
struct Model_subRoutes : Codable {

        let destinationAddresses : [String]?
        let originAddresses : [String]?
        let rows : [Model_Row]?
        let status : String?

        enum CodingKeys: String, CodingKey {
                case destinationAddresses = "destination_addresses"
                case originAddresses = "origin_addresses"
                case rows = "rows"
                case status = "status"
        }
    
        init(from decoder: Decoder) throws {
                let values = try decoder.container(keyedBy: CodingKeys.self)
                destinationAddresses = try values.decodeIfPresent([String].self, forKey: .destinationAddresses)
                originAddresses = try values.decodeIfPresent([String].self, forKey: .originAddresses)
                rows = try values.decodeIfPresent([Model_Row].self, forKey: .rows)
                status = try values.decodeIfPresent(String.self, forKey: .status)
        }

}
