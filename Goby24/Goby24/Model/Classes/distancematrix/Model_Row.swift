//
//  Model_Row.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on July 30, 2021

import Foundation

struct Model_Row : Codable {

        let elements : [Model_Element]?

        enum CodingKeys: String, CodingKey {
                case elements = "elements"
        }
    
        init(from decoder: Decoder) throws {
                let values = try decoder.container(keyedBy: CodingKeys.self)
                elements = try values.decodeIfPresent([Model_Element].self, forKey: .elements)
        }

}
