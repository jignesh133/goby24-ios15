//
//  Model_Distance.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on July 30, 2021

import Foundation

struct Model_Distance : Codable {

        let text : String?
        let value : Int?

        enum CodingKeys: String, CodingKey {
                case text = "text"
                case value = "value"
        }
    
        init(from decoder: Decoder) throws {
                let values = try decoder.container(keyedBy: CodingKeys.self)
                text = try values.decodeIfPresent(String.self, forKey: .text)
                value = try values.decodeIfPresent(Int.self, forKey: .value)
        }

}
