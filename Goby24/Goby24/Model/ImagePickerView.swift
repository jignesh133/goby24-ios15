//
//  ImagePickerView.swift
//  Goby24
//
//  Created by Jignesh Bhensadadiya on 7/24/21.
//

import SwiftUI
struct OpenGallary: UIViewControllerRepresentable {

    let isShown: Binding<Bool>
    let image: Binding<Image?>
    let sourceType: UIImagePickerController.SourceType
    let onImagePicked: (UIImage) -> Void

    class Coordinator: NSObject, UINavigationControllerDelegate, UIImagePickerControllerDelegate {

        let isShown: Binding<Bool>
        let image: Binding<Image?>
        let onImagePicked: (UIImage) -> Void
        private let sourceType: UIImagePickerController.SourceType

        init(isShown: Binding<Bool>, image: Binding<Image?>,sourceType:UIImagePickerController.SourceType,onImagePicked: @escaping (UIImage) -> Void) {
            self.isShown = isShown
            self.image = image
            self.onImagePicked = onImagePicked
            self.sourceType = sourceType

        }

        func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
            if let uiImage = info[UIImagePickerController.InfoKey.editedImage] as? UIImage  {

                self.image.wrappedValue = Image(uiImage: uiImage)
                onImagePicked(uiImage)
                DispatchQueue.main.async {
                    self.isShown.wrappedValue = false
                }
            }else{
                guard let uiImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage else {return}
                self.image.wrappedValue = Image(uiImage: uiImage)
                onImagePicked(uiImage)
                DispatchQueue.main.async {
                    self.isShown.wrappedValue = false
                }

            }

        }

        func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
            isShown.wrappedValue = false
        }

    }

    func makeCoordinator() -> Coordinator {
        return Coordinator(isShown: isShown, image: image, sourceType: sourceType, onImagePicked: onImagePicked)
    }

    func makeUIViewController(context: UIViewControllerRepresentableContext<OpenGallary>) -> UIImagePickerController {
        

            let picker = UIImagePickerController()
            picker.delegate = context.coordinator
            picker.sourceType = sourceType
            return picker
      

    }

    func updateUIViewController(_ uiViewController: UIImagePickerController,
                                context: UIViewControllerRepresentableContext<OpenGallary>) {

    }
}

//struct ImagePickerView: View {
//    var body: some View {
//        Text(/*@START_MENU_TOKEN@*/"Hello, World!"/*@END_MENU_TOKEN@*/)
//    }
//}
//
//struct ImagePickerView_Previews: PreviewProvider {
//    static var previews: some View {
//        ImagePickerView()
//    }
//}
//struct ImagePicker: UIViewControllerRepresentable {
//
//    @Environment(\.presentationMode)
//    private var presentationMode
//
//    let sourceType: UIImagePickerController.SourceType
//    let onImagePicked: (UIImage) -> Void
//
//    final class Coordinator: NSObject,
//    UINavigationControllerDelegate,
//    UIImagePickerControllerDelegate {
//
//        @Binding
//        private var presentationMode: PresentationMode
//        private let sourceType: UIImagePickerController.SourceType
//        private let onImagePicked: (UIImage) -> Void
//
//        init(presentationMode: Binding<PresentationMode>,
//             sourceType: UIImagePickerController.SourceType,
//             onImagePicked: @escaping (UIImage) -> Void) {
//            _presentationMode = presentationMode
//            self.sourceType = sourceType
//            self.onImagePicked = onImagePicked
//        }
//
//        func imagePickerController(_ picker: UIImagePickerController,
//                                   didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
//            let uiImage = info[UIImagePickerController.InfoKey.originalImage] as! UIImage
//            onImagePicked(uiImage)
//            presentationMode.dismiss()
//
//        }
//
//        func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
//            presentationMode.dismiss()
//        }
//
//    }
//
//    func makeCoordinator() -> Coordinator {
//        return Coordinator(presentationMode: presentationMode,
//                           sourceType: sourceType,
//                           onImagePicked: onImagePicked)
//    }
//
//    func makeUIViewController(context: UIViewControllerRepresentableContext<ImagePicker>) -> UIImagePickerController {
//        let picker = UIImagePickerController()
//        picker.sourceType = sourceType
//        picker.delegate = context.coordinator
//        return picker
//    }
//
//    func updateUIViewController(_ uiViewController: UIImagePickerController,
//                                context: UIViewControllerRepresentableContext<ImagePicker>) {
//
//    }
//
//}

