//
//  SignInWithApple.swift
//  Goby24
//
//  Created by Jignesh Bhensadadiya on 8/6/21.
//

import SwiftUI
import AuthenticationServices

// 1
final class SignInWithApple: UIViewRepresentable {
    // 2
    func makeUIView(context: Context) -> ASAuthorizationAppleIDButton {
        return  ASAuthorizationAppleIDButton(authorizationButtonType: .continue,authorizationButtonStyle: .black)
    }
    func updateUIView(_ uiView: ASAuthorizationAppleIDButton, context: Context) {
    }
}
